import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import { FeatureCheckerService } from '@abp/features/feature-checker.service';
import { LocalizationService } from '@abp/localization/localization.service';
import { MessageService } from '@abp/message/message.service';
import { AbpMultiTenancyService } from '@abp/multi-tenancy/abp-multi-tenancy.service';
import { NotifyService } from '@abp/notify/notify.service';
import { SettingService } from '@abp/settings/setting.service';
import { Injector } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppUrlService } from '@shared/common/nav/app-url.service';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { AppUiCustomizationService } from '@shared/common/ui/app-ui-customization.service';
import { PrimengTableHelper } from 'shared/helpers/PrimengTableHelper';
import { UiCustomizationSettingsDto, UserLoginInfoDto } from '@shared/service-proxies/service-proxies';

export abstract class AppComponentBase {

   
    localizationSourceName = AppConsts.localization.defaultLocalizationSourceName;

    localization: LocalizationService;
    permission: PermissionCheckerService;
    feature: FeatureCheckerService;
    notify: NotifyService;
    setting: SettingService;
    message: MessageService;
    multiTenancy: AbpMultiTenancyService;
    appSession: AppSessionService;
    primengTableHelper: PrimengTableHelper;
    ui: AppUiCustomizationService;
    appUrlService: AppUrlService;

    constructor(injector: Injector) {
        this.localization = injector.get(LocalizationService);
        this.permission = injector.get(PermissionCheckerService);
        this.feature = injector.get(FeatureCheckerService);
        this.notify = injector.get(NotifyService);
        this.setting = injector.get(SettingService);
        this.message = injector.get(MessageService);
        this.multiTenancy = injector.get(AbpMultiTenancyService);
        this.appSession = injector.get(AppSessionService);
        this.ui = injector.get(AppUiCustomizationService);
        this.appUrlService = injector.get(AppUrlService);
        this.primengTableHelper = new PrimengTableHelper();
    }

    

    flattenDeep(array) {
        return array.reduce((acc, val) =>
            Array.isArray(val) ?
            acc.concat(this.flattenDeep(val)) :
            acc.concat(val),
            []);
    }

    l(key: string, ...args: any[]): string {
        args.unshift(key);
        args.unshift(this.localizationSourceName);
        return this.ls.apply(this, args);
    }

    ls(sourcename: string, key: string, ...args: any[]): string {
        let localizedText = this.localization.localize(key, sourcename);

        if (!localizedText) {
            localizedText = key;
        }

        if (!args || !args.length) {
            return localizedText;
        }

        args.unshift(localizedText);
        return abp.utils.formatString.apply(this, this.flattenDeep(args));
    }

    isGranted(permissionName: string): boolean {
        return this.permission.isGranted(permissionName);
    }

    isGrantedAny(...permissions: string[]): boolean {
        if (!permissions) {
            return false;
        }

        for (const permission of permissions) {
            if (this.isGranted(permission)) {
                return true;
            }
        }

        return false;
    }

    s(key: string): string {
        return abp.setting.get(key);
    }

    appRootUrl(): string {
        return this.appUrlService.appRootUrl;
    }

    get currentTheme(): UiCustomizationSettingsDto {
        return this.appSession.theme;
    }

    fnddmmyyyy(va) {
        if (va != "" && va != null && va != undefined) {
            var ddmmyyyy = va.substr(6, 2) + "/" + va.substr(4, 2) + "/" + (parseInt(va.substr(0, 4)) + 543 * 1);
            return ddmmyyyy;
        }
    }

    fnddmmyyyygrid(va) {
        if (va.value != "" && va.value != null && va.value != undefined) {
            var ddmmyyyy = va.value.substr(6, 2) + "/" + va.value.substr(4, 2) + "/" + (parseInt(va.value.substr(0, 4)) + 543 * 1);
            return ddmmyyyy;
        }
    }
    fnmmyyyygrid(va) {
        if (va.value != "" && va.value != null && va.value != undefined) {
            var mmyyyy = va.value.substr(4, 2) + "/" + (parseInt(va.value.substr(0, 4)) + 543 * 1);
            return mmyyyy;
        }
    }

    fnyyyymmdd(va) {
        if (va != "" && va != null && va != undefined) {
            var yyyymmdd = (parseInt(va.substr(6, 4)) - 543 * 1) + va.substr(3, 2) + va.substr(0, 2);
            return yyyymmdd;
        }
    }

    fninyyyymmdd(va) {
        if (va != "" && va != null && va != undefined) {
            var yyyymmdd = (parseInt(va.substr(6, 4)) - 543 * 1) +'-'+ va.substr(3, 2) +'-'+ va.substr(0, 2);
            return yyyymmdd;
        }
    }
    fninISOddmmyyyy(va) {
        if (va != "" && va != null && va != undefined) {
            var ddmmyyyy = va.substr(8, 2) + "/" + va.substr(5, 2) + "/" + (parseInt(va.substr(0, 4)) + 543 * 1);
            return ddmmyyyy;
        }
    }

    fnmmyyyy(va) {
        if (va != "" && va != null && va != undefined) {
            var mmyyyy = va.substr(4, 2) + "/" + (parseInt(va.substr(0, 4)) + 543 * 1);
            return mmyyyy;
        }
    }

    fnyyyymm(va) {
        if (va != "" && va != null && va != undefined) {
            var yyyymmdd = (parseInt(va.substr(3, 4)) - 543 * 1) + va.substr(0, 2) ;
            return yyyymmdd;
        }
    }
    fnyyyymm2(va) {
        if (va != "" && va != null && va != undefined) {
            var yyyymmdd = (parseInt(va.substr(3, 4)) * 1) + va.substr(0, 2) ;
            return yyyymmdd;
        }
    }

    fnyyyy(va) {
        if (va != "" && va != null && va != undefined) {
            var yyyy = (parseInt(va )- 543 * 1);
            return yyyy;
        }
    }
    fnyyyy543(va) {
        if (va != "" && va != null && va != undefined) {
            var yyyy = (parseInt(va )+ 543 * 1);
            return yyyy;
        }
    }
    fnyyyy543grid(va) {
        if (va.value != "" && va.value != null && va.value != undefined) {
            var yyyy =  (parseInt(va.value) + 543 * 1).toString();
            return yyyy;
        }
    }
    fnformat(x) {
        var a = parseFloat((x.toString()).replace(/,/g, ""))
            .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (a == 'NaN') {
            a = '0.00';
            // this.datas.A1 = '0';
        }
        return a;
    }

    fnformatInt(x) {
        var a = parseFloat((x.toString()).replace(/,/g, ""))
            
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (a == 'NaN') {
            a = '0';
            // this.datas.A1 = '0';
        }
        return a;
    }

    fnreplace(x) {
        if(typeof x == 'number'){
            var a = parseFloat((x.toString()).replace(/,/g, ""))
            if (a == NaN) {
                a = 0;
            }
            return a;

        }else if (typeof x == 'string'){
            var a = parseFloat(x.replace(/,/g, ""))
            if (a == NaN) {
                a = 0;
            }
            return a;
        }
       
    }

    format_dec() {
        $('input[type="text"].decimal').each(function () {
            var data_ = $(this).val();

            var a = parseFloat((data_.toString()).replace(/,/g, ""))
                .toFixed(2)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            if (a == 'NaN') {
                a = '0.00';
            }
            $(this).val(a);
        });
    }

    sum(items, field) {
        return items.reduce(function (a, b) {
            return a + b[field];
        }, 0);
    }
}
