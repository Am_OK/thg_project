import { AbpHttpInterceptor } from '@abp/abpHttpInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import * as ApiServiceProxies from './service-proxies';
import { CourseAppserviceServiceProxy } from './service-proxies';

@NgModule({
    providers: [
        ApiServiceProxies.AuditLogServiceProxy,
        ApiServiceProxies.CachingServiceProxy,
        ApiServiceProxies.ChatServiceProxy,
        ApiServiceProxies.CommonLookupServiceProxy,
        ApiServiceProxies.EditionServiceProxy,
        ApiServiceProxies.FriendshipServiceProxy,
        ApiServiceProxies.HostSettingsServiceProxy,
        ApiServiceProxies.InstallServiceProxy,
        ApiServiceProxies.LanguageServiceProxy,
        ApiServiceProxies.NotificationServiceProxy,
        ApiServiceProxies.OrganizationUnitServiceProxy,
        ApiServiceProxies.PermissionServiceProxy,
        ApiServiceProxies.ProfileServiceProxy,
        ApiServiceProxies.RoleServiceProxy,
        ApiServiceProxies.SessionServiceProxy,
        ApiServiceProxies.TenantServiceProxy,
        ApiServiceProxies.TenantDashboardServiceProxy,
        ApiServiceProxies.TenantSettingsServiceProxy,
        ApiServiceProxies.TimingServiceProxy,
        ApiServiceProxies.UserServiceProxy,
        ApiServiceProxies.UserLinkServiceProxy,
        ApiServiceProxies.UserLoginServiceProxy,
        ApiServiceProxies.WebLogServiceProxy,
        ApiServiceProxies.AccountServiceProxy,
        ApiServiceProxies.TokenAuthServiceProxy,
        ApiServiceProxies.TenantRegistrationServiceProxy,
        ApiServiceProxies.HostDashboardServiceProxy,
        ApiServiceProxies.PaymentServiceProxy,
        ApiServiceProxies.DemoUiComponentsServiceProxy,
        ApiServiceProxies.InvoiceServiceProxy,
        ApiServiceProxies.SubscriptionServiceProxy,
        ApiServiceProxies.InstallServiceProxy,
        ApiServiceProxies.UiCustomizationSettingsServiceProxy,
        ApiServiceProxies.PayPalPaymentServiceProxy,
        ApiServiceProxies.StripePaymentServiceProxy,
        ApiServiceProxies.DbmtabServiceProxy,
        ApiServiceProxies.MSMPSN00ServiceProxy,
        ApiServiceProxies.TRBUD00AppserviceServiceProxy,
        ApiServiceProxies.TRBUD01AppserviceServiceProxy,
        ApiServiceProxies.CourseAppserviceServiceProxy,
        ApiServiceProxies.CourseDetailAppserviceServiceProxy,
        ApiServiceProxies.AccDashBoardServiceProxy,
        ApiServiceProxies.ExpensesTmpAppserviceServiceProxy,
        ApiServiceProxies.ExpensesAppserviceServiceProxy,
        ApiServiceProxies.KPShareServiceProxy,
        ApiServiceProxies.SelfAddAppserviceServiceProxy,
        ApiServiceProxies.EmployeeCarsServiceProxy,
        ApiServiceProxies.EmployeePhoneNumbersAppserviceServiceProxy,
        ApiServiceProxies.EmployeePicturesServiceProxy,
        ApiServiceProxies.JobCertificatesServiceProxy,
        ApiServiceProxies.MemberOfFamilysServiceProxy,
        ApiServiceProxies.PersonnalInformationsServiceProxy,
        ApiServiceProxies.EmployeeInformationsServiceProxy,
        ApiServiceProxies.MoreIncomesServiceProxy,
        ApiServiceProxies.MoreBankAccountsServiceProxy,
        ApiServiceProxies.INCPOS00ServiceProxy,
        ApiServiceProxies.EmployeeEducationsServiceProxy,
        ApiServiceProxies.WorkingExperiencesServiceProxy,
        ApiServiceProxies.EmployeeCommendationServiceProxy,
        ApiServiceProxies.PunishmentInformationServiceProxy,
        ApiServiceProxies.EmployeeLoanAccountServiceProxy,
        ApiServiceProxies.EmployeeBenefitsServiceProxy,
        ApiServiceProxies.EmployeeUpSaralyServiceProxy,
        ApiServiceProxies.EmployeeAddressServiceProxy,
        ApiServiceProxies.EmployeeGuarantorServiceProxy,
        ApiServiceProxies.EmployeeAssetonHandServiceProxy,
        ApiServiceProxies.EmergencyContactServiceProxy,
        ApiServiceProxies.EmployeePromotionsServiceProxy,
        ApiServiceProxies.BonusInformationServiceProxy,
        ApiServiceProxies.EmployeeMovmentsServiceProxy,
        ApiServiceProxies.EmployeeResignsServiceProxy,
        ApiServiceProxies.ImportantLetterServiceProxy,
        ApiServiceProxies.ManPowerServiceProxy, 
        ApiServiceProxies.RecruitmentServiceProxy, 
        ApiServiceProxies.ManPowerListServiceProxy, 
        ApiServiceProxies.WorkAcrossPositionsServiceProxy,
        ApiServiceProxies.DbmrunServiceProxy,
        ApiServiceProxies.EnglishCertificatesServiceProxy,
        ApiServiceProxies.PositionCertificatesServiceProxy,
        ApiServiceProxies.WorkplanServiceProxy,
        ApiServiceProxies.LeaveInformationServiceProxy,
        
        { provide: HTTP_INTERCEPTORS, useClass: AbpHttpInterceptor, multi: true }
    ]
})
export class ServiceProxyModule { }
