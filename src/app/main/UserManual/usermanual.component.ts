import { Component, Injector, OnInit, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
@Component({
    selector: 'usermanual',
    templateUrl: './usermanual.component.html',
    animations: [appModuleAnimation()]
})
export class UserManualComponent extends AppComponentBase implements OnInit {
    data_show: { "topic": string;  "title": string;"detail": string; "img_url": string;  }[];



    constructor(
        injector: Injector,
        private router: Router
    ) {
        super(injector);
    }

    ngOnInit(): void {

        this.data_show = [{
            "topic": 'คู่มือการใช้งาน Employee Master',
            "title": 'Employee Master',
            "detail": AppConsts.remoteServiceBaseUrl+'/Manual/Employee Master.pdf',
            "img_url": AppConsts.remoteServiceBaseUrl+'/Manual/kwanp.jpg',
        }, {
            "topic": 'คู่มือการใช้งาน Employee Work Plan',
            "title": 'Employee Work Plan',
            "detail": AppConsts.remoteServiceBaseUrl+'/Manual/Workplan SUTH.pdf',
            "img_url": AppConsts.remoteServiceBaseUrl+'/Manual/kwanp.jpg',
        },
        {
            "topic": 'คู่มือการใช้งาน Asset Management',
            "title": 'Asset Management',
            "detail": AppConsts.remoteServiceBaseUrl+'/Manual/Management SUTH.pdf',
            "img_url": AppConsts.remoteServiceBaseUrl+'/Manual/kwanp.jpg',
        },
        {
            "topic": 'คู่มือการใช้งาน Time Attendance Setting',
            "title": 'Time Attendance Setting',
            "detail": AppConsts.remoteServiceBaseUrl+'/Manual/Time Attendance Setting(SUTH).pdf',
            "img_url": AppConsts.remoteServiceBaseUrl+'/Manual/kwanp.jpg',
        },
        // {
        //     "topic": 'คู่มือการใช้งานระบบ MIS การคืนเงินโดยไม่ได้ทำเรื่องจากระบบ',
        //     "title": 'การคืนเงินโดยไม่ได้ทำเรื่องจากระบบ',
        //     "detail": 'http://mis.mcu.ac.th:3000/manual/คู่มือMISการคืนเงินโดยไม่ได้ทำเรื่องจากระ.pdf',
        //     "img_url": AppConsts.remoteServiceBaseUrl + '/HB/kkwanp.jpg',
        // },
      
        ];
    }

    show(): void {
        
    }
    showmn(pathf){
        this.router.navigate([pathf])
    }


}