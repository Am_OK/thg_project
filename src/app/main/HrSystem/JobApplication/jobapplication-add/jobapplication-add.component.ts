import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
    selector: 'jobapplicationadd',
    templateUrl: './jobapplication-add.component.html',
})
export class JobAppaddComponent extends AppComponentBase {
    
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('nameInput') nameInput: ElementRef;
    @ViewChild('dd') dd: ElementRef;

    active: boolean = false;
    saving: boolean = false;

    constructor(
        injector: Injector,
    ) {
        super(injector);
    }
    src;
    show(): void {
        this.active = true;
        this.src =''; 
        this.modal.show();
    }

    onShown(): void {
    }

    save(): void {
        this.saving = true;
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

       
   
}
