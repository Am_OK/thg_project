import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
    templateUrl: './jobapplication.component.html',
    animations: [appModuleAnimation()]
})
export class JobAppComponent extends AppComponentBase implements OnInit {
    
    filter: string = '';
    saving: boolean = false;    
    filterTab : string = 'POSITION';
    shownLoginName: string = "";
    shownLoginDepartment: string = "";
  
    constructor(
        injector: Injector,
    ) {
        super(injector);
    }

    // refreshpage(): void {
    //     window.location.reload();
    // }

    ngOnInit(): void {
    }



    rowClickEvent(e) {
        //debugger;
        console.log(e);
        alert('in rowClickEvent');        
      };

    onContentReady(e) {
        e.component.columnOption("command:edit", {
           visibleIndex: -1,
           width: 70
       });
    }     
}
 