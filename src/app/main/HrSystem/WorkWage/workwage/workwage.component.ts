import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-workwage',
  templateUrl: './workwage.component.html',
  styleUrls: ['./workwage.component.css']
})
export class WorkwageComponent extends AppComponentBase implements OnInit {

  constructor(injector: Injector,) {super(injector); }

  ngOnInit() {
  }

}
