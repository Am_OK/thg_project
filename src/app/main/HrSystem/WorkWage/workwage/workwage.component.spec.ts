import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkwageComponent } from './workwage.component';

describe('WorkwageComponent', () => {
  let component: WorkwageComponent;
  let fixture: ComponentFixture<WorkwageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkwageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkwageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
