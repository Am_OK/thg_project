import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkwageAddComponent } from './workwage-add.component';

describe('WorkwageAddComponent', () => {
  let component: WorkwageAddComponent;
  let fixture: ComponentFixture<WorkwageAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkwageAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkwageAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
