import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'appworkwageadd',
  templateUrl: './workwage-add.component.html',
  styleUrls: ['./workwage-add.component.css']
})
export class WorkwageAddComponent extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  constructor( injector: Injector,) 
  {super(injector); }

  ngOnInit() {
  }
  show():void{
    this.modal.show()
  }
  close(){
    this.modal.hide()
  }
}
