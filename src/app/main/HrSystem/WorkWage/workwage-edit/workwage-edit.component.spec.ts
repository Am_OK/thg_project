import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkwageEditComponent } from './workwage-edit.component';

describe('WorkwageEditComponent', () => {
  let component: WorkwageEditComponent;
  let fixture: ComponentFixture<WorkwageEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkwageEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkwageEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
