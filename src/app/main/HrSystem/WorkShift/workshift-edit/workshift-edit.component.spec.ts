import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshiftEditComponent } from './workshift-edit.component';

describe('WorkshiftEditComponent', () => {
  let component: WorkshiftEditComponent;
  let fixture: ComponentFixture<WorkshiftEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshiftEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshiftEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
