import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-workshift',
  templateUrl: './workshift.component.html',
  styleUrls: ['./workshift.component.css']
})
export class WorkshiftComponent extends AppComponentBase implements OnInit {
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit() {
  }
}
