import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshiftAddComponent } from './workshift-add.component';

describe('WorkshiftAddComponent', () => {
  let component: WorkshiftAddComponent;
  let fixture: ComponentFixture<WorkshiftAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkshiftAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshiftAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
