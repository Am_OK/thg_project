import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-workshift-add',
  templateUrl: './workshift-add.component.html',
  styleUrls: ['./workshift-add.component.css']
})
export class WorkshiftAddComponent extends AppComponentBase implements OnInit {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }
  ngOnInit(){
    
  }
  show():void{
    this.modal.show()
  }
  close(){
    this.modal.hide()
  }
}
