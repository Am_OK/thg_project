import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'vacationadd',
  templateUrl: './vacation-add.component.html',
  styleUrls: ['./vacation-add.component.css']
})
export class VacationAddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
 
 
  constructor(
    injector: Injector,
  

  ) {
    super(injector);
   
  }
  ngOnInit() {
  }
  show(): void {
    this.active = true;
    this.modal.show();
  }
  close(): void {
    this.modal.hide();
    this.active = false;
  }
}
