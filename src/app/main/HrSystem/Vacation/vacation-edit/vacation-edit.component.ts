import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'vacationedit',
  templateUrl: './vacation-edit.component.html',
  styleUrls: ['./vacation-edit.component.css']
})
export class VacationEditComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
 
 
  constructor(
    injector: Injector,
  

  ) {
    super(injector);
   
  }
  ngOnInit() {
  }
  show(): void {
    this.active = true;
    this.modal.show();
  }
  close(): void {
    this.modal.hide();
    this.active = false;
  }
}
