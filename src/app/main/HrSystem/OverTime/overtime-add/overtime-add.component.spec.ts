import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertimeAddComponent } from './overtime-add.component';

describe('OvertimeAddComponent', () => {
  let component: OvertimeAddComponent;
  let fixture: ComponentFixture<OvertimeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OvertimeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertimeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
