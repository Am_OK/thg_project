import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-overtime-add',
  templateUrl: './overtime-add.component.html',
  styleUrls: ['./overtime-add.component.css']
})
export class OvertimeAddComponent  extends AppComponentBase implements OnInit {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }
  ngOnInit(){
    
  }
  show():void{
    this.modal.show()
  }
  close(){
    this.modal.hide()
  }
}
