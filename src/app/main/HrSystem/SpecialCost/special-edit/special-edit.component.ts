import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'specialedit',
  templateUrl: './special-edit.component.html',
  styleUrls: ['./special-edit.component.css']
})
export class SpecialEditComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
 
 
  constructor(
    injector: Injector,

  ) {
    super(injector);
   
  }
  ngOnInit() {
  }
  show(): void {
    this.active = true;
    this.modal.show();
  }
  close(): void {
    this.modal.hide();
    this.active = false;
  }
}
