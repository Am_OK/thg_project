import { Component, OnInit, Injector, Output, EventEmitter, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { WorkplanServiceProxy, AddWorkPlan, DbmtabListDto, UpdateWorkPlan } from '@shared/service-proxies/service-proxies';
import { WorkplanComponent } from '../workplan.component';

@Component({
  selector: 'appworkplanadd2',
  templateUrl: './workplan-add2.component.html',
  styleUrls: ['./workplan-add2.component.css']
})
export class WorkplanAdd2Component extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  createworkplan: AddWorkPlan = new AddWorkPlan()
  updateworkplan: UpdateWorkPlan = new UpdateWorkPlan()
  dbmtabshiftcode: DbmtabListDto[];
  loadingVisible: boolean;
  constructor(
    injector: Injector,
    private WorkplanServiceProxy: WorkplanServiceProxy,
    private WorkplanComponent: WorkplanComponent
  ) {
    super(injector);
  }
  arr2;
  isDouble = 0;
  clickon = 0
  prevCellData;
  employee = []
  employeeOrg = []
  indicatorUrl: string = "https://js.devexpress.com/Demos/WidgetsGallery/JSDemos/images/Loading.gif";
  active: boolean;
  show(): void {
    this.active = true
    setTimeout(() => {
      this.modal.show()
      setTimeout(() => {
        document.getElementById("pbb").style.width = this.progressbarss;
        $("#chkd").attr('disabled', 'disabled').val('')
        $('#CHK2DIS').attr('disabled', 'disabled')
        $('#CHK3DIS').attr('disabled', 'disabled')
        $('#CHK4DIS').attr('disabled', 'disabled')
      }, 100);

    }, 100);
    // $(".form_datetimes").datetimepicker({ format: 'DD/MM/YYYY', locale: 'th'})
    // $(".form_datetimes").datetimepicker({ format: 'DD/MM/YYYY', locale: 'th' ,minDate: this.WorkplanComponent.firstDay ,maxDate: this.WorkplanComponent.lastDay})
    this.loadingVisible = false
    this.progressbarss = "0%"




    this.CHECKDTE = false

    this.tag_userlist = []

    this.OTCHECK = false
    this.OCCHECK = false
    this.ODCHECK = false
    this.OSCHECK = false

    this.OTCHECK2 = false
    this.OCCHECK2 = false
    this.ODCHECK2 = false
    this.OSCHECK2 = false

    this.OTCHECK3 = false
    this.OCCHECK3 = false
    this.ODCHECK3 = false
    this.OSCHECK3 = false

    this.OTCHECK4 = false
    this.OCCHECK4 = false
    this.ODCHECK4 = false
    this.OSCHECK4 = false

    this.CHK = false
    this.CHK2 = false
    this.CHK3 = false
    this.CHK4 = false


    this.employeeOrg = []
    this.employee = this.WorkplanComponent.resourcesData3s
    for (let index = 0; index < this.employee.length; index++) {
      this.employeeOrg.push(this.employee[index])
    }








    // if (e.cellData.startDate == this.prevCellData.startDate && e.cellData.endDate == this.prevCellData.endDate && e.cellData.groups == this.prevCellData.groups) {
    //   this.clickon = 1
    //   this.isDouble = 0;
    //   this.prevCellData = null;
    //   var dt = new Date(e.cellData.endDate);
    //   var dte = dt.toISOString().substr(0, 10);

    //   this.arr2 = (this.WorkplanComponent.arry.filter(res => res.wrkempid == e.cellData.groups.wrkempid && res.dte == dte)[0])

    //   this.createworkplan.wrkdte = dte
    //   this.createworkplan.wrkdep = this.WorkplanComponent.dep
    //   this.createworkplan.wrkempid = e.cellData.groups.wrkempid
    this.dbmtabshiftcode = this.WorkplanComponent.dbmtabshiftcode
    $('.m_select2_1at').select2({
      placeholder: "Please Select.."
    });


    // }
    // else {
    //   this.prevCellData = null;
    //   this.isDouble = 0;
    // }







    //console.log(e)

  }
  close() {

    this.isDouble = 0;
    this.clickon = 0;
    this.prevCellData = null;
    this.modal.hide()
    this.active = false
  }
  tag_userlist = [];
  tag_user(id) {

    var data = this.employee.filter(data => data.id == id);
    var chk = 0;
    data.forEach(element => {
      if (element.id == id) {
        chk = 1;
      }
    });
    if (chk == 1) {
      var gen_tag_userlist = {
        "psnpid": data[0].id,
        "name": data[0].text
      }
      this.tag_userlist.push(gen_tag_userlist);
      setTimeout(() => {
        this.employee.splice(this.employee.findIndex(data => data.id == id), 1)
      }, 25);

    }

    console.log(this.tag_userlist);

  }
  delete_tag_user(ids) {
    this.tag_userlist.splice(this.tag_userlist.findIndex(da => da.psnpid == ids), 1)
    // for (const key in this.tag_userlist) {
    //   if (this.tag_userlist[key].psnpid == id) delete this.tag_userlist[key]
    // }
    var datas = this.employeeOrg.filter(dat => dat.id == ids)
    // console.log(this.employeeOrg)
    // console.log(datas)
    setTimeout(() => {
      this.employee.push(datas[0])
    }, 25);
    // console.log(id);

  }

  // OT = >1
  // ONCALL = @
  //ค่าเวรบ่ายดึก = ! เดิม $
  //ค่าเวร SUP = #

  Chkdte() {
    setTimeout(() => {
      if (this.CHECKDTE == true) {
        $("#chkd").removeAttr('disabled')
      }
      else {
        $("#chkd").attr('disabled', 'disabled')
      }
    }, 25);
  }

  chktime() {
    if (this.CHK == true) {

    }
    if (this.CHK2 == true) {

    }
    if (this.CHK3 == true) {

    }
    if (this.CHK4 == true) {

    }



  }

  chkot(selecttime) {
    var selecttimef = selecttime + '>1'
    return selecttimef
  }

  chkoc(selecttime) {
    var selecttimef = selecttime + '@'
    return selecttimef
  }

  chkod(selecttime) {
    var selecttimef = selecttime + '!'
    return selecttimef
  }

  chkos(selecttime) {
    var selecttimef = selecttime + '#'
    return selecttimef
  }
  chkti1(sltime, sltime2) {
    var shift0 = sltime
    var shift = sltime2
    if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1)
      && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))
      || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2)
        && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))
      || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1)
        && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))) {
      this.chkloop = 1;
      alert("tr2")
    }
  }
  chkti2(sltime, sltime2, sltime3) {
    var shift0 = sltime
    var shift = sltime2
    var shift2 = sltime3
    if ((((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1
    )
      && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2
      ))
      || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2
      )
        && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2
        ))
      || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1
        && (
          this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG1
        )))

    )
      || (((
        this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1)
        && (
          this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG2))
        || ((
          this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2)
          && (
            this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG2))
        || ((
          (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2
            &&
            this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG2)))

      )


      || (((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG1)
        && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))
        || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG2)
          && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))
        || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1)
          && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift2)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))


      )
    ) {
      this.chkloop = 1;
      alert("tr3")
    }
  }
  chkti3(sltime, sltime2) {
    var shift0 = sltime
    var shift = sltime2
    if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1)
      && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))
      || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2)
        && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))
      || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1)
        && (this.dbmtabshiftcode.filter(res => res.tabtB2 == shift0)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))) {
      this.chkloop = 1;

    }
  }
  chkloop = 0
  progressbarss = ""
  checkseltime() {
    setTimeout(() => {
      if (this.CHK == true) {
        $('#CHK2DIS').removeAttr('disabled')
        if (this.CHK2 == true) {
          $('#CHK3DIS').removeAttr('disabled')
          if (this.CHK3 == true) {
            $('#CHK4DIS').removeAttr('disabled')
          }
          else {
            $('#CHK4DIS').attr('disabled', 'disabled')
            this.CHK4 = false
          }
        }
        else {
          $('#CHK3DIS').attr('disabled', 'disabled')
          $('#CHK4DIS').attr('disabled', 'disabled')
          this.CHK3 = false
          this.CHK4 = false
        }

      }
      else {
        $('#CHK2DIS').attr('disabled', 'disabled')
        $('#CHK3DIS').attr('disabled', 'disabled')
        $('#CHK4DIS').attr('disabled', 'disabled')
        this.CHK2 = false
        this.CHK3 = false
        this.CHK4 = false
      }
    }, 25);

  }
  save(empid, sltime, sltime2, sltime3, sltime4, dte1, dte2) {
    this.createworkplan.wrkshfcod = ''

    var chkdt
    if (dte1 != '' && dte1 != null && dte1 != undefined) {
      if (this.CHECKDTE == true) {
        if (dte2 != '' && dte2 != null && dte2 != undefined) {
          chkdt = '1'
        }
        else {
          chkdt = '0'
        }
      }
      else {
        chkdt = '1'
      }
      if (chkdt == '1') {
        if (this.tag_userlist.length > 0) {
          this.loadingVisible = true


          var createworkplanShiftCodeORG = ''
          this.createworkplan.wrkdep = this.WorkplanComponent.dep
          //this.createworkplan.wrkyeamon = this.WorkplanComponent.dtes
          var selecttime = sltime
          var selecttime2 = sltime2
          var selecttime3 = sltime3
          var selecttime4 = sltime4
          this.WorkplanComponent.log_savefail = ''
          if (this.OTCHECK == true) {
            selecttime = this.chkot(selecttime)
          }
          if (this.OTCHECK2 == true) {
            selecttime2 = this.chkot(selecttime2)
          }
          if (this.OTCHECK3 == true) {
            selecttime3 = this.chkot(selecttime3)
          }
          if (this.OTCHECK4 == true) {
            selecttime4 = this.chkot(selecttime4)
          }

          setTimeout(() => {
            if (this.OCCHECK == true) {
              selecttime = this.chkoc(selecttime)
            }
            if (this.OCCHECK2 == true) {
              selecttime2 = this.chkoc(selecttime2)
            }
            if (this.OCCHECK3 == true) {
              selecttime3 = this.chkoc(selecttime3)
            }
            if (this.OCCHECK4 == true) {
              selecttime4 = this.chkoc(selecttime4)
            }
          }, 30);

          setTimeout(() => {
            if (this.ODCHECK == true) {
              selecttime = this.chkod(selecttime)
            }
            if (this.ODCHECK2 == true) {
              selecttime2 = this.chkod(selecttime2)
            }
            if (this.ODCHECK3 == true) {
              selecttime3 = this.chkod(selecttime3)
            }
            if (this.ODCHECK4 == true) {
              selecttime4 = this.chkod(selecttime4)
            }
          }, 60);

          setTimeout(() => {
            if (this.OSCHECK == true) {
              selecttime = this.chkos(selecttime)
            }
            if (this.OSCHECK2 == true) {
              selecttime2 = this.chkos(selecttime2)
            }
            if (this.OSCHECK3 == true) {
              selecttime3 = this.chkos(selecttime3)
            }
            if (this.OSCHECK4 == true) {
              selecttime4 = this.chkos(selecttime4)
            }
          }, 90);


          setTimeout(() => {
            if (this.CHK == true) {
              this.createworkplan.wrkshfcod = selecttime
              createworkplanShiftCodeORG = sltime
              if (this.CHK2 == true) {
                this.createworkplan.wrkshfcod = selecttime + "," + selecttime2
                createworkplanShiftCodeORG = sltime + "," + sltime2
                if (this.CHK3 == true) {
                  this.createworkplan.wrkshfcod = selecttime + "," + selecttime2 + "," + selecttime3
                  createworkplanShiftCodeORG = sltime + "," + sltime2 + "," + sltime3
                  if (this.CHK4 == true) {
                    this.createworkplan.wrkshfcod = selecttime + "," + selecttime2 + "," + selecttime3 + "," + selecttime4
                    createworkplanShiftCodeORG = sltime + "," + sltime2 + "," + sltime3 + "," + sltime4
                  }
                }
              }
            }
          }, 150);
          setTimeout(() => {


            if (this.createworkplan.wrkshfcod != '' && this.createworkplan.wrkshfcod != null && this.createworkplan.wrkshfcod != undefined) {
              if (this.CHECKDTE == true) {
                var dteck = parseInt(dte2.substr(0, 2))
                var dteckbe = parseInt(dte1.substr(0, 2))
                console.log(dteck)
                console.log(dteckbe)
                var g = this.tag_userlist.length
                var f = 0
                var l = ((dteck - dteckbe) + 1 * 1)
                var per = 100 / this.tag_userlist.length / l
                console.log(per)
                var fl = 0
                for (let h = dteckbe; h <= dteck; h++ , f++) {

                  setTimeout(() => {

                    var str = new Date(dte1.substr(6, 4) + '-' + dte1.substr(3, 2) + '-' + h)


                    for (let i = 0; i < this.tag_userlist.length; i++) {
                      var chkloop = 0
                      // console.log(i)
                      setTimeout(() => {

                        fl = fl + 1
                        var pr = (per * fl).toFixed(2)
                        console.log(pr)
                        console.log(fl)
                        this.createworkplan.wrkdte = dte1.substr(6, 4) + dte1.substr(3, 2) + ("0" + str.getDate()).slice(-2)

                        this.createworkplan.wrkyeamon = dte1.substr(6, 4) + dte1.substr(3, 2)

                        this.createworkplan.wrkempid = this.tag_userlist[i].psnpid

                        var chkdata = this.WorkplanComponent.workplan.filter(res => res.wrkempid == this.createworkplan.wrkempid && res.wrkdte == this.createworkplan.wrkdte)
                        if (chkdata.length > 0) {
                          var datachklenght = (chkdata[0].wrkshfcod + "," + this.createworkplan.wrkshfcod).split(",").length
                          if (datachklenght <= 4) {
                            var arryshifc = chkdata[0].wrkshfcod.split(",")
                            console.log(chkdata[0].wrkshfcod)
                            console.log(arryshifc)
                            console.log(createworkplanShiftCodeORG)
                            var arryshifc2 = createworkplanShiftCodeORG.split(",")

                            for (let k = 0; k < arryshifc.length; k++) {
                              var fndin = 0
                              setTimeout(() => {
                                if (arryshifc[k].search(">1") > 0) {
                                  fndin = arryshifc[k].indexOf(">1")
                                } else if (arryshifc[k].search("@") > 0) {
                                  fndin = arryshifc[k].indexOf("@")
                                } else if (arryshifc[k].search("!") > 0) {
                                  fndin = arryshifc[k].indexOf("!")
                                } else if (arryshifc[k].search("#") > 0) {
                                  fndin = arryshifc[k].indexOf("#")
                                }
                                if (fndin > 0) {
                                  var shift = arryshifc[k].substr(0, fndin)
                                }
                                else {
                                  var shift = arryshifc[k]
                                }
                                for (let j = 0; j < arryshifc2.length; j++) {
                                  console.log(shift)
                                  console.log(arryshifc2[j])
                                  if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG2) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))) {
                                    chkloop = 1;
                                  }

                                }
                              }, 20);


                            }
                            setTimeout(() => {
                              if (chkloop != 1) {
                                if (chkdata[0].wrkshfcod.length > 0) {
                                  this.updateworkplan.wrkshfcod = chkdata[0].wrkshfcod + "," + this.createworkplan.wrkshfcod
                                }
                                else {
                                  this.updateworkplan.wrkshfcod = this.createworkplan.wrkshfcod
                                }

                                this.updateworkplan.id = chkdata[0].id
                                this.updateworkplan.wrkdoc = chkdata[0].wrkdoc
                                this.updateworkplan.wrkdte = chkdata[0].wrkdte
                                this.updateworkplan.wrkempid = chkdata[0].wrkempid
                                this.updateworkplan.wrkyeamon = chkdata[0].wrkyeamon
                                this.updateworkplan.wrkdep = chkdata[0].wrkdep
                                this.WorkplanServiceProxy.updateWorkPlan(this.updateworkplan).subscribe(() => {


                                })
                              } else {
                                this.WorkplanComponent.log_savefailRe = this.WorkplanComponent.log_savefailRe + "พนักงาน: " + this.WorkplanComponent.resourcesDataOrg.filter(res => res.id == this.createworkplan.wrkempid)[0].text + " วันที่: " + this.createworkplan.wrkdte.substr(6, 2) + "/" + this.createworkplan.wrkdte.substr(4, 2) + "/" + this.createworkplan.wrkdte.substr(0, 4) + "\n"
                              }
                            }, 80);
                            this.progressbarss = pr + "%"
                            document.getElementById("pbb").style.width = this.progressbarss;
                          }
                          else {
                            this.WorkplanComponent.log_savefail = this.WorkplanComponent.log_savefail + "พนักงาน: " + this.WorkplanComponent.resourcesDataOrg.filter(res => res.id == this.createworkplan.wrkempid)[0].text + " วันที่: " + this.createworkplan.wrkdte.substr(6, 2) + "/" + this.createworkplan.wrkdte.substr(4, 2) + "/" + this.createworkplan.wrkdte.substr(0, 4) + "\n"
                            this.progressbarss = pr + "%"
                            document.getElementById("pbb").style.width = this.progressbarss;
                          }
                        }
                        else {
                          this.WorkplanServiceProxy.addWorkPlan(this.createworkplan).subscribe(() => {
                            this.progressbarss = pr + "%"
                            document.getElementById("pbb").style.width = this.progressbarss;
                          })
                        }
                      }, 350 * i);
                    }
                    if (dteck == h) {
                      setTimeout(() => {
                        this.modalSave.emit();
                        this.loadingVisible = false
                        this.message.success('เสร็จสมบูรณ์', 'Complete')
                        this.progressbarss = "0%"
                        document.getElementById("pbb").style.width = this.progressbarss;
                        this.close()

                      }, 1000);
                    }
                  }, 350 * f * g);
                }
              }
              else {
                var per = 100 / this.tag_userlist.length
                this.createworkplan.wrkdte = dte1.substr(6, 4) + dte1.substr(3, 2) + dte1.substr(0, 2);
                this.createworkplan.wrkyeamon = dte1.substr(6, 4) + dte1.substr(3, 2)
                for (let i = 0; i < this.tag_userlist.length; i++) {
                  f = i + 1
                  var chkloop = 0
                  setTimeout(() => {

                    var pr = (per * (i + 1 * 1)).toFixed(2)
                    this.createworkplan.wrkempid = this.tag_userlist[i].psnpid
                    var chkdata = this.WorkplanComponent.workplan.filter(res => res.wrkempid == this.createworkplan.wrkempid && res.wrkdte == this.createworkplan.wrkdte)
                    if (chkdata.length > 0) {
                      var datachklenght = (chkdata[0].wrkshfcod + "," + this.createworkplan.wrkshfcod).split(",").length
                      if (datachklenght <= 4) {
                        var arryshifc = chkdata[0].wrkshfcod.split(",")
                        console.log(chkdata[0].wrkshfcod)
                        console.log(arryshifc)
                        console.log(createworkplanShiftCodeORG)
                        var arryshifc2 = createworkplanShiftCodeORG.split(",")

                        for (let k = 0; k < arryshifc.length; k++) {
                          var fndin = 0
                          setTimeout(() => {
                            if (arryshifc[k].search(">1") > 0) {
                              fndin = arryshifc[k].indexOf(">1")
                            } else if (arryshifc[k].search("@") > 0) {
                              fndin = arryshifc[k].indexOf("@")
                            } else if (arryshifc[k].search("!") > 0) {
                              fndin = arryshifc[k].indexOf("!")
                            } else if (arryshifc[k].search("#") > 0) {
                              fndin = arryshifc[k].indexOf("#")
                            }
                            if (fndin > 0) {
                              var shift = arryshifc[k].substr(0, fndin)
                            }
                            else {
                              var shift = arryshifc[k]
                            }
                            for (let j = 0; j < arryshifc2.length; j++) {
                              console.log(shift)
                              console.log(arryshifc2[j])
                              if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG2) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc2[j])[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))) {
                                chkloop = 1;
                              }

                            }
                          }, 20);


                        }
                        setTimeout(() => {

                          if (chkloop != 1) {
                            if (chkdata[0].wrkshfcod.length > 0) {
                              this.updateworkplan.wrkshfcod = chkdata[0].wrkshfcod + "," + this.createworkplan.wrkshfcod
                            }
                            else {
                              this.updateworkplan.wrkshfcod = this.createworkplan.wrkshfcod
                            }
                            // this.updateworkplan.wrkshfcod = chkdata[0].wrkshfcod + "," + this.createworkplan.wrkshfcod
                            this.updateworkplan.id = chkdata[0].id
                            this.updateworkplan.wrkdoc = chkdata[0].wrkdoc
                            this.updateworkplan.wrkdte = chkdata[0].wrkdte
                            this.updateworkplan.wrkempid = chkdata[0].wrkempid
                            this.updateworkplan.wrkyeamon = chkdata[0].wrkyeamon
                            this.updateworkplan.wrkdep = chkdata[0].wrkdep
                            this.WorkplanServiceProxy.updateWorkPlan(this.updateworkplan).subscribe(() => {
                              // if (this.tag_userlist.length == i + 1) {
                              //   this.modalSave.emit();
                              //   this.loadingVisible = false
                              //   this.close()
                              // }

                            })
                          } else {
                            this.WorkplanComponent.log_savefailRe = this.WorkplanComponent.log_savefailRe + "พนักงาน: " + this.WorkplanComponent.resourcesDataOrg.filter(res => res.id == this.createworkplan.wrkempid)[0].text + " วันที่: " + this.createworkplan.wrkdte.substr(6, 2) + "/" + this.createworkplan.wrkdte.substr(4, 2) + "/" + this.createworkplan.wrkdte.substr(0, 4) + "\n"
                          }
                        }, 80);
                        this.progressbarss = pr + "%"
                        document.getElementById("pbb").style.width = this.progressbarss;
                      }
                      else {
                        this.WorkplanComponent.log_savefail = this.WorkplanComponent.log_savefail + "พนักงาน: " + this.WorkplanComponent.resourcesDataOrg.filter(res => res.id == this.createworkplan.wrkempid)[0].text + " วันที่: " + this.createworkplan.wrkdte.substr(6, 2) + "/" + this.createworkplan.wrkdte.substr(4, 2) + "/" + this.createworkplan.wrkdte.substr(0, 4) + "\n"
                        this.progressbarss = pr + "%"
                        document.getElementById("pbb").style.width = this.progressbarss;
                      }
                    }
                    else {
                      this.WorkplanServiceProxy.addWorkPlan(this.createworkplan).subscribe(() => {
                        this.progressbarss = pr + "%"
                        document.getElementById("pbb").style.width = this.progressbarss;
                      })
                    }

                    if (this.tag_userlist.length == i + 1) {
                      setTimeout(() => {

                        this.loadingVisible = false
                        this.message.success('เสร็จสมบูรณ์', 'Complete')
                        this.progressbarss = "0%"
                        document.getElementById("pbb").style.width = this.progressbarss;
                        this.close()
                        this.modalSave.emit();



                      }, 1000);
                    }


                  }, 350 * f);

                }


              }
            }
            else {
              this.loadingVisible = false
              alert("กรุณาเลือกช่วงเวลา โดยเรียงลำดับจาก 1-4")
            }
          }, 200);
        }
        else {
          alert("กรุณาเลือกพนักงานที่ต้องการเพิ่มตารางเวร")
        }
      }
      else {
        alert("กรุณาระบุถึงวันที่ที่ต้องการเพิ่มตารางเวร")
      }
    }
    else {
      alert("กรุณาระบุวันที่ที่ต้องการเพิ่มตารางเวร")
    }
  }

  log_savefail = []
  test() {
    alert(this.OTCHECK)
  }

  CHECKDTE: boolean;
  OTCHECK: boolean;
  OCCHECK: boolean;
  ODCHECK: boolean;
  OSCHECK: boolean;

  OTCHECK2: boolean;
  OCCHECK2: boolean;
  ODCHECK2: boolean;
  OSCHECK2: boolean;

  OTCHECK3: boolean;
  OCCHECK3: boolean;
  ODCHECK3: boolean;
  OSCHECK3: boolean;

  OTCHECK4: boolean;
  OCCHECK4: boolean;
  ODCHECK4: boolean;
  OSCHECK4: boolean;

  CHK: boolean;
  CHK2: boolean;
  CHK3: boolean;
  CHK4: boolean;

}
