import { Component, OnInit, Injector, ViewChild } from '@angular/core';

import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { WorkplanServiceProxy, WorkPlanList, DbmtabServiceProxy, DbmtabListDto, UpdateWorkPlan,  WorkplanEmpList, KPShareServiceProxy, MSMPSN00ServiceProxy } from '@shared/service-proxies/service-proxies';
import { DxSchedulerComponent } from 'devextreme-angular';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  templateUrl: './workplan.component.html',
  animations: [appModuleAnimation()],
  styleUrls: ['./workplan.component.css']
})


export class WorkplanComponent extends AppComponentBase implements OnInit {
  timeou: number;
  loadingVisible: boolean;
  updateworkplan: UpdateWorkPlan = new UpdateWorkPlan();
  departmant: DbmtabListDto[] = []

  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private DbmtabStorePServiceProxy: KPShareServiceProxy,
    private WorkplanServiceProxy: WorkplanServiceProxy,
    private EmpMaster: MSMPSN00ServiceProxy
  ) {
    super(injector);
  }
  resourcesData = []
  resourcesDataOrg = []
  // resourcesData2 = [
  //   {
  //     "text": "นายกิตติพงษ์ แซ่โค้ว",
  //     "id": "1"

  //   }, {
  //     "text": "นายทวัช พงศิริ",
  //     "id": "2"

  //   }, {
  //     "text": "นายกิตติศักดิ์ เสนา",
  //     "id": "3"

  //   }, {
  //     "text": "นางสาวลลิตา ขวัญเมือง",
  //     "id": "4"
  //   }, {
  //     "text": "นางสาวสมอ อ้อยทอง",
  //     "id": "5"
  //   }
  // ]
  ngOnInit() {

    // $(".form_datetime").datetimepicker({ format: 'MM/YYYY', locale: 'th' });
    $('.m_select2_1as').select2({
      placeholder: "Please Select.."
    });
    $('.m_select2_1ass').select2({
      placeholder: "Please Select.."
    });
    this._dbmtabService.getDbmtabFromTABFG1('SHIFTCODE', '').subscribe((result) => {
      this.dbmtabshiftcode = result.items
      this.fndata()
    })
    this._dbmtabService.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
      this.departmant = result.items

    });
    $('#selemp').change(function () {
      $('#btsearch').click()
  });
    //this.resourcesData = [{ "id": '1', "text": '', "color": this.getRandomColor() }]
    // for (let i = 0; i < this.resourcesData2.length; i++) {
    //   this.resourcesData.push({ "id": this.resourcesData2[i].id, "text": this.resourcesData2[i].text, "color": this.getRandomColor() })

    // }
    // this.EmpMaster.getEmployeeByDepartmentAndEmpId(this.dep,this.dep,'','zz').subscribe((result)=>{
    //   this.employee = result.items
    //   for (let i = 0; i < this.employee.length; i++) {
    //     this.resourcesData.push({ "id": this.employee[i].psnidn, "text": this.employee[i].psnfnm, "color": this.getRandomColor() })
    //   }




    // })


  }
  h = 0;
  getRandomColor() {
    // var letters = '0123456789ABCDEF';
    // var color = '#';
    // for (var i = 0; i < 6; i++) {
    //   color += letters[Math.floor(Math.random() * 16)];
    // }
    
    var color = ''
    if (this.h == 0) {
      color = '#0040ff';
      this.h = 1
    } else {
      color = '#055000';
      this.h = 0
    }
    return color;
  }
  arry = []
  arryOrg = []
  currentDate: Date = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
  workplan: WorkPlanList[] = [];
  dbmtabshiftcode: DbmtabListDto[];
  firstDay;
  lastDay;
  resourcesData3 = []
  resourcesData3s = []
  fndata() {
    this.dep = '10AC'
    this.arryOrg = []
    this.arry = []
    this.resourcesData3 = []
    this.resourcesData3s = []
    this.resourcesData = []
    this.h = 0
    // this.EmpMaster.getEmployeeByDepartmentAndEmpId(this.dep, this.dep, '', 'zz').subscribe((result) => {
    //   this.employee = result.items
    //   for (let i = 0; i < this.employee.length; i++) {
    //     this.resourcesData3.push({ "id": this.employee[i].psnidn, "text": this.employee[i].psnfnm + ' ' + this.employee[i].psnlnm + '(' + this.employee[i].psnnic + ')', "color": this.getRandomColor() })
    //   }
    //   setTimeout(() => {
    //     this.resourcesDataOrg = this.resourcesData3
    //     this.resourcesData = this.resourcesData3
    //     setTimeout(() => {
    //       $('.m_select2_1as').val('').trigger('change');
    //     }, 50);
    //   }, 300);
    // })
    var date = new Date();
    this.dtes = date.getFullYear() + '' + ("0" + (date.getMonth() + 1)).slice(-2)
    $('#date1z').val(this.dtes.substr(4, 2) + "/" + this.dtes.substr(0, 4))
    var newdte = this.dtes.substr(4, 2) + "/" + this.dtes.substr(0, 4)
    this.firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    this.lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    // this.WorkplanServiceProxy.getWorkplanByYeaMonDep(this.dtes, this.dep).subscribe((result) => {
    //   this.workplan = result.items

    //   for (let i = 0; i < this.workplan.length; i++) {

    //     setTimeout(() => {


    //       var sc = []
    //       if (this.workplan[i].wrkshfcod.length > 1) {

    //         if (this.workplan[i].wrkshfcod.search(",") < 0) {
    //           sc.push(this.workplan[i].wrkshfcod)
    //         }
    //         else {
    //           sc = this.workplan[i].wrkshfcod.split(",")
    //         }

    //         for (let h = 0; h < sc.length; h++) {
    //           var shift = sc[h]
    //           if (sc[h].search(">1") > 0) {
    //             shift = shift.replace(">1", "")
    //             var ot = true
    //           }
    //           else {
    //             var ot = false
    //           }
    //           if (sc[h].search("@") > 0) {
    //             shift = shift.replace("@", "")
    //             var oc = true
    //           }
    //           else {
    //             var oc = false
    //           }
    //           if (sc[h].search("!") > 0) {
    //             shift = shift.replace("!", "")
    //             var od = true
    //           }
    //           else {
    //             var od = false
    //           }
    //           if (sc[h].search("#") > 0) {
    //             shift = shift.replace("#", "")
    //             var os = true
    //           }
    //           else {
    //             var os = false
    //           }

    //           this.arry.push({
    //             "text": sc[h],
    //             "startDate": new Date(this.workplan[i].wrkdte + 'T' + this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1),
    //             "endDate": new Date(this.workplan[i].wrkdte + 'T' + this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2),
    //             "wrkempid": this.workplan[i].wrkempid,
    //             "dte": this.workplan[i].wrkdte,
    //             "doc": this.workplan[i].wrkdoc,
    //             "id": this.workplan[i].id,
    //             "shiftcod": this.workplan[i].wrkshfcod,
    //             "orgshift": shift,
    //             "OT": ot,
    //             "OC": oc,
    //             "OD": od,
    //             "OS": os,
    //             "timest": this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1,
    //             "timeend": this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2
    //           })
    //           console.log(this.arry[h])
    //           this.arryOrg.push(this.arry[h])



    //         }
    //       }
    //     }, 5 * i);
    //   }

    // });
    setTimeout(() => {
      this.fndatadte(newdte, this.dep)
    }, 100);


  }
  dtes = ''
  dep = ''
  employee: WorkplanEmpList[] = []
  employeeorg: WorkplanEmpList[] = []
  fndatadte(dte, dep) {
    var chksel = $('.m_select2_1as').val()
    this.h = 0
    this.dtes = dte.substr(3, 6) + dte.substr(0, 2)
    var date = new Date(this.dtes.substr(0, 4) + "-" + this.dtes.substr(4, 2) + "-" + '01');
    this.firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    this.lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    this.currentDate = this.firstDay
    this.arryOrg = []
    this.arry = []
    this.dep = dep
    this.resourcesData = []
    this.resourcesData3 = []
    this.resourcesData3s = []
    // $(".form_datetimes").datetimepicker({ format: 'DD/MM/YYYY', locale: 'th' ,minDate: this.firstDay ,maxDate: this.lastDay})
    this.DbmtabStorePServiceProxy.getEmployeeByDepartmentAndEmpIdFromWorkplan(this.dtes, dep, dep, '', 'zz','Master').subscribe((results) => {
      this.employeeorg = results
      for (let i = 0; i < this.employeeorg.length; i++) {
        this.resourcesData3s.push({ "id": this.employeeorg[i].psnidn, "text": this.employeeorg[i].psnempname, "color": this.getRandomColor() })
      }
    })
    this.DbmtabStorePServiceProxy.getEmployeeByDepartmentAndEmpIdFromWorkplan(this.dtes, dep, dep, '', 'zz','Workplan').subscribe((result) => {
      this.employee = result

      this.employees = result

      for (let i = 0; i < this.employee.length; i++) {
        this.resourcesData3.push({ "id": this.employee[i].psnidn, "text": this.employee[i].psnempname, "color": this.getRandomColor() })
      }
      setTimeout(() => {
        this.resourcesDataOrg = this.resourcesData3
        this.resourcesData = this.resourcesData3
        setTimeout(() => {
          
          console.log(chksel)
          if(chksel != 'all'){
            $('.m_select2_1as').val(chksel).trigger('change'); 
          }
          else{
            $('.m_select2_1as').val('all').trigger('change');
          }
          
        }, 50);

      }, 300);

    })


    this.WorkplanServiceProxy.getWorkplanByYeaMonDep(this.dtes, dep).subscribe((result) => {
      this.workplan = result.items


      for (let i = 0; i < this.workplan.length; i++) {
        //console.log(this.workplan[i])

        var sc = []
        if (this.workplan[i].wrkshfcod.length > 1) {
          sc = this.workplan[i].wrkshfcod.split(",")
          for (let h = 0; h < sc.length; h++) {
            //console.log(sc[h])
            var shift = sc[h]
            if (sc[h].search(">1") > 0) {
              shift = shift.replace(">1", "")
              var ot = true
              //console.log(shift)
            }
            else {
              var ot = false
            }
            if (sc[h].search("@") > 0) {
              shift = shift.replace("@", "")
              var oc = true
            }
            else {
              var oc = false
            }
            if (sc[h].search("!") > 0) {
              shift = shift.replace("!", "")
              var od = true
            }
            else {
              var od = false
            }
            if (sc[h].search("#") > 0) {
              shift = shift.replace("#", "")
              var os = true
            }
            else {
              var os = false
            }
            var datadte = this.workplan[i].wrkdte.substr(0, 4) + "-" + this.workplan[i].wrkdte.substr(4, 2) + "-" + this.workplan[i].wrkdte.substr(6, 2)
            this.arry.push({
              "text": sc[h],
              "startDate": new Date(datadte + 'T' + this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1),
              "endDate": new Date(datadte + 'T' + this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2),
              "wrkempid": this.workplan[i].wrkempid,
              "dte": this.workplan[i].wrkdte,
              "doc": this.workplan[i].wrkdoc,
              "id": this.workplan[i].id,
              "shiftcod": this.workplan[i].wrkshfcod,
              "orgshift": shift,
              "OT": ot,
              "OC": oc,
              "OD": od,
              "OS": os,
              "timest": this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1,
              "timeend": this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2
            })
            //console.log(this.arry[h])
            this.arryOrg.push(this.arry[h])
          }
        }

      }
      //this.fnTimeline()
      // 
    })
    setTimeout(() => {
      if (this.log_savefail.length > 0) {
        
        alert("Eror: Workshift เกินจำนวนที่กำหนด\n-----------------------------------------------------------------\n" + this.log_savefail)
        this.log_savefail = ''
      }
      if (this.log_savefailRe.length > 0) {
        
        alert("Eror: Workshift มีช่วงเวลาที่ซ้ำ\n--------------------------------------------------------------------\n" + this.log_savefailRe)
        this.log_savefailRe = ''
      }
    }, 500);
  }
  log_savefail = ''
  log_savefailRe = ''
  fndatacurdte() {
    this.h = 0
    this.resourcesData = this.resourcesDataOrg
    $('.m_select2_1as').val('all').trigger('change');
    this.arryOrg = []
    this.arry = []
    this.WorkplanServiceProxy.getWorkplanByYeaMonDep(this.dtes, this.dep).subscribe((result) => {
      this.workplan = result.items


      for (let i = 0; i < this.workplan.length; i++) {
        //console.log(this.workplan[i])

        var sc = []
        if (this.workplan[i].wrkshfcod.length > 1) {
          sc = this.workplan[i].wrkshfcod.split(",")

          for (let h = 0; h < sc.length; h++) {
            //console.log(sc[h])
            var shift = sc[h]
            if (sc[h].search(">1") > 0) {
              shift = shift.replace(">1", "")
              var ot = true
            }
            else {
              var ot = false
            }
            if (sc[h].search("@") > 0) {
              shift = shift.replace("@", "")
              var oc = true
            }
            else {
              var oc = false
            }
            if (sc[h].search("!") > 0) {
              shift = shift.replace("!", "")
              var od = true
            }
            else {
              var od = false
            }
            if (sc[h].search("#") > 0) {
              shift = shift.replace("#", "")
              var os = true
            }
            else {
              var os = false
            }
            var datadte = this.workplan[i].wrkdte.substr(0, 4) + "-" + this.workplan[i].wrkdte.substr(4, 2) + "-" + this.workplan[i].wrkdte.substr(6, 2)

            this.arry.push({
              "text": sc[h],
              "startDate": new Date(datadte + 'T' + this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1),
              "endDate": new Date(datadte + 'T' + this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2),
              "wrkempid": this.workplan[i].wrkempid,
              "dte": this.workplan[i].wrkdte,
              "doc": this.workplan[i].wrkdoc,
              "id": this.workplan[i].id,
              "shiftcod": this.workplan[i].wrkshfcod,
              "orgshift": shift,
              "OT": ot,
              "OC": oc,
              "OD": od,
              "OS": os,
              "timest": this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1,
              "timeend": this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2
            })
            this.arryOrg.push(this.arry[h])
          }


        }
      }
      //this.fnTimeline()
      // this.shedul.instance.getDataSource().reload()
    })
    setTimeout(() => {
      if (this.log_savefail.length > 0) {
        alert("Eror: Workshift เกินจำนวนที่กำหนด\n-----------------------------------------------------------------\n" + this.log_savefail)
        this.log_savefail = ''
      }
    }, 500);
  }


  @ViewChild("schedulerID") shedul: DxSchedulerComponent
  test(e) {
    alert(5555)
    console.log(e)

  }
  searchper(empid) {
    this.resourcesData = []
    if (empid == 'all') {
      //this.fndatacurdte()
      this.resourcesData = this.resourcesDataOrg
    }
    else {
      var arr = this.arryOrg.filter(res => res.wrkempid == empid)

      this.resourcesData = this.resourcesDataOrg.filter(res => res.id == empid)
      // setTimeout(() => {
      //   this.arry = arr
      //   console.log(this.arry)
      // }, 2000);

    }

  }
  searchall() {
    //$('.m_select2_1as').val('').trigger('change');


  }
  canc(e) {
    e.cancel = true;
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }

  deleteDetails(data) {
    this.shedul.instance.hideAppointmentTooltip();
    //console.log(data)
    this.message.confirm(
      this.l('AreYouSureToDeleteTheData'),
      isConfirmed => {
        if (isConfirmed) {
          var arryshifc = data.shiftcod.split(",")
          arryshifc.splice(arryshifc.findIndex(dt => dt == data.text), 1);
          var newshifc = ''
          for (let i = 0; i < arryshifc.length; i++) {
            newshifc = newshifc + arryshifc[i] + ","
          }
          setTimeout(() => {
            var nshift = newshifc.substr(0, newshifc.length - 1)
            this.updateworkplan.id = data.id
            this.updateworkplan.wrkshfcod = nshift
            this.updateworkplan.wrkdoc = data.doc
            this.updateworkplan.wrkdte = data.dte
            this.updateworkplan.wrkempid = data.wrkempid
            this.updateworkplan.wrkyeamon = this.dtes
            this.updateworkplan.wrkdep = this.dep
            this.WorkplanServiceProxy.updateWorkPlan(this.updateworkplan).subscribe(() => {
              var dte = $('#date1z').val()
              this.fndatadte(dte, this.updateworkplan.wrkdep)
            })
          }, 100);
        }
      })

  }
  editDetails(data) {

  }

  employees: WorkplanEmpList[] = []
  @ViewChild('modalt') modalt: ModalDirective;
  searchdep(dep) {
    this.employees = [];
    this.DbmtabStorePServiceProxy.getEmployeeByDepartmentAndEmpIdFromWorkplan(this.dtes, dep, dep, '', 'zz','Master').subscribe((result) => {
      this.employees = result
    })
  }
  tag_userlist = [];
  tag_user(id) {

    var data = this.employees.filter(data => data.psnidn == id);
    var chk = 0;
    data.forEach(element => {
      if (element.psnidn == id) {
        chk = 1;
      }
    });
    if (chk == 1) {
      var gen_tag_userlist = {
        "psnpid": data[0].psnidn,
        "name": data[0].psnempname
      }
      this.tag_userlist.push(gen_tag_userlist);
    }

    console.log(this.tag_userlist);

  }
  delete_tag_user(id) {
    //var indx = this.tag_userlist.findIndex(da => da.psnpid == id)
    this.tag_userlist.splice(this.tag_userlist.findIndex(da => da.psnpid == id), 1)
    // for (const key in this.tag_userlist) {
    //   if (this.tag_userlist[key].psnpid == id) delete this.tag_userlist[key]
    // }

    // console.log(id);

  }

  showtotal(vart) {
    this.tag_userlist = []
    this.varty = vart
    this.modalt.show()
    setTimeout(() => {
      // $(".form_datetime2").datetimepicker({ format: 'MM/YYYY', locale: 'th' });
      $('.m_select2_1ass2').select2({
        placeholder: "Please Select.."
      });
      $('.m_select2_1at2').select2({
        placeholder: "Please Select.."
      });
    }, 1000);

  }
  closetotal() {

    this.modalt.hide()
  }
  saveemp() {
    for (let i = 0; i < this.tag_userlist.length; i++) {
      this.resourcesData3s.push({ "id": this.tag_userlist[i].psnpid, "text": this.tag_userlist[i].name, "color": this.getRandomColor() })
    }
    this.closetotal()
    // setTimeout(() => {
    //   
    // }, 1000);

  }
  varty;
  settings(){

  }
  @ViewChild('modalset') modalset: ModalDirective;
  
  settingsD(){
    this.modalset.show()
  }
  closestD(){
    this.modalset.hide()
  }
  savestD(){
    
  }

  saveset(){

  }

  canset(){

  }
}
