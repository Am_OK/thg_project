import { Component, OnInit, Injector, Output, EventEmitter, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { WorkplanComponent } from '../workplan.component';
import { UpdateWorkPlan, DbmtabListDto, WorkplanServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'appworkplanedit',
  templateUrl: './workplan-edit.component.html',
  styleUrls: ['./workplan-edit.component.css']
})
export class WorkplanEditComponent extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  updateworkplan: UpdateWorkPlan = new UpdateWorkPlan()
  dbmtabshiftcode: DbmtabListDto[] = []

  constructor(
    injector: Injector,
    private WorkplanServiceProxy: WorkplanServiceProxy,
    private WorkplanComponent: WorkplanComponent
  ) {
    super(injector);
  }
  arr2
  empname = '' 
  active : boolean;
  show(e): void {
    this.active = true
    
    this.WorkplanComponent.shedul.instance.hideAppointmentTooltip();
    setTimeout(() => {
      this.modal.show()
    }, 100);
    //console.log(e)
    this.dbmtabshiftcode = this.WorkplanComponent.dbmtabshiftcode;

    var dt = new Date(e.endDate);
    var dte = dt.toISOString().substr(0, 10);
    this.arr2 = (this.WorkplanComponent.arry.filter(res => res.wrkempid == e.wrkempid && res.dte == dte)[0])
    //console.log(this.arr2)
    this.oldshift = e.text
    this.shiftbefor = e.shiftcod
    this.updateworkplan.wrkdte = dte
    this.updateworkplan.wrkempid = e.wrkempid
    this.empname = this.WorkplanComponent.resourcesDataOrg.filter(res => res.id == this.updateworkplan.wrkempid)[0].text
    this.updateworkplan.wrkshfcod = e.orgshift
    this.updateworkplan.wrkdoc = e.doc
    this.updateworkplan.id = e.id
    this.updateworkplan.wrkdep = this.WorkplanComponent.dep
    this.OTCHECK = e.OT
    this.OTCHECKORG = e.OT
    this.OCCHECK = e.OC
    this.OCCHECKORG = e.OC
    this.ODCHECK = e.OD
    this.ODCHECKORG = e.OD
    this.OSCHECK = e.OS
    this.OSCHECKORG = e.OS
   
    
    setTimeout(() => {
      $('.m_select2_1at').select2({
        placeholder: "Please Select.."
      });
    }, 200);

  }
  shiftbefor;
  oldshift;
  close() {
    this.modal.hide()
    this.active = false
  }
  OTCHECK: boolean;
  OTCHECKORG: boolean;
  OCCHECK: boolean;
  OCCHECKORG: boolean;
  ODCHECK: boolean;
  ODCHECKORG: boolean;
  OSCHECK: boolean;
  OSCHECKORG: boolean;
  save(selecttime2) {
    this.updateworkplan.wrkshfcod = selecttime2
    var arryshifc = this.shiftbefor.split(",")
    var newshifc = ''
    var chkloop = 0
     
    if ((arryshifc.findIndex(dt => dt == this.updateworkplan.wrkshfcod) < 0) || ((arryshifc.findIndex(dt => dt == this.updateworkplan.wrkshfcod) >= 0 && (this.OTCHECK != this.OTCHECKORG || this.OCCHECK != this.OCCHECKORG || this.ODCHECK != this.ODCHECKORG || this.OSCHECK != this.OSCHECKORG)))) {
      var indshift = arryshifc.findIndex(dt => dt == this.oldshift)
      var arryshifcnew = arryshifc
      arryshifcnew.splice(arryshifcnew.findIndex(dt => dt == this.oldshift), 1);

      for (let i = 0; i < arryshifcnew.length; i++) {
        if (arryshifcnew[i].search(">1") > 0) {
          var shift = arryshifcnew[i].substring(0, arryshifcnew[i].length - 2)
          if (shift != this.updateworkplan.wrkshfcod) {
            if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG2) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))) {
              chkloop = 1;
            }
          }
        }
        else {
          if (arryshifcnew[i] != this.updateworkplan.wrkshfcod) {
            if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifcnew[i])[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifcnew[i])[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifcnew[i])[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG2) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifcnew[i])[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifcnew[i])[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.updateworkplan.wrkshfcod)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifcnew[i])[0].tabfG2))) {
              chkloop = 1;
            }
          }
        }
      }
      var wshiftcode = this.updateworkplan.wrkshfcod
      if (this.OTCHECK == true) {
        wshiftcode = wshiftcode + ">1"
      }
      setTimeout(() => {
        if (this.OCCHECK == true) {
          wshiftcode = wshiftcode + "@"
        }
      }, 20);
      setTimeout(() => {
        if (this.ODCHECK == true) {
          wshiftcode = wshiftcode + "!"
        }
      }, 40);
      setTimeout(() => {
        if (this.OSCHECK == true) {
          wshiftcode = wshiftcode + "#"
        }
      }, 60);
      setTimeout(() => {
        arryshifc[indshift] = wshiftcode
      }, 67);
      setTimeout(() => {
        for (let i = 0; i < arryshifc.length; i++) {
          newshifc = newshifc + arryshifc[i] + ","
        }
      }, 75);


      setTimeout(() => {
        if (chkloop == 0) {
          var nshift = newshifc.substr(0, newshifc.length - 1)
          this.updateworkplan.wrkshfcod = nshift
          this.updateworkplan.wrkyeamon = this.WorkplanComponent.dtes
          this.updateworkplan.wrkdte = this.updateworkplan.wrkdte.substr(0,4)+this.updateworkplan.wrkdte.substr(5,2)+this.updateworkplan.wrkdte.substr(8,2)
          this.WorkplanServiceProxy.updateWorkPlan(this.updateworkplan).subscribe(() => {
            this.modalSave.emit(this.updateworkplan);
            this.close()
          })
        }
        else {
          alert("ช่วงเวลาต้องไม่อยู่ในช่วงเดียวกัน")
        }
      }, 100);
    }
    else {
      alert("ช่วงเวลาต้องไม่ซ้ำกัน")
    }
  }
}
