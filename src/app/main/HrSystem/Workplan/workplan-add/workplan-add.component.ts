import { Component, OnInit, Injector, Output, EventEmitter, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { WorkplanServiceProxy, AddWorkPlan, DbmtabListDto, UpdateWorkPlan } from '@shared/service-proxies/service-proxies';
import { WorkplanComponent } from '../workplan.component';

@Component({
  selector: 'appworkplanadd',
  templateUrl: './workplan-add.component.html',
  styleUrls: ['./workplan-add.component.css']
})
export class WorkplanAddComponent extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  createworkplan: AddWorkPlan = new AddWorkPlan()
  updateworkplan: UpdateWorkPlan = new UpdateWorkPlan()
  dbmtabshiftcode: DbmtabListDto[];
  constructor(
    injector: Injector,
    private WorkplanServiceProxy: WorkplanServiceProxy,
    private WorkplanComponent: WorkplanComponent
  ) {
    super(injector);
  }
  arr2;
  isDouble = 0;
  clickon = 0
  prevCellData;
  empname = ''
  active : boolean;
  show(e): void {
    this.active = true
    this.OTCHECK = false
    this.OCCHECK = false
    this.ODCHECK = false
    this.OSCHECK = false
    this.isDouble = this.isDouble + 1;
    if (this.isDouble == 1) {
      this.prevCellData = e.cellData;
    }





    setTimeout(() => {
      if (this.clickon == 0) {
        if (this.isDouble == 2) {
          if (e.cellData.startDate == this.prevCellData.startDate && e.cellData.endDate == this.prevCellData.endDate && e.cellData.groups == this.prevCellData.groups) {
            this.clickon = 1
            this.isDouble = 0;
            this.prevCellData = null;
            var dt = new Date(e.cellData.endDate);
            var dte = dt.toISOString().substr(0, 10);
            var dtedata = dte.substr(0, 4) + dte.substr(5, 2) + dte.substr(8, 2)
            this.arr2 = (this.WorkplanComponent.arry.filter(res => res.wrkempid == e.cellData.groups.wrkempid && res.dte == dtedata)[0])
            //console.log(this.arr2)
            this.createworkplan.wrkdte = dte
            this.createworkplan.wrkdep = this.WorkplanComponent.dep
            this.createworkplan.wrkempid = e.cellData.groups.wrkempid
            this.empname = this.WorkplanComponent.resourcesDataOrg.filter(res => res.id == this.createworkplan.wrkempid)[0].text
            this.dbmtabshiftcode = this.WorkplanComponent.dbmtabshiftcode
            $('.m_select2_1at').select2({
              placeholder: "Please Select.."
            });
            this.modal.show()
          }
          else {
            this.prevCellData = null;
            this.isDouble = 0;
          }
        }
        else {
          //alert('Click');
          this.isDouble = 0;
          this.clickon = 0;
          this.prevCellData = null;
        }
      }
      else {
        this.isDouble = 0;
        this.clickon = 0;
        this.prevCellData = null;
      }
    }, 500);




    //console.log(e)

  }
  close() {

    this.isDouble = 0;
    this.clickon = 0;
    this.prevCellData = null;
    this.modal.hide()
    this.active = false
  }
  save(selecttime) {

    this.createworkplan.wrkyeamon = this.WorkplanComponent.dtes
    if (selecttime != '' && selecttime != null) {
      this.createworkplan.wrkshfcod = selecttime
      if (this.arr2 != undefined) {
        var arryshifc = this.arr2.shiftcod.split(",")
        var chkloop = 0
        for (let i = 0; i < arryshifc.length; i++) {
          var fndin = 0
          if (arryshifc[i].search(">1") > 0) {
            fndin = arryshifc[i].indexOf(">1")
          } else if (arryshifc[i].search("@") > 0) {
            fndin = arryshifc[i].indexOf("@")
          } else if (arryshifc[i].search("!") > 0) {
            fndin = arryshifc[i].indexOf("!")
          } else if (arryshifc[i].search("#") > 0) {
            fndin = arryshifc[i].indexOf("#")
          }

          if (fndin > 0) {
            var shift = arryshifc[i].substr(0, fndin)
            if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG2) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == shift)[0].tabfG2))) {
              chkloop = 1;
            }
          }
          else {

            if (((this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc[i])[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc[i])[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc[i])[0].tabfG1 < this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG2) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG2 < this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc[i])[0].tabfG2)) || ((this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG1 <= this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc[i])[0].tabfG1) && (this.dbmtabshiftcode.filter(res => res.tabtB2 == this.createworkplan.wrkshfcod)[0].tabfG2 >= this.dbmtabshiftcode.filter(res => res.tabtB2 == arryshifc[i])[0].tabfG2))) {
              chkloop = 1;
            }

          }
        }
        if (arryshifc.findIndex(dt => dt == this.createworkplan.wrkshfcod) < 0) {
          var letter_Count = 0



          for (var position = 0; position < this.arr2.shiftcod.length; position++) {
            if (this.arr2.shiftcod.charAt(position) == ",") {
              letter_Count += 1;
            }
          }
          setTimeout(() => {
            if (chkloop == 0) {
              if (letter_Count < 3) {
                var wshiftcode = this.createworkplan.wrkshfcod
                if (this.OTCHECK == true) {
                  wshiftcode = wshiftcode + ">1"
                }
                setTimeout(() => {
                  if (this.OCCHECK == true) {
                    wshiftcode = wshiftcode + "@"
                  }
                }, 20);
                setTimeout(() => {
                  if (this.ODCHECK == true) {
                    wshiftcode = wshiftcode + "!"
                  }
                }, 40);
                setTimeout(() => {
                  if (this.OSCHECK == true) {
                    wshiftcode = wshiftcode + "#"
                  }
                }, 60);
                if (this.arr2.shiftcod.length > 0) {
                  // if (this.OTCHECK == true) {
                  //   this.updateworkplan.wrkshfcod = this.arr2.shiftcod + "," + this.createworkplan.wrkshfcod + ">1"
                  // }
                  // else {
                  //   this.updateworkplan.wrkshfcod = this.arr2.shiftcod + "," + this.createworkplan.wrkshfcod
                  // }

                  setTimeout(() => {
                    this.updateworkplan.wrkshfcod = this.arr2.shiftcod + "," + wshiftcode
                    this.updateworkplan.id = this.arr2.id
                    this.updateworkplan.wrkdoc = this.arr2.doc
                    this.updateworkplan.wrkdte = this.arr2.dte
                    this.updateworkplan.wrkempid = this.arr2.wrkempid
                    this.updateworkplan.wrkyeamon = this.WorkplanComponent.dtes
                    this.updateworkplan.wrkdep = this.WorkplanComponent.dep
                    this.WorkplanServiceProxy.updateWorkPlan(this.updateworkplan).subscribe(() => {
                      this.modalSave.emit(this.updateworkplan);
                      this.close()
                    })
                  }, 75);

                }
                else {
                  // if (this.OTCHECK == true) {
                  //   this.updateworkplan.wrkshfcod = this.createworkplan.wrkshfcod + ">1"
                  // }
                  // else {
                  //   this.updateworkplan.wrkshfcod = this.createworkplan.wrkshfcod
                  // }
                  setTimeout(() => {
                    this.updateworkplan.wrkshfcod = wshiftcode
                    this.updateworkplan.id = this.arr2.id
                    this.updateworkplan.wrkdoc = this.arr2.doc
                    this.updateworkplan.wrkdte = this.arr2.dte
                    this.updateworkplan.wrkempid = this.arr2.wrkempid
                    this.updateworkplan.wrkyeamon = this.WorkplanComponent.dtes
                    this.updateworkplan.wrkdep = this.WorkplanComponent.dep
                    this.WorkplanServiceProxy.updateWorkPlan(this.updateworkplan).subscribe(() => {
                      this.modalSave.emit(this.updateworkplan);
                      this.close()
                    })
                  }, 75);
                }
              }
              else {
                alert("ช่วงเวลาต้องไม่เกิน 4")
              }
            }
            else {
              alert("ช่วงเวลาต้องไม่อยู่ในช่วงเดียวกัน")
            }
          }, 100);
        }
        else {
          alert("ช่วงเวลาต้องไม่ซ้ำกัน")
        }
      }
      else {
        // if (this.OTCHECK == true) {
        //   this.createworkplan.wrkshfcod = this.createworkplan.wrkshfcod + ">1"
        // }
        var wshiftcode = this.createworkplan.wrkshfcod
        if (this.OTCHECK == true) {
          wshiftcode = wshiftcode + ">1"
        }
        setTimeout(() => {
          if (this.OCCHECK == true) {
            wshiftcode = wshiftcode + "@"
          }
        }, 20);
        setTimeout(() => {
          if (this.ODCHECK == true) {
            wshiftcode = wshiftcode + "!"
          }
        }, 40);
        setTimeout(() => {
          if (this.OSCHECK == true) {
            wshiftcode = wshiftcode + "#"
          }
        }, 60);
        setTimeout(() => {
          this.createworkplan.wrkshfcod = wshiftcode
          this.createworkplan.wrkdte = this.createworkplan.wrkdte.substr(0, 4) + this.createworkplan.wrkdte.substr(5, 2) + this.createworkplan.wrkdte.substr(8, 2)
          this.WorkplanServiceProxy.addWorkPlan(this.createworkplan).subscribe(() => {
            this.modalSave.emit(this.createworkplan);
            this.close()
          })
        }, 75);


      }
    }
    else {
      alert("กรุณาเลือกช่วงเวลา")
    }
  }

  test() {
    alert(this.OTCHECK)
  }
  OTCHECK: boolean;
  OCCHECK: boolean;
  ODCHECK: boolean;
  OSCHECK: boolean;

}
