import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'workingovertime',
  templateUrl: './workingovertime.component.html'
})
export class WorkingoverTimeComponent extends AppComponentBase implements OnInit {
 
  constructor(
    injector: Injector,
  ) {
    super(injector);


  }
  namesur;
  ngOnInit() {
  
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
