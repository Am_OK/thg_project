import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'Workingovertimeadd',
  templateUrl: './workingovertime-add.component.html',
})
export class WorkingovertimeAddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  constructor(
    injector: Injector,
  

  ) {
    super(injector);
   
  }
  ngOnInit() {
 
  }
  show(): void {
    
    this.active = true;
    this.modal.show();
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    $('#md').find("input").val('').end()   
    this.modal.hide();
    this.active = false;
  }
}
