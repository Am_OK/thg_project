import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'specialcostentry',
  templateUrl: './specialcostentry.component.html',
})
export class SpecialcostentryComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,

  ) {
    super(injector);


  }

  ngOnInit() {

  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
