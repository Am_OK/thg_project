import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'timerecordadd',
  templateUrl: './timerecord-add.component.html',
})
export class TimerecordentryAddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;

 
  constructor(
    injector: Injector,
  

  ) {
    super(injector);
   
  }
  ngOnInit() {
 
  }
  show(): void {
    // $("#dte1").val(moment().add(543, 'years').format('DD/MM/YYYY'));
    // $("#dte2").val(moment().add(543, 'years').format('DD/MM/YYYY'));

    // $("#holidayyear").val(moment().add(543, 'years').format('YYYY'));
    // $("#holidaycode").val(moment().add(543, 'years').format('YYYY'));
    
    this.active = true;
    this.modal.show();
  }
 
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    // // $('#md').find("input").val('').end()   
    this.modal.hide();
    this.active = false;
  }
}
