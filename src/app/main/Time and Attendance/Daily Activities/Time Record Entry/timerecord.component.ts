import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'timerecord',
  templateUrl: './timerecord.component.html'
})
export class TimeRecordEntryComponent extends AppComponentBase implements OnInit {
 
  constructor(
    injector: Injector,
  ) {
    super(injector);


  }
  namesur;
  ngOnInit() {
  //   $("#year").val(moment().add(543, 'years').format('YYYY'));
  //   $("#date1").val(moment().add(543, 'years').format('DD/MM/YYYY'));
  //   $("#date2").val(moment().add(543, 'years').format('DD/MM/YYYY'));
  //   $("#date3").val(moment().add(543, 'years').format('DD/MM/YYYY'));
  //   $("#month").val(moment().format('MM'));

  //   this._empMasterService.getEmployees('').subscribe((result) => {
  //     this.emp = result.items;
  //     this.namesur = result.items[0].psnfnm + result.items[0].psnlnm;
  // });
  
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
