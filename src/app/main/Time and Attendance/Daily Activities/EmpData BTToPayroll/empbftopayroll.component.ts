import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'empbftopayroll',
  templateUrl: './empbftopayroll.component.html'
})
export class EmpbfTopayrollComponent extends AppComponentBase implements OnInit {
 
  constructor(
    injector: Injector,
  ) {
    super(injector);


  }
  namesur;
  ngOnInit() {
  
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
