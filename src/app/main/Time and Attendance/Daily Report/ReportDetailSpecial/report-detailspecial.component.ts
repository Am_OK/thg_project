import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    templateUrl: './report-detailspecial.component.html',
    animations: [appModuleAnimation()]
})
export class ReportDetailSpecialComponent extends AppComponentBase implements OnInit {
    departmentCode: string;

    constructor(
        injector: Injector,

    ) {
        super(injector);

    }
    ngOnInit(): void {

    }


}
