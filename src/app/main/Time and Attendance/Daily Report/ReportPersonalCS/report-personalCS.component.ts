import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    templateUrl: './report-personalCS.component.html',
    animations: [appModuleAnimation()]
})
export class ReportPersonalCSComponent extends AppComponentBase implements OnInit {
    departmentCode: string;

    constructor(
        injector: Injector,

    ) {
        super(injector);

    }
    ngOnInit(): void {

    }

}
