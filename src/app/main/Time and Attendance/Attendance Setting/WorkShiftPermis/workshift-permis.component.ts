import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'workshift-permis',
  templateUrl: './workshift-permis.component.html'
})
export class WorkshiftpermisComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,

  ) {
    super(injector);


  }

  ngOnInit() {
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
