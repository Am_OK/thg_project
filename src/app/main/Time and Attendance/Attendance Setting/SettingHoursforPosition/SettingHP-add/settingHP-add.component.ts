import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'settingHPadd',
  templateUrl: './settingHP-add.component.html',
})
export class SettingHPAddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  array_data_complete = [({
    'hpyear': '', 'hpmonth': ''
    , 'hpnumwork': '', 'hphourot': '', 'hphourotn': '', 'hpworking': ''
  })];

 
  constructor(
    injector: Injector,
  

  ) {
    super(injector);
   
  }
  ngOnInit() {
 
  }
  show(): void {
    // $("#holidaydate").val(moment().add(543, 'years').format('DD/MM/YYYY'));
    // $("#year").val(moment().add(543, 'years').format('YYYY'));
    // $("#month").val(moment().format('MM'));
    // $("#to").val(moment().format('MM'));

    
    this.active = true;
    this.modal.show();
    this.array_data_complete = [];
  }
  idx = 0;
  insertd(hpyear, hpmonth, hpnumwork, hphourot,hphourotn,hpworking) {
    // $("#panel").slideToggle("slow");
    this.idx = this.idx + 1

    this.array_data_complete.push({
      'hpyear': hpyear, 'hpmonth': hpmonth
      ,'hpnumwork': hpnumwork ,'hphourot': hphourot
      ,'hphourotn': hphourotn,'hpworking': hpworking
    })

  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    // $('#md').find("input").val('').end()   
    this.modal.hide();
    this.active = false;
  }
}
