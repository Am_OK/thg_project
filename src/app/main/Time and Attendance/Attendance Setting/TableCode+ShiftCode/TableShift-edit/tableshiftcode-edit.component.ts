import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
// import { moment } from 'ngx-bootstrap/chronos/test/chain';
// import { DbmtabServiceProxy, CreateDbmtabInput } from '@shared/service-proxies/service-proxies';
// import { finalize } from 'rxjs/operators';

@Component({
  selector: 'tableshiftcodeedit',
  templateUrl: './tableshiftcode-edit.component.html',
})
export class TableShiftcodeEditComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  saving: boolean = false;
  // createshiftcode:CreateDbmtabInput = new CreateDbmtabInput(); 


  constructor(
    injector: Injector,
      // private DbmtabServiceProxy : DbmtabServiceProxy,


  ) {
    super(injector);
   
  }
  ngOnInit() {
 
  }
  show(): void {
    
    this.active = true;
    this.modal.show();
  }
  save(){
    // this.createshiftcode.tabtB1 = 'SHIFTCODE'
    // this.DbmtabServiceProxy.createDbmtab(this.createshiftcode)
    // .pipe(finalize(() => { this.saving = false; }))
    // .subscribe(() => {

    //   this.notify.info(this.l('SavedSuccessfully'));
    //   this.modalSave.emit(this.createshiftcode);
    //   this.close()
    // });
    // console.log(this.createshiftcode);
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    // // $('#md').find("input").val('').end()   
    this.modal.hide();
    this.active = false;
  }
}
