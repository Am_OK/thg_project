import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-shiftcode',
  templateUrl: './table-shiftcode.component.html'
})
export class TableshiftcodeComponent extends AppComponentBase implements OnInit {
  // // Dbshiftcode: DbmtabListDto[] = [];

  constructor(
    injector: Injector,
    // private DbmtabServiceProxy : DbmtabServiceProxy,

  ) {
    super(injector);


  }

  ngOnInit() {
  //   this.DbmtabServiceProxy.getDbmtabFromTABFG1('SHIFTCODE','').subscribe((result) => {
  //     this.Dbshiftcode = result.items;
  // });
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
