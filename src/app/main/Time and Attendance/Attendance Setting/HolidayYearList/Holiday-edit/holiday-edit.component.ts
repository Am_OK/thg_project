import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'holidayedit',
    templateUrl: './holiday-edit.component.html'
})
export class HolidayyearEditComponent extends AppComponentBase implements OnInit {
    active: boolean = false;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal')  modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;


    constructor(
        injector: Injector,


    ) {
        super(injector);

    }
    ngOnInit() {
    }
    show(): void {
        this.active = true;
        this.modal.show();
    }
    close(): void {
        // $('#md').find("input").val('').end()
        this.modal.hide();
        this.active = false;
    }
    show2(): void {

        this.modal2.show();
    
    }
    close2(): void {
        this.modal2.hide();
        
    }

}
