import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'level-control',
  templateUrl: './level-control.component.html'
})
export class LevelControlComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,

  ) {
    super(injector);


  }

  ngOnInit() {
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
