import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'levelcontroladd',
  templateUrl: './levelcontrol-add.component.html',
})
export class LevelControlAddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  array_data_complete = [({
    'hdtyp': '', 'hdyear': ''
    , 'hddate': '', 'hddescr': ''
  })];

 
  constructor(
    injector: Injector,
  

  ) {
    super(injector);
   
  }
  ngOnInit() {
 
  }
  show(): void {
    // $("#holidaydate").val(moment().add(543, 'years').format('DD/MM/YYYY'));
    // $("#holidayyear").val(moment().add(543, 'years').format('YYYY'));
    // $("#holidaycode").val(moment().add(543, 'years').format('YYYY'));
    
    this.active = true;
    this.modal.show();
    this.array_data_complete = [];
  }
  idx = 0;
  // insertd(hdtyp, hdyear, hddate, hddescr) {
  //   // $("#panel").slideToggle("slow");
  //   this.idx = this.idx + 1

  //   this.array_data_complete.push({
  //     'hdtyp': hdtyp, 'hdyear': hdyear
  //     , 'hddate': this.fnyyyymmdd(hddate),'hddescr': hddescr
  //   })

  // }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    // // $('#md').find("input").val('').end()   
    this.modal.hide();
    this.active = false;
  }
}
