import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AccDashBoardServiceProxy, CreateAccDashBoardInput, AccDashBoardListDto } from '@shared/service-proxies/service-proxies';
import { subscribeOn, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-dashboardclientipd',
  templateUrl: './dashboardclientipd.component.html',
  styleUrls: ['./dashboardclientipd.component.css'],
  animations: [appModuleAnimation()]
})
export class DashboardclientipdComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,
    private AccDashBoard: AccDashBoardServiceProxy
  ) {
    super(injector);
  }
  show_month: string;
  search
  month = 0
  year = 0
  check = true
  loop_day = []
  loop_month = [
    { month: "OCT", val: 10 },
    { month: "NOV", val: 11 },
    { month: "DEC", val: 12 },
    { month: "JAN", val: 1 },
    { month: "FEB", val: 2 },
    { month: "MAR", val: 3 },
    { month: "APR", val: 4 },
    { month: "MAY", val: 5 },
    { month: "JUN", val: 6 },
    { month: "JUL", val: 7 },
    { month: "AUG", val: 8 },
    { month: "SEP", val: 9 },
  ]

  ngOnInit() {
    for (let index = 1; index < 32; index++) {
      this.loop_day.push({ day: [index] })
    }
    var date = new Date()
    this.search = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + (date.getFullYear() + 543)
    $("#serach").val(this.search)
    this.getall_data(this.search);

  }
  editor: string = 'la-close';
  edit() {
    if (this.check == true) {
      this.check = false
      this.editor = 'la-check';
    } else {
      this.check = true
      this.editor = 'la-close';
    }
  }
  Create: CreateAccDashBoardInput = new CreateAccDashBoardInput();
  AccDashBoardListDto: AccDashBoardListDto[] = [];
  setdate;
  check_data(reporttype, date) {
    this.message.confirm(
      this.l('คุณต้องการบันทึกข้อมูล'),
      this.l('AreYouSure'),
      isConfirmed => {
        if (isConfirmed) {
          this.setdate = date;
          if (this.typereport.filter(data => data.type == reporttype)[0].data == true) {
            this.update(reporttype);
          } else {
            this.add(reporttype);
          }
        }
      }
    );
  }
  add(reporttype) {
    this.Create.reporttyp = reporttype
    this.Create.reporttname = this.typereport.filter(data => data.type == reporttype)[0].name
    this.Create.reportmonth = this.fnyyyymm(this.setdate)
    this.Create.reportuser = this.appSession.user.userName;
    this.Create.keyuP1 = '';
    this.Create.keyuP2 = '';

    this.AccDashBoard.createAccDashBoard(this.Create)
      .pipe(finalize(() => { }))
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.getall_data(this.setdate)
      });
  }
  update(reporttype) {
    for (var prop in this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0]) {
      if (this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0].hasOwnProperty(prop)) {
        if (
          prop == "reporttyp" ||
          prop == "reporttname" ||
          prop == "reportmonth" ||
          prop == "reportuser" ||
          prop == "setup" ||
          prop == "keyuP1" ||
          prop == "keyuP2" ||
          prop == "isDeleted" ||
          prop == "deletionTime" ||
          prop == "lastModificationTime" ||
          prop == "creationTime"
        ) { } else {

         
          this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop] = this.fnreplace(this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop]);
        if(this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop] == undefined)this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop] = 0;
          if (prop == 'id') {
            this.AccDashBoard.updataAccDashBoard(this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0])
              .pipe(finalize(() => { }))
              .subscribe(() => {
                this.notify.info(this.l('Update Success'));
                this.getall_data(this.setdate)
              });
          }
        }
      }
    }

  }
  delete001(reporttype) {

  }
  data_of_chart
  getall_data(data) {
    this.month = data.substring(0, 2);
    this.year = data.substring(3, 7);
    this.show_month = this.loop_month.filter(result => result.val == this.month)[0].month
    this.typereport.forEach(element => {
      element.chart_bar = [];
      element.chart_line_day = [];
      element.chart_line_month = [];
    });
    console.log(this.setuplinechart_day[0].name = this.show_month + " " + (this.year - 1))
    console.log(this.setuplinechart_day[1].name = this.show_month + " " + this.year)
    this.AccDashBoardListDto = []
    this.AccDashBoard.getAccDashBoardLists('All', '', this.fnyyyymm(data), '', '', '', '').subscribe((result) => {
      this.AccDashBoardListDto = result.items;

      this.typereport.forEach(element => {
        if (this.AccDashBoardListDto.filter(data => data.reporttyp == element.type).length > 0) {
          element.data = true;
          element.datadsc = 'อัพเดท';
          this.set_chart_bar(element.type);
          this.set_chart_line_day(element.type);
          this.set_chart_line_month(element.type);
        } else {
          element.data = false;
          element.datadsc = 'เพิ่ม';
        }
      });
    })
  }
  dataset_chart_line = [];
  set_chart_bar(type) {
    var databarchart = [{
      'type': type,
      'name': 'OPD per Day',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opya,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyl
    },
    {
      'type': type,
      'name': 'OPD per Month',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opmp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opma,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opml
    },
    {
      'type': type,
      'name': 'OPD per Year',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyP2,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyA2,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyL2
    },
    {
      'type': type,
      'name': 'Average OPD per Visit',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aopvp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aopva,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aopvl
    }]
    this.typereport.filter(data => data.type == type)[0].chart_bar = databarchart;

  }

  set_chart_line_day(type) {
    var datalinechart = [
      { day: "1", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY01, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY01, },
      { day: "2", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY02, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY02, },
      { day: "3", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY03, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY03, },
      { day: "4", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY04, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY04, },
      { day: "5", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY05, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY05, },
      { day: "6", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY06, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY06, },
      { day: "7", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY07, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY07, },
      { day: "8", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY08, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY08, },
      { day: "9", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY09, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY09, },
      { day: "10", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY10, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY10, },
      { day: "11", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY11, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY11, },
      { day: "12", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY12, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY12, },
      { day: "13", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY13, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY13, },
      { day: "14", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY14, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY14, },
      { day: "15", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY15, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY15, },
      { day: "16", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY16, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY16, },
      { day: "17", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY17, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY17, },
      { day: "18", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY18, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY18, },
      { day: "19", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY19, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY19, },
      { day: "20", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY20, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY20, },
      { day: "21", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY21, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY21, },
      { day: "22", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY22, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY22, },
      { day: "23", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY23, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY23, },
      { day: "24", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY24, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY24, },
      { day: "25", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY25, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY25, },
      { day: "26", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY26, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY26, },
      { day: "27", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY27, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY27, },
      { day: "28", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY28, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY28, },
      { day: "29", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY29, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY29, },
      { day: "30", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY30, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY30, },
      { day: "31", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY31, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY31, },
    ];
    this.typereport.filter(data => data.type == type)[0].chart_line_day = datalinechart;

  }
  set_chart_line_month(type) {

    var datalinechart = [{
      month: "OCT",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].octp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].octl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].octi,
    }, {
      month: "NOV",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].novp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].novl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].novi,
    }, {
      month: "DEC",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].decp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].decl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].deci,
    }, {
      month: "JAN",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].janp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].janl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].jani,
    }, {
      month: "FEB",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].febp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].febl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].febi,
    }, {
      month: "MAR",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].marp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].marl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mari,
    }, {
      month: "APR",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aprp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aprl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].apri,
    }, {
      month: "MAY",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mayp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mayl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mayi,
    }, {
      month: "JUN",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].junp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].junl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].juni,
    }, {
      month: "JUL",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].julp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].jull,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].juli,
    }, {
      month: "AUG",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].augp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].augl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].augi,
    }, {
      month: "SEP",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].sepp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].sepl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].sepi,
    }];
    this.typereport.filter(data => data.type == type)[0].chart_line_month = datalinechart;
  }

  onKey() {
    console.log(this.AccDashBoardListDto);
  }

  setuplinechart_day = [
    { value: "yearnow", name: "" },
    { value: "lastyear", name: "" },
  ];
  setuplinechart_month = [
    { value: "lastyear", name: "Last Year" },
    { value: "plan", name: "Plan" },
    { value: "yearnow", name: "Year Now" },
  ];

  typereport = [
    { type: '001', name: 'เปนียบเทียบผู้รับบริการผู้ป่วยนอก แบบเดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '002', name: 'เปนียบเทียบผู้รับบริการผู้ป่วยนอก แบบปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '003', name: 'OPD PER MUNTH', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '004', name: 'เปรียบเทียบจำนวนคนไข้ OPD', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '005', name: 'OPD PER DAY OF MONTH', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '006', name: 'AVG OPD PER VISIT MONTH', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '007', name: 'เปรียบเทียบผู้รับบริการผู้ป่วยใน แบบเดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '008', name: 'เปรียบเทียบผู้รับบริการผู้ป่วยใน แบบปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '009', name: 'Patient day of mouth', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '010', name: 'avg admiss per day of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '011', name: 'avg IPD per day month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '012', name: 'avg IPD Discharge', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '013', name: 'Occpancy rate of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '014', name: 'รายงานแสดงฐานะการเงิน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '015', name: 'ตารางแสดงผลประกอบด้วนงบดุล', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '016', name: 'ตารางเปรียบเทียบรายได้ แบบเดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '017', name: 'ตารางเปรียบเทียบรายได้ แบบปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '018', name: 'ตารางเปรียบเทียบรายได้ครึ่งปีแรก', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '019', name: 'Compare Rev OPD of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '020', name: 'Compare Rev IPD of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '021', name: 'Compare Rev of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '022', name: 'เปรียบเทียบระยะเวลาเรียกเก็บหนี้ ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '023', name: 'งบกำไรขาดทุนเปรียบเทียบแผน เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '024', name: 'INCOM STATEMENT เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '025', name: 'งบกำไรขาดทุนเปรียบเทียบแผน ไตรมาส', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '026', name: 'งบกำไรขาดทุนครึ่งปีกับแผน ไตรมาส', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '027', name: 'งบกำไรขาดทุนผู้ป่วยนอกและใน เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '028', name: 'งบกำไรขาดทุนผู้ป่วยนอกและใน ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '029', name: 'งบกำไรขาดทุนผู้ป่วยนอก เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '030', name: 'งบกำไรขาดทุนผู้ป่วยนอก ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '031', name: 'งบกำไรขาดทุนผู้ป่วยใน เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] },
    { type: '032', name: 'งบกำไรขาดทุนผู้ป่วยใน ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [] }
  ]

}
