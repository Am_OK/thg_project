import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AccDashBoardServiceProxy, CreateAccDashBoardInput, AccDashBoardListDto } from '@shared/service-proxies/service-proxies';
import { subscribeOn, finalize } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';
import { lookup } from 'dns';

@Component({
  selector: 'app-dashboardbala',
  templateUrl: './dashboardbala.component.html',
  styleUrls: ['./dashboardbala.component.css'],
  animations: [appModuleAnimation()]
})
export class DashboardbalaComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,
    private AccDashBoard: AccDashBoardServiceProxy
  ) {
    super(injector);
  }
  show_month: string;
  show_quarters: string;
  search
  month = 0
  year = 0
  check = true
  loop_day = []
  loop_month = [
    { month: "OCT", val: 10, quarters: "Q1" },
    { month: "NOV", val: 11, quarters: "Q1" },
    { month: "DEC", val: 12, quarters: "Q1" },
    { month: "JAN", val: 1, quarters: "Q2" },
    { month: "FEB", val: 2, quarters: "Q2" },
    { month: "MAR", val: 3, quarters: "Q2" },
    { month: "APR", val: 4, quarters: "Q3" },
    { month: "MAY", val: 5, quarters: "Q3" },
    { month: "JUN", val: 6, quarters: "Q3" },
    { month: "JUL", val: 7, quarters: "Q4" },
    { month: "AUG", val: 8, quarters: "Q4" },
    { month: "SEP", val: 9, quarters: "Q4F" },
  ]

  ngOnInit() {
    for (let index = 1; index < 32; index++) {
      this.loop_day.push({ day: [index] })
    }
    var date = new Date()
    this.search = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + (date.getFullYear() + 543)
    $("#serach").val(this.search)
    this.getall_data(this.search);

    var data = [{ "reporttyp": "014", "reporttname": "รายงานแสดงฐานะการเงิน", "reportmonth": "201906", "reportuser": "wittawat", "setup": "6000", "keyuP1": "", "keyuP2": "", "opyp": 0, "opya": 0, "opyl": 0, "opyvp": 0, "opyvl": 0, "opmp": 0, "opma": 0, "opml": 0, "opmvp": 0, "opmvl": 0, "opyP2": 0, "opyA2": 0, "opyL2": 0, "opyvP2": 0, "opyvL2": 0, "aopvp": 0, "aopva": 0, "aopvl": 0, "aopvvp": 0, "aopvvl": 0, "appdp": 0, "appda": 0, "appdl": 0, "appdvp": 0, "appdvl": 0, "apdp": 0, "apda": 0, "apdl": 0, "apdvp": 0, "apdvl": 0, "arpdp": 0, "arpda": 0, "arpdl": 0, "arpdvp": 0, "arpdvy": 0, "arpdisp": 0, "arpdisa": 0, "arpdisl": 0, "arpdisvp": 0, "arpdisvl": 0, "losp": 0, "losa": 0, "losl": 0, "losvp": 0, "losvl": 0, "orp": 0, "ora": 0, "orl": 0, "orvp": 0, "orvl": 0, "octi": 0, "novi": 0, "deci": 0, "jani": 0, "febi": 0, "mari": 0, "apri": 0, "mayi": 0, "juni": 0, "juli": 0, "augi": 0, "sepi": 0, "octl": 0, "novl": 0, "decl": 0, "janl": 0, "febl": 0, "marl": 0, "aprl": 0, "mayl": 0, "junl": 0, "jull": 0, "augl": 0, "sepl": 0, "octp": 0, "novp": 0, "decp": 0, "janp": 0, "febp": 0, "marp": 0, "aprp": 0, "mayp": 0, "junp": 0, "julp": 0, "augp": 0, "sepp": 0, "avgm": 0, "mcalavG1": 0, "mcalavG2": 0, "mcalavG3": 0, "daY01": 0, "daY02": 0, "daY03": 0, "daY04": 0, "daY05": 0, "daY06": 0, "daY07": 0, "daY08": 0, "daY09": 0, "daY10": 0, "daY11": 0, "daY12": 0, "daY13": 0, "daY14": 0, "daY15": 0, "daY16": 0, "daY17": 0, "daY18": 0, "daY19": 0, "daY20": 0, "daY21": 0, "daY22": 0, "daY23": 0, "daY24": 0, "daY25": 0, "daY26": 0, "daY27": 0, "daY28": 0, "daY29": 0, "daY30": 0, "daY31": 0, "ldaY01": 0, "ldaY02": 0, "ldaY03": 0, "ldaY04": 0, "ldaY05": 0, "ldaY06": 0, "ldaY07": 0, "ldaY08": 0, "ldaY09": 0, "ldaY10": 0, "ldaY11": 0, "ldaY12": 0, "ldaY13": 0, "ldaY14": 0, "ldaY15": 0, "ldaY16": 0, "ldaY17": 0, "ldaY18": 0, "ldaY19": 0, "ldaY20": 0, "ldaY21": 0, "ldaY22": 0, "ldaY23": 0, "ldaY24": 0, "ldaY25": 0, "ldaY26": 0, "ldaY27": 0, "ldaY28": 0, "ldaY29": 0, "ldaY30": 0, "ldaY31": 0, "davgm": 0, "dcalavG1": 0, "dcalavG2": 0, "dcalavG3": 0, "asscurim": 0, "assnoncurim": 0, "liacurim": 0, "lianoncurim": 0, "fundim": 0, "asscuimper": 0, "assnoncurimper": 0, "liacurimper": 0, "lianoncurimper": 0, "fundimper": 0, "asscuriml": 0, "assnoncuriml": 0, "liacuriml": 0, "lianoncuriml": 0, "fundiml": 0, "asscuimperl": 0, "assnoncurimperl": 0, "liacurimperl": 0, "lianoncurimperl": 0, "fundimperl": 0, "asscuadd": 0, "assnoncuradd": 0, "liacuradd": 0, "lianoncuradd": 0, "fundadd": 0, "cropdp": 0, "cropda": 0, "cropdl": 0, "cropdvp": 0, "cropdvl": 0, "cripdp": 0, "cripda": 0, "cripdl": 0, "cripdvp": 0, "cripdvl": 0, "dimp": 0, "dima": 0, "diml": 0, "dimvp": 0, "dimvl": 0, "cpsp": 0, "cpsa": 0, "cpsl": 0, "cpsvp": 0, "cpsvl": 0, "othp": 0, "otha": 0, "othl": 0, "othvp": 0, "othvl": 0, "losscropdp": 0, "losscropdppeR1": 0, "losscropdppeR2": 0, "losscropda": 0, "losscropdapeR1": 0, "losscropdapeR2": 0, "losscropdl": 0, "losscropdlpeR1": 0, "losscropdlpeR2": 0, "losscropdvl": 0, "losscropdvp": 0, "losscripdp": 0, "losscripdppeR1": 0, "losscripdppeR2": 0, "losscripda": 0, "losscripdapeR1": 0, "losscripdapeR2": 0, "losscripdl": 0, "losscripdlpeR1": 0, "losscripdlpeR2": 0, "losscripdvl": 0, "losscripdvp": 0, "lossdimcp": 0, "lossdimcppeR1": 0, "lossdimcppeR2": 0, "lossdimca": 0, "lossdimcapeR1": 0, "lossdimcapeR2": 0, "lossdimcl": 0, "lossdimclpeR1": 0, "lossdimclpeR2": 0, "lossdimcvl": 0, "lossdimcvp": 0, "lossvcp": 0, "lossvcppeR1": 0, "lossvcppeR2": 0, "lossvca": 0, "lossvcapeR1": 0, "lossvcapeR2": 0, "lossvcl": 0, "lossvclpeR1": 0, "lossvclpeR2": 0, "lossvcvl": 0, "lossvcvp": 0, "lossfcp": 0, "lossfcppeR1": 0, "lossfcppeR2": 0, "lossfca": 0, "lossfcapeR1": 0, "lossfcapeR2": 0, "lossfcl": 0, "lossfclpeR1": 0, "lossfclpeR2": 0, "lossfcvl": 0, "lossfcvp": 0, "lossaep": 0, "lossaeppeR1": 0, "lossaeppeR2": 0, "lossaea": 0, "lossaeapeR1": 0, "lossaeapeR2": 0, "lossael": 0, "lossaelpeR1": 0, "lossaelpeR2": 0, "lossaevl": 0, "lossaevp": 0, "lossdpcp": 0, "lossdpcppeR1": 0, "lossdpcppeR2": 0, "lossdpca": 0, "lossdpcapeR1": 0, "lossdpcapeR2": 0, "lossdpcl": 0, "lossdpclpeR1": 0, "lossdpclpeR2": 0, "lossdpcvl": 0, "lossdpcvp": 0, "lossiep": 0, "lossieppeR1": 0, "lossieppeR2": 0, "lossiea": 0, "lossieapeR1": 0, "lossieapeR2": 0, "lossiel": 0, "lossielpeR1": 0, "lossielpeR2": 0, "lossievl": 0, "lossievp": 0, "lossifdp": 0, "lossifdppeR1": 0, "lossifdppeR2": 0, "lossifda": 0, "lossifdapeR1": 0, "lossifdapeR2": 0, "lossifdl": 0, "lossifdlpeR1": 0, "lossifdlpeR2": 0, "lossifdvl": 0, "lossifdvp": 0, "losstcrp": 0, "losstcrppeR1": 0, "losstcrppeR2": 0, "losstcra": 0, "losstcrapeR1": 0, "losstcrapeR2": 0, "losstcrl": 0, "losstcrlpeR1": 0, "losstcrlpeR2": 0, "losstcrvl": 0, "losstcrvp": 0, "lossothp": 0, "lossothpeR1": 0, "lossothppeR2": 0, "lossotha": 0, "lossothapeR1": 0, "lossothapeR2": 0, "lossothl": 0, "lossothpeR12": 0, "lossothpeR22": 0, "lossothvl": 0, "lossothvp": 0, "lossaeP2": 0, "lossaepeR1": 0, "lossaeppeR22": 0, "lossaeA2": 0, "lossaeapeR12": 0, "lossaeapeR22": 0, "lossaeL2": 0, "lossaepeR12": 0, "lossaepeR2": 0, "lossaevL2": 0, "lossaevP2": 0, "fnstsasS01": "500,000", "fnstsasS02": "30", "fnstsasS03": "0", "fnstsasS04": "10", "fnstsasS05": "0", "fnstsasS06": "0", "fnstsasS07": "0", "fnstsasS08": "0", "fnstsasS09": "0", "fnstsasS10": "0", "fnstsasS11": "0", "fnstsasS12": "0", "fnstsasS13": "0", "fnstsasS14": "0", "fnstsasS15": 0, "fnstsasS16": 0, "fnstsasS17": 0, "fnstsasS18": 0, "fnstsdeP01": "69", "fnstsdeP02": "44", "fnstsdeP03": 0, "fnstsdeP04": "0", "fnstsdeP05": "30", "fnstsdeP06": "0", "fnstsdeP07": "40", "fnstsdeP08": "0", "fnstsdeP09": 0, "fnstsdeP10": 0, "fnstsdeP11": 0, "fnstsdeP12": 0, "fnstsdeP13": 0, "fnstsdeP14": 0, "fnstsdeP15": 0, "fnstsdeP16": 0, "fnstsdeP17": "0", "fnstsdeP18": "0", "isDeleted": false, "deleterUserId": null, "lastModificationTime": "2019-06-28T04:01:17.933Z", "lastModifierUserId": 7, "creationTime": "2019-06-25T02:22:27.863Z", "creatorUserId": 3, "id": 95 }];
    // AccDashBoardListDto[0].opya


  }


  editor: string = 'la-close';
  edit() {
    if (this.check == true) {
      this.check = false
      this.editor = 'la-check';
    } else {
      this.check = true
      this.editor = 'la-close';
    }
  }
  Create: CreateAccDashBoardInput = new CreateAccDashBoardInput();
  AccDashBoardListDto: AccDashBoardListDto[] = [];
  setdate;
  check_data(reporttype, date) {

    this.message.confirm(
      this.l('คุณต้องการบันทึกข้อมูล'),
      this.l('AreYouSure'),
      isConfirmed => {
        if (isConfirmed) {
          this.setdate = date;
          if (this.typereport.filter(data => data.type == reporttype)[0].data == true) {
            this.update(reporttype);
          } else {
            this.add(reporttype);
          }
        }
      }
    );
  }
  add(reporttype) {
    // alert('add')
    this.Create.reporttyp = reporttype
    this.Create.reporttname = this.typereport.filter(data => data.type == reporttype)[0].name
    this.Create.reportmonth = this.fnyyyymm(this.setdate)
    this.Create.reportuser = this.appSession.user.userName;
    this.Create.keyuP1 = '';
    this.Create.keyuP2 = '';

    this.AccDashBoard.createAccDashBoard(this.Create)
      .pipe(finalize(() => { }))
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.getall_data(this.setdate)
      });
  }
  update(reporttype) {
    for (var prop in this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0]) {
      if (this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0].hasOwnProperty(prop)) {
        if (
          prop == "reporttyp" ||
          prop == "reporttname" ||
          prop == "reportmonth" ||
          prop == "reportuser" ||
          prop == "setup" ||
          prop == "keyuP1" ||
          prop == "keyuP2" ||
          prop == "isDeleted" ||
          prop == "deletionTime" ||
          prop == "lastModificationTime" ||
          prop == "creationTime"
        ) { } else {

         
          this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop] = this.fnreplace(this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop]);
        if(this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop] == undefined)this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0][prop] = 0;
          if (prop == 'id') {
            this.AccDashBoard.updataAccDashBoard(this.AccDashBoardListDto.filter(data => data.reporttyp == reporttype)[0])
              .pipe(finalize(() => { }))
              .subscribe(() => {
                this.notify.info(this.l('Update Success'));
                this.getall_data(this.setdate)
              });
          }
        }
      }
    }





  }
  delete001(reporttype) {

  }
  data_of_chart
  getall_data(data) {
    this.month = data.substring(0, 2);
    this.year = data.substring(3, 7);
    this.show_quarters = this.loop_month.filter(result => result.val == this.month)[0].quarters
    this.show_month = this.loop_month.filter(result => result.val == this.month)[0].month
    // console.log(this.setuplinechart_day[0].name = this.show_month + " " + (this.year - 1))
    // console.log(this.setuplinechart_day[1].name = this.show_month + " " + this.year)
    this.AccDashBoardListDto = []
    this.AccDashBoard.getAccDashBoardLists('All', '', this.fnyyyymm(data), '', '', '', '').subscribe((result) => {
      this.AccDashBoardListDto = result.items;

      this.typereport.forEach(element => {
        if (this.AccDashBoardListDto.filter(data => data.reporttyp == element.type).length > 0) {
          element.data = true;
          element.datadsc = 'อัพเดท';
          this.set_chart_bar(element.type);
          this.set_chart_line_day(element.type);
          this.set_chart_line_month(element.type);
          this.set_pie_asset01(element.type);
          this.set_pie_asset02(element.type);
        } else {
          element.data = false;
          element.datadsc = 'เพิ่ม';
        }
      });
    })
  }
  dataset_chart_line = [];
  set_chart_bar(type) {
    var databarchart = [{
      'type': type,
      'name': 'OPD per Day',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opya,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyl
    },
    {
      'type': type,
      'name': 'OPD per Month',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opmp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opma,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opml
    },
    {
      'type': type,
      'name': 'OPD per Year',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyP2,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyA2,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].opyL2
    },
    {
      'type': type,
      'name': 'Average OPD per Visit',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aopvp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aopva,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aopvl
    }]
    this.typereport.filter(data => data.type == type)[0].chart_bar = databarchart;

    //chartงบดุล
    var databarchart = [{
      'type': type,
      'name': 'Total Revenue',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].cropdp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].cripdp - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].dimp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].othp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].cropda + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].cripda - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].dima + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].otha,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].cropdl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].cripdl - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].diml + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].othl,
    }]
    this.typereport.filter(data => data.type == type)[0].chart_bar2 = databarchart;

    //chartงบดุล
    var databarchart = [{
      'type': type,
      'name': 'Total Revenue',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdp - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropda + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripda - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossotha,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdl - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothl,
    }, {
      'type': type,
      'name': 'Direct Cost',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfca,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcl,
    }, {
      'type': type,
      'name': 'Gross Profit',
      'p': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdp - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothp) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcp),
      'a': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropda + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripda - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossotha) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfca),
      'l': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdl - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothl) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcl),
    }, {
      'type': type,
      'name': 'Admin Expenses',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaep,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaea,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossael,
    }, {
      'type': type,
      'name': 'EBITDA',
      'p': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdp - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothp) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcp) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaep,
      'a': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropda + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripda - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossotha) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfca) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaea,
      'l': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdl - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothl) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcl) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossael,
    }, {
      'type': type,
      'name': 'Depreciation',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpcp,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpca,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpcl,
    }, {
      'type': type,
      'name': 'EBIT',
      'p': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdp - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothp) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcp) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaep - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpcp,
      'a': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropda + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripda - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossotha) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfca) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaea - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpca,
      'l': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdl - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothl) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcl) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossael - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpcl,
    }, {
      'type': type,
      'name': 'Interest Expenses',
      'p': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossiep,
      'a': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossiea,
      'l': this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossiel,
    }, {
      'type': type,
      'name': 'Net Profit',
      'p': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdp - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothp) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcp + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcp) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaep - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpcp - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossiep + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossifdp,
      'a': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropda + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripda - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossotha) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvca + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfca) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossaea - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpca - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossiea + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossifda,
      'l': (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscropdl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].losscripdl - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdimcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossothl) - (this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossvcl + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossfcl) - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossael - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossdpcl - this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossiel + this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].lossifdl,
    },]
    this.typereport.filter(data => data.type == type)[0].chart_bar3 = databarchart;

  }

  set_chart_line_day(type) {
    var datalinechart = [
      { day: "1", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY01, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY01, },
      { day: "2", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY02, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY02, },
      { day: "3", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY03, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY03, },
      { day: "4", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY04, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY04, },
      { day: "5", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY05, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY05, },
      { day: "6", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY06, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY06, },
      { day: "7", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY07, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY07, },
      { day: "8", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY08, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY08, },
      { day: "9", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY09, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY09, },
      { day: "10", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY10, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY10, },
      { day: "11", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY11, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY11, },
      { day: "12", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY12, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY12, },
      { day: "13", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY13, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY13, },
      { day: "14", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY14, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY14, },
      { day: "15", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY15, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY15, },
      { day: "16", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY16, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY16, },
      { day: "17", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY17, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY17, },
      { day: "18", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY18, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY18, },
      { day: "19", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY19, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY19, },
      { day: "20", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY20, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY20, },
      { day: "21", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY21, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY21, },
      { day: "22", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY22, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY22, },
      { day: "23", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY23, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY23, },
      { day: "24", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY24, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY24, },
      { day: "25", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY25, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY25, },
      { day: "26", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY26, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY26, },
      { day: "27", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY27, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY27, },
      { day: "28", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY28, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY28, },
      { day: "29", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY29, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY29, },
      { day: "30", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY30, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY30, },
      { day: "31", yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].daY31, lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].ldaY31, },
    ];
    this.typereport.filter(data => data.type == type)[0].chart_line_day = datalinechart;

  }
  set_chart_line_month(type) {

    var datalinechart = [{
      month: "OCT",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].octp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].octl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].octi,
    }, {
      month: "NOV",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].novp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].novl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].novi,
    }, {
      month: "DEC",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].decp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].decl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].deci,
    }, {
      month: "JAN",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].janp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].janl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].jani,
    }, {
      month: "FEB",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].febp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].febl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].febi,
    }, {
      month: "MAR",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].marp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].marl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mari,
    }, {
      month: "APR",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aprp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].aprl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].apri,
    }, {
      month: "MAY",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mayp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mayl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].mayi,
    }, {
      month: "JUN",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].junp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].junl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].juni,
    }, {
      month: "JUL",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].julp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].jull,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].juli,
    }, {
      month: "AUG",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].augp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].augl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].augi,
    }, {
      month: "SEP",
      plan: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].sepp,
      lastyear: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].sepl,
      yearnow: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].sepi,
    }];
    this.typereport.filter(data => data.type == type)[0].chart_line_month = datalinechart;
  }

  set_pie_asset01(type) {

    var datapiechart = [{
      name: "เงินสด",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS01 * 5,
    }, {
      name: "เงินฝากธนาคาร",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS02 * 5,
    }, {
      name: "ลูกหนี้",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS03 * 5,
    }, {
      name: "สินค้าคงเหลือ",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS04 * 5,
    }, {
      name: "สินทรัพย์หมุนเวียนอื่น",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS05 * 5,
    }, {
      name: "อาคารและสิ่งปลูกสร้าง-สุทธิ",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS06 * 5,
    }, {
      name: "งานระหว่างก่อสร้าง",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS07 * 5,
    }, {
      name: "งานระหว่างทำ",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS08 * 5,
    }, {
      name: "งานระบบสาธารณูปโภค",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS09 * 5,
    }, {
      name: "ครุภัณฑ์-สุทธิ",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS10 * 5,
    }, {
      name: "สินทรัพย์จากบริจาค",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS11 * 5,
    }, {
      name: "สินทรัพย์ไม่ม่ตัวตน",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS12 * 5,
    }, {
      name: "เงินมัดจำจ่าย",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS13 * 5,
    }, {
      name: "ค่าใช้จ่ายรอตัดบัญชี",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsasS14 * 5,
    }];
    this.typereport.filter(data => data.type == type)[0].chart_pie1 = datapiechart;
  }

  set_pie_asset02(type) {

    var datapiechart = [{
      name: "เจ้าหนี้",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsdeP01 * 5,
    }, {
      name: "หนี้สินหมุนเวียนอื่น",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsdeP02 * 5,
    }, {
      name: "เจ้าหนี้ระยะยาว",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsdeP03 * 5,
    }, {
      name: "รายได้จากการรับบริจาค",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsdeP04 * 5,
    }, {
      name: "ทุนประเดิม",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsdeP05 * 5,
    }, {
      name: "ขาดทุนสะสม-สุทธิ",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsdeP06 * 5,
    }, {
      name: "รายได้สูง/(ต่ำกว่า)ค่าใช้จ่าย",
      value: this.AccDashBoardListDto.filter(data => data.reporttyp == type)[0].fnstsdeP07 * 5,
    }];
    this.typereport.filter(data => data.type == type)[0].chart_pie2 = datapiechart;
  }
  customizeLabel(point) {
    return (point.valueText / 5) + "% :" + point.argumentText;
  }

  onKey() {
    // console.log(this.AccDashBoardListDto);
  }

  setuplinechart_day = [
    { value: "yearnow", name: "" },
    { value: "lastyear", name: "" },
  ];
  setuplinechart_month = [
    { value: "lastyear", name: "Last Year" },
    { value: "plan", name: "Plan" },
    { value: "yearnow", name: "Year Now" },
  ];

  typereport = [
    { type: '001', name: 'เปนียบเทียบผู้รับบริการผู้ป่วยนอก แบบเดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '002', name: 'เปนียบเทียบผู้รับบริการผู้ป่วยนอก แบบปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '003', name: 'OPD PER MUNTH', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '004', name: 'เปรียบเทียบจำนวนคนไข้ OPD', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '005', name: 'OPD PER DAY OF MONTH', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '006', name: 'AVG OPD PER VISIT MONTH', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '007', name: 'เปรียบเทียบผู้รับบริการผู้ป่วยใน แบบเดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '008', name: 'เปรียบเทียบผู้รับบริการผู้ป่วยใน แบบปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '009', name: 'Patient day of mouth', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '010', name: 'avg admiss per day of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '011', name: 'avg IPD per day month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '012', name: 'avg IPD Discharge', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '013', name: 'Occpancy rate of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '014', name: 'รายงานแสดงฐานะการเงิน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '015', name: 'ตารางแสดงผลประกอบด้วนงบดุล', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '016', name: 'ตารางเปรียบเทียบรายได้ แบบเดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '017', name: 'ตารางเปรียบเทียบรายได้ แบบปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '018', name: 'ตารางเปรียบเทียบรายได้ครึ่งปีแรก', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '019', name: 'Compare Rev OPD of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '020', name: 'Compare Rev IPD of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '021', name: 'Compare Rev of month', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '022', name: 'เปรียบเทียบระยะเวลาเรียกเก็บหนี้ ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '023', name: 'งบกำไรขาดทุนเปรียบเทียบแผน เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '024', name: 'INCOM STATEMENT เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '025', name: 'งบกำไรขาดทุนเปรียบเทียบแผน ไตรมาส', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '026', name: 'งบกำไรขาดทุนครึ่งปีกับแผน ไตรมาส', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '027', name: 'งบกำไรขาดทุนผู้ป่วยนอกและใน เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '028', name: 'งบกำไรขาดทุนผู้ป่วยนอกและใน ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '029', name: 'งบกำไรขาดทุนผู้ป่วยนอก เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '030', name: 'งบกำไรขาดทุนผู้ป่วยนอก ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '031', name: 'งบกำไรขาดทุนผู้ป่วยใน เดือน', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] },
    { type: '032', name: 'งบกำไรขาดทุนผู้ป่วยใน ปี', data: false, datadsc: 'เพิ่ม', chart_line_day: [], chart_bar: [], chart_line_month: [], chart_bar2: [], chart_bar3: [], chart_pie1: [], chart_pie2: [] }
  ]

}
