import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  //selector: 'app-dashboardtest',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
// export class DashboardComponent extends AppComponentBase implements AfterViewInit
export class DashboardComponent extends AppComponentBase implements OnInit {
  sum_debit_asset: any;
  sum_debit_asset_loop: any;
  analy_alldata: any[];
  res: number;
  income: { state: string; firstyear: number; lastyear: number; }[];
  sum_income: any;
  pay: { state: string; firstyear: number; lastyear: number; }[];
  sum_pay: any;
  debit_asset: { state: string; firstyear: number; lastyear: number; firstyearpersen: number; lastyearpersen: number; }[];
  debit_asset_loop: { state: string; firstyear: number; lastyear: number; firstyearpersen: number; lastyearpersen: number; }[];
  EnergyDescription;
  CountryInfo;
  GrossProduct; data_s;
  data_opd: { title: string; val: number; }[];
  data_ipd: { title: string; val: number; }[];
  netprofit: any;
  spider_map_netprofit = [];
  expemses: any;
  spider_map_expemses = [];
  compare_opd: { title: string; last: number; get: number; budget: number; }[];
  compare_ipd: { title: string; last: number; get: number; budget: number; }[];
  compare_total: { title: string; last: number; get: number; budget: number; }[];
  EnergyIPD: { value: string; name: string; }[];
  EnergyTotal: { value: string; name: string; }[];
  EnergyOPD: { value: string; name: string; }[];
  debtor_list: { country: string; GCD: number; Security: number; insurance: number; Insurance_Company: number; Counterparty: number; Family: number; other: number; }[];
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }
  state: string;
  firstyear: number;
  lastyear: number;
  year2004: number;

  asset;
  asset_loop;
  asset_input;

  sum_asset;
  sum_asset_loop
  customizeTooltip(arg) {
    return {
      text: arg.valueText
    }
  }
  model: any = {};
  onSubmit() {
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.model))
  }
  pointClick(e: any) {
    alert(e.target.data.title);

  }
  main1;
  main2;
  resing;
  test() {
    this.resing = parseInt(this.main1) + parseInt(this.main2);
  }

  highAverage = 38001;
  lowAverage = 27022;

  Plan_opd = 37732;
  Plan_ipd = 26109;
  chk_compare = 1;
  chk_compare_f(data) {
    this.chk_compare = data;
  }
  ngOnInit() {

    // $(".form_datetime1").datetimepicker({ format: 'MM/YYYY' });
    this.data_mont_10();

    this.EnergyOPD = [
      { value: "last", name: "OPD18" },
      { value: "get", name: "OPD19" },
      { value: "budget", name: "Plan" }
    ];
    this.EnergyIPD = [
      { value: "last", name: "IPD18" },
      { value: "get", name: "IPD19" },
      { value: "budget", name: "Plan" }
    ];
    this.EnergyTotal = [
      { value: "last", name: "2018" },
      { value: "get", name: "2019" },
      { value: "budget", name: "Plan" }
    ];


    this.compare_opd = [{
      title: "OCT",
      last: 28413,
      get: 33499,
      budget: 37732
    }, {
      title: "NOV",
      last: 27391,
      get: 31114,
      budget: 35635
    }, {
      title: "DEC",
      last: 25053,
      get: 0,
      budget: 34868
    }, {
      title: "JAN",
      last: 28201,
      get: 0,
      budget: 36747
    }, {
      title: "FEB",
      last: 25714,
      get: 0,
      budget: 34961
    }, {
      title: "MAR",
      last: 29354,
      get: 0,
      budget: 36944
    }, {
      title: "APR",
      last: 27617,
      get: 0,
      budget: 35560
    }, {
      title: "MAY",
      last: 33181,
      get: 0,
      budget: 39789
    }, {
      title: "JUN",
      last: 29806,
      get: 0,
      budget: 41546
    }, {
      title: "JUL",
      last: 29886,
      get: 0,
      budget: 46191
    }, {
      title: "AUG",
      last: 33025,
      get: 0,
      budget: 41019
    }, {
      title: "SEP",
      last: 29805,
      get: 0,
      budget: 38466
    }];

    this.compare_ipd = [{
      title: "OCT",
      last: 23648,
      get: 24738,
      budget: 26109
    }, {
      title: "NOV",
      last: 19357,
      get: 19699,
      budget: 25267
    }, {
      title: "DEC",
      last: 16489,
      get: 0,
      budget: 23806
    }, {
      title: "JAN",
      last: 17355,
      get: 0,
      budget: 23134
    }, {
      title: "FEB",
      last: 15872,
      get: 0,
      budget: 23273
    }, {
      title: "MAR",
      last: 16180,
      get: 0,
      budget: 24872
    }, {
      title: "APR",
      last: 14285,
      get: 0,
      budget: 23037
    }, {
      title: "MAY",
      last: 17493,
      get: 0,
      budget: 24702
    }, {
      title: "JUN",
      last: 17407,
      get: 0,
      budget: 24937
    }, {
      title: "JUL",
      last: 18148,
      get: 0,
      budget: 25263
    }, {
      title: "AUG",
      last: 18651,
      get: 0,
      budget: 35058
    }, {
      title: "SEP",
      last: 20220,
      get: 0,
      budget: 35606
    }];

    this.compare_total = [{
      title: "OCT",
      last: 63841,
      get: 58237,
      budget: 52061
    }, {
      title: "NOV",
      last: 60902,
      get: 50470,
      budget: 47090
    }, {
      title: "DEC",
      last: 58674,
      get: 0,
      budget: 41642
    }, {
      title: "JAN",
      last: 59881,
      get: 0,
      budget: 45556
    }, {
      title: "FEB",
      last: 58234,
      get: 0,
      budget: 41613
    }, {
      title: "MAR",
      last: 61816,
      get: 0,
      budget: 45534
    }, {
      title: "APR",
      last: 58597,
      get: 0,
      budget: 41902
    }, {
      title: "MAY",
      last: 61816,
      get: 0,
      budget: 45534
    }, {
      title: "JUN",
      last: 66483,
      get: 0,
      budget: 47954
    }, {
      title: "JUL",
      last: 71454,
      get: 0,
      budget: 47293
    }, {
      title: "AUG",
      last: 76077,
      get: 0,
      budget: 51676
    }, {
      title: "SEP",
      last: 74072,
      get: 0,
      budget: 50025
    }];

    this.netprofit = [{
      title: "OPD",
      plan: 37732,
      actual: 33578,
      lastyear: 28665
    }, {
      title: "IPD",
      plan: 26109,
      actual: 24735,
      lastyear: 23641
    }, {
      title: "Diff",
      plan: 6600,
      actual: 2869,
      lastyear: 8884
    }, {
      title: "Other Income",
      plan: 2438,
      actual: 291,
      lastyear: 217
    }];

    this.expemses = [{
      title: "Direct Costs",
      plan: 46.727,
      actual: 43.422,
      lastyear: 45.008
    }, {
      title: "Admin Expenses",
      plan: 15.324,
      actual: 8.788,
      lastyear: 9.328
    }, {
      title: "Depreciation",
      plan: 4.321,
      actual: 3.508,
      lastyear: 3.491
    }, {
      title: "Interest",
      plan: 0.850,
      actual: 0.712,
      lastyear: 0.568
    }];

    this.data_opd = [{
      title: "Actual",
      val: 33579
    }, {
      title: "Last Year",
      val: 28665
    }, {
      title: "Plan",
      val: 37732
    }];
    this.data_ipd = [{
      title: "Actual",
      val: 24735
    }, {
      title: "Last Year",
      val: 23641
    }, {
      title: "Plan",
      val: 26109
    }];


    this.netprofit.forEach(element => {
      var data = {
        arg: element.title,
        actual: (element.actual * 100) / element.plan,
        lastyear: (element.lastyear * 100) / element.plan
      }
      this.spider_map_netprofit.push(data);
    });

    this.expemses.forEach(element => {
      var data = {
        arg: element.title,
        actual: (element.actual * 100) / element.plan,
        lastyear: (element.lastyear * 100) / element.plan
      }
      this.spider_map_expemses.push(data);
    });
    console.log(this.spider_map);

    this.debtor_list = [{
      country: "2018",
      GCD: 71,
      Security: 6,
      insurance: 10,
      Insurance_Company: 6,
      Counterparty: 3,
      Family: 1,
      other: 1
    }, {
      country: "2019",
      GCD: 79,
      Security: 2,
      insurance: 5,
      Insurance_Company: 6,
      Counterparty: 3,
      Family: 3,
      other: 1
    }];



  }
  spider_map = [];

  customizePoint = (arg: any) => {

    if (arg.argument == "Actual") {
      return { color: "#ff7c7c", hoverStyle: { color: "#ff7c7c" } };
    } else if (arg.argument == "Last Year") {
      return { color: "#8c8cff", hoverStyle: { color: "#8c8cff" } };
    } else if (arg.argument == "Plan") {
      return { color: "#8c8cff", hoverStyle: { color: "#8c8cfc" } };
    }
  }

  change_reload_data_main(val_data) {
    if (val_data == '10') this.data_mont_10();
    if (val_data == '11') this.data_mont_11();
  }

  sum(items, field) {
    return items.reduce(function (a, b) {
      return a + b[field];
    }, 0);
  }

  customizeLabel(arg) {
    return arg.valueText + " (" + arg.percentText + ")";
  }



  data_mont_10() {
    this.asset = [{
      state: "เงินสด",
      firstyear: 240720,
      lastyear: 248978,
      firstyearpersen: 0.01,
      lastyearpersen: 0.01
    }, {
      state: "เงินฝากธนาคาร",
      firstyear: 38655842.86,
      lastyear: 19025728.68,
      firstyearpersen: 1.16,
      lastyearpersen: 0.74

    }, {
      state: "ลูกหนี้และเงินยืมทดรองจ่าย",
      firstyear: 58321583.92,
      lastyear: 88243981.11,
      firstyearpersen: 1.76,
      lastyearpersen: 3.42
    }, {
      state: "สินค้าคงเหลือ",
      firstyear: 38087373.25,
      lastyear: 35712330.89,
      firstyearpersen: 1.15,
      lastyearpersen: 1.38
    }, {
      state: "สินทรัพย์หมุนเวียนอื่น",
      firstyear: 51180474.71,
      lastyear: 24056983.65,
      firstyearpersen: 1.54,
      lastyearpersen: 0.93
    }];



    this.asset_loop = [{
      state: "อาคารและสิ่งปลูกสร้าง -สุทธิ",
      firstyear: 839779476.35,
      lastyear: 856761162.9,
      firstyearpersen: 25.29,
      lastyearpersen: 33.19
    }, {
      state: "งานระหว่างก่อสร้าง - สุทธิ",
      firstyear: 2134396541.42,
      lastyear: 1372687489.29,
      firstyearpersen: 64.27,
      lastyearpersen: 53.18
    }, {
      state: "  งานระหว่างทำ - ระบบคอมพิวเตอร์",
      firstyear: 298500,
      lastyear: 0,
      firstyearpersen: 0.01,
      lastyearpersen: 0.00
    }, {
      state: "ระบบสาธารณูปโภค - สุทธิ",
      firstyear: 13451865.37,
      lastyear: 14529153.9,
      firstyearpersen: 0.41,
      lastyearpersen: 0.56
    }, {
      state: "ครุภัณฑ์ - สุทธิ",
      firstyear: 60633344.06,
      lastyear: 67415490.24,
      firstyearpersen: 1.83,
      lastyearpersen: 2.61
    }
      , {
      state: " สินทรัพย์จากการบริจาค - สุทธิ",
      firstyear: 15708932.71,
      lastyear: 17163327.19,
      firstyearpersen: 0.47,
      lastyearpersen: 0.00
    }
      , {
      state: "สินทรัพย์ที่ไม่มีตัวตน - สุทธิ",
      firstyear: 39753123.58,
      lastyear: 45009907.52,
      firstyearpersen: 1.20,
      lastyearpersen: 0.00
    }
      , {
      state: " เงินมัดจำจ่าย",
      firstyear: 165500,
      lastyear: 165500,
      firstyearpersen: 0.00,
      lastyearpersen: 0.01
    }

      , {
      state: "ค่าใช้จ่ายรอตัดบัญชี",
      firstyear: 30489311.77,
      lastyear: 40049012.83,
      firstyearpersen: 0.92,
      lastyearpersen: 0.00
    }];


    this.debit_asset = [{
      state: "เจ้าหนี้ ",
      firstyear: 181606556.87,
      lastyear: 255622202.64,
      firstyearpersen: 5.47,
      lastyearpersen: 9.90
    }, {
      state: "หนี้สินหมุนเวียนอื่น",
      firstyear: 22673226.53,
      lastyear: 21775894.56,
      firstyearpersen: 0.77,
      lastyearpersen: 0.84
    }];

    this.debit_asset_loop = [{
      state: " เจ้าหนี้ระยะยาว ",
      firstyear: 1298512907.63,
      lastyear: 1053621195.12,
      firstyearpersen: 39.10,
      lastyearpersen: 40.82
    }, {
      state: "รายได้จากการรับบริจาครอการรับรู้",
      firstyear: 61704961.25,
      lastyear: 38441126.06,
      firstyearpersen: 1.86,
      lastyearpersen: 1.49
    },
    {
      state: " เงินประกันสัญญา",
      firstyear: 266618,
      lastyear: 166117.5,
      firstyearpersen: 0.01,
      lastyearpersen: 0.00
    },
    {
      state: "เงินมัดจำรับ",
      firstyear: 0,
      lastyear: 0,
      firstyearpersen: 0.00,
      lastyearpersen: 0.00
    }];

    this.income = [{
      state: " รายได้จากค่าบริการการรักษาพยาบาล ",
      firstyear: 58313858.5,
      lastyear: 52305547.61
    }, {
      state: "รายได้เงินอุดหนุนงบส่งเสริมสุขภาพจากมหาวิทยาลัย",
      firstyear: 14500,
      lastyear: 0
    },
    {
      state: " รายได้เงินอุดหนุนโครงการ สปสช.",
      firstyear: 0,
      lastyear: 0
    },
    {
      state: "รายได้อื่น",
      firstyear: 597994.6,
      lastyear: 338504.9
    }];

    this.pay = [{
      state: " เงินเดือนและค่าจ้าง ",
      firstyear: 14560485.51,
      lastyear: 16027505.82
    }, {
      state: "ค่าสวัสดิการ",
      firstyear: 1353244.94,
      lastyear: 1021741
    },
    {
      state: "ค่าตอบแทน",
      firstyear: 8009800.25,
      lastyear: 8280908.5
    },
    {
      state: "ค่าใช้สอย",
      firstyear: 4349409.61,
      lastyear: 4559095.83
    }, {
      state: "ค่าวัสดุ",
      firstyear: 19663502.75,
      lastyear: 21695587.36
    }, {
      state: "ค่าสาธารณูปโภค",
      firstyear: 2075004.24,
      lastyear: 1885138.44
    }, {
      state: "ค่าเสื่อมราคา",
      firstyear: 3508240.6,
      lastyear: 3491259.36
    }, {
      state: "ค่าใช้จ่ายอื่น",
      firstyear: 3964779.08,
      lastyear: 10318351.77
    }];


    this.sum_income = this.sum(this.income, 'firstyear');
    this.sum_pay = this.sum(this.pay, 'firstyear');

    this.sum_debit_asset = this.sum(this.debit_asset, 'firstyear');
    this.sum_debit_asset_loop = this.sum(this.debit_asset_loop, 'firstyear');

    this.sum_asset = this.sum(this.asset, 'firstyear');
    this.sum_asset_loop = this.sum(this.asset_loop, 'firstyear');

    this.res = (this.sum_asset + this.sum_asset_loop) - (this.sum_debit_asset + this.sum_debit_asset_loop);

    this.analy_alldata = [{
      title: "สินทรัพย์หมุนเวียน",
      amt: this.sum_asset
    }, {
      title: "สินทรัพย์ไม่หมุนเวียน",
      amt: this.sum_asset_loop
    },
    {
      title: "หนี้สินหมุนเวียน",
      amt: this.sum_debit_asset
    },
    {
      title: "หนี้สินไม่หมุนเวียน",
      amt: this.sum_debit_asset_loop
    }];
  }
  data_mont_11() {
    this.asset = [{
      state: "เงินสด",
      firstyear: 179715,
      lastyear: 156285,
      firstyearpersen: 0.01,
      lastyearpersen: 0.01
    }, {
      state: "เงินฝากธนาคาร",
      firstyear: 57984279.72,
      lastyear: 27138468.69,
      firstyearpersen: 1.74,
      lastyearpersen: 1.01

    }, {
      state: "ลูกหนี้และเงินยืมทดรองจ่าย",
      firstyear: 48119213.85,
      lastyear: 65971869.84,
      firstyearpersen: 1.45,
      lastyearpersen: 2.45
    }, {
      state: "สินค้าคงเหลือ",
      firstyear: 40027332.5,
      lastyear: 38844797.13,
      firstyearpersen: 1.20,
      lastyearpersen: 1.44
    }, {
      state: "สินทรัพย์หมุนเวียนอื่น",
      firstyear: 52221719.5,
      lastyear: 24984773.37,
      firstyearpersen: 1.57,
      lastyearpersen: 0.93
    }];



    this.asset_loop = [{
      state: "อาคารและสิ่งปลูกสร้าง -สุทธิ",
      firstyear: 838236468.95,
      lastyear: 855212797.64,
      firstyearpersen: 25.18,
      lastyearpersen: 31.71
    }, {
      state: "งานระหว่างก่อสร้าง - สุทธิ",
      firstyear: 2134520951.42,
      lastyear: 1501358703.85,
      firstyearpersen: 64.12,
      lastyearpersen: 55.67
    }, {
      state: "  งานระหว่างทำ - ระบบคอมพิวเตอร์",
      firstyear: 298500,
      lastyear: 0,
      firstyearpersen: 0.01,
      lastyearpersen: 0.00
    }, {
      state: "ระบบสาธารณูปโภค - สุทธิ",
      firstyear: 13227204.77,
      lastyear: 14317400.44,
      firstyearpersen: 0.40,
      lastyearpersen: 0.53
    }, {
      state: "ครุภัณฑ์ - สุทธิ",
      firstyear: 59666505.92,
      lastyear: 66630050.8,
      firstyearpersen: 1.79,
      lastyearpersen: 2.47
    }
      , {
      state: " สินทรัพย์จากการบริจาค - สุทธิ",
      firstyear: 15587733.17,
      lastyear: 17042127.65,
      firstyearpersen: 0.47,
      lastyearpersen: 0.00
    }
      , {
      state: "สินทรัพย์ที่ไม่มีตัวตน - สุทธิ",
      firstyear: 39229773.07,
      lastyear: 44494784.84,
      firstyearpersen: 1.18,
      lastyearpersen: 0.00
    }
      , {
      state: " เงินมัดจำจ่าย",
      firstyear: 165500,
      lastyear: 165500,
      firstyearpersen: 0.00,
      lastyearpersen: 0.01
    }

      , {
      state: "ค่าใช้จ่ายรอตัดบัญชี",
      firstyear: 29491335.69,
      lastyear: 40402446.95,
      firstyearpersen: 0.89,
      lastyearpersen: 0.00
    }];


    this.debit_asset = [{
      state: "เจ้าหนี้ ",
      firstyear: 198734378.82,
      lastyear: 120210758.35,
      firstyearpersen: 5.97,
      lastyearpersen: 4.46
    }, {
      state: "หนี้สินหมุนเวียนอื่น",
      firstyear: 24722847.6,
      lastyear: 28115662.92,
      firstyearpersen: 0.74,
      lastyearpersen: 1.04
    }];

    this.debit_asset_loop = [{
      state: " เจ้าหนี้ระยะยาว ",
      firstyear: 1298512907.63,
      lastyear: 1203505176.3,
      firstyearpersen: 39.01,
      lastyearpersen: 44.63
    }, {
      state: "รายได้จากการรับบริจาครอการรับรู้",
      firstyear: 63733797.11,
      lastyear: 38942358.77,
      firstyearpersen: 1.91,
      lastyearpersen: 1.44
    },
    {
      state: " เงินประกันสัญญา",
      firstyear: 323668,
      lastyear: 185385.5,
      firstyearpersen: 0.01,
      lastyearpersen: 0.00
    },
    {
      state: "เงินมัดจำรับ",
      firstyear: 0,
      lastyear: 0,
      firstyearpersen: 0.00,
      lastyearpersen: 0.00
    }];

    this.income = [{
      state: " รายได้จากค่าบริการการรักษาพยาบาล ",
      firstyear: 50508498.93,
      lastyear: 47369518.2
    }, {
      state: "รายได้เงินอุดหนุนงบส่งเสริมสุขภาพจากมหาวิทยาลัย",
      firstyear: 0,
      lastyear: 61200
    },
    {
      state: " รายได้เงินอุดหนุนโครงการ สปสช.",
      firstyear: 0,
      lastyear: 0
    },
    {
      state: "รายได้อื่น",
      firstyear: 473364.45,
      lastyear: 278953.31
    }];

    this.pay = [{
      state: " เงินเดือนและค่าจ้าง ",
      firstyear: 15364930.34,
      lastyear: 16172714.16
    }, {
      state: "ค่าสวัสดิการ",
      firstyear: 1508074.65,
      lastyear: 1022096.79
    },
    {
      state: "ค่าตอบแทน",
      firstyear: 7173866.5,
      lastyear: 7101515.75
    },
    {
      state: "ค่าใช้สอย",
      firstyear: 4520952.6,
      lastyear: 4157965.02
    }, {
      state: "ค่าวัสดุ",
      firstyear: 21562126.04,
      lastyear: 19211437.71
    }, {
      state: "ค่าสาธารณูปโภค",
      firstyear: 2020436.96,
      lastyear: 1614004.73
    }, {
      state: "ค่าเสื่อมราคา",
      firstyear: 3627457.49,
      lastyear: 3493080.38
    }, {
      state: "ค่าใช้จ่ายอื่น",
      firstyear: 5858159.21,
      lastyear: 12259552.93
    }];


    this.sum_income = this.sum(this.income, 'firstyear');
    this.sum_pay = this.sum(this.pay, 'firstyear');

    this.sum_debit_asset = this.sum(this.debit_asset, 'firstyear');
    this.sum_debit_asset_loop = this.sum(this.debit_asset_loop, 'firstyear');

    this.sum_asset = this.sum(this.asset, 'firstyear');
    this.sum_asset_loop = this.sum(this.asset_loop, 'firstyear');

    this.res = (this.sum_asset + this.sum_asset_loop) - (this.sum_debit_asset + this.sum_debit_asset_loop);

    this.analy_alldata = [{
      title: "สินทรัพย์หมุนเวียน",
      amt: this.sum_asset
    }, {
      title: "สินทรัพย์ไม่หมุนเวียน",
      amt: this.sum_asset_loop
    },
    {
      title: "หนี้สินหมุนเวียน",
      amt: this.sum_debit_asset
    },
    {
      title: "หนี้สินไม่หมุนเวียน",
      amt: this.sum_debit_asset_loop
    }];
  }


}

// import { AfterViewInit, Component, Injector, ViewEncapsulation } from '@angular/core';
// import { AppSalesSummaryDatePeriod } from '@shared/AppEnums';
// import { appModuleAnimation } from '@shared/animations/routerTransition';
// import { AppComponentBase } from '@shared/common/app-component-base';
// import { TenantDashboardServiceProxy } from '@shared/service-proxies/service-proxies';
// declare let d3, Datamap: any;

// @Component({
//     templateUrl: './dashboard.component.html',
//     styleUrls: ['./dashboard.component.less'],
//     encapsulation: ViewEncapsulation.None,
//     animations: [appModuleAnimation()]
// })
// export class DashboardComponent extends AppComponentBase implements AfterViewInit {

//     appSalesSummaryDateInterval = AppSalesSummaryDatePeriod;
//     selectedSalesSummaryDatePeriod: any = AppSalesSummaryDatePeriod.Daily;
//     dashboardHeaderStats: DashboardHeaderStats;
//     salesSummaryChart: SalesSummaryChart;
//     regionalStatsTable: RegionalStatsTable;
//     generalStatsPieChart: GeneralStatsPieChart;
//     dailySalesLineChart: DailySalesLineChart;
//     profitSharePieChart: ProfitSharePieChart;
//     memberActivityTable: MemberActivityTable;


//     constructor(
//         injector: Injector,
//         private _dashboardService: TenantDashboardServiceProxy
//     ) {
//         super(injector);
//         this.dashboardHeaderStats = new DashboardHeaderStats();
//         this.salesSummaryChart = new SalesSummaryChart(this._dashboardService, 'salesStatistics');
//         this.regionalStatsTable = new RegionalStatsTable(this._dashboardService);
//         this.generalStatsPieChart = new GeneralStatsPieChart(this._dashboardService);
//         this.dailySalesLineChart = new DailySalesLineChart(this._dashboardService, '#m_chart_daily_sales');
//         this.profitSharePieChart = new ProfitSharePieChart(this._dashboardService, '#m_chart_profit_share');
//         this.memberActivityTable = new MemberActivityTable(this._dashboardService);
//     }

//     getDashboardStatisticsData(datePeriod): void {
//         this.salesSummaryChart.showLoading();
//         this.generalStatsPieChart.showLoading();

//         this._dashboardService
//             .getDashboardData(datePeriod)
//             .subscribe(result => {
//                 this.dashboardHeaderStats.init(result.totalProfit, result.newFeedbacks, result.newOrders, result.newUsers);
//                 this.generalStatsPieChart.init(result.transactionPercent, result.newVisitPercent, result.bouncePercent);
//                 this.dailySalesLineChart.init(result.dailySales);
//                 this.profitSharePieChart.init(result.profitShares);
//                 this.salesSummaryChart.init(result.salesSummary, result.totalSales, result.revenue, result.expenses, result.growth);
//             });
//     }

//     ngAfterViewInit(): void {
//         this.getDashboardStatisticsData(AppSalesSummaryDatePeriod.Daily);
//         this.regionalStatsTable.init();
//         this.memberActivityTable.init();
//     }
// }


// abstract class DashboardChartBase {
//     loading = true;

//     showLoading() {
//         setTimeout(() => { this.loading = true; });
//     }

//     hideLoading() {
//         setTimeout(() => { this.loading = false; });
//     }
// }

// class SalesSummaryChart extends DashboardChartBase {
//     //Sales summary => MorrisJs: https://github.com/morrisjs/morris.js/

//     instance: morris.GridChart;
//     totalSales = 0; totalSalesCounter = 0;
//     revenue = 0; revenuesCounter = 0;
//     expenses = 0; expensesCounter = 0;
//     growth = 0; growthCounter = 0;

//     constructor(private _dashboardService: TenantDashboardServiceProxy, private _containerElement: any) {
//         super();
//     }

//     init(salesSummaryData, totalSales, revenue, expenses, growth) {
//         this.instance = Morris.Area({
//             element: this._containerElement,
//             padding: 0,
//             behaveLikeLine: false,
//             gridEnabled: false,
//             gridLineColor: 'transparent',
//             axes: false,
//             fillOpacity: 1,
//             data: salesSummaryData,
//             lineColors: ['#399a8c', '#92e9dc'],
//             xkey: 'period',
//             ykeys: ['sales', 'profit'],
//             labels: ['Sales', 'Profit'],
//             pointSize: 0,
//             lineWidth: 0,
//             hideHover: 'auto',
//             resize: true
//         });

//         this.totalSales = totalSales;
//         this.totalSalesCounter = totalSales;

//         this.revenue = revenue;
//         this.expenses = expenses;
//         this.growth = growth;

//         this.hideLoading();
//     }

//     reload(datePeriod) {
//         this.showLoading();
//         this._dashboardService
//             .getSalesSummary(datePeriod)
//             .subscribe(result => {
//                 this.instance.setData(result.salesSummary, true);
//                 this.hideLoading();
//             });
//     }
// }

// class RegionalStatsTable extends DashboardChartBase {
//     stats: Array<any>;

//     constructor(private _dashboardService: TenantDashboardServiceProxy) {
//         super();
//     }

//     init() {
//         this.reload();
//     }

//     _initSparklineChart(src, data, color, border) {
//         if (src.length === 0) {
//             return;
//         }

//         let config = {
//             type: 'line',
//             data: {
//                 labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October"],
//                 datasets: [{
//                     label: "",
//                     borderColor: color,
//                     borderWidth: border,

//                     pointHoverRadius: 4,
//                     pointHoverBorderWidth: 12,
//                     pointBackgroundColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
//                     pointBorderColor: Chart.helpers.color('#000000').alpha(0).rgbString(),
//                     pointHoverBackgroundColor: mUtil.getColor('danger'),
//                     pointHoverBorderColor: Chart.helpers.color('#000000').alpha(0.1).rgbString(),
//                     fill: false,
//                     data: data,
//                 }]
//             },
//             options: {
//                 title: {
//                     display: false,
//                 },
//                 tooltips: {
//                     enabled: false,
//                     intersect: false,
//                     mode: 'nearest',
//                     xPadding: 10,
//                     yPadding: 10,
//                     caretPadding: 10
//                 },
//                 legend: {
//                     display: false,
//                     labels: {
//                         usePointStyle: false
//                     }
//                 },
//                 responsive: true,
//                 maintainAspectRatio: true,
//                 hover: {
//                     mode: 'index'
//                 },
//                 scales: {
//                     xAxes: [{
//                         display: false,
//                         gridLines: false,
//                         scaleLabel: {
//                             display: true,
//                             labelString: 'Month'
//                         }
//                     }],
//                     yAxes: [{
//                         display: false,
//                         gridLines: false,
//                         scaleLabel: {
//                             display: true,
//                             labelString: 'Value'
//                         },
//                         ticks: {
//                             beginAtZero: true
//                         }
//                     }]
//                 },

//                 elements: {
//                     point: {
//                         radius: 4,
//                         borderWidth: 12
//                     },
//                 },

//                 layout: {
//                     padding: {
//                         left: 0,
//                         right: 10,
//                         top: 5,
//                         bottom: 0
//                     }
//                 }
//             }
//         };

//         return new Chart(src, config);
//     }

//     reload() {
//         let self = this;
//         this.showLoading();
//         this._dashboardService
//             .getRegionalStats({})
//             .subscribe(result => {
//                 this.stats = result.stats;
//                 this.hideLoading();
//                 var colors = ['accent', 'danger', 'success', 'warning'];
//                 setTimeout(() => {
//                     var $canvasItems = $('canvas.m_chart_sales_by_region');
//                     for (var i = 0; i < $canvasItems.length; i++) {
//                         var $canvas = $canvasItems[i];
//                         self._initSparklineChart($canvas, this.stats[i].change, mUtil.getColor(colors[i % 4]), 2);
//                     }
//                 });
//             });
//     }
// }

// class GeneralStatsPieChart extends DashboardChartBase {
//     //General stats =>  EasyPieChart: https://rendro.github.io/easy-pie-chart/

//     transactionPercent = {
//         value: 0,
//         options: {
//             barColor: '#F8CB00',
//             trackColor: '#f9f9f9',
//             scaleColor: '#dfe0e0',
//             scaleLength: 5,
//             lineCap: 'round',
//             lineWidth: 3,
//             size: 75,
//             rotate: 0,
//             animate: {
//                 duration: 1000,
//                 enabled: true
//             }
//         }
//     };
//     newVisitPercent = {
//         value: 0,
//         options: {
//             barColor: '#1bbc9b',
//             trackColor: '#f9f9f9',
//             scaleColor: '#dfe0e0',
//             scaleLength: 5,
//             lineCap: 'round',
//             lineWidth: 3,
//             size: 75,
//             rotate: 0,
//             animate: {
//                 duration: 1000,
//                 enabled: true
//             }
//         }
//     };
//     bouncePercent = {
//         value: 0,
//         options: {
//             barColor: '#F3565D',
//             trackColor: '#f9f9f9',
//             scaleColor: '#dfe0e0',
//             scaleLength: 5,
//             lineCap: 'round',
//             lineWidth: 3,
//             size: 75,
//             rotate: 0,
//             animate: {
//                 duration: 1000,
//                 enabled: true
//             }
//         }
//     };

//     constructor(private _dashboardService: TenantDashboardServiceProxy) {
//         super();
//     }

//     init(transactionPercent, newVisitPercent, bouncePercent) {
//         this.transactionPercent.value = transactionPercent;
//         this.newVisitPercent.value = newVisitPercent;
//         this.bouncePercent.value = bouncePercent;
//         this.hideLoading();
//     }

//     reload() {
//         this.showLoading();
//         this._dashboardService
//             .getGeneralStats({})
//             .subscribe(result => {
//                 this.init(result.transactionPercent, result.newVisitPercent, result.bouncePercent);
//             });
//     }
// }

// class DailySalesLineChart extends DashboardChartBase {
//     //== Daily Sales chart.
//     //** Based on Chartjs plugin - http://www.chartjs.org/

//     _canvasId: string;

//     constructor(private _dashboardService: TenantDashboardServiceProxy, canvasId: string) {
//         super();
//         this._canvasId = canvasId;
//     }

//     init(data) {
//         let dayLabels = [];
//         for (let day = 1; day <= data.length; day++) {
//             dayLabels.push('Day ' + day);
//         }

//         let chartData = {
//             labels: dayLabels,
//             datasets: [{
//                 //label: 'Dataset 1',
//                 backgroundColor: mUtil.getColor('success'),
//                 data: data
//             }, {
//                 //label: 'Dataset 2',
//                 backgroundColor: '#f3f3fb',
//                 data: data
//             }]
//         };

//         let chartContainer = $(this._canvasId);

//         if (chartContainer.length === 0) {
//             return;
//         }

//         let chart = new Chart(chartContainer, {
//             type: 'bar',
//             data: chartData,
//             options: {
//                 title: {
//                     display: false,
//                 },
//                 tooltips: {
//                     intersect: false,
//                     mode: 'nearest',
//                     xPadding: 10,
//                     yPadding: 10,
//                     caretPadding: 10
//                 },
//                 legend: {
//                     display: false
//                 },
//                 responsive: true,
//                 maintainAspectRatio: false,
//                 barRadius: 4,
//                 scales: {
//                     xAxes: [{
//                         display: false,
//                         gridLines: false,
//                         stacked: true
//                     }],
//                     yAxes: [{
//                         display: false,
//                         stacked: true,
//                         gridLines: false
//                     }]
//                 },
//                 layout: {
//                     padding: {
//                         left: 0,
//                         right: 0,
//                         top: 0,
//                         bottom: 0
//                     }
//                 }
//             }
//         });

//         this.hideLoading();
//     }

//     reload() {
//         this.showLoading();
//         this._dashboardService
//             .getSalesSummary(AppSalesSummaryDatePeriod.Monthly)
//             .subscribe(result => {
//                 this.init(result.salesSummary);
//                 this.hideLoading();
//             });
//     }
// }

// class ProfitSharePieChart extends DashboardChartBase {
//     //== Profit Share Chart.
//     //** Based on Chartist plugin - https://gionkunz.github.io/chartist-js/index.html

//     _canvasId: string;
//     data: number[];

//     constructor(private _dashboardService: TenantDashboardServiceProxy, canvasId: string) {
//         super();
//         this._canvasId = canvasId;
//     }

//     init(data: number[]) {
//         this.data = data;
//         if ($(this._canvasId).length === 0) {
//             return;
//         }

//         let chart = new Chartist.Pie(this._canvasId, {
//             series: [{
//                 value: data[0],
//                 className: 'custom',
//                 meta: {
//                     color: mUtil.getColor('brand')
//                 }
//             },
//             {
//                 value: data[1],
//                 className: 'custom',
//                 meta: {
//                     color: mUtil.getColor('accent')
//                 }
//             },
//             {
//                 value: data[2],
//                 className: 'custom',
//                 meta: {
//                     color: mUtil.getColor('warning')
//                 }
//             }
//             ],
//             labels: [1, 2, 3]
//         }, {
//                 donut: true,
//                 donutWidth: 17,
//                 showLabel: false
//             });

//         chart.on('draw', (data) => {
//             if (data.type === 'slice') {
//                 // Get the total path length in order to use for dash array animation
//                 let pathLength = data.element._node.getTotalLength();

//                 // Set a dasharray that matches the path length as prerequisite to animate dashoffset
//                 data.element.attr({
//                     'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
//                 });

//                 // Create animation definition while also assigning an ID to the animation for later sync usage
//                 let animationDefinition = {
//                     'stroke-dashoffset': {
//                         id: 'anim' + data.index,
//                         dur: 1000,
//                         from: -pathLength + 'px',
//                         to: '0px',
//                         easing: Chartist.Svg.Easing.easeOutQuint,
//                         // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
//                         fill: 'freeze',
//                         'stroke': data.meta.color
//                     }
//                 };

//                 // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
//                 if (data.index !== 0) {
//                     (animationDefinition['stroke-dashoffset'] as any).begin = 'anim' + (data.index - 1) + '.end';
//                 }

//                 // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

//                 data.element.attr({
//                     'stroke-dashoffset': -pathLength + 'px',
//                     'stroke': data.meta.color
//                 });

//                 // We can't use guided mode as the animations need to rely on setting begin manually
//                 // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
//                 data.element.animate(animationDefinition, false);
//             }
//         });

//         this.hideLoading();
//     }
// }

// class DashboardHeaderStats extends DashboardChartBase {

//     totalProfit = 0; totalProfitCounter = 0;
//     newFeedbacks = 0; newFeedbacksCounter = 0;
//     newOrders = 0; newOrdersCounter = 0;
//     newUsers = 0; newUsersCounter = 0;

//     totalProfitChange = 76; totalProfitChangeCounter = 0;
//     newFeedbacksChange = 85; newFeedbacksChangeCounter = 0;
//     newOrdersChange = 45; newOrdersChangeCounter = 0;
//     newUsersChange = 57; newUsersChangeCounter = 0;

//     init(totalProfit, newFeedbacks, newOrders, newUsers) {
//         this.totalProfit = totalProfit;
//         this.newFeedbacks = newFeedbacks;
//         this.newOrders = newOrders;
//         this.newUsers = newUsers;
//         this.hideLoading();
//     }
// }

// class MemberActivityTable extends DashboardChartBase {

//     memberActivities: Array<any>;

//     constructor(private _dashboardService: TenantDashboardServiceProxy) {
//         super();
//     }

//     init() {
//         this.reload();
//     }

//     reload() {
//         this.showLoading();
//         this._dashboardService
//             .getMemberActivity()
//             .subscribe(result => {
//                 this.memberActivities = result.memberActivities;
//                 this.hideLoading();
//             });
//     }
// }
