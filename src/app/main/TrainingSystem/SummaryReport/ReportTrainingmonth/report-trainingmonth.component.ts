import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    templateUrl: './report-trainingmonth.component.html',
    animations: [appModuleAnimation()]
})
export class ReportTrainingmonthComponent extends AppComponentBase implements OnInit {
    departmentCode: string;

    constructor(
        injector: Injector,

    ) {
        super(injector);

    }
    ngOnInit(): void {

    }
}
