import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ListDto, MSMPSN00ServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';
import { data_course } from '../../data_centercourse';




@Component({
    selector: 'emplist',
    templateUrl: './emplist.component.html',
})
export class EmplistComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    emp: MSMPSN00ListDto[] = [];
    dbmtabemti: DbmtabListDto[] = [];

    active: boolean;
    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private _dbmtabService: DbmtabServiceProxy,
        private data_course: data_course


    ) {
        super(injector);
    }
    ngOnInit(): void {
        if (this.data_course.empmasterl != 1) {
            this.data_course.Course()
        }
        this.emp = this.data_course.emp

        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
            this.dbmtabemti = result.items;
        });
    }
    chksho2;    
    show(id): void {
        this.modal.show();
        this.active = true;
        this.chksho2 = id
    }
    get_datan2(e) {
        console.log(e);
        var emp = this.emp.filter(res => res.psnidn == e)
        if (this.chksho2 == '1') {$('#hsreids1').val(emp[0].psnidn),this.close(),$('#emidf').val(emp[0].psnidn),this.close()
        ,$('#idempf').val(emp[0].psnidn),this.close(),$('#emidr1').val(emp[0].psnidn),this.close()}
        if (this.chksho2 == '2') {$('#hsreids2').val(emp[0].psnidn),this.close(),$('#emidt').val(emp[0].psnidn),this.close()
        ,$('#emidr2').val(emp[0].psnidn),this.close(),$('#idempt').val(emp[0].psnidn),this.close()}

    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

}

