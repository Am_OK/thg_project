import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './report-plantraining.component.html',
    animations: [appModuleAnimation()]
})
export class ReportPlantrainingComponent extends AppComponentBase implements OnInit {

    DEPARTMENT: DbmtabListDto[] = [];

    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,


    ) {
        super(injector);

    }
    ngOnInit(): void {
        this.getDBMTab()
    }
    show(): void {
      

    }
    getDBMTab() {
        setTimeout(() => {
            $('.m_select2_1').select2({
                placeholder: "Please Select.."
            });
        }, 500);
        this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;

        });
    }
    prints(depcom,year,depcode1,depcode2) {
        if (depcom != "" && depcode2 != "" && year != "") {
            if(depcode2 == ''){
                depcode2 ='ALL'
            }
            if(depcode2 == 'ALL'){
                depcode1 =''
            }
            if (depcode2 == "" || depcode2 == null || depcode2 == undefined) { depcode2 = '' }
            window.open("http://27.254.140.187:5433/Training/RegistrationByDep?COM=" + depcom + "&YEAR=" + (year - 543 * 1) + "&FROMDEP=" + depcode1 +"&TODEP=" + depcode2 + "&ReportType=PDF")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
    download(depcom,year,depcode1,depcode2) {
        if (depcom != "" && depcode2 != "" && year != "") {
            if (depcode2 == "" || depcode2 == null || depcode2 == undefined) { depcode2 = '' }
            window.open("http://27.254.140.187:5433/Training/RegistrationByDep?COM=" + depcom + "&YEAR=" + (year - 543 * 1) + "&FROMDEP=" + depcode1 +"&TODEP=" + depcode2 + "&ReportType=EXCEL")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
}
