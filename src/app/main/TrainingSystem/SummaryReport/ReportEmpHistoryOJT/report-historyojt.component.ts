import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';

@Component({
    templateUrl: './report-historyojt.component.html',
    animations: [appModuleAnimation()]
})
export class ReportHistoryOJTComponent extends AppComponentBase implements OnInit {

    DEPARTMENT: DbmtabListDto[] = [];
    POSITION: DbmtabListDto[] = [];

    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,


    ) {
        super(injector);

    }
    ngOnInit(): void {

        this.getDBMTab()
    }
    show(): void {


    }
   
    getDBMTab() {
        setTimeout(() => {
            $('.m_select2_1').select2({
                placeholder: "Please Select.."
            });
        }, 500);
        this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;

        });
        this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
            this.POSITION = result.items;

        });
    }
 
    typojt1:boolean
    prints(yyy,position,positionto,emidf,emidt,depcode1,depcode2,rehead,typoj){
        var typojt = 'True'
        this.typojt1 == true ? typojt = 'True' : typojt = 'False'
        if(positionto == ''){
            positionto == 'ALL'
        }else{
            position ='' 
        }
        if(emidt == ''){
            emidt == 'ALL'
        }else{
            emidf ='' 
        }
        if(depcode2 == ''){
            depcode2 == 'ALL'
        }else{
            depcode1 ='' 
        }
        if (positionto != "" && emidt != "" && depcode2 != "") {
            if(rehead == 'NO'){ rehead == 'NO' }
            if(rehead == 'DEP'){ rehead == 'DEP' }
            if(rehead == 'POS'){ rehead == 'POS' }
            if (emidt == "" || emidt == null || emidt == undefined ||positionto == "" || positionto == null || positionto == undefined||depcode2 == "" || depcode2 == null || depcode2 == undefined) { emidt = '' ,positionto='',depcode2=''}
            http://27.254.140.187:5433/Training/RegistrationOJT?company_code=001&year=2019&from_position=&to_position=zz&fromempid=&toempid=zz&departmentfrom=&departmentto=zz&order=NO&Excel=False
            window.open("http://27.254.140.187:5433/Training/RegistrationOJT?company_code=001" + "&year=" + (yyy - 543 * 1) + "&from_position=" + position + "&to_position=" + positionto + "&fromempid="+ emidf + "&toempid=" + emidt + "&departmentfrom=" + depcode1 + "&departmentto=" + depcode2 + "&order=" + rehead + "&Excel=" + typojt )
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }

    }
}
