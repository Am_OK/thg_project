import { Component, OnInit, Injector, ViewChild, Output, EventEmitter } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { CourseAppserviceServiceProxy, CourseListDto } from '@shared/service-proxies/service-proxies';
import { id } from '@swimlane/ngx-charts/release/utils';

@Component({
    templateUrl: './report-register.component.html',
    animations: [appModuleAnimation()]
})
export class ReportregisterComponent extends AppComponentBase implements OnInit {
    active: boolean = false;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal_course') public modal_course: ModalDirective;
    getcourse: CourseListDto[] = [];
    constructor(
        injector: Injector,
        private CourseAppserviceServiceProxy: CourseAppserviceServiceProxy,


    ) {
        super(injector);

    }
    ngOnInit(): void {

    }
    showc(): void {

        this.active = true;
        this.modal_course.show();
        this.getTCourse()

    }
    getTCourse(): void {
        this.CourseAppserviceServiceProxy.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
            this.getcourse = result.items;
        });

    }
    get_course(datan) {
      
        var datad = this.getcourse.filter(data => data.id == datan)
        
        $('#idcourse').val(datad[0].id)
        $('#placod').val(datad[0].placod)
        $('#namecourse').val(datad[0].planam)
        this.closecourse();
    }
    prints(idc){
        if (idc != "" ) {
            if (idc == "" || idc == null || idc == undefined) { idc = '' }
            window.open("http://27.254.140.187:5433/Training/RegistrationForm?SndId=" + idc + "&ReportType=PDF");
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
    download(placod){
        if (placod != "" ) {
            if (placod == "" || placod == null || placod == undefined) { placod = '' }
            window.open("http://27.254.140.187:5433/Training/RegistrationForm?SndId=" + placod + "&ReportType=EXCEL");
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
    closecourse() {

        this.modal_course.hide();
    }
}
