import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    templateUrl: './report-coursemonth.component.html',
    animations: [appModuleAnimation()]
})
export class ReportCoursemonthComponent extends AppComponentBase implements OnInit {
    departmentCode: string;

    constructor(
        injector: Injector,

    ) {
        super(injector);

    }
    ngOnInit(): void {

    }
    
    prints(yearmo,d1as,d2as,emidr1,emidr2){
        if (emidr2 != "" && d2as != "" && yearmo != "") {
            if (emidr2 == "" || emidr2 == null || emidr2 == undefined || d2as == "" || d2as == null || d2as == undefined || yearmo == "" || yearmo == null || yearmo == undefined) { emidr2 = '',d2as='' ,yearmo=''}
            window.open("http://27.254.140.187:5433/Training/RegistrationByMON?YEAR=" + (yearmo - 543 * 1) + "&FmDate=" + this.fnyyyymmdd(d1as) + "&ToDate=" + this.fnyyyymmdd(d2as) + "&FROMID=" + emidr1 + "&TOID=" + emidr2 + "&ReportType=PDF")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }

    }
    dowload(yearmo,d1as,d2as,emidr1,emidr2){
        if (emidr2 != "" && d2as != "" && yearmo != "") {
            if (emidr2 == "" || emidr2 == null || emidr2 == undefined || d2as == "" || d2as == null || d2as == undefined || yearmo == "" || yearmo == null || yearmo == undefined) { emidr2 = '',d2as='' ,yearmo=''}
            window.open("http://27.254.140.187:5433/Training/RegistrationByMON?YEAR=" + (yearmo - 543 * 1) + "&FmDate=" + this.fnyyyymmdd(d1as) + "&ToDate=" + this.fnyyyymmdd(d2as) + "&FROMID=" + emidr1 + "&TOID=" + emidr2 + "&ReportType=EXCEL")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }

    }
}
