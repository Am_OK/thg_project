import { Component, OnInit, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, CourseListDto,CourseAppserviceServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    templateUrl: './report-trainingemp.component.html',
    animations: [appModuleAnimation()]
})
export class ReportTrainingempComponent extends AppComponentBase implements OnInit {
    active: boolean = false;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal_course') public modal_course: ModalDirective;
    @ViewChild('modal_course2') public modal_course2: ModalDirective;

    getcourse: CourseListDto[] = [];
    DEPARTMENT: DbmtabListDto[] = [];
    POSITION: DbmtabListDto[] = [];
    
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private CourseAppserviceServiceProxy: CourseAppserviceServiceProxy,


    ) {
        super(injector);

    }
    ngOnInit(): void {

        this.getDBMTab()
    }
    show(): void {


    }
    getDBMTab() {
        setTimeout(() => {
            $('.m_select2_1').select2({
                placeholder: "Please Select.."
            });
        }, 500);
        this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;

        });
        this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
            this.POSITION = result.items;

        });
    }
    showc(): void {

        this.active = true;
        this.modal_course.show();
        this.getTCourse()

    }
    getTCourse(): void {
        this.CourseAppserviceServiceProxy.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
            this.getcourse = result.items;
        });

    }
    get_course(datav) {
        var datad = this.getcourse.filter(data => data.id == datav)
        $('#placod').val(datad[0].placod)

        
        // $('#namecourse').val(datad[0].planam)
        this.closecourse();
    }
    closecourse() {

        this.modal_course.hide();
    }
    showc2(): void {

        this.active = true;
        this.modal_course2.show();
        this.getTCourse()

    }
    get_course2(datan) {
        var datad = this.getcourse.filter(data => data.id == datan)
        $('#placod2').val(datad[0].placod)

        
        // $('#namecourse').val(datad[0].planam)
        this.closecourse2();
    }
    closecourse2() {

        this.modal_course2.hide();
    }
    prints(yea1,yea2,postrf,postrt,idempf,idempt,depart,depart1,placod,placod2,dtefm,dteto,passed,training,reporthea){
        if (yea2 != "" && postrt != "" && idempt != "" && depart1 != "" && placod2 != "" ) {
            if(idempt == ''){
                
                $('#idempf').val('ทั้งหมด')
            }
            if(placod2 == ''){
                $('#placod2').val('ทั้งหมด')
            }
            if (postrt == "" || postrt == null || postrt == undefined ||idempt == "" || idempt == null || idempt == undefined||depart1 == "" || depart1 == null || depart1 == undefined) { postrt = '' ,idempt = '',depart1 = ''}
            http://27.254.140.187:5433/Training/EmpTraining?LnkCom=001&LnkYea=2019&LnkTYea=2019&LNKID=&LNKTID=zzz&LNKPOS=&LNKTPOS=zzz&LnkFms=&LnkTos=zzz&LNKCOD=&LNKTCOD=zzz&LNKDTE=20190601&LNKTDTE=20190631&LnkResult=ALL&LnkAT=ALL&LnkOrder=IDN&ReportType=PDF
            window.open("http://27.254.140.187:5433/Training/EmpTraining?LnkCom=001" + "&LnkYea=" + (yea1 - 543 * 1) + "&LnkTYea=" + (yea2 - 543 * 1) + "&LNKID=" + idempf + "&LNKTID=" + idempt + "&LNKPOS=" + postrf + "&LNKTPOS=" + postrt +  "&LnkFms=" + depart + "&LnkTos=" + depart1 + "&LNKCOD=" + placod + "&LNKTCOD=" + placod2 + "&LNKDTE=" + this.fnyyyymmdd(dtefm) 
            + "&LNKTDTE=" + this.fnyyyymmdd(dteto) + "&LnkResult=" + passed + "&LnkAT=" + training + "&LnkOrder=" + reporthea + "&ReportType=PDF")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
    dowload(yea1,yea2,postrf,postrt,idempf,idempt,depart,depart1,placod,placod2,dtefm,dteto,passed,training,reporthea){
        if (yea2 != "" && postrt != "" && idempt != "" && depart1 != "" && placod2 != "" ) {
            if (postrt == "" || postrt == null || postrt == undefined ||idempt == "" || idempt == null || idempt == undefined||depart1 == "" || depart1 == null || depart1 == undefined) { postrt = '' ,idempt = '',depart1 = ''}
            http://27.254.140.187:5433/Training/EmpTraining?LnkCom=001&LnkYea=2019&LnkTYea=2019&LNKID=&LNKTID=zzz&LNKPOS=&LNKTPOS=zzz&LnkFms=&LnkTos=zzz&LNKCOD=&LNKTCOD=zzz&LNKDTE=20190601&LNKTDTE=20190631&LnkResult=ALL&LnkAT=ALL&LnkOrder=IDN&ReportType=PDF
            window.open("http://27.254.140.187:5433/Training/EmpTraining?LnkCom=001" + "&LnkYea=" + (yea1 - 543 * 1) + "&LnkTYea=" + (yea2 - 543 * 1) + "&LNKID=" + idempf + "&LNKTID=" + idempt + "&LNKPOS=" + postrf + "&LNKTPOS=" + postrt +  "&LnkFms=" + depart + "&LnkTos=" + depart1 + "&LNKCOD=" + placod + "&LNKTCOD=" + placod2 + "&LNKDTE=" + this.fnyyyymmdd(dtefm) 
            + "&LNKTDTE=" + this.fnyyyymmdd(dteto) + "&LnkResult=" + passed + "&LnkAT=" + training + "&LnkOrder=" + reporthea + "&ReportType=EXCEL")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
}
