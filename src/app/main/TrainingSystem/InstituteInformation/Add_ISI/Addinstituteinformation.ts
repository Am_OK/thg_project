import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'Addinstituteinformation',
  templateUrl: './Addinstituteinformation.html',
})
export class AddinstituteinformationComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') modal2: ModalDirective;
  array_data_complete = [({
    'sbyear': '', 'sbdep': ''
    , 'sbbudget': '', 'sbtype': ''
  })];


  constructor(
    injector: Injector,


  ) {
    super(injector);

  }
  ngOnInit() {

  }
  show(): void {

    this.active = true;
    this.modal.show();
    this.array_data_complete = [];

  }
  idx;
  insertd(sbyear, sbdep, sbbudget, sbtype) {
    // $("#panel").slideToggle("slow");
    // this.idx = this.idx + 1
    var typ0 = $('input[id=typ0]:checked').val();

    if(typ0 =='OUT'){
      this.array_data_complete.push({
        'sbyear': sbyear, 'sbdep': sbdep
        , 'sbbudget': sbbudget, 'sbtype': sbtype='OUT'
      })
    }
    if(typ0 =='IN')
    {
      this.array_data_complete.push({
        'sbyear': sbyear, 'sbdep': sbdep
        , 'sbbudget': sbbudget, 'sbtype': sbtype ="IN"
      })
    }

  }

  show2(): void {


    this.modal.hide();
    this.modal2.show();


  }

  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    $('#md').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
  close2(): void {
    this.modal2.hide();
    this.modal.show();
  }
  clear1(): void {
  $('#md').find("input").val('').end()

  }
  clear2(): void {
    $('#us').find("input").val('').end()
  
    }
}