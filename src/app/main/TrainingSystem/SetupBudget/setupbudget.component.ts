import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TRBUD00ListDto, TRBUD00AppserviceServiceProxy } from '@shared/service-proxies/service-proxies';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'setupbudget',
  templateUrl: './setupbudget.component.html'
})
export class SetupbudgetComponent extends AppComponentBase implements OnInit {

  TRBUD00: TRBUD00ListDto[] = [];
  constructor(
    injector: Injector,
    private TRBUD00AppserviceServiceProxy: TRBUD00AppserviceServiceProxy
  ) {
    super(injector);


  }

  ngOnInit() {
    this.getTRBUD00();
   
  }

  getTRBUD00(){
 this.TRBUD00AppserviceServiceProxy.getTRBUD00('','',undefined) .subscribe((result) => {
      this.TRBUD00 = result.items;
  });

  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
