import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TRBUD00AppserviceServiceProxy, TRBUD00ListDto, CreateTRBUD00Input, DbmtabListDto, DbmtabServiceProxy, TRBUD01ListDto, TRBUD01AppserviceServiceProxy, CreateTRBUD01Input } from '@shared/service-proxies/service-proxies';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'setupbudgetadd',
  templateUrl: './setupbudget-add.component.html',
})
export class SetupbudgetAddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') modal2: ModalDirective;
  // array_data_complete = [({
  //   'sbyear': '', 'sbdep': ''
  //   , 'sbbudget': '', 'sbtype': ''
  // })];

  // TRBUD00: TRBUD00ListDto[] = [];
  dbmtab: DbmtabListDto[] = [];
  dbmtab2: DbmtabListDto[] = [];
  TRBUD00: CreateTRBUD00Input = new CreateTRBUD00Input;
  TRBUD01: CreateTRBUD01Input = new CreateTRBUD01Input;
  budtyp = { "data": [{ "tabtB2": "IN", "tabdsc": "ภายใน ", }, { "tabtB2": "OUT", "tabdsc": "ภานนอก", }] };
  pipe = new DatePipe('en-US');

  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private TRBUD00AppserviceServiceProxy: TRBUD00AppserviceServiceProxy,
    private TRBUD01AppserviceServiceProxy: TRBUD01AppserviceServiceProxy

  ) {
    super(injector);

  }
  ngOnInit() {

  }
  show(): void {
    this.Chk = 0
    this.active = true;
    this.clear1();
    this.clear2();
    this.Chke();
    this.modal.show();
    this.array_data_complete = [];
    $('#budyear2').val(this.fnyyyy543(this.pipe.transform(Date(), 'yyyy')))

  }
  idx;
  // insertd(sbyear, sbdep, sbbudget, sbtype) {
  //   // $("#panel").slideToggle("slow");
  //   // this.idx = this.idx + 1
  //   var typ0 = $('input[id=typ0]:checked').val();

  //   if (typ0 == 'OUT') {
  //     this.array_data_complete.push({
  //       'sbyear': sbyear, 'sbdep': sbdep
  //       , 'sbbudget': sbbudget, 'sbtype': sbtype = 'OUT'
  //     })
  //   }
  //   if (typ0 == 'IN') {
  //     this.array_data_complete.push({
  //       'sbyear': sbyear, 'sbdep': sbdep
  //       , 'sbbudget': sbbudget, 'sbtype': sbtype = "IN"
  //     })
  //   }

  // }
  TRBUD002: TRBUD00ListDto[] = [];
  TRBUD001: TRBUD01ListDto[] = [];
  TRBUD0022: TRBUD00ListDto[] = [];
  datess;

  show2(date): void {
    this.dateH = date;
    var datte = this.fnyyyy(date).toString()
    setTimeout(() => {
      this.TRBUD00AppserviceServiceProxy.getTRBUD00(this.TRBUD00.budcod, datte, undefined).subscribe((result) => {
        this.TRBUD0022 = result.items
        this.datess = this.TRBUD0022[0].budyea
      });

      setTimeout(() => {
        if (datte == this.datess) {
          this.message.warn('ไม่สามารถบันทึกปี' + date + 'ได้เนื่องจากมีปีปัจุบันอยู่แล้ว')
        } else {
          this.modal.hide();
          this.modal2.show();
          this.budamt2 = this.TRBUD00.budamt
          this.budcod2 = this.TRBUD00.budcod
          this.budyea2 = date
          // this.SaveH(date)
        }
      }, 500);
    }, 300);



    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.dbmtab = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('DIVISION', '').subscribe((result) => {
      this.dbmtab2 = result.items;
    });

    setTimeout(() => {
      $('.m_select2_1a').select2({
        placeholder: "Please Select.."
      });
    }, 250);
    setTimeout(() => {
      this.fnnum()
      this.fnnum1()
    }, 500);
  }
  fnnum() {
    if ($('#amt').val() != '' && $('#amt').val() != null && $('#amt').val() != undefined) {
      var a = $('#amt').val();
      $('#amt').val(this.fnformat(a));
    }
    if ($('#budamt2').val() != '' && $('#budamt2').val() != null && $('#budamt2').val() != undefined) {
      var a = $('#budamt2').val();
      $('#budamt2').val(this.fnformat(a));
    }


  }
  fnnum1() {
    if ($('#budamt1').val() != '' && $('#budamt1').val() != null && $('#budamt1').val() != undefined) {
      var a = $('#budamt1').val();
      $('#budamt1').val(this.fnformat(a));
    }
  


  }
  fnformat(x) {
    var a = parseFloat((x.toString()).replace(/,/g, ""))
      .toFixed(2)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (a == 'NaN') {
      a = '0.00';
      // this.datas.A1 = '0';
    }
    return a;
  }
  budamt2;
  budcod2;
  budyea2;
  idHH;
  // Shww(idH) {
  //   this.idHH = idH.id
  //   this.TRBUD00AppserviceServiceProxy.getId(this.idHH).subscribe((result) => {
  //     this.TRBUD002 = result.items;
  //     this.budamt2 = this.TRBUD002[0].budamt
  //     this.budcod2 = this.TRBUD002[0].budcod
  //     this.budyea2 = this.fnyyyy543(this.TRBUD002[0].budyea)
  //   });
  // }

  Chk;
  Chke() {

    if (this.TRBUD00.budamt != null && this.TRBUD00.budamt != undefined) {
      this.Chk = 1
    } else {
      this.Chk = 0
    }

  }

  kuacc(dt) {
    if (dt.indexOf("+") >= 0) {
      var accode = dt.replace("+", "")
      this.amt = accode;
    }
  }
  SaveH(date) {
    this.dateH = date;
    var datte = this.fnyyyy(date).toString()
    setTimeout(() => {
      this.TRBUD00AppserviceServiceProxy.getTRBUD00(this.TRBUD00.budcod, date, undefined).subscribe((result) => {
        this.TRBUD0022 = result.items
        this.datess = this.TRBUD0022[0].budyea
      });
        setTimeout(() => {
          if (datte == this.datess) {
            this.message.warn('ไม่สามารถบันทึกปี' + date + 'ได้เนื่องจากมีปีปัจุบันอยู่แล้ว')
          } else {
            if (this.TRBUD00.budamt != null && this.TRBUD00.budamt != undefined) {
              this.TRBUD00.budyea = this.fnyyyy(date).toString();
              this.TRBUD00AppserviceServiceProxy.createTRBUD00(this.TRBUD00)
                .subscribe((result) => {
                  this.SaveD(result.id)
                  this.modalSave.emit();
                  this.notify.info(this.l('SavedSuccessfully'));
                  this.close()
                });
               
            } else {
              this.message.info("กรุณากรอกจำนวนงบประมาณ")
            }

          }
        }, 200);
      

    }, 500);
  }

  array_data_complete = [({ 'budcod': '', 'budyea': '', 'budcoD2': '', 'buddev': '', 'buddep': '', 'budamt': '', 'budtyp': '', 'refId': '' })];
  amt

  CreateTRBUD01Input: CreateTRBUD01Input[] = [];
  Add2(dips, dips2, typeP) {
    var Amtt = ((this.fnreplace(this.budamt2) - this.amt) * 1)
    // var budtyp = $('input[id=typ0]:checked').val();
    if (Amtt < 0) {
      this.message.info('ยอดเงินที่ใช้เกินจำนวน')
    } else {
      this.CreateTRBUD01InputObject = new CreateTRBUD01Input
      this.CreateTRBUD01InputObject.budcod = this.budcod2
      this.CreateTRBUD01InputObject.budyea = this.fnyyyy(this.budyea2).toString()
      this.CreateTRBUD01InputObject.budcoD2 = dips + typeP
      this.CreateTRBUD01InputObject.buddev = dips2
      this.CreateTRBUD01InputObject.buddep = dips
      this.CreateTRBUD01InputObject.budamt = this.amt
      this.CreateTRBUD01InputObject.budtyp = typeP
      this.CreateTRBUD01InputObject.refId = this.idHH
      this.CreateTRBUD01Input.push(this.CreateTRBUD01InputObject)

      this.budamt2 = Amtt
      this.amt = ''
    }
  }
  Deletes22(e) {
    console.log(e)
    this.budamt2 = (parseInt(e.data.budamt) + parseInt(this.budamt2)) * 1

  }
  CreateTRBUD01InputObject: CreateTRBUD01Input = new CreateTRBUD01Input
  dateH;
  SaveD(refid) {
    // this.SaveH(this.dateH)
    for (let i = 0; i < this.CreateTRBUD01Input.length; i++) {
      this.CreateTRBUD01Input[i].refId = refid
      if (this.CreateTRBUD01Input.length <= (i + 1 * 1)) {
        this.TRBUD01AppserviceServiceProxy.createTRBUD01(this.CreateTRBUD01Input).subscribe(() => {
          this.modalSave.emit();
          this.notify.info(this.l('SavedSuccessfully'));
          this.close3()
          this.close();
        });
      }

    }


  }

  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    $('#md').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
  close2(): void {
    this.modal2.hide();
    this.modal.show();
  }
  close3(): void {
    this.modal2.hide();
  }
  clear1(): void {
    $('#md').find("input").val('').end()

  }
  clear2(): void {
    $('#us').find("input").val('').end()

  }
}
