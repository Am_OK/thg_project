import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TRBUD00AppserviceServiceProxy, TRBUD00ListDto, CreateTRBUD00Input, DbmtabListDto, DbmtabServiceProxy, TRBUD01ListDto, TRBUD01AppserviceServiceProxy, CreateTRBUD01Input, UpdateTRBUD00Input } from '@shared/service-proxies/service-proxies';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'SetupBudgetedit',
  templateUrl: './SetupBudget-edit.html',
})
export class SetupBudgeteditComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') modal2: ModalDirective;
  // array_data_complete = [({
  //   'sbyear': '', 'sbdep': ''
  //   , 'sbbudget': '', 'sbtype': ''
  // })];

  // TRBUD00: TRBUD00ListDto[] = [];
  dbmtab: DbmtabListDto[] = [];
  dbmtab2: DbmtabListDto[] = [];
  TRBUD00: UpdateTRBUD00Input = new UpdateTRBUD00Input();
  TRBUD01: CreateTRBUD01Input = new CreateTRBUD01Input;
  TRBUD0dital: TRBUD01ListDto[] = [];
  budtyp = { "data": [{ "tabtB2": "IN", "tabdsc": "ภายใน ", }, { "tabtB2": "OUT", "tabdsc": "ภานนอก", }] };

  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private TRBUD00AppserviceServiceProxy: TRBUD00AppserviceServiceProxy,
    private TRBUD01AppserviceServiceProxy: TRBUD01AppserviceServiceProxy

  ) {
    super(injector);

  }
  ngOnInit() {
    this.format_dec();
  }
  ids
  cod2
  show(id): void {

    this.ids = id
    setTimeout(() => {
      this.TRBUD00AppserviceServiceProxy.getId(id).subscribe((result) => {
        this.TRBUD00 = result.items[0];
        this.TRBUD00.budamt = this.TRBUD00.budamt
        this.budcod2 = result.items[0].budcod
        this.TRBUD00.budyea = this.fnyyyy543(result.items[0].budyea).toString()
        this.budyea2 = this.TRBUD00.budyea
        this.budamt2 = result.items[0].budamt
        setTimeout(() => {
          this.formatcur()

        }, 100);
      });
    }, 200);

    this.Chk = 1
    this.active = true;
    this.clear1();
    this.clear2();

    this.modal.show();
    this.array_data_complete = [];

  }
  fnformat(x) {
    var a = parseFloat((x.toString()).replace(/,/g, ""))
      .toFixed(2)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (a == 'NaN') {
      a = '0.00';
      // this.datas.A1 = '0';
    }
    return a;
  }
  idx;
  // insertd(sbyear, sbdep, sbbudget, sbtype) {
  //   // $("#panel").slideToggle("slow");
  //   // this.idx = this.idx + 1
  //   var typ0 = $('input[id=typ0]:checked').val();

  //   if (typ0 == 'OUT') {
  //     this.array_data_complete.push({
  //       'sbyear': sbyear, 'sbdep': sbdep
  //       , 'sbbudget': sbbudget, 'sbtype': sbtype = 'OUT'
  //     })
  //   }
  //   if (typ0 == 'IN') {
  //     this.array_data_complete.push({
  //       'sbyear': sbyear, 'sbdep': sbdep
  //       , 'sbbudget': sbbudget, 'sbtype': sbtype = "IN"
  //     })
  //   }

  // }
  formatcur() {
    $("#budamt").val(this.fnformat(this.TRBUD00.budamt))

  }
  kuacc(dt) {
    if (dt.indexOf("+") >= 0) {
      var accode = dt.replace("+", "")
      this.amt = accode;
    }
  }
  TRBUD002: TRBUD00ListDto[] = [];
  TRBUD001: TRBUD01ListDto[] = [];
  sumtotalbudamt = 0;
  show2(date): void {
    this.modal.hide();
    this.modal2.show();
    this.dateH = date;
    // this.SaveH(date)
    this.sumtotalbudamt = 0;

    this.TRBUD01AppserviceServiceProxy.getRefId(this.ids).subscribe((result) => {
      this.TRBUD0dital = result.items;
      for (let i = 0; i < this.TRBUD0dital.length; i++) {
        this.sumtotalbudamt = this.TRBUD0dital[i].budamt + this.sumtotalbudamt;
      }
      this.budamt2 = parseInt(this.budamt2) - this.sumtotalbudamt
     
    });


    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.dbmtab = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('DIVISION', '').subscribe((result) => {
      this.dbmtab2 = result.items;
    });

    setTimeout(() => {
      $('.m_select2_1a').select2({
        placeholder: "Please Select.."
      });
    }, 250);
    setTimeout(() => {
      this.fnnum()

    }, 100);
    
  }
  
  fnnum()
  {
    if ($('#amt1').val() != '' && $('#amt1').val() != null && $('#amt1').val() != undefined) {
      var a = $('#amt1').val();
      $('#amt1').val(this.fnformat(a));
    }
    if ($('#budamtt2').val() != '' && $('#budamtt2').val() != null && $('#budamtt2').val() != undefined) {
      var a = $('#budamtt2').val();
      $('#budamtt2').val(this.fnformat(a));
    }

  }
  fnformatc(x) {
    var a = parseFloat((x.toString()).replace(/,/g, ""))
      .toFixed(2)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (a == 'NaN') {
      a = '0.00';
      // this.datas.A1 = '0';
    }
    return a;
  }
  Deletes() {
    this.TRBUD00AppserviceServiceProxy.deleteTRBUD00(this.ids).subscribe(() => {
      this.TRBUD01AppserviceServiceProxy.deletebyRefId(this.ids).subscribe(() => {
        this.modalSave.emit()
        this.notify.info(this.l('SavedSuccessfully'));
        this.close()
      });
    });
  }


  budamt2;
  budcod2;
  budyea2;
  idHH;
  // Shww(idH) {
  //   this.idHH = idH.id
  //   this.TRBUD00AppserviceServiceProxy.getId(this.ids).subscribe((result) => {
  //     this.TRBUD002 = result.items;
  //     this.budamt2 = this.TRBUD002[0].budamt
  //     this.budcod2 = this.TRBUD002[0].budcod
  //     this.budyea2 = this.fnyyyy543(this.TRBUD002[0].budyea)
  //   });
  // }

  Chk;
  Chke() {
    if (this.TRBUD00.budamt != null && this.TRBUD00.budamt != undefined) {
      this.Chk = 1
    } else {
      this.Chk = 0
    }

  }

  SaveH(date) {
    if (this.TRBUD00.budamt != null && this.TRBUD00.budamt != undefined) {
      this.TRBUD00.budyea = this.fnyyyy(date).toString();
      this.TRBUD00AppserviceServiceProxy.createTRBUD00(this.TRBUD00)
        .subscribe((id) => {
          // this.Shww(id)
          this.notify.info(this.l('SavedSuccessfully'));
        });
    } else {
      this.message.info("กรุณากรอกจำนวนงบประมาณ")
    }


  }

  array_data_complete = [({ 'budcod': '', 'budyea': '', 'budcoD2': '', 'buddev': '', 'buddep': '', 'budamt': '', 'budtyp': '', 'refId': '' })];
  amt

  CreateTRBUD01Input: CreateTRBUD01Input[] = [];
  @ViewChild("tarDataGrid") tarDataGrid: DxDataGridComponent
  Add(dips, dips2, typeP2) {
    var Amtt = ((this.budamt2 - this.amt) * 1)
    // var budtyp = $('input[id=typ0]:checked').val();
    if (Amtt < 0) {
      this.message.info('ยอดเงินที่ใช้เกินจำนวน')
    } else {

      this.CreateTRBUD01InputObject.budcod = this.budcod2
      this.CreateTRBUD01InputObject.budyea = this.fnyyyy(this.budyea2).toString()
      this.CreateTRBUD01InputObject.budcoD2 = dips + typeP2.toString()
      this.CreateTRBUD01InputObject.buddev = dips2
      this.CreateTRBUD01InputObject.buddep = dips
      this.CreateTRBUD01InputObject.budamt = this.amt
      this.CreateTRBUD01InputObject.budtyp = typeP2.toString()
      this.CreateTRBUD01InputObject.refId = this.ids
      this.TRBUD01AppserviceServiceProxy.createBudgetDetail(this.CreateTRBUD01InputObject).subscribe(() => {
        this.TRBUD01AppserviceServiceProxy.getRefId(this.ids).subscribe((result) => {
          this.TRBUD0dital = result.items;
          for (let i = 0; i < this.TRBUD0dital.length; i++) {
            this.sumtotalbudamt = this.TRBUD0dital[i].budamt + this.sumtotalbudamt;
          }
          this.budamt2 = parseInt(this.budamt2) - this.sumtotalbudamt
        });

        this.notify.info(this.l('SavedSuccessfully'));
      });
      // this.CreateTRBUD01Input.push(this.CreateTRBUD01InputObject)
      this.budamt2 = Amtt
      this.amt = ''
    }
  }
  Deletes23(e) {
    console.log(e)
    this.budamt2 = (parseInt(e.data.budamt) + parseInt(this.budamt2)) * 1

    this.TRBUD01AppserviceServiceProxy.deleteTRBUD01(e.data.id).subscribe(() => {
      this.tarDataGrid.instance.refresh();
      this.notify.info(this.l('SavedSuccessfully'));
    });

  }
  CreateTRBUD01InputObject: CreateTRBUD01Input = new CreateTRBUD01Input
  dateH;
  SaveD() {
    // this.SaveH(this.dateH)
    // this.TRBUD01AppserviceServiceProxy.createTRBUD01(this.CreateTRBUD01Input).subscribe(() => {
    // });
    this.modalSave.emit();
    this.notify.info(this.l('SavedSuccessfully'));
    this.close3()
  }

  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    $('#md').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
  close2(): void {
    this.modal2.hide();
    this.modal.show();
  }
  close3(): void {
    this.modal2.hide();
  }
  clear1(): void {
    $('#md').find("input").val('').end()

  }
  clear2(): void {
    $('#us').find("input").val('').end()

  }
}
