import { Component, Injectable, OnInit, Injector } from '@angular/core';
import { DbmtabServiceProxy, DbmtabListDto, MSMPSN00ListDto ,MSMPSN00ServiceProxy, CourseListDto, CourseAppserviceServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Injectable()
export class data_course extends AppComponentBase implements OnInit {
  timeou = 0;
  emp: MSMPSN00ListDto[] = [];
  coursec: CourseListDto[] = [];
  filterTab: string = 'POSITION';
  dbmtab: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  dbmtabdep: DbmtabListDto[] = [];
  dbmtabpay: DbmtabListDto[] = [];
  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,

    
  ) {
    super(injector);
  }
  ngOnInit(): void {
    this.Course();
  }
  empmasterl = 0
  
  Course() {
    this.empmasterl = 0
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((resultD) => {
        this.emp = resultD.items
      })
  
      this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
        this.coursec = result.items;
  
      });
      this._dbmtabService.getDbmtabFromTABTB1(this.filterTab, '').subscribe((result) => {
        this.dbmtab = result.items;
      });
      this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
        this.dbmtabemti = result.items;
  
      });
      this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
        this.dbmtabdep = result.items;
  
      });
      this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
        this.dbmtabpay = result.items;
  
      });
      this.empmasterl = 1

  }
  
}