import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, CourseAppserviceServiceProxy, CreateCourseInput, DbmtabListDto, CreateDbmtabInput, MSMPSN00ServiceProxy, CourseListDto, CreateCourseDetailInput, CourseDetailAppserviceServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { SetuptrainingComponent } from '../setuptraining.component';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { filter } from 'rxjs/operators';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { AppConsts } from '@shared/AppConsts';
declare var kwanp: any;
@Component({
  selector: 'SetupEmptrainingadd',
  templateUrl: './SetupEmptraining_add.html',
})
export class SetupEmptrainingaddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') public modal2: ModalDirective;
  saving: boolean;
  Course: CourseListDto[] = [];



  createcource: CreateCourseInput = new CreateCourseInput();
  DEPARTMENT: DbmtabListDto[] = [];
  DIVISION: DbmtabListDto[] = [];
  TrainingCourse: DbmtabListDto[] = [];
  TrainingPay: DbmtabListDto[] = [];

  emp = [];
  filterTab: string = 'POSITION';
  dbmtab: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  dbmtabdep: DbmtabListDto[] = [];
  dbmtabpay: DbmtabListDto[] = [];

  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseDetailAppserviceServiceProxy: CourseDetailAppserviceServiceProxy,
    private SetuptrainingComponent: SetuptrainingComponent
  ) {
    super(injector);
    
    this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadFilesToServer' + '?folderNameStr=excelSalary';
    // this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadFilesToServer' + '?folderNameStr=excelSalary';
  }
  ngOnInit() {
  }
  uploadUrl: string;
  uploadedFilesexcel: any[] = [];
  getDisplayExpr(item) {
    // console.log(item)
    if (!item) {
      return "";
    }
    // console.log(item.psnfnm + " " + item.psnlnm)
    return item.psnfnm + " " + item.psnlnm
  }
  docic
  // UpdateCourseDetailInputData: CreateCourseDetailInput[] = []
  show(doc): void {
    this.docic = doc
    this.CreateCourseDetailInputData = []
    this.CourseAppservice.getTCourse('', doc, '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;
      $('#expper22').val(this.Course[0].placod);
      $('#date1').val(this.fnddmmyyyy(this.Course[0].pladtefm));
      $('#tami1').val(this.Course[0].plastrtim);
      $('#date2').val(this.fnddmmyyyy(this.Course[0].pladteto));
      $('#tami2').val(this.Course[0].plaendtim);
    });
    if (this.SetuptrainingComponent.empmasterl!= 1) {
      this.SetuptrainingComponent.getDataMaster()
  }
    this.emp = this.SetuptrainingComponent.emp
    this.CourseDetailAppserviceServiceProxy.getCourseDetail('', doc, '', '', '', '', undefined).subscribe((resultD) => {
      this.CreateCourseDetailInputData = resultD.items
    })

    this.active = true;
    this.modal.show();
  }

  close(): void {

    this.modal.hide();
    this.active = false;
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }

  /////////////////////////////////222////////////////////////////////
  show2(): void {
    this.modal2.show();

  }

  clearP() {
    this.CreateCourseDetailInput = new CreateCourseDetailInput()
  }


  CreateCourseDetailInput: CreateCourseDetailInput = new CreateCourseDetailInput()
  CreateCourseDetailInputData: CreateCourseDetailInput[] = []
  @ViewChild("targetDataGridempsE") targetDataGridemp: DxDataGridComponent



  get_datan2(e) {
    console.log(e)
    var data = e.split(",");
    for (let i = 0; i < data.length; i++) {
      var arryHav = this.CreateCourseDetailInputData.filter(res => res.hsreid == data[i])
      if (arryHav.length <= 0) {
        this.CreateCourseDetailInput = new CreateCourseDetailInput()
        this.CreateCourseDetailInput.hsreid = data[i]
        this.CreateCourseDetailInput.hrsunam = this.emp.filter(r => r.psnidn == data[i])[0].psnfnm + ' ' + this.emp.filter(r => r.psnidn == data[i])[0].psnlnm;
        this.CreateCourseDetailInput.hsrcsu = this.Course[0].placod
        this.CreateCourseDetailInput.hsryea = this.Course[0].playea
        this.CreateCourseDetailInput.hsrfmd = this.Course[0].pladtefm
        this.CreateCourseDetailInput.hsrtod = this.Course[0].pladteto
        this.CreateCourseDetailInput.hsrfmt = this.Course[0].plastrtim
        this.CreateCourseDetailInput.hsrtot = this.Course[0].plaendtim
        this.CreateCourseDetailInput.hsrndy = this.Course[0].plahrs
        this.CreateCourseDetailInput.hsramT1 = 0
        this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)

        if (data.length <= (i + 1 * 1)) {
          console.log(this.CreateCourseDetailInputData)
        }
      }
    }
    this.close2();
  }

  nameempcs;
  kuacc(dt) {
    this.nameempcs = ''
    if (dt.indexOf("+") >= 0) {
      var accode = dt.replace("+", "")
      $('#hsreids').val(accode).toString()
    }
  }
  chgindex(event) {
    var id = "#" + event
    var hsreid = $('#hsreids').val().toString()
    setTimeout(() => {
      if (hsreid != null && hsreid != undefined) {
        var empc = this.emp.filter(res => res.psnidn == hsreid)
        if (empc != null && empc != undefined) {
          this.nameempcs = empc[0].psnfnm + ' ' + empc[0].psnlnm
          $(id).focus()
          // 
        } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
      } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
    }, 500);


  }
  savemini(hsreids) {
    this.get_datan2(hsreids)
    $('#hsreids').focus()
    this.nameempcs = ''
    $('#hsreids').val('').toString()

  }
  // Deletesemp(e) {
  //   console.log(e)
  //   this.CourseDetailAppserviceServiceProxy.deleteCourseDetail(e.data.id).subscribe(() => {
  //     this.targetDataGridemp.instance.refresh();
  //     
  //   });

  // }


  saveY() {

    this.CourseDetailAppserviceServiceProxy.deleteCourseDetailByDocNo(this.docic).subscribe(() => {
      this.CourseDetailAppserviceServiceProxy.createCourseDetail(this.CreateCourseDetailInputData).subscribe(() => {
        this.close();
        this.notify.info(this.l('Successfully'));
      })
    })
  }

  close2(): void {

    this.modal2.hide();
    this.active = false;
  }

  imported = 0
  importf() {
    this.imported = 1

  }
  onBeforeSend(event): void {
    console.log(event)
    event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
  }
  onUpload(event): void {
    console.log(event);
    this.uploadedFilesexcel = [];
    for (const file of event.files) {
      this.uploadedFilesexcel.push(file);
    }
    //console.log("remote Address: "+AppConsts.remoteServiceBaseUrl);
    setTimeout(() => {
      for (let y = 0; y < this.uploadedFilesexcel.length; y++) {
        //this.atth.push({ 'flename': this.uploadedFilesexcel[y].name, 'flesize': this.uploadedFilesexcel[y].size, 'fletyp': this.uploadedFilesexcel[y].type, 'flepath': AppConsts.remoteServiceBaseUrl + '/DocumentExcel/' + this.uploadedFilesexcel[y].name })
        console.log(AppConsts.remoteServiceBaseUrl + '/excelSalary/' + this.uploadedFilesexcel[y].name)
        this.test2s(AppConsts.remoteServiceBaseUrl + '/excelSalary/' + this.uploadedFilesexcel[y].name)
        //this.PhoneBookComponent.dataurl = AppConsts.remoteServiceBaseUrl + '/excel/' + this.uploadedFilesexcel[y].name
        // this.test2s(AppConsts.remoteServiceBaseUrl + '/excelSalary/' + this.uploadedFilesexcel[y].name)

      }
    }, 1000);

    this.imported = 0
  }

  erorimport = []
  loadPanelimport: boolean;
  progressbarss = ""
  test2s(data: string) {
    // var url : string = "http://testmis.mcu.ac.th:3000/excel/Test.xlsx";
    this.erorimport = []
    console.log(data)
    this.loadPanelimport = true
    for (let i = 0; i < 2; i++) {
      setTimeout(() => {

        var dataa2 = kwanp(data);
        if (i == 1) {
          dataa2.splice(0, 2);
            console.log(dataa2)
            for (let f = 0; f < dataa2.length; f++) {
              var arryHavS = this.CreateCourseDetailInputData.filter(res => res.hsreid == dataa2[f].EmpID)
              if(arryHavS.length<=0){
                  if (dataa2.length == (f + 1 * 1)) {
                    
                    var per = 100 / (dataa2.length)
                    
                   
                    for (let j = 0; j < dataa2.length; j++) {
                      // setTimeout(() => {
                        var pr = (per * (j + 1 * 1)).toFixed(2)
                       
                          this.CreateCourseDetailInput = new CreateCourseDetailInput()
                          this.CreateCourseDetailInput.hsreid = dataa2[j].EmpID.toString()
                          this.CreateCourseDetailInput.hsrcsu = this.Course[0].placod
                          this.CreateCourseDetailInput.hsryea = this.Course[0].playea
                          this.CreateCourseDetailInput.hsrfmd = this.fnyyyymmdd(dataa2[j].StartDate.toString())
                          this.CreateCourseDetailInput.hsrtod = this.fnyyyymmdd(dataa2[j].EndDate.toString())
                          this.CreateCourseDetailInput.hsrfmt = dataa2[j].StartTime.toString()
                          this.CreateCourseDetailInput.hsrtot = dataa2[j].EndTime.toString()
                          this.CreateCourseDetailInput.hsrndy = dataa2[j].AllTime
                          this.CreateCourseDetailInput.hsramT1 = 0
                          
                          this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)
                  
                          if (data.length <= (i + 1 * 1)) {
                            console.log(this.CreateCourseDetailInputData)

                          }
                        
                        
                        
                        ///ใช้
                        // this.progressbarss = pr + "%"
                        // document.getElementById("pbb").style.width = this.progressbarss;
                        // if (dataa2.length <= (j + 1 * 1)) {
                        //   this.loadPanelimport = false
                        //   this.message.success('เสร็จสมบูรณ์', 'Complete')
                        //   this.progressbarss = "0%"
                        //   document.getElementById("pbb").style.width = this.progressbarss;
                        //   this.modalSave.emit(this.CreateUseBudgetHeaderInput);
                        // }

                      // }, 200 * j);
                    }
                  }
                }else{
                  alert('ขออภัย, พบข้อผิดพลาด มีพนักงานอยู่ในระบบแล้ว')
                  this.loadPanelimport = false
                  break
                }
              }
              

            }  
      }, 500 * i);
    }
  }
  templ = 0

}
