import { Component, OnInit, Injector, Output, ViewChild, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CourseAppserviceServiceProxy, CourseListDto, DbmtabServiceProxy, DbmtabListDto, MSMPSN00ServiceProxy, UpdateCourseInput } from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';
import { ModalDirective } from 'ngx-bootstrap';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'selectkpi',
  templateUrl: './selectkpi.component.html'
})
export class selectkpiComponent extends AppComponentBase implements OnInit {
  @ViewChild('modal') public modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  Course: CourseListDto[] = [];
  static DIVISION: any[];
  static TrainingCourse: any[];
  static TrainingPay: any[];
  static dbmtabemti: any[];
  static emp: any[];
  constructor(
    injector: Injector,
    private CourseAppservice: CourseAppserviceServiceProxy,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy

  ) {
    super(injector);

  }

  ngOnInit() {
    this.getTCourse();
    this.getDataMaster()
  }
  yearLbl = ''
  getCourseValue(yearsSr, typ, datet1sr, datet2sr) {

    if (yearsSr != '' && yearsSr != null && yearsSr != undefined && yearsSr != NaN) {
      var year = this.fnyyyy(yearsSr).toString()
      this.yearLbl = 'พ.ศ.' + yearsSr
    } else {
      var year = 'All'
      this.yearLbl = ''
    }

    // if (yearsSr != '' && typ != '') {
    this.CourseAppservice.getTCourseByTypAndDate(year, typ, this.fnyyyymmdd(datet1sr), this.fnyyyymmdd(datet2sr)).subscribe((result) => {
      this.Course = result.items;

    });
    // }else{
    //   this.CourseAppservice.getTCourse(year)
    // }
  }
  getTCourse(): void {
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;

    });
  }
  UpdateCourse: UpdateCourseInput = new UpdateCourseInput();
  DEPARTMENT: DbmtabListDto[] = [];
  DIVISION: DbmtabListDto[] = [];
  TrainingCourse: DbmtabListDto[] = [];
  TrainingPay: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  emp = [];
  getDataMaster() {
    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.DEPARTMENT = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('DIVISION', '').subscribe((result) => {
      this.DIVISION = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
      this.dbmtabemti = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('TrainingCourse', '').subscribe((result) => {
      this.TrainingCourse = result.items;
      $('.m_select2_1a').select2({

      });
    });
    this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
      this.TrainingPay = result.items;

    });
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
      this.emp = result.items;
    });
  }
  show(): void {
    this.modal.show()
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
  savetrue(e) {
    console.log(e)
    var data = e.split(",");
    for (let i = 0; i < data.length; i++) {
      this.CourseAppservice.getTCourse('',data[i], '', '', '', undefined, '').subscribe((result) => {
        this.Course = result.items;
        this.UpdateCourse.id = this.Course[i].id
        this.UpdateCourse.hrschrs = 'Y'
        this.UpdateCourse.pladtefm = this.Course[i].pladtefm
        this.UpdateCourse.pladteto = this.Course[i].pladteto
        this.UpdateCourse.playea = this.Course[i].playea
        this.UpdateCourse.platyp = this.Course[i].platyp
        this.UpdateCourse.pladep = this.Course[i].pladep
        this.UpdateCourse.pladev = this.Course[i].pladev
        this.UpdateCourse.platyP2 = this.Course[i].platyP2
        this.UpdateCourse.plaiou = this.Course[i].plaiou
        this.UpdateCourse.plabrk = this.Course[i].plabrk
        this.UpdateCourse.plahrs = this.Course[i].plahrs
        this.UpdateCourse.plaendtim = this.Course[i].plaendtim
        this.UpdateCourse.plastrtim = this.Course[i].plastrtim
        this.UpdateCourse.planaM4 = this.Course[i].planaM4
        this.UpdateCourse.placare = this.Course[i].placare
        this.UpdateCourse.platgt = this.Course[i].platgt
        this.UpdateCourse.plaseq = this.Course[i].plaseq
        this.UpdateCourse.planaM3 = this.Course[i].planaM3

        
        this.CourseAppservice.updateCourse(this.UpdateCourse).subscribe(() => {
          this.close2()
        });
      });
    }
  }

  DownloadTemp() {
    window.open(AppConsts.remoteServiceBaseUrl + '/Training/Training.xls', '_blank')

  }
  close2() {
    this.modal.hide();

  }
}
