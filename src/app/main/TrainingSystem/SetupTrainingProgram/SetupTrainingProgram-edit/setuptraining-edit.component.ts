import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, CourseAppserviceServiceProxy, CreateCourseInput, DbmtabListDto, CreateDbmtabInput, UpdateCourseInput, MSMPSN00ServiceProxy, UpdateCourseDetailInput, CourseDetailAppserviceServiceProxy, ExpensesAppserviceServiceProxy, ExpensesTmpAppserviceServiceProxy, ExpensesTmpListDto, CreateCourseDetailInput, CourseDetailListDto, CreateExpensesTmpInput, ExpensesListDto, KPShareServiceProxy, GetSumTrnCostList } from '@shared/service-proxies/service-proxies';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { finalize } from 'rxjs/operators';
import { DxDataGridComponent } from 'devextreme-angular';
import { SetuptrainingComponent } from '../setuptraining.component';

@Component({
  selector: 'setuptrainingedit',
  templateUrl: './setuptraining-edit.component.html',
})
export class SetuptrainingEditComponent extends AppComponentBase {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') modal2: ModalDirective;
  @ViewChild('modal3') modal3: ModalDirective;
  @ViewChild('modal4') modal4: ModalDirective;
  @ViewChild('modal5') modal5: ModalDirective;
  @ViewChild('modal_outsourse') modal_outsourse: ModalDirective;


  saving: boolean;
  UpdateCourse: UpdateCourseInput = new UpdateCourseInput();
  DEPARTMENT: DbmtabListDto[] = [];
  DIVISION: DbmtabListDto[] = [];
  TrainingCourse: DbmtabListDto[] = [];
  TrainingPay: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  sumtr: GetSumTrnCostList[] = [];
  dbmtabpos: DbmtabListDto[] = [];

  emp = [];
  array_data_complete = [({
    'sbyear': '', 'sbdep': ''
    , 'sbbudget': '', 'sbtype': ''
  })];




  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseAppserviceServiceProxy: CourseAppserviceServiceProxy,
    private CourseDetailAppserviceServiceProxy: CourseDetailAppserviceServiceProxy,
    private ExpensesAppserviceServiceProxy: ExpensesAppserviceServiceProxy,
    private ExpensesTmpAppserviceServiceProxy: ExpensesTmpAppserviceServiceProxy,
    private SetuptrainingComponent: SetuptrainingComponent,
    private KPShareService: KPShareServiceProxy,


  ) {
    super(injector);

  }


  getDisplayExpr(item) {
    // console.log(item)
    if (!item) {
      return "";
    }
    // console.log(item.psnfnm + " " + item.psnlnm)
    return item.psnfnm + " " + item.psnlnm
  }
  pladepnam;
  ExpensesTmpListDto: ExpensesTmpListDto[] = []
  docRef = ''
  playeaS = ''
  pladtefmS = ''
  pladtetoS = ''
  playtyp = ''
  show(id): void {
    // $("#years").val(moment().add(543, 'years').format('YYYY'));
    // $("#datefm").val(moment().add(543, 'years').format('DD/MM/YYYY'));
    // $("#dateto").val(moment().add(543, 'years').format('DD/MM/YYYY'));

    this.DEPARTMENT = this.SetuptrainingComponent.DEPARTMENT
    this.DIVISION = this.SetuptrainingComponent.DIVISION
    this.TrainingCourse = this.SetuptrainingComponent.TrainingCourse
    this.TrainingPay = this.SetuptrainingComponent.TrainingPay
    this.dbmtabemti = this.SetuptrainingComponent.dbmtabemti
    this.emp = this.SetuptrainingComponent.emp
    if (this.SetuptrainingComponent.empmasterl != 1) {
      this.SetuptrainingComponent.getDataMaster()
  }
    this.dbmtabpos = this.SetuptrainingComponent.dbmtabpos

    this.setzero()
    this.active = true;
    this.modal.show();
    this.array_data_complete = [];




    this.CourseAppserviceServiceProxy.getTCourseById(id).subscribe((result) => {
      this.UpdateCourse = result.items[0]
      this.playeaS = this.UpdateCourse.playea
      this.pladtefmS = this.UpdateCourse.pladtefm
      this.pladtetoS = this.UpdateCourse.pladteto
      this.playtyp = this.UpdateCourse.platyp

      this.UpdateCourse.pladtefm = this.fnddmmyyyy(this.UpdateCourse.pladtefm)
      this.UpdateCourse.pladteto = this.fnddmmyyyy(this.UpdateCourse.pladteto)
      this.UpdateCourse.playea = this.fnyyyy543(this.UpdateCourse.playea).toString()
      this.docRef = result.items[0].placod
      setTimeout(() => {
        $('.m_select2_1a').select2({
          placeholder: "Please Select.."
        });
      }, 250);
      setTimeout(() => {
        this.formatcur()

      }, 100);

      this.CourseDetailAppserviceServiceProxy.getCourseDetail('', this.docRef, '', '', '', '', undefined).subscribe((resultD) => {
        this.UpdateCourseDetailInputData = resultD.items
        this.clearP()
      })

      this.ExpensesTmpAppserviceServiceProxy.getExpensesTmp('', this.docRef, '', '', undefined).subscribe((resultE) => {
        this.ExpensesTmpListDto = resultE.items
        for (let i = 0; i < this.ExpensesTmpListDto.length; i++) {
          this.idexper = (this.idexper + 1 * 1)
          this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.ExpensesTmpListDto[i].empcod, 'ExpID': this.ExpensesTmpListDto[i].empitm, 'ExpAmt': this.ExpensesTmpListDto[i].empamt })

        }
      })
      setTimeout(() => {
        if (this.UpdateCourse.platyp != 'OUT' && this.UpdateCourse.plaiou != '002') {
          this.KPShareService.getSumTrnCost(this.docRef).subscribe((result) => {
            this.sumtr = result
            this.sumtr.forEach(element => {
              this.idexp = (this.idexp + 1 * 1)
              this.Expense.push({ 'id': this.idexp, 'ExpID': element.trnitm, 'ExpAmt': element.trnsumitm })
            });

          })
        }
      }, 200);



    })

  }
  formatcur() {

    $("#plaamt1").val(this.fnformat(this.UpdateCourse.plaamt))

  }
  fnnum1(){
    if ($('#expperamts').val() != '' && $('#expperamts').val() != null && $('#expperamts').val() != undefined) {
      var a = $('#expperamts').val();
      $('#expperamts').val(this.fnformat(a));
    }
  }
 
  fnformat(x) {
    var a = parseFloat((x.toString()).replace(/,/g, ""))
      .toFixed(2)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (a == 'NaN') {
      a = '0.00';
      // this.datas.A1 = '0';
    }
    return a;
  }
  idx;
  setzero() {
    this.ExpPerData = []
    this.ExpPerDataP = []
    this.UpdateCourse = new UpdateCourseInput()
    this.Expense = []
  }
  Delete() {
    this.message.confirm(
      this.l('AreYouSureToDeleteTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.CourseAppserviceServiceProxy.deleteCourse(this.UpdateCourse.id).subscribe(() => {
            this.CourseDetailAppserviceServiceProxy.deleteCourseDetailByDocNo(this.docRef).subscribe(() => {
              this.modalSave.emit()
              this.close()
            })
          })
          this.ExpensesTmpAppserviceServiceProxy.deleteExpensesTmpByDocNo(this.docRef).subscribe(() => {
          })
        }
      })

  }

  RemovedEmp(e) {
    this.ExpPerData = this.ExpPerData.filter(res => res.EmpID != e.data.hsreid)
    this.cancelselectExp()
  }

  RemovedExp(e) {
    console.log(this.ExpPerData)
    console.log(e)
    this.ExpPerData.splice(this.ExpPerData.findIndex(res => res.EmpID == e.data.EmpID && res.ExpID == e.data.ExpID), 1)

    var plaamt = 0
    for (let i = 0; i < this.ExpPerDataP.length; i++) {
      plaamt = (plaamt + parseFloat(parseFloat(this.ExpPerDataP[i].ExpAmt).toFixed(2)))
      if (this.ExpPerDataP.length <= (i + 1 * 1)) {
        this.UpdateCourseDetailInput.hsramT1 = plaamt
      }

    }
    if (this.ExpPerDataP.length <= 0) {
      this.UpdateCourseDetailInput.hsramT1 = 0
    }
  }
  credd12 = 0;
  sumupdate = 0
  sumd2() {
    this.credd12 = 0;
    for (let i = 0; i < this.ExpPerDataP.length; i++) {
      this.credd12 = this.credd12 + this.ExpPerDataP[i].ExpAmt;
    }
  }
  saveExpper(id, amt) {
    var code = this.ExpPerDataP.filter(res => res.ExpID == id)
    if (code.length > 0) {
      alert('ขออภัย, รหัสค่าใช้จ่ายนี้ถูกใช้แล้ว')
    } else {
      this.idexper = (this.idexper + 1 * 1)
      var ExAmt = parseFloat(amt.replace(/,/g, "")).toFixed(2)
      if (this.playtyp == 'OUT' && this.UpdateCourse.plaiou == '002') {
        var empids = this.ExpensesTmpListDto.filter(data => data.empcod == data.empcod)[0]

        this.sumd2()


        if (empids == undefined && empids == null) {
          var idemp = this.UpdateCourseDetailInputData.filter(data => data.hsreid == this.UpdateCourseDetailInput.hsreid)[0]
          console.log(idemp.hsramT1) 
          console.log((parseFloat(amt.replace(/,/g, ""))))

          this.sumupdate = idemp.hsramT1  + (parseFloat(amt.replace(/,/g, "")))

          console.log(this.sumupdate)
          if (this.sumupdate <= 10000) {
            this.ExpPerDataP.push({ 'id': this.idexper, 'EmpID': this.UpdateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
            this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.UpdateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
          } else {
            alert("ค่าใช้จ่ายคุณเกิน 10,0000 บาท")
          }
        }else{
        console.log(empids.empamt)
        console.log(this.credd12)
        console.log(parseFloat(amt.replace(/,/g, "")))

        this.sumupdate = this.credd12 + (parseFloat(amt.replace(/,/g, "")))

        console.log(this.sumupdate)
        if (this.sumupdate <= 10000) {
          this.ExpPerDataP.push({ 'id': this.idexper, 'EmpID': this.UpdateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
          this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.UpdateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })

        }
        else {
          alert("ค่าใช้จ่ายคุณเกิน 10,000 บาท")
        }
      }
      } else {
        this.ExpPerDataP.push({ 'id': this.idexper, 'EmpID': this.UpdateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
        this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.UpdateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })

      }
      $('#expperamts').val('')
      var plaamt = 0
      for (let i = 0; i < this.ExpPerDataP.length; i++) {
        plaamt = (plaamt + parseFloat(parseFloat(this.ExpPerDataP[i].ExpAmt).toFixed(2)))
        if (this.ExpPerDataP.length <= (i + 1 * 1)) {
          this.UpdateCourseDetailInput.hsramT1 = plaamt
        }

      }

    }
  }
  insertd(sbyear, sbdep, sbbudget, sbtype) {
    // $("#panel").slideToggle("slow");
    // this.idx = this.idx + 1
    var typ0 = $('input[id=typ0]:checked').val();

    if (typ0 == 'OUT') {
      this.array_data_complete.push({
        'sbyear': sbyear, 'sbdep': sbdep
        , 'sbbudget': sbbudget, 'sbtype': sbtype = 'OUT'
      })
    }
    if (typ0 == 'IN') {
      this.array_data_complete.push({
        'sbyear': sbyear, 'sbdep': sbdep
        , 'sbbudget': sbbudget, 'sbtype': sbtype = "IN"
      })
    }

  }
  varty;
  show2(): void {
    this.dataGrid5s.instance.clearSelection();
    this.emp = this.SetuptrainingComponent.emp
    this.chk_id = 1
    this.modal.hide();
    this.modal2.show();
    this.clearP()
    $('#outsource').val('001')

    // this.UpdateCourseDetailInput.hsrcsu = this.docRef
    // this.UpdateCourseDetailInput.hsrndy = this.UpdateCourse.plabrk

  }
  fnout(v) {
    if (v == '001') {
      this.chk_id = 1
      this.UpdateCourseDetailInput.hsreid = " "

    } else {
      this.chk_id = 0
      this.UpdateCourseDetailInput.hsreid = "OUTSOURCE"

    }
    // this.UpdateCourseDetailInput.hsreid = v
    // this.UpdateCourseDetailInput.hsreid = "OUTSOURCE"
    // if(this.UpdateCourseDetailInput.hsreid == "OUTSOURCE")
    // {
    //   this.chk_id = 1
    //   this.nameempc = ''
    // }
  }
  createtypetraining: CreateDbmtabInput = new CreateDbmtabInput();

  show3(data): void {

    this.varty = data
    this.modal.hide();
    this.modal3.show();


  }
  Expense = []
  idexp = 0
  saveAMT(id, amt) {
    var code = this.Expense.filter(res => res.ExpID == id)
    if (code.length > 0) {
      alert('ขออภัย, รหัสค่าใช้จ่ายนี้ถูกใช้แล้ว')
    } else {
      this.idexp = (this.idexp + 1 * 1)
      var ExAmt = parseFloat(amt.replace(/,/g, "")).toFixed(2)
      this.Expense.push({ 'id': this.idexp, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
      $('#expam').val('')
      var plaamt = 0
      for (let i = 0; i < this.Expense.length; i++) {
        plaamt = (plaamt + parseFloat(parseFloat(this.Expense[i].ExpAmt).toFixed(2)))
        if (this.Expense.length <= (i + 1 * 1)) {
          this.UpdateCourse.plaamt = plaamt
        }

      }

    }

  }
  save(dtefm, dteto, playea) {
    if (this.ExpPerData.length > 0) {
      this.UpdateCourse.plaamt = this.sum(this.ExpPerData, 'ExpAmt')
    }
    this.UpdateCourse.pladtefm = this.fnyyyymmdd(dtefm)
    this.UpdateCourse.pladteto = this.fnyyyymmdd(dteto)
    this.UpdateCourse.playea = this.fnyyyy(playea).toString()
    this.UpdateCourse.platyp = $('#platyps').val().toString()
    this.UpdateCourse.pladep = $('#pladeps').val().toString()
    this.UpdateCourse.pladev = $('#pladevs').val().toString()
    this.UpdateCourse.platyP2 = $('#platyP2s').val().toString()
    if (this.UpdateCourse.platyp != 'OUT') {
      this.UpdateCourse.plaiou = null
    }
    this.CourseAppserviceServiceProxy.updateCourse(this.UpdateCourse).subscribe(() => {
      this.saveDetail()
      this.saveExpTem()
      this.modalSave.emit()
      this.close()

    })
  }
  CreateExpensesTmpInputObject: CreateExpensesTmpInput = new CreateExpensesTmpInput()
  CreateExpensesTmpInput: CreateExpensesTmpInput[] = []

  saveExpTem() {
    if (this.ExpPerData.length <= 0 && this.Expense.length > 0) {
      var Courselght = this.UpdateCourseDetailInputData.filter(res => res.hsreid != 'OUTSOURCE').length
      for (let j = 0; j < this.Expense.length; j++) {
        var expAmt = this.Expense[j].ExpAmt / Courselght
        for (let i = 0; i < this.UpdateCourseDetailInputData.length; i++) {
          if (this.UpdateCourseDetailInputData[i].hsreid != 'OUTSOURCE') {
            this.CreateExpensesTmpInputObject = new CreateExpensesTmpInput()
            this.CreateExpensesTmpInputObject.empyea = this.UpdateCourse.playea
            this.CreateExpensesTmpInputObject.emptrn = this.docRef
            this.CreateExpensesTmpInputObject.empcod = this.UpdateCourseDetailInputData[i].hsreid
            this.CreateExpensesTmpInputObject.empitm = this.Expense[j].ExpID
            this.CreateExpensesTmpInputObject.empamt = expAmt
            this.CreateExpensesTmpInput.push(this.CreateExpensesTmpInputObject)
          }
          if ((this.UpdateCourseDetailInputData.length <= (i + 1 * 1)) && (this.Expense.length <= (j + 1 * 1))) {
            console.log(this.CreateExpensesTmpInput)
            this.ExpensesTmpAppserviceServiceProxy.deleteExpensesTmpByDocNo(this.docRef).subscribe(() => {


              this.ExpensesTmpAppserviceServiceProxy.createExpensesTmp(this.CreateExpensesTmpInput).subscribe(() => {
              })
            })
          }
        }
      }
      if (this.Expense.length <= 0) {
        this.ExpensesTmpAppserviceServiceProxy.deleteExpensesTmpByDocNo(this.docRef).subscribe(() => {
        })
      }
    }
    else {
      for (let i = 0; i < this.ExpPerData.length; i++) {
        this.CreateExpensesTmpInputObject = new CreateExpensesTmpInput()

        this.CreateExpensesTmpInputObject.empyea = this.UpdateCourse.playea
        this.CreateExpensesTmpInputObject.emptrn = this.docRef
        this.CreateExpensesTmpInputObject.empcod = this.ExpPerData[i].EmpID
        this.CreateExpensesTmpInputObject.empitm = this.ExpPerData[i].ExpID
        this.CreateExpensesTmpInputObject.empamt = this.ExpPerData[i].ExpAmt

        this.CreateExpensesTmpInput.push(this.CreateExpensesTmpInputObject)
        if (this.ExpPerData.length <= (i + 1 * 1)) {
          this.ExpensesTmpAppserviceServiceProxy.deleteExpensesTmpByDocNo(this.docRef).subscribe(() => {

            this.ExpensesTmpAppserviceServiceProxy.createExpensesTmp(this.CreateExpensesTmpInput).subscribe(() => {
            })
          })

        }
      }
      if (this.Expense.length <= 0) {
        this.ExpensesTmpAppserviceServiceProxy.deleteExpensesTmpByDocNo(this.docRef).subscribe(() => {
        })
      }
    }
  }
  varty2;
  Addtype(data): void {

    this.varty2 = data
    this.modal3.hide();
    this.modal4.show();
  }
  savetypetraining() {
    this.message.confirm(
      this.l('AreYouSureToSaveTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.saving = true;
          this.createtypetraining.tabtB1 = 'TrainingCourse'
          this._dbmtabService.createDbmtab(this.createtypetraining)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.notify.info(this.l('SavedSuccessfully'));
              this.modalSave.emit();
              this._dbmtabService.getDbmtabFromTABTB1('TrainingCourse', '').subscribe((result) => {
                this.TrainingCourse = result.items;
                this.SetuptrainingComponent.TrainingCourse = this.TrainingCourse

              });
              this.close4();

            });
        }

      })
  }

  Addpay(): void {
    this.modal.hide();
    this.modal5.show();
  }
  createpaytraining: CreateDbmtabInput = new CreateDbmtabInput();

  savepaytraining() {
    this.message.confirm(
      this.l('AreYouSureToSaveTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.saving = true;
          this.createpaytraining.tabtB1 = 'TrainingPay'
          this._dbmtabService.createDbmtab(this.createpaytraining)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.notify.info(this.l('SavedSuccessfully'));
              this.modalSave.emit();
              this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
                this.TrainingPay = result.items;
                this.SetuptrainingComponent.TrainingPay = this.TrainingPay

              });
              this.close5();

            });
        }

      })
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {

    this.modal.hide();
    this.active = false;
  }
  close2(): void {
    this.modal2.hide();
    this.modal.show();
  }
  close3(): void {
    this.modal3.hide();
    this.modal.show();
  }
  close4(): void {
    this.modal4.hide();
    this.modal3.show();
  }
  close5(): void {
    this.modal4.hide();
    this.modal.show();
  }
  clear1(): void {
    $('#md').find("input").val('').end()

  }
  clear2(): void {
    $('#us').find("input").val('').end()

  }

  saveEmppro(dtefm, dteto, playea) {
    if (this.UpdateCourseDetailInput.hsreid == 'OUTSOURCE') {
      this.UpdateCourseDetailInput.hsryea = this.fnyyyy(playea).toString()
      this.UpdateCourseDetailInput.hsrfmd = this.fnyyyymmdd(dtefm)
      this.UpdateCourseDetailInput.hsrtod = this.fnyyyymmdd(dteto)
      this.UpdateCourseDetailInput.hsrfmt = this.UpdateCourse.plastrtim
      this.UpdateCourseDetailInput.hsrtot = this.UpdateCourse.plaendtim
      this.UpdateCourseDetailInput.hrsunam = this.UpdateCourseDetailInput.hrsunam
      this.UpdateCourseDetailInput.hrstel = this.UpdateCourseDetailInput.hrstel

      if (this.playtyp == 'OUT') {
        this.UpdateCourseDetailInput.hsrtty = this.playtyp + this.UpdateCourse.plaiou
      } else {
        this.UpdateCourseDetailInput.hsrtty = this.playtyp
      }
      // this.UpdateCourseDetailInput.hsrtty = this.playtyp
      this.UpdateCourseDetailInput.hsramT1 = 0
      this.UpdateCourseDetailInputData.push(this.UpdateCourseDetailInput)
      this.clearP()
    } else {

      var arryHav = this.UpdateCourseDetailInputData.filter(res => res.hsreid == this.UpdateCourseDetailInput.hsreid)
      if (arryHav.length <= 0) {
        this.UpdateCourseDetailInput.hsryea = this.fnyyyy(playea).toString()
        this.UpdateCourseDetailInput.hsrfmd = this.fnyyyymmdd(dtefm)
        this.UpdateCourseDetailInput.hsrtod = this.fnyyyymmdd(dteto)
        this.UpdateCourseDetailInput.hsrfmt = this.UpdateCourse.plastrtim
        this.UpdateCourseDetailInput.hsrtot = this.UpdateCourse.plaendtim

        if (this.playtyp == 'OUT') {
          this.UpdateCourseDetailInput.hsrtty = this.playtyp + this.UpdateCourse.plaiou
        } else {
          this.UpdateCourseDetailInput.hsrtty = this.playtyp
        }
        // this.UpdateCourseDetailInput.hsrtty = this.playtyp
        this.UpdateCourseDetailInput.hsramT1 = 0
        this.UpdateCourseDetailInputData.push(this.UpdateCourseDetailInput)
        this.clearP()
      } else {
        alert('ขออภัย, มีรหัสพนักงานนี้แล้ว')
      }
    }
  }

  UpdateCourseDetailInput: CreateCourseDetailInput = new CreateCourseDetailInput()

  UpdateCourseDetailInputData: CreateCourseDetailInput[] = []
  saveDetail() {
    if (this.ExpPerData.length <= 0 && this.Expense.length > 0) {
      var expper = this.UpdateCourse.plaamt / this.UpdateCourseDetailInputData.filter(res => res.hsreid != 'OUTSOURCE').length
      for (let i = 0; i < this.UpdateCourseDetailInputData.length; i++) {
        if (this.UpdateCourseDetailInputData[i].hsreid != 'OUTSOURCE') {
          this.UpdateCourseDetailInputData[i].hsramT1 = expper
        }
        else {
          this.UpdateCourseDetailInputData[i].hsramT1 = 0

        }
        if (this.UpdateCourseDetailInputData.length <= (i + 1 * 1)) {
          this.CourseDetailAppserviceServiceProxy.deleteCourseDetailByDocNo(this.docRef).subscribe(() => {


            this.CourseDetailAppserviceServiceProxy.createCourseDetail(this.UpdateCourseDetailInputData).subscribe(() => {
              this.UpdateCourseDetailInput.hrstel = this.UpdateCourseDetailInput.hrstel

            })
          })
        }
      }
    } else {

      this.CourseDetailAppserviceServiceProxy.deleteCourseDetailByDocNo(this.docRef).subscribe(() => {
        this.CourseDetailAppserviceServiceProxy.createCourseDetail(this.UpdateCourseDetailInputData).subscribe(() => {
        })
      })
    }
  }
  /////////////////////////MODAL3:H//////////////////////////////
  show5() {
    this.modal5.show();
    this.emp = this.SetuptrainingComponent.emp

  }
  nameempc
  @ViewChild("gEmployees") gEmployeea: DxDataGridComponent
  get_datan(e) {
    // this.UpdateCourseDetailInput.hsreid = datan
    // var empc = this.emp.filter(res => res.psnidn == datan)
    // this.nameempc = empc[0].psnfnm + ' ' + empc[0].psnlnm

    // this.closeemp();
    console.log(e)
    var data = e.split(",");
    // if (data.length == 1) {
    //   this.CreateCourseDetailInput.hsreid = data[0]
    //   var empc = this.emp.filter(res => res.psnidn == data[0])
    //   this.nameempc = empc[0].psnfnm + ' ' + empc[0].psnlnm
    // }
    // this.CreateCourseDetailInput.hsreid = datan
    // var empc = this.emp.filter(res => res.psnidn == datan)
    // this.nameempc = empc[0].psnfnm + ' ' + empc[0].psnlnm

    // this.closeemp();

    ///////
    var hrs = this.UpdateCourse.plahrs
    for (let i = 0; i < data.length; i++) {
      var arryHav = this.UpdateCourseDetailInputData.filter(res => res.hsreid == data[i])
      if (arryHav.length <= 0) {
        this.UpdateCourseDetailInput = new CreateCourseDetailInput()
        this.UpdateCourseDetailInput.hsreid = data[i]
        this.UpdateCourseDetailInput.hrsunam = this.emp.filter(r => r.psnidn == data[i])[0].psnfnm + ' ' + this.emp.filter(r => r.psnidn == data[i])[0].psnlnm;
        this.UpdateCourseDetailInput.hsrcsu = this.docRef
        this.UpdateCourseDetailInput.hsryea = this.playeaS
        this.UpdateCourseDetailInput.hsrfmd = this.pladtefmS
        this.UpdateCourseDetailInput.hsrtod = this.pladtetoS
        this.UpdateCourseDetailInput.hsrfmt = this.UpdateCourse.plastrtim
        this.UpdateCourseDetailInput.hsrtot = this.UpdateCourse.plaendtim

        if (this.playtyp == 'OUT') {
          this.UpdateCourseDetailInput.hsrtty = this.playtyp + this.UpdateCourse.plaiou
        } else {
          this.UpdateCourseDetailInput.hsrtty = this.playtyp
        }
        // this.UpdateCourseDetailInput.hsrtty = this.playtyp
        this.UpdateCourseDetailInput.hsrndy = hrs
        this.UpdateCourseDetailInput.hsramT1 = 0
        this.UpdateCourseDetailInputData.push(this.UpdateCourseDetailInput)
      }
      if (data.length <= (i + 1 * 1)) {

        this.closeemp();
      }
    }
  }
  closeemp() {
    this.gEmployeea.instance.clearSelection()
    this.modal5.hide();
  }


  kuacc(dt) {
    this.UpdateCourseDetailInput.hrsunam = ''
    if (dt.indexOf("+") >= 0) {
      var accode = dt.replace("+", "")
      this.UpdateCourseDetailInput.hsreid = accode
    }
  }
  nameser2

  chk_id = 0
  chgindex(hsreid) {
    if (this.UpdateCourseDetailInput.hsreid == "outsource") {
      this.chk_id = 1
    } else {
      setTimeout(() => {
        var empc = this.emp.filter(res => res.psnidn == hsreid)
        this.UpdateCourseDetailInput.hrsunam = empc[0].psnfnm + ' ' + empc[0].psnlnm
        this.nameser2 = empc[0].psnfnm + ' ' + empc[0].psnlnm
      }, 500);
    }
  }


  ExpPerData = []
  idexper = 0
  ExpPerDataP = []
  selectExp(e) {
    this.ExpPerDataP = []
    console.log(e)
    this.UpdateCourseDetailInput = e.selectedRowKeys[0]
    this.ExpPerDataP = this.ExpPerData.filter(res => res.EmpID == this.UpdateCourseDetailInput.hsreid)
    if (this.UpdateCourseDetailInput.hsreid != 'OUTSOURCE') {
      $("#outsource").val('001')
      this.chk_id = 1
    } else {
      $("#outsource").val('OUTSOURCE')
      this.chk_id = 0
    }

    this.chgindex(e.selectedRowKeys[0].hsreid)
  }
  @ViewChild("targetDataGrid2s") dataGrid5s: DxDataGridComponent
  cancelselectExp() {
    this.dataGrid5s.instance.clearSelection();
    this.clearP()
  }
  clearP() {
    this.UpdateCourseDetailInput = new CreateCourseDetailInput()
    this.UpdateCourseDetailInput.hsrcsu = this.docRef
    this.UpdateCourseDetailInput.hsrndy = this.UpdateCourse.plahrs
    this.UpdateCourseDetailInput.hrsunam = ''

  }
  plabrk = 1
  keybrk(v) {
    this.plabrk = v
  }
  fncaltime(d1, d2) {
    var dtetime = (((parseFloat(this.UpdateCourse.plaendtim) * 1) - (parseFloat(this.UpdateCourse.plastrtim) * 1)) / 100) - (this.plabrk * 1)
    var today
    var tomor
    today = new Date(this.fninyyyymmdd(d1));
    tomor = new Date(this.fninyyyymmdd(d2));
    var diff = Date.parse(tomor) - Date.parse(today);
    var dte = Math.floor(diff / 86400000);
    if (dte == 0) {
      this.UpdateCourse.plabrk = (this.plabrk * (dte + 1 * 1))
      this.UpdateCourse.plahrs = dtetime
    } else {
      this.UpdateCourse.plabrk = (this.plabrk * (dte + 1 * 1))
      this.UpdateCourse.plahrs = dtetime * (dte + 1 * 1)
    }

  }
}
