import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, CourseAppserviceServiceProxy, DbmrunServiceProxy, CreateCourseInput, DbmtabListDto, CreateDbmtabInput, UpdateCourseDetailInput, CourseDetailAppserviceServiceProxy, CreateCourseDetailInput, MSMPSN00ServiceProxy, CreateExpensesTmpInput, ExpensesTmpAppserviceServiceProxy, KPShareServiceProxy, CourseListDto, CourseDetailListDto } from '@shared/service-proxies/service-proxies';

import { finalize } from 'rxjs/operators';
import { DxDataGridComponent } from 'devextreme-angular';
import { DatePipe } from '@angular/common';
import { SetuptrainingComponent } from '../setuptraining.component';
import { workerData } from 'worker_threads';

import { ReportHistoryOJTComponent } from '../../SummaryReport/ReportEmpHistoryOJT/report-historyojt.component';

@Component({
  selector: 'setuptrainingadd',
  templateUrl: './setuptraining-add.component.html',
})
export class SetuptrainingAddComponent extends AppComponentBase {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') modal2: ModalDirective;
  @ViewChild('modal3') modal3: ModalDirective;
  @ViewChild('modal4') modal4: ModalDirective;
  @ViewChild('modal5') modal5: ModalDirective;
  @ViewChild('modal6') modal6: ModalDirective;
  @ViewChild('modal_outsourse') modal_outsourse: ModalDirective;

  saving: boolean;
  createcource: CreateCourseInput = new CreateCourseInput();
  course: CourseListDto[] = [];
  DEPARTMENT: DbmtabListDto[] = [];
  DIVISION: DbmtabListDto[] = [];
  TrainingCourse: DbmtabListDto[] = [];
  TrainingPay: DbmtabListDto[] = [];
  MAPCOURSETYPE: DbmtabListDto[] = [];
  emp = [];
  array_data_complete = [({
    'sbyear': '', 'sbdep': ''
    , 'sbbudget': '', 'sbtype': ''
  })];
  filterTab: string = 'POSITION';
  dbmtab: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  dbmtabdep: DbmtabListDto[] = [];
  dbmtabpay: DbmtabListDto[] = [];
  dbmtabpos: DbmtabListDto[] = [];
  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private CourseAppserviceServiceProxy: CourseAppserviceServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseDetailAppserviceServiceProxy: CourseDetailAppserviceServiceProxy,
    private ExpensesTmpAppserviceServiceProxy: ExpensesTmpAppserviceServiceProxy,
    private KPShare: KPShareServiceProxy,
    private Dbmrun: DbmrunServiceProxy,
    private SetuptrainingComponent: SetuptrainingComponent,


  ) {
    super(injector);

  }

  // getEmployees(): void {
  //   this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
  //     this.emp = result.items;
  //   });
  // }
  getDisplayExpr(item) {
    // console.log(item)

    if (!item) {
      return "";
    }
    // console.log(item.psnfnm + " " + item.psnlnm)
    return item.psnfnm + " " + item.psnlnm


  }
  pladepnam;
  chk_type = '0'
  chk;
  chk_year = 0
  show(): void {
    this.active = true;
    setTimeout(() => {
      this.modal.show();
      $('#datefm').val(this.fnddmmyyyy(this.pipe.transform(Date(), 'yyyyMMdd')))
      $('#dateto').val(this.fnddmmyyyy(this.pipe.transform(Date(), 'yyyyMMdd')))
      // $('#playea').val(this.fnyyyy543(this.pipe.transform(Date(), 'yyyy')))
    }, 100);
    this.setzero()
    this.disabInput = 0
    this.createcource.hrsnum = 0
    this.array_data_complete = [];
    this.createcource.plabrk = 1;
    this.chk_type = '0'
    this.chk_year = 1
    $('#md').find("input").val('').end()
    this.DEPARTMENT = this.SetuptrainingComponent.DEPARTMENT
    this.DIVISION = this.SetuptrainingComponent.DIVISION
    this.TrainingCourse = this.SetuptrainingComponent.TrainingCourse
    this.TrainingPay = this.SetuptrainingComponent.TrainingPay
    this.dbmtabemti = this.SetuptrainingComponent.dbmtabemti
    if (this.SetuptrainingComponent.empmasterl != 1) {
      this.SetuptrainingComponent.getDataMaster()
  }
    this.dbmtabpos = this.SetuptrainingComponent.dbmtabpos
    setTimeout(() => {
      $('.m_select2_1a').select2({
        placeholder: "Please Select.."
      });
    }, 250);
    $(document).ready(function () {
      $('#platyp').change(function () {
        $('#platypChg').click()
      });
    })
    $(document).ready(function () {
      $('#playea').change(function () {
        $('#playearChg').click()
      });
    })
    setTimeout(() => {
      this.fnnum()
    }, 500);
    //   $("#kt_inputmask_5").inputmask({
    //     "mask": "9",
    //     "repeat": 4,
    //     "greedy": false
    // });
  }

 
  fnnum() {
    if ($('#plaamt').val() != '' && $('#plaamt').val() != null && $('#plaamt').val() != undefined) {
      var a = $('#plaamt').val();
      $('#plaamt').val(this.fnformat(a));
    }
    if ($('#expam').val() != '' && $('#expam').val() != null && $('#expam').val() != undefined) {
      var a = $('#expam').val();
      $('#expam').val(this.fnformat(a));
    }
  

  }
  fnnum1(){
    if ($('#expperamt').val() != '' && $('#expperamt').val() != null && $('#expperamt').val() != undefined) {
      var a = $('#expperamt').val();
      $('#expperamt').val(this.fnformat(a));
    }
  }
  fnformat(x) {
    var a = parseFloat((x.toString()).replace(/,/g, ""))
      .toFixed(2)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (a == 'NaN') {
      a = '0.00';
      // this.datas.A1 = '0';
    }
    return a;
  }
  fnchgyear(y) {
    if (y == '' || y == null || y == undefined) {
      this.chk_year = 1
    }
    else {
      this.chk_year = 0
    }
  }
  fnchgtyp(v) {
    this.createcource.platyp = v
  }
  chkvalu(ye) {
    alert(ye)
   
  }
  idx;
  insertd(sbyear, sbdep, sbbudget, sbtype) {
    // $("#panel").slideToggle("slow");
    // this.idx = this.idx + 1
    var typ0 = $('input[id=typ0]:checked').val();

    if (typ0 == 'OUT') {
      this.array_data_complete.push({
        'sbyear': sbyear, 'sbdep': sbdep
        , 'sbbudget': sbbudget, 'sbtype': sbtype = 'OUT'
      })
    }
    if (typ0 == 'IN') {
      this.array_data_complete.push({
        'sbyear': sbyear, 'sbdep': sbdep
        , 'sbbudget': sbbudget, 'sbtype': sbtype = "IN"
      })
    }

  }
  varty;

  createtypetraining: CreateDbmtabInput = new CreateDbmtabInput();

  show3(data): void {

    this.varty = data
    this.modal.hide();
    this.modal3.show();


  }
  Expense = []
  idexp = 0
  saveAMT(id, amt) {
    var code = this.Expense.filter(res => res.ExpID == id)
    if (code.length > 0) {
      alert('ขออภัย, รหัสค่าใช้จ่ายนี้ถูกใช้แล้ว')
    } else {
      this.idexp = (this.idexp + 1 * 1)
      var ExAmt = parseFloat(amt.replace(/,/g, "")).toFixed(2)
      this.Expense.push({ 'id': this.idexp, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
      $('#expam').val('')
      var plaamt = 0
      for (let i = 0; i < this.Expense.length; i++) {
        plaamt = (plaamt + parseFloat(parseFloat(this.Expense[i].ExpAmt).toFixed(2)))
        if (this.Expense.length <= (i + 1 * 1)) {
          this.createcource.plaamt = plaamt
          this.fnnum()
        }
      }
    }


  }

  save(dtefm, dteto, playea) {
    if (this.ExpPerData.length > 0) {
      this.createcource.plaamt = this.sum(this.ExpPerData, 'ExpAmt')
    }
    if (this.createcource.placod != '' && this.createcource.placod != null && this.createcource.placod != undefined) {
      this.createcource.pladtefm = this.fnyyyymmdd(dtefm)
      this.createcource.pladteto = this.fnyyyymmdd(dteto)
      this.createcource.playea = this.fnyyyy(playea).toString()
      this.createcource.platyp = $('#platyp').val().toString()
      this.createcource.pladep = $('#pladep').val().toString()
      this.createcource.pladev = $('#pladev').val().toString()
      this.createcource.platyP2 = $('#platyP2').val().toString()
      if (this.createcource.platyp != 'OUT') {
        this.createcource.plaiou = null
      }
      this.CourseAppserviceServiceProxy.createCourse(this.createcource).subscribe(() => {

        this.saveDetail()
        this.saveExpTem()
        this.modalSave.emit()
        this.close()
      })
    }
    else {
      var cod = this.corseget.filter(res => res.placod == this.createcource.placod && res.plaseq == this.createcource.plaseq)
      if (playea != '' && playea != null && playea != undefined && this.createcource.platyp != '' && this.createcource.platyp != null && this.createcource.platyp != undefined
        && this.createcource.plaseq != null && this.createcource.plaseq != undefined && this.createcource.platyP2 != '' && this.createcource.platyP2 != null && this.createcource.platyP2 != undefined) {
        this.message.confirm(
          this.l('คุณต้องการบันทึกข้อมูล'),
          this.l('AreYouSure'),
          isConfirmed => {
            if (isConfirmed) {

              if (cod.length <= 0) {
                this.KPShare.getDocumentNumber($('#platyp').val().toString(), this.fnyyyymmdd(dtefm), "THI").subscribe((result) => {
                  this.doc_ss = result
                  this.docsucess = this.doc_ss[0].documentNumber;
                  this.createcource.placod = this.docsucess
                  this.createcource.pladtefm = this.fnyyyymmdd(dtefm)
                  this.createcource.pladteto = this.fnyyyymmdd(dteto)
                  this.createcource.playea = this.fnyyyy(playea).toString()
                  this.createcource.platyp = $('#platyp').val().toString()
                  this.createcource.pladep = $('#pladep').val().toString()
                  this.createcource.pladev = $('#pladev').val().toString()
                  this.createcource.platyP2 = $('#platyP2').val().toString()
                  this.CourseAppserviceServiceProxy.createCourse(this.createcource).subscribe(() => {

                    this.saveDetail()
                    this.saveExpTem()
                    this.modalSave.emit()
                    this.close()
                  })
                });
              }
            }
            // } else {
            //   this.message.warn('คุณไม่สามารถบันทึกได้ กรุณาระบุวันที่ฝึกอบรม')
            // }
          }

        );
      } else {

        this.message.warn('กรุณาระบุปี ประเภท รุ่นที่ และประเภทวิทยากร')
      }
    }
  }

  varty2;
  Addtype(data): void {

    this.varty2 = data
    this.modal3.hide();
    this.modal4.show();
  }
  savetypetraining() {
    this.message.confirm(
      this.l('AreYouSureToSaveTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.saving = true;
          this.createtypetraining.tabtB1 = 'TrainingCourse'
          this._dbmtabService.createDbmtab(this.createtypetraining)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.notify.info(this.l('SavedSuccessfully'));
              this.modalSave.emit();
              this._dbmtabService.getDbmtabFromTABTB1('TrainingCourse', '').subscribe((result) => {
                this.TrainingCourse = result.items;
                this.SetuptrainingComponent.TrainingCourse = this.TrainingCourse

              });
              this.close4();

            });
        }

      })
  }

  Addpay(): void {
    this.modal.hide();
    this.modal5.show();
  }
  createpaytraining: CreateDbmtabInput = new CreateDbmtabInput();

  savepaytraining() {
    this.message.confirm(
      this.l('AreYouSureToSaveTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.saving = true;
          this.createpaytraining.tabtB1 = 'TrainingPay'
          this._dbmtabService.createDbmtab(this.createpaytraining)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
              this.notify.info(this.l('SavedSuccessfully'));
              this.modalSave.emit();
              this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
                this.TrainingPay = result.items;
                this.SetuptrainingComponent.TrainingPay = this.TrainingPay

              });
              this.close5();

            });
        }

      })
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {

    this.modal.hide();
    this.active = false;
  }
  close2(y): void {
    this.modal2.hide();
    this.modal.show();
    this.chk_year = 1
  }
  close3(): void {
    this.modal3.hide();
    this.modal.show();
  }
  close4(): void {
    this.modal4.hide();
    this.modal3.show();
  }
  close5(): void {
    this.modal4.hide();
    this.modal.show();
  }
  clear1(): void {
    $('#md').find("input").val('').end()

  }
  clear2(): void {
    $('#us').find("input").val('').end()

  }
  setzero() {
    this.ExpPerData = []
    this.ExpPerDataP = []
    this.createcource = new CreateCourseInput()
    this.CreateCourseDetailInputData = []
    this.Expense = []
  }
  ///////////////////////////////////////////////////////////////////////////////
  pipe = new DatePipe('en-US');
  doc_ss
  docsucess
  playeaS = ''
  pladtefmS = ''
  pladtetoS = ''
  disabInput = 0
  playtyp = ''
  CreateCourseDetailCheck: CreateCourseDetailInput[] = []
  coursedetail: CourseDetailListDto[] = []
  show2(playea, platyp, pladtefm, pladteto): void {
    this.chk_id = 1

    var cod = this.corseget.filter(res => res.placod == this.createcource.placod && res.plaseq == this.createcource.plaseq)
    // var seq = this.course.filter(res => res.plaseq == res.plaseq)
    setTimeout(() => {
      if (cod.length <= 0) {
        if (this.createcource.placod != '' && this.createcource.placod != null && this.createcource.placod != undefined
          && this.createcource.plaseq != null && this.createcource.plaseq != undefined && this.createcource.platyP2 != '' && this.createcource.platyP2 != null && this.createcource.platyP2 != undefined) {
          this.modal.hide();
          this.modal2.show();
          this.playeaS = this.fnyyyy(playea).toString()
          this.pladtefmS = this.fnyyyymmdd(pladtefm)
          this.pladtetoS = this.fnyyyymmdd(pladteto)
          this.playtyp = platyp
          this.clearP()
        }
        else {

          if (playea != '' && playea != null && playea != undefined && platyp != '' && platyp != null && platyp != undefined
            && this.createcource.plaseq != null && this.createcource.plaseq != undefined) {

            this.message.confirm(
              this.l('คุณต้องการสร้างเลขที่รหัสหลักสูตร'),
              this.l('AreYouSure'),
              isConfirmed => {
                if (isConfirmed) {
                  this.playeaS = this.fnyyyy(playea).toString()
                  this.pladtefmS = this.fnyyyymmdd(pladtefm)
                  this.pladtetoS = this.fnyyyymmdd(pladteto)
                  this.playtyp = platyp
                  if (this.playtyp == 'OUT' && this.createcource.plaiou == '002') {
                    //ค้นหาจาก typ กับ ปี
                    console.log('เพิ่มพูนทักษะ/สนใจ')
                    this.CourseDetailAppserviceServiceProxy.getCourseDetailBy(this.fnyyyy(playea).toString(), 'OUT2').subscribe((result) => {
                      this.CreateCourseDetailCheck = result.items


                    })
                  } else
                    if (this.playtyp == 'OUT' && this.createcource.plaiou == '001') {
                      console.log('นโยบาย/คำสั่ง')
                      this.CourseDetailAppserviceServiceProxy.getCourseDetailBy(this.fnyyyy(playea).toString(), 'OUT1').subscribe((redt) => {
                        this.CreateCourseDetailCheck = redt.items


                      })
                    }

                  this.KPShare.getDocumentNumber(platyp, this.fnyyyymmdd(pladtefm), "THI").subscribe((result) => {
                    this.doc_ss = result
                    this.docsucess = this.doc_ss[0].documentNumber;
                    this.createcource.placod = this.docsucess
                    this.notify.success(this.l('Successfully'));


                    this.modal.hide();
                    this.modal2.show();
                    this.disabInput = 1
                    this.CreateCourseDetailInput = new CreateCourseDetailInput()
                    this.CreateCourseDetailInput.hsrcsu = this.docsucess
                    this.CreateCourseDetailInput.hsrndy = this.createcource.plahrs
                  });


                  this.CourseDetailAppserviceServiceProxy.getCourseDetail('', '', '', '', '', '', undefined).subscribe((result) => {
                    this.coursedetail = result.items;
                  });
                }
              }
            );

          } else {

            this.message.warn('กรุณาระบุปี ประเภท รุ่นที่ ประเภทวิทยากร')
          }
        }
      } else {
        this.message.warn('คุณไม่สามารถบันทึกได้ กรุณาระบุรุ่นที่')
      }
    }, 500);
  }
  plabrk = 1
  keybrk(v) {
    this.plabrk = v
  }
  chk_id = 0
  fnout(v) {
    if (v == '001') {
      this.chk_id = 1
      this.CreateCourseDetailInput.hsreid = " "

    } else {
      this.chk_id = 0
      this.CreateCourseDetailInput.hsreid = "OUTSOURCE"

    }
  }
  fncaltime(d1, d2) {
    var dtetime = (((parseInt(this.createcource.plaendtim) * 1) - (parseInt(this.createcource.plastrtim) * 1)) / 100) - (this.plabrk * 1)
    var today
    var tomor
    today = new Date(this.fninyyyymmdd(d1));
    tomor = new Date(this.fninyyyymmdd(d2));
    var diff = Date.parse(tomor) - Date.parse(today);
    var dte = Math.floor(diff / 86400000);
    if (dte == 0) {
      this.createcource.plabrk = (this.plabrk * (dte + 1 * 1))
      this.createcource.plahrs = dtetime
    } else {
      this.createcource.plabrk = (this.plabrk * (dte + 1 * 1))
      this.createcource.plahrs = dtetime * (dte + 1 * 1)
    }

  }
  //////////////////////////////////////MODAL2:DETAIL///////////////////////////////
  CreateCourseDetailInput: CreateCourseDetailInput = new CreateCourseDetailInput()
  CreateCourseDetailInputData: CreateCourseDetailInput[] = []
  empid = ''
  ExpPerData = []
  ExpPerDataP = []
  idexper = 0

  selectExp(e) {
    this.ExpPerDataP = []
    console.log(e)
    this.CreateCourseDetailInput = e.selectedRowKeys[0]
    this.ExpPerDataP = this.ExpPerData.filter(res => res.EmpID == this.CreateCourseDetailInput.hsreid)
    if (this.CreateCourseDetailInput.hsreid != 'OUTSOURCE') {
      $("#outsource1").val('001')
      this.chk_id = 1
    } else {
      $("#outsource1").val('OUTSOURCE')
      this.chk_id = 0
    }
    this.chgindex(e.selectedRowKeys[0].hsreid)

  }
  @ViewChild("targetDataGrid2") dataGrid5: DxDataGridComponent
  cancelselectExp() {
    this.dataGrid5.instance.clearSelection();
    this.clearP()
  }
  saveEmppro(dtefm, dteto, playea) {
    if (this.CreateCourseDetailInput.hsreid == 'OUTSOURCE') {
      this.CreateCourseDetailInput.hsryea = this.fnyyyy(playea).toString()
      this.CreateCourseDetailInput.hsrfmd = this.fnyyyymmdd(dtefm)
      this.CreateCourseDetailInput.hsrtod = this.fnyyyymmdd(dteto)
      this.CreateCourseDetailInput.hsrfmt = this.createcource.plastrtim
      this.CreateCourseDetailInput.hsrtot = this.createcource.plaendtim
      this.CreateCourseDetailInput.hrsunam = this.CreateCourseDetailInput.hrsunam
      this.CreateCourseDetailInput.hrstel = this.CreateCourseDetailInput.hrstel

      if (this.playtyp == 'OUT') {
        this.CreateCourseDetailInput.hsrtty = this.playtyp + this.createcource.plaiou
      } else {
        this.CreateCourseDetailInput.hsrtty = this.playtyp
      }

      this.CreateCourseDetailInput.hsramT1 = 0
      this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)
      this.clearP()
    } else {
      var arryHav = this.CreateCourseDetailInputData.filter(res => res.hsreid == this.CreateCourseDetailInput.hsreid)
      if (arryHav.length <= 0) {
        this.CreateCourseDetailInput.hsryea = this.fnyyyy(playea).toString()
        this.CreateCourseDetailInput.hsrfmd = this.fnyyyymmdd(dtefm)
        this.CreateCourseDetailInput.hsrtod = this.fnyyyymmdd(dteto)
        this.CreateCourseDetailInput.hsrfmt = this.createcource.plastrtim
        this.CreateCourseDetailInput.hsrtot = this.createcource.plaendtim
        if (this.playtyp == 'OUT') {
          this.CreateCourseDetailInput.hsrtty = this.playtyp + this.createcource.plaiou
        } else {
          this.CreateCourseDetailInput.hsrtty = this.playtyp
        }

        this.CreateCourseDetailInput.hsramT1 = 0
        this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)
        this.clearP()
      } else {
        alert('ขออภัย, มีรหัสพนักงานนี้แล้ว')
      }
    }
  }
  clearP() {
    this.CreateCourseDetailInput = new CreateCourseDetailInput()
    this.CreateCourseDetailInput.hsrcsu = this.createcource.placod
    this.CreateCourseDetailInput.hsrndy = this.createcource.plahrs
    this.CreateCourseDetailInput.hrsunam = ''
  }
  saveDetail() {
    if (this.ExpPerData.length <= 0 && this.Expense.length > 0) {
      var expper = this.createcource.plaamt / this.CreateCourseDetailInputData.filter(res => res.hsreid != 'OUTSOURCE').length
      for (let i = 0; i < this.CreateCourseDetailInputData.length; i++) {
        if (this.CreateCourseDetailInputData[i].hsreid != 'OUTSOURCE') {
          this.CreateCourseDetailInputData[i].hsramT1 = expper
        } else {
          this.CreateCourseDetailInputData[i].hsramT1 = 0
        }
        if (this.CreateCourseDetailInputData.length <= (i + 1 * 1)) {
          this.CourseDetailAppserviceServiceProxy.createCourseDetail(this.CreateCourseDetailInputData).subscribe(() => {
            this.CreateCourseDetailInput.hrstel = this.CreateCourseDetailInput.hrstel
          })
        }
      }
    } else {
      this.CourseDetailAppserviceServiceProxy.createCourseDetail(this.CreateCourseDetailInputData).subscribe(() => {

      })
    }
  }
  CreateExpensesTmpInputObject: CreateExpensesTmpInput = new CreateExpensesTmpInput()
  CreateExpensesTmpInput: CreateExpensesTmpInput[] = []

  saveExpTem() {
    if (this.ExpPerData.length <= 0 && this.Expense.length > 0) {

      var Courselght = this.CreateCourseDetailInputData.filter(res => res.hsreid != 'OUTSOURCE').length
      for (let j = 0; j < this.Expense.length; j++) {
        var expAmt = this.Expense[j].ExpAmt / Courselght
        for (let i = 0; i < this.CreateCourseDetailInputData.length; i++) {
          if (this.CreateCourseDetailInputData[i].hsreid != 'OUTSOURCE') {
            this.CreateExpensesTmpInputObject = new CreateExpensesTmpInput()
            this.CreateExpensesTmpInputObject.empyea = this.createcource.playea
            this.CreateExpensesTmpInputObject.emptrn = this.createcource.placod
            this.CreateExpensesTmpInputObject.empcod = this.CreateCourseDetailInputData[i].hsreid
            this.CreateExpensesTmpInputObject.empitm = this.Expense[j].ExpID
            this.CreateExpensesTmpInputObject.empamt = expAmt
            this.CreateExpensesTmpInput.push(this.CreateExpensesTmpInputObject)
          }
          if ((this.CreateCourseDetailInputData.length <= (i + 1 * 1)) && (this.Expense.length <= (j + 1 * 1))) {
            console.log(this.CreateExpensesTmpInput)
            this.ExpensesTmpAppserviceServiceProxy.createExpensesTmp(this.CreateExpensesTmpInput).subscribe(() => {
            })
          }

        }
      }

    }
    else {
      for (let i = 0; i < this.ExpPerData.length; i++) {
        this.CreateExpensesTmpInputObject = new CreateExpensesTmpInput()

        this.CreateExpensesTmpInputObject.empyea = this.createcource.playea
        this.CreateExpensesTmpInputObject.emptrn = this.createcource.placod
        this.CreateExpensesTmpInputObject.empcod = this.ExpPerData[i].EmpID
        this.CreateExpensesTmpInputObject.empitm = this.ExpPerData[i].ExpID
        this.CreateExpensesTmpInputObject.empamt = this.ExpPerData[i].ExpAmt
        // sumExpen = (sumExpen+this.CreateExpensesTmpInputObject.empamt*1)
        // this.CreateExpensesInputTS = this.CreateExpensesInput.filter(res=>res.payItmCod == this.ExpPerData[i].ExpID)
        // if(this.CreateExpensesInputTS.length <= 0){

        //   this.CreateExpensesInput.push({'ExpID':this.ExpPerData[i].ExpID,'ExpAmt':parseFloat(this.ExpPerData[i].ExpAmt)})
        // }else{
        //   data[0].ExpAmt = (parseFloat(data[0].ExpAmt)+this.ExpPerData[i].ExpAmt*1)
        // }

        this.CreateExpensesTmpInput.push(this.CreateExpensesTmpInputObject)
        if (this.ExpPerData.length <= (i + 1 * 1)) {

          this.ExpensesTmpAppserviceServiceProxy.createExpensesTmp(this.CreateExpensesTmpInput).subscribe(() => {

          })

        }
      }
    }
  }
  credd12 = 0;
  sumd2() {
    this.credd12 = 0;
    for (let i = 0; i < this.ExpPerDataP.length; i++) {
      this.credd12 = this.credd12 + this.ExpPerDataP[i].ExpAmt;
    }
  }
  sumamt = 0
  saveExpper(id, amt) {
    var code = this.ExpPerDataP.filter(res => res.ExpID == id)
    if (code.length > 0) {
      alert('ขออภัย, รหัสค่าใช้จ่ายนี้ถูกใช้แล้ว')
    } else {
      this.idexper = (this.idexper + 1 * 1)
      var ExAmt = parseFloat(amt.replace(/,/g, "")).toFixed(2)

      if (this.playtyp == 'OUT' && this.createcource.plaiou == '002') {
        var empids = this.CreateCourseDetailCheck.filter(data => data.hsreid == this.CreateCourseDetailInput.hsreid)[0]
        this.sumd2()
       
        setTimeout(() => {
          if (empids == undefined && empids == null) {
            var idemp = this.CreateCourseDetailInputData.filter(data => data.hsreid == this.CreateCourseDetailInput.hsreid)[0]
            console.log(idemp.hsramT1) 
            console.log((parseFloat(amt.replace(/,/g, ""))))

            this.sumamt = idemp.hsramT1  + (parseFloat(amt.replace(/,/g, "")))

            console.log(this.sumamt)
            if (this.sumamt <= 10000) {
              this.ExpPerDataP.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
              this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
            } else {
              alert("ค่าใช้จ่ายคุณเกิน 10,0000 บาท")
            }
          } else {
            this.sumamt = empids.hsramT1 + this.credd12 + (parseFloat(amt.replace(/,/g, "")))

            console.log(this.sumamt)
            if (this.sumamt <= 10000) {
              this.ExpPerDataP.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
              this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
            } else {
              alert("ค่าใช้จ่ายคุณเกิน 10,0000 บาท")
            }
          }
        }, 200);
       

        // this.sumamt = empids.hsramT1 + this.credd12 + (parseFloat(amt.replace(/,/g, "")))

        // console.log(this.sumamt)
        // if (this.sumamt <= 10000) {
        //   this.ExpPerDataP.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
        //   this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
        // } else {
        //   alert("ค่าใช้จ่ายคุณเกิน 10,0000 บาท")
        // }

      } else {
        this.ExpPerDataP.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
        this.ExpPerData.push({ 'id': this.idexper, 'EmpID': this.CreateCourseDetailInput.hsreid, 'ExpID': id, 'ExpAmt': parseFloat(ExAmt) })
      }

      $('#expperamt').val('')
      var plaamt = 0
      for (let i = 0; i < this.ExpPerDataP.length; i++) {
        plaamt = (plaamt + parseFloat(parseFloat(this.ExpPerDataP[i].ExpAmt).toFixed(2)))
        if (this.ExpPerDataP.length <= (i + 1 * 1)) {
          this.CreateCourseDetailInput.hsramT1 = plaamt
        }

      }

    }
  }
  RemovedEmp(e) {
    this.ExpPerData = this.ExpPerData.filter(res => res.EmpID != e.data.hsreid)
    this.cancelselectExp()
  }

  RemovedExp(e) {

    this.ExpPerData.splice(this.ExpPerData.findIndex(res => res.id == e.data.id), 1)

    var plaamt = 0
    for (let i = 0; i < this.ExpPerDataP.length; i++) {
      plaamt = (plaamt + parseFloat(parseFloat(this.ExpPerDataP[i].ExpAmt).toFixed(2)))
      if (this.ExpPerDataP.length <= (i + 1 * 1)) {
        this.CreateCourseDetailInput.hsramT1 = plaamt
      }

    }
  }


  /////////////////////////MODAL3:H//////////////////////////////
  show5() {
    this.modal5.show();
    this.emp = this.SetuptrainingComponent.emp

  }
  nameempc
  @ViewChild("gEmployeea") gEmployeea: DxDataGridComponent
  get_datan(e) {
    console.log(e)
    var data = e.split(",");

    // if (data.length == 1) {
    //   this.CreateCourseDetailInput.hsreid = data[0]
    //   var empc = this.emp.filter(res => res.psnidn == data[0])
    //   this.nameempc = empc[0].psnfnm + ' ' + empc[0].psnlnm
    // }
    // this.CreateCourseDetailInput.hsreid = datan
    // var empc = this.emp.filter(res => res.psnidn == datan)
    // this.nameempc = empc[0].psnfnm + ' ' + empc[0].psnlnm

    // this.closeemp();

    ///////
    var hrs = this.CreateCourseDetailInput.hsrndy
    if (this.playtyp == 'OUT' && this.createcource.plaiou == '002') {
      for (let i = 0; i < data.length; i++) {
        if (this.CreateCourseDetailCheck.filter(res => res.hsreid == data[i]).length <= 1) {
          if (data.length <= (i + 1 * 1)) {
            for (let j = 0; j < data.length; j++) {
              var arryHav = this.CreateCourseDetailInputData.filter(res => res.hsreid == data[j])
              if (arryHav.length <= 0) {
                this.CreateCourseDetailInput = new CreateCourseDetailInput()
                this.CreateCourseDetailInput.hsreid = data[j]
                this.CreateCourseDetailInput.hrsunam = this.emp.filter(r => r.psnidn == data[j])[0].psnfnm + ' ' + this.emp.filter(r => r.psnidn == data[i])[0].psnlnm;
                this.CreateCourseDetailInput.hsrcsu = this.createcource.placod
                this.CreateCourseDetailInput.hsryea = this.playeaS
                this.CreateCourseDetailInput.hsrfmd = this.pladtefmS
                this.CreateCourseDetailInput.hsrtod = this.pladtetoS
                this.CreateCourseDetailInput.hsrfmt = this.createcource.plastrtim
                this.CreateCourseDetailInput.hsrtot = this.createcource.plaendtim

                this.CreateCourseDetailInput.hsrtty = this.playtyp + '2'

                this.CreateCourseDetailInput.hsrndy = hrs
                this.CreateCourseDetailInput.hsramT1 = 0
                this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)
              }
              if (data.length <= (j + 1 * 1)) {
                this.closeemp();
              }
            }
          }
        } else {
          alert('มีพนักงานอบรมครบจำนวน 2 ครั้งแล้ว/n' + data[i])
          break;
        }
      }
    } else if (this.playtyp == 'OUT' && this.createcource.plaiou == '001') {


      for (let t = 0; t < data.length; t++) {
        var ar = this.CreateCourseDetailCheck.filter(res => res.hsreid == data[t]).length
        for (let i = 0; i < data.length; i++) {
          var arryHav = this.CreateCourseDetailInputData.filter(res => res.hsreid == data[i])
          if (arryHav.length <= 0) {
            this.CreateCourseDetailInput = new CreateCourseDetailInput()
            this.CreateCourseDetailInput.hsreid = data[i]
            this.CreateCourseDetailInput.hrsunam = this.emp.filter(r => r.psnidn == data[i])[0].psnfnm + ' ' + this.emp.filter(r => r.psnidn == data[i])[0].psnlnm;
            this.CreateCourseDetailInput.hsrcsu = this.createcource.placod
            this.CreateCourseDetailInput.hsryea = this.playeaS
            this.CreateCourseDetailInput.hsrfmd = this.pladtefmS
            this.CreateCourseDetailInput.hsrtod = this.pladtetoS
            this.CreateCourseDetailInput.hsrfmt = this.createcource.plastrtim
            this.CreateCourseDetailInput.hsrtot = this.createcource.plaendtim

            if (this.playtyp == 'OUT') {
              this.CreateCourseDetailInput.hsrtty = this.playtyp + '1'
            } else {
              this.CreateCourseDetailInput.hsrtty = this.playtyp
            }
            this.CreateCourseDetailInput.hsrndy = hrs
            this.CreateCourseDetailInput.hsramT1 = 0

            this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)
          }

          console.log(ar)
          alert('อบรมครั้งที่ ' + (ar + 1 * 1))
          if (data.length <= (i + 1 * 1)) {

          }
          this.closeemp();


        }
      }


    } else {
      for (let i = 0; i < data.length; i++) {
        var arryHav = this.CreateCourseDetailInputData.filter(res => res.hsreid == data[i])
        if (arryHav.length <= 0) {
          this.CreateCourseDetailInput = new CreateCourseDetailInput()
          this.CreateCourseDetailInput.hsreid = data[i]
          this.CreateCourseDetailInput.hrsunam = this.emp.filter(r => r.psnidn == data[i])[0].psnfnm + ' ' + this.emp.filter(r => r.psnidn == data[i])[0].psnlnm;
          this.CreateCourseDetailInput.hsrcsu = this.createcource.placod
          this.CreateCourseDetailInput.hsryea = this.playeaS
          this.CreateCourseDetailInput.hsrfmd = this.pladtefmS
          this.CreateCourseDetailInput.hsrtod = this.pladtetoS
          this.CreateCourseDetailInput.hsrfmt = this.createcource.plastrtim
          this.CreateCourseDetailInput.hsrtot = this.createcource.plaendtim

          if (this.playtyp == 'OUT') {
            this.CreateCourseDetailInput.hsrtty = this.playtyp + '1'
          } else {
            this.CreateCourseDetailInput.hsrtty = this.playtyp
          }
          this.CreateCourseDetailInput.hsrndy = hrs
          this.CreateCourseDetailInput.hsramT1 = 0

          this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)
        }
        if (data.length <= (i + 1 * 1)) {
          this.closeemp();
        }
        $('#hrsunam').val('')
        $('#hsreid').val('')

      }
    }

    this.cancelselectExp()
  }
  closeemp() {
    this.gEmployeea.instance.clearSelection()
    this.modal5.hide();
  }


  kuacc(dt) {
    this.CreateCourseDetailInput.hrsunam = ''
    if (dt.indexOf("+") >= 0) {
      var accode = dt.replace("+", "")
      this.CreateCourseDetailInput.hsreid = accode
    }
  }
  nameser
  chgindex(hsreid) {
    setTimeout(() => {
      var empc = this.emp.filter(res => res.psnidn == hsreid)
      this.CreateCourseDetailInput.hrsunam = empc[0].psnfnm + ' ' + empc[0].psnlnm
      this.nameser = empc[0].psnfnm + ' ' + empc[0].psnlnm
    }, 500);

  }
  corseget: CourseListDto[] = [];
  //////////////////////////////////modal6/////////////////////////
  show6() {
    this.modal6.show();
    this.getTCourse()

  }
  getTCourse(): void {
    this.CourseAppserviceServiceProxy.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.corseget = result.items;
    });

  }

  get_course(e) {
    var datad = this.corseget.filter(data => data.id == e)

    this.createcource.platyp = this.TrainingCourse.filter(data => data.tabtB2 == datad[0].platyp)[0].tabtB2;
    $('#platyp').val(this.createcource.platyp).trigger('change')
    this.TrainingCourse[0].tabdsc
    this.createcource.placod = datad[0].placod
    this.createcource.planam = datad[0].planam
    this.closecourse();
  }
  closecourse() {
    this.modal6.hide();
  }
  //////////////////////////////////modal_outsource/////////////////////////

  showoutsourse(): void {

    this.modal_outsourse.show();

  }
  closeos() {
    this.modal_outsourse.hide();
  }
  saveoutsource(dtefm, dteto, playea) {

    this.CreateCourseDetailInput.hsryea = this.fnyyyy(playea).toString()
    this.CreateCourseDetailInput.hsrfmd = this.fnyyyymmdd(dtefm)
    this.CreateCourseDetailInput.hsrtod = this.fnyyyymmdd(dteto)
    this.CreateCourseDetailInput.hsrfmt = this.createcource.plastrtim
    this.CreateCourseDetailInput.hsrtot = this.createcource.plaendtim
    if (this.playtyp == 'OUT') {
      this.CreateCourseDetailInput.hsrtty = this.playtyp + this.createcource.plaiou
    } else {
      this.CreateCourseDetailInput.hsrtty = this.playtyp
    }

    this.CreateCourseDetailInput.hsramT1 = 0
    this.CreateCourseDetailInputData.push(this.CreateCourseDetailInput)
    this.clearP()

  }
}
