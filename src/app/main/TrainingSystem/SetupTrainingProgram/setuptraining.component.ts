import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CourseAppserviceServiceProxy, CourseListDto, DbmtabServiceProxy, DbmtabListDto, MSMPSN00ServiceProxy, UpdateCourseInput, MSMPSN00ListDto } from '@shared/service-proxies/service-proxies';
import { AppConsts } from '@shared/AppConsts';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'setuptraining',
  templateUrl: './setuptraining.component.html'
})
export class SetuptrainingComponent extends AppComponentBase implements OnInit {

  Course: CourseListDto[] = [];
  static DIVISION: any[];
  static TrainingCourse: any[];
  static TrainingPay: any[];
  static dbmtabemti: any[];
  static emp: any[];
  constructor(
    injector: Injector,
    private CourseAppservice: CourseAppserviceServiceProxy,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy

  ) {
    super(injector);

  }
  empmasterl = 0

  ngOnInit() {
    this.getTCourse();
    this.getDataMaster()
  }
  yearLbl = ''
  getCourseValue(yearsSr, typ, datet1sr, datet2sr) {

    if (yearsSr != '' && yearsSr != null && yearsSr != undefined && yearsSr != NaN) {
      var year = this.fnyyyy(yearsSr).toString()
      this.yearLbl = 'พ.ศ.' + yearsSr
    } else {
      var year = 'All'
      this.yearLbl = ''
    }
    
    // if (yearsSr != '' && typ != '') {
    this.CourseAppservice.getTCourseByTypAndDate(year, typ, this.fnyyyymmdd(datet1sr), this.fnyyyymmdd(datet2sr)).subscribe((result) => {
      this.Course = result.items;

    });
    // }else{
    //   this.CourseAppservice.getTCourse(year)
    // }
  }
  getTCourse(): void {
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;

    });
  }
  DEPARTMENT: DbmtabListDto[] = [];
  DIVISION: DbmtabListDto[] = [];
  TrainingCourse: DbmtabListDto[] = [];
  TrainingPay: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  emp=[];
  dbmtabpos: DbmtabListDto[] = [];

  // emp = [];
  getDataMaster() {
    this.empmasterl = 0
    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.DEPARTMENT = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('DIVISION', '').subscribe((result) => {
      this.DIVISION = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
      this.dbmtabemti = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('TrainingCourse', '').subscribe((result) => {
      this.TrainingCourse = result.items;
      $('.m_select2_1a').select2({

      });
    });
    this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
      this.TrainingPay = result.items;

    });
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
      this.emp = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
      this.dbmtabpos = result.items;
  });
  this.empmasterl = 1
  }

  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
  show():void{
    
  }
 
  DownloadTemp(){
    window.open(AppConsts.remoteServiceBaseUrl+'/Training/Training.xls','_blank')
    
  }
}
