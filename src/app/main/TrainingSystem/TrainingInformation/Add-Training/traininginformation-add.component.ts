import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, MSMPSN00ServiceProxy, DbmtabListDto, MSMPSN00ListDto } from '@shared/service-proxies/service-proxies';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';
import { data_course } from '../../data_centercourse';

@Component({
  selector: 'traininginformation',
  templateUrl: './traininginformation-add.component.html'
})
export class TraininginformationComponent extends AppComponentBase implements OnInit {

  emp = [];
  filterTab: string = 'POSITION';
  dbmtab: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  dbmtabdep: DbmtabListDto[] = [];
  dbmtabpay: DbmtabListDto[] = [];

  constructor(
    injector: Injector,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private _dbmtabService: DbmtabServiceProxy,
    private data_course: data_course,



  ) {
    super(injector);


  }

  ngOnInit() {
    this.getEmployees();
    if (this.data_course.empmasterl != 1) {
      this.data_course.Course()
  }
    this._dbmtabService.getDbmtabFromTABTB1(this.filterTab, '').subscribe((result) => {
        this.dbmtab = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
        this.dbmtabemti = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
      this.dbmtabemti = result.items;

  });
    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.dbmtabdep = result.items;
     
    });
    this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
      this.dbmtabpay = result.items;
    
    });
  }
  
  getEmployees(): void {
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
        this.emp = result.items;

    });
}
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
