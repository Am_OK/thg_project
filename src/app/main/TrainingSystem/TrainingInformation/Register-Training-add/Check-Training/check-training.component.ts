import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, CourseAppserviceServiceProxy, CreateCourseInput, DbmtabListDto, CreateDbmtabInput, MSMPSN00ServiceProxy, CourseListDto, CreateCourseDetailInput, CourseDetailAppserviceServiceProxy, MSMPSN00ListDto, SelfAddAppserviceServiceProxy, CreateSelfAddInput, SelfAddListDto, UpdateSelfAddInput, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { TrainingregisterComponent } from '../trainingregister.component';
import { data_course } from '../../../data_centercourse';

@Component({
  selector: 'checktraining',
  templateUrl: './check-training.component.html',
})
export class checktrainingComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') public modal2: ModalDirective;
  saving: boolean;
  Course: CourseListDto[] = [];
  SelfAdd: SelfAddListDto[] = [];
  emp: MSMPSN00ListDto[] = [];

  array_data_complete = [({
    'id': 0, 'tnsts': '', 'tncod': '', 'tneid': ''
    , 'tnauT1STAMP': '', 'tnaut1': '', 'tnaut1stamp': '', 'tnaut2': '', 'tnaut2stamp': '', 'tnaut3': '', 'tnaut3stamp': ''
    , 'tnauthr': '', 'tnauthrstamp': '', 'tnempname': '', 'index': 0,
  })];
  createcource: CreateCourseInput = new CreateCourseInput();
  updateselfadd: UpdateSelfAddInput = new UpdateSelfAddInput();
  filterTab: string = 'POSITION';
  shownLoginid13;
  constructor(
    injector: Injector,
    private CourseAppservice: CourseAppserviceServiceProxy,
    private CourseDetailAppserviceServiceProxy: CourseDetailAppserviceServiceProxy,
    private SelfAddAppservice: SelfAddAppserviceServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private KPShare: KPShareServiceProxy,
    private TrainingregisterComponent: TrainingregisterComponent,
    private data_course: data_course,


  ) {
    super(injector);
    this.shownLoginid13 = this.appSession.user.userName;

  }
  ngOnInit() {
  }
  docic
  idx = 0;
  // UpdateCourseDetailInputData: CreateCourseDetailInput[] = []
  show(doc): void {
    this.docic = doc
    this.proveheaddep = "";
    this.proveHR = "";
    this.array_data_complete = [];
    this.getset()
    this.CourseAppservice.getTCourse('', doc, '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;
      $('#exper22').val(this.Course[0].placod);

    });

    this.emp = this.TrainingregisterComponent.emp
    // this.emp = this.data_course.emp


    this.SelfAddAppservice.getSelfAddByTNCOD(doc).subscribe((resultD) => {
      this.updateselfadd = resultD.items[0]
    })
    this.SelfAddAppservice.getSelfAddByTNCOD(doc).subscribe((result) => {
      this.SelfAdd = result.items;

    });

    this.active = true;
    this.modal.show();
  }

  getset() {
    this.array_data_complete = []
    setTimeout(() => {


      this.SelfAddAppservice.getSelfAddByTNCOD(this.docic).subscribe((result) => {
        this.SelfAdd = result.items;
        for (let i = 0; i < this.SelfAdd.length; i++) {
          this.idx = this.idx + 1
          this.array_data_complete.push({
            'id': this.SelfAdd[i].id,
            'tnsts': this.SelfAdd[i].tnsts,
            'tncod': this.SelfAdd[i].tncod,
            'tneid': this.SelfAdd[i].tneid,
            'tnempname': this.SelfAdd[i].tneid,
            'tnauT1STAMP': this.SelfAdd[i].tnauT1STAMP,
            'tnaut1': this.SelfAdd[i].tnauT1,
            'tnaut1stamp': this.SelfAdd[i].tnauT1STAMP,
            'tnaut2': this.SelfAdd[i].tnauT2,
            'tnaut2stamp': this.SelfAdd[i].tnauT2STAMP,
            'tnaut3': this.SelfAdd[i].tnauT3,
            'tnaut3stamp': this.SelfAdd[i].tnauT3STAMP,
            'tnauthr': this.SelfAdd[i].tnauthr,
            'tnauthrstamp': this.SelfAdd[i].tnauthrstamp,
            'index': this.idx

          })
        }
      });

    }, 500);
  }
  // insertd(expper22, date1, empids) {

  //     this.idx = this.idx + 1

  //     this.array_data_complete.push({
  //       'expper22': expper22, 'date1': this.fnyyyymmdd(date1),'empids': empids,'tnsts': 'F','tnaut1': '',
  //        'tnaut1stamp': this.fnyyyymmdd(date1) ,
  //        'tnaut2': '','tnaut2stamp':this.fnyyyymmdd(date1) ,
  //        'tnaut3': '','tnaut3stamp': this.fnyyyymmdd(date1)
  //        ,'tnauthr': '','tnauthrstamp':this.fnyyyymmdd(date1)
  //     })

  //   }
  close(): void {

    this.modal.hide();
    this.active = false;
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  empname;
  editxs = { 'tnsts': '', 'tncod': '', 'tneid': '', 'tnauT1STAMP': '', 'tnaut1': '', 'tnaut1stamp': '', 'tnaut2': '', 'tnaut2stamp': '', 'tnaut3': '', 'tnaut3stamp': '', 'tnauthr': '', 'tnauthrstamp': '', 'tnempname': '', 'index': 0 }; conf = 0;
  funti(e) {
    // console.log(e)
    if (e.selectedRowKeys.length != 0) {
      this.conf = 1
      var xr = this.array_data_complete.filter(data => data.index == e.selectedRowsData[0].index)[0]
      this.editxs.tnsts = xr.tnsts
      this.editxs.tncod = xr.tncod
      this.editxs.tneid = xr.tneid
      this.editxs.tnempname = xr.tneid
      this.editxs.tnauT1STAMP = xr.tnauT1STAMP
      if (xr.tnaut1 != '' && xr.tnaut1 != null && xr.tnaut1 != undefined) {
        this.editxs.tnaut1 = this.emp.filter(res => res.psnidn == xr.tnaut1)[0].psnfnm
      } else {
        this.editxs.tnaut1 = xr.tnaut1
      }

      this.editxs.tnaut1stamp = xr.tnaut1stamp

      if (xr.tnaut2 != '' && xr.tnaut2 != null && xr.tnaut2 != undefined) {
        this.editxs.tnaut2 = this.emp.filter(res => res.psnidn == xr.tnaut2)[0].psnfnm
      } else {
        this.editxs.tnaut2 = xr.tnaut2
      }
      this.editxs.tnaut2stamp = xr.tnaut2stamp
      if (xr.tnaut3 != '' && xr.tnaut3 != null && xr.tnaut3 != undefined) {
        this.editxs.tnaut3 = this.emp.filter(res => res.psnidn == xr.tnaut3)[0].psnfnm
      } else {
        this.editxs.tnaut3 = xr.tnaut3
      }

      this.editxs.tnaut3stamp = xr.tnaut3stamp

      if (xr.tnauthr != '' && xr.tnauthr != null && xr.tnauthr != undefined) {
        this.editxs.tnauthr = this.emp.filter(res => res.psnidn == xr.tnauthr)[0].psnfnm
      } else {
        this.editxs.tnauthr = xr.tnauthr
      }
      this.editxs.tnauthrstamp = xr.tnauthrstamp
      this.editxs.index = xr.index

      setTimeout(() => {
        $('#date_dt1').val(this.fnddmmyyyy(this.editxs.tnaut1stamp))
        var vn = this.emp.filter(res => res.psnidn == this.editxs.tneid)
        this.empname = vn[0].psnfnm + ' ' + vn[0].psnlnm
      }, 100);
    }
  }






  // CreateCourseDetailInput: CreateCourseDetailInput = new CreateCourseDetailInput()
  // CreateCourseDetailInputData: CreateCourseDetailInput[] = []

  getDisplayExpr(item) {
    // console.log(item)
    if (!item) {
      return "";
    }
    // console.log(item.psnfnm + " " + item.psnlnm)
    return item.psnfnm + " " + item.psnlnm
  }

  @ViewChild("targetDataGridemp") targetDataGridemp: DxDataGridComponent




  nameempcs;



  proveheaddep = "";
  proveHR = "";
  approve(e) {

    console.log(e)
    var data = e.split(",");
    for (let i = 0; i < data.length; i++) {
      var arryHav = this.array_data_complete.filter(res => res.tneid == data[i])
      if (this.docic == 'INH') {
        if (arryHav[0].tnauthr == null || arryHav[0].tnauthr == '') {
          this.KPShare.approvedSelfAddTR(this.updateselfadd.tncod, data, this.shownLoginid13, "PROVE").subscribe(() => {
            console.log(e)
            this.updateselfadd.id = arryHav[0].id
            this.updateselfadd.tncod = arryHav[0].tncod
            this.updateselfadd.tneid = arryHav[0].tneid
            this.updateselfadd.tnsts = " "
            this.updateselfadd.tnauT1 = arryHav[0].tnaut1
            this.updateselfadd.tnauT1STAMP = arryHav[0].tnauT1STAMP
            this.updateselfadd.tnauT2 = arryHav[0].tnaut2
            this.updateselfadd.tnauT2STAMP = arryHav[0].tnaut2stamp
            this.updateselfadd.tnauT3 = arryHav[0].tnaut3
            this.updateselfadd.tnauT3STAMP = arryHav[0].tnaut3stamp
            this.updateselfadd.tnauthr = this.shownLoginid13
            this.updateselfadd.tnauthrstamp = arryHav[0].tnauthrstamp
          })

        }


      } else {
        if (arryHav[0].tnaut3 == null || arryHav[0].tnaut3 == '') {
          this.KPShare.approvedSelfAddTR(this.updateselfadd.tncod, data, this.shownLoginid13, "PROVE").subscribe(() => {
            this.updateselfadd.id = arryHav[0].id
            this.updateselfadd.tnsts = " "
            this.updateselfadd.tncod = arryHav[0].tncod
            this.updateselfadd.tneid = arryHav[0].tneid
            this.updateselfadd.tnauT1 = arryHav[0].tnaut1;
            this.updateselfadd.tnauT1STAMP = arryHav[0].tnauT1STAMP
            this.updateselfadd.tnauT2 = arryHav[0].tnaut2;
            this.updateselfadd.tnauT2STAMP = arryHav[0].tnaut2stamp
            this.updateselfadd.tnauT3 = this.shownLoginid13;
            this.updateselfadd.tnauT3STAMP = arryHav[0].tnaut3stamp
            this.updateselfadd.tnauthr = arryHav[0].tnauthr;
            this.updateselfadd.tnauthrstamp = arryHav[0].tnauthrstamp
          })
        } else {
          if (arryHav[0].tnauthr == null || arryHav[0].tnauthr == '') {
            this.KPShare.approvedSelfAddTR(this.updateselfadd.tncod, data, this.shownLoginid13, "PROVE").subscribe(() => {
              this.updateselfadd.id = arryHav[0].id
              this.updateselfadd.tncod = arryHav[0].tncod
              this.updateselfadd.tneid = arryHav[0].tneid
              this.updateselfadd.tnsts = "T"
              this.updateselfadd.tnauT1 = arryHav[0].tnaut1
              this.updateselfadd.tnauT1STAMP = arryHav[0].tnauT1STAMP
              this.updateselfadd.tnauT2 = arryHav[0].tnaut2
              this.updateselfadd.tnauT2STAMP = arryHav[0].tnaut2stamp
              this.updateselfadd.tnauT3 = arryHav[0].tnaut3
              this.updateselfadd.tnauT3STAMP = arryHav[0].tnaut3stamp
              this.updateselfadd.tnauthr = this.shownLoginid13;
              this.updateselfadd.tnauthrstamp = arryHav[0].tnauthrstamp

            })
          }
        }
      }
    }
    console.log(this.updateselfadd)

    this.notify.info(this.l('Successfully'));

    this.getset()
  }

  cancel(e) {
    console.log(e)
    var data = e.split(",");
    this.message.confirm(
      this.l('คุณต้องการยกเลิกใช่หรือไม่'),
      this.l('AreYouSure'),
      isConfirmed => {
        if (isConfirmed) {
          this.KPShare.approvedSelfAddTR(this.updateselfadd.tncod, data, this.shownLoginid13, "CANCLE").subscribe(() => {
            this.updateselfadd.tnauT3STAMP = ' '
            this.updateselfadd.tnauthrstamp = ' '
          })
        }
        console.log(this.updateselfadd)
        this.notify.info(this.l('Successfully'));
        this.getset()
      })

  }
  disapprove(e) {
    console.log(e)
    var data = e.split(",");
    this.message.confirm(
      this.l('คุณต้องไม่ต้องการ Approve ใช่หรือไม่'),
      this.l('AreYouSure'),
      isConfirmed => {
        if (isConfirmed) {
          this.KPShare.approvedSelfAddTR(this.updateselfadd.tncod, data, this.shownLoginid13, "DISPROVE").subscribe(() => {
            this.updateselfadd.tnauT3STAMP = ' '
            this.updateselfadd.tnauthrstamp = ' '
          })
        }
        console.log(this.updateselfadd)
        this.notify.info(this.l('Successfully'));
        this.getset()
      })

  }


}
