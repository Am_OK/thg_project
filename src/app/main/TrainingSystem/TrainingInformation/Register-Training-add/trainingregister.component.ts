import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, MSMPSN00ServiceProxy, DbmtabListDto, CourseDetailListDto, CourseDetailAppserviceServiceProxy, MSMPSN00ListDto, CourseListDto, CourseAppserviceServiceProxy } from '@shared/service-proxies/service-proxies';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'registertraining',
  templateUrl: './trainingregister.component.html'
})
export class TrainingregisterComponent extends AppComponentBase implements OnInit {
    
    emp : MSMPSN00ListDto[] = [];
    Course: CourseListDto[] = [];
    filterTab: string = 'POSITION';
    dbmtab: DbmtabListDto[] = [];
    dbmtabemti: DbmtabListDto[] = [];
    dbmtabdep: DbmtabListDto[] = [];
    dbmtabpay: DbmtabListDto[] = [];
  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,




  ) {
    super(injector);


  }

  ngOnInit() {
    this.getTCourse();

    this._dbmtabService.getDbmtabFromTABTB1(this.filterTab, '').subscribe((result) => {
        this.dbmtab = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
        this.dbmtabemti = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.dbmtabdep = result.items;
     
    });
    this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
      this.dbmtabpay = result.items;
    
    });
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
        this.emp = result.items;
        
    });
    
  }
  psnidn;

  getTCourse(): void {
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;

    });
  }


  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
