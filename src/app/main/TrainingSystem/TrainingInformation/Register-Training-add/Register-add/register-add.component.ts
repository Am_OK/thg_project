import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, CourseAppserviceServiceProxy, CreateCourseInput, DbmtabListDto, CreateDbmtabInput, MSMPSN00ServiceProxy, CourseListDto, CreateCourseDetailInput, CourseDetailAppserviceServiceProxy, MSMPSN00ListDto, SelfAddAppserviceServiceProxy, CreateSelfAddInput, SelfAddListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { DxDataGridComponent } from 'devextreme-angular/ui/data-grid';
import { data_Empmas } from '@app/main/EmpMaster/data_center';
import { createSelf } from '@angular/compiler/src/core';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'registeradd',
  templateUrl: './register-add.component.html',
})
export class RegisteraddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  @ViewChild('modal2') public modal2: ModalDirective;
  saving: boolean;
  Course: CourseListDto[] = [];
  SelfAdd: SelfAddListDto[] = [];
  emp: MSMPSN00ListDto[] = [];
  shownLoginid13;
  shownLoginName;
  shownLoginSurname;
  shownLoginDepartment;
  array_data_complete = [({
    'expper22': '', 'date1': '', 'tnsts': ''
    , 'empids': '', 'tnempname': '', 'tnaut1': '', 'tnaut1stamp': '', 'tnaut2': '', 'tnaut2stamp': '', 'tnaut3': '', 'tnaut3stamp': ''
    , 'tnauthr': '', 'tnauthrstamp': ''
  })];
  createselfadd: CreateSelfAddInput = new CreateSelfAddInput();

  createcource: CreateCourseInput = new CreateCourseInput();

  filterTab: string = 'POSITION';

  constructor(
    injector: Injector,
    private CourseAppservice: CourseAppserviceServiceProxy,
    private SelfAddAppservice: SelfAddAppserviceServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,


  ) {
    super(injector);
    this.shownLoginid13 = this.appSession.user.userName;
    this.shownLoginName = this.appSession.user.name;
    this.shownLoginSurname = this.appSession.user.surname;
  }
  ngOnInit() {

  }
  docic

  // UpdateCourseDetailInputData: CreateCourseDetailInput[] = []
  show(doc): void {
    this.docic = doc
    setTimeout(() => {
      this.CreateSelfAddInput.tneid = this.shownLoginid13
      this.nameempcs = this.shownLoginName + ' ' +this.shownLoginSurname
    }, 200);
  
    this.array_data_complete = [];
    // this.getselfadd();
    this.CreateSelfAddInputData = [];
    this.CourseAppservice.getTCourse('', doc, '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;
      $('#expper22').val(this.Course[0].placod);
      $('#date1').val(this.fnddmmyyyy(this.Course[0].pladtefm));


    });

    //   this.CourseDetailAppserviceServiceProxy.getCourseDetail('', doc, '', '', '', '', undefined).subscribe((resultD) => {
    //     this.CreateCourseDetailInputData = resultD.items
    //   })

    this.SelfAddAppservice.getSelfAddByTNCOD(doc).subscribe((resultD) => {
      this.SelfAdd = resultD.items
    })

    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
      this.emp = result.items;
    });
    this.active = true;
    this.modal.show();
  }


  close(): void {
    $('#md').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }


  clearP() {
    //   this.CreateCourseDetailInput = new CreateCourseDetailInput()
    this.CreateSelfAddInput = new CreateSelfAddInput()
    this.nameempcs = ''
    $('#empids').val('').toString()

  }


  // CreateCourseDetailInput: CreateCourseDetailInput = new CreateCourseDetailInput()
  // CreateCourseDetailInputData: CreateCourseDetailInput[] = []
  CreateSelfAddInput: CreateSelfAddInput = new CreateSelfAddInput()
  CreateSelfAddInputData: CreateSelfAddInput[] = []


  @ViewChild("targetDataGridemp") targetDataGridemp: DxDataGridComponent




  nameempcs;
  kuacc(dt) {
    this.nameempcs = ''
    if (dt.indexOf("+") >= 0) {
      var accode = dt.replace("+", "")
      $('#empids').val(accode).toString()
    }
  }
  chgindex(event) {

    var id = "#" + event
    var empids = $('#empids').val().toString()
    setTimeout(() => {
      if (empids != null && empids != undefined) {
        var empc = this.emp.filter(res => res.psnidn == empids)
        if (empc != null && empc != undefined) {
          this.nameempcs = empc[0].psnfnm + ' ' + empc[0].psnlnm
          $(id).focus()
          // 
        } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
      } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
    }, 500);


  }
  idx = 0;
  insertd(expper22, date1, empids) {
    var arryHav2 = this.SelfAdd.filter(res => res.tneid == empids)
    if (arryHav2.length <= 0) {
      this.idx = this.idx + 1
      this.array_data_complete.push({
        'expper22': expper22, 'date1': '', 'tnsts': 'D', 'empids': empids,
        'tnempname': '', 'tnaut1': '', 'tnaut1stamp': this.fnyyyymmdd(date1), 'tnaut2': '', 'tnaut2stamp': this.fnyyyymmdd(date1)
        , 'tnaut3': '', 'tnaut3stamp': this.fnyyyymmdd(date1), 'tnauthr': '', 'tnauthrstamp': this.fnyyyymmdd(date1)
      })
      $('#empids').val('').toString()
      this.nameempcs = ''
    } else {
      alert('ขออภัย, มีรหัสพนักงานนี้แล้ว')
    }

  }

  // Deletesemp(e) {
  //   console.log(e)
  //   this.CourseDetailAppserviceServiceProxy.deleteCourseDetail(e.data.id).subscribe(() => {
  //     this.targetDataGridemp.instance.refresh();
  //     
  //   });

  // }


  save() {

    var arryHav = this.CreateSelfAddInputData.filter(res => res.tneid == this.CreateSelfAddInput.tneid)
    if (arryHav.length <= 0) {
      this.message.confirm(
        this.l('คุณต้องการบันทึกข้อมูล'),
        isConfirmed => {
          if (isConfirmed) {
            this.CreateSelfAddInput.tneid = this.array_data_complete[0].empids
            this.CreateSelfAddInput.tncod = this.array_data_complete[0].expper22
            this.CreateSelfAddInput.tnsts = this.array_data_complete[0].tnsts
            this.CreateSelfAddInputData.push(this.CreateSelfAddInput)
            this.SelfAddAppservice.createSelfAdd(this.CreateSelfAddInput).subscribe(() => {
              this.notify.info(this.l('SavedSuccessFully'))
              this.modalSave.emit()
              this.close()
            })


          }
        })
    }

  }
  ClearP() {
    this.CreateSelfAddInput = new CreateSelfAddInput()
    this.nameempcs = ''
    $('#empids').val('').toString()

  }



  /////////////////////////////////222////////////////////////////////
  // show2(): void {
  //   this.modal2.show();

  // }
  // get_datan2(e) {
  //   console.log(e)
  //   var data = e.split(",");
  //   for (let i = 0; i < data.length; i++) {
  //     var arryHav = this.CreateSelfAddInputData.filter(res => res.tneid == data[i])
  //     if (arryHav.length <= 0) {
  //       var arryHav2 = this.array.filter(res => res.tneid == data[i])
  //       if (arryHav2.length <= 0) {
  //         this.CreateSelfAddInput = new CreateSelfAddInput()
  //         this.CreateSelfAddInput.tneid = data[i]
  //         this.CreateSelfAddInput.tncod = this.Course[0].placod
  //         this.CreateSelfAddInput.tnauT1STAMP = this.Course[0].pladtefm
  //         this.CreateSelfAddInput.tnsts = 'F'
  //         this.CreateSelfAddInputData.push(this.CreateSelfAddInput)
  //       }
  //     }
  //   }
  //   this.close2();
  // }
  // close2(): void {

  //   this.modal2.hide();
  //   this.active = false;
  // }
}
