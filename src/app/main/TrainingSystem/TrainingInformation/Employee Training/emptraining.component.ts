import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MSMPSN00ListDto, MSMPSN00ServiceProxy, CourseDetailListDto, CourseDetailAppserviceServiceProxy, DbmtabListDto, DbmtabServiceProxy, CourseListDto, CourseAppserviceServiceProxy, UpdateCourseDetailInput } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'emptraining',
  templateUrl: './emptraining.component.html',
})
export class EmpTrainingComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;

  emp: MSMPSN00ListDto[] = [];
  coursedetail:CourseDetailListDto[] = [];
  dbmtabtitle: DbmtabListDto[] = [];
  Course: CourseListDto[] = [];
  updatedetailcourse: UpdateCourseDetailInput = new UpdateCourseDetailInput();

  constructor( 
    injector: Injector,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseDetailAppservice:CourseDetailAppserviceServiceProxy,
    private _dbmtabService: DbmtabServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,

    
       
  ) {
    super(injector);

  }
  empid;
  datan;
  ngOnInit() {
    this.gatdata(this.empid,this.datan)

  }
  empcode;
  empname;
  csuname;
  show(empid,datan): void {
    this.empid = empid
    this.datan = datan
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
      this.dbmtabtitle = result.items;
      this.dbmtabtitle.filter(data => data.tabdsc == datan[0].psntth);

  });
  this.coursedetail=[]
    this.active = true;
    this.modal.show();
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
      this.emp = result.items;
      var vn = this.emp.filter(res => res.psnidn == this.empid)
      this.empcode = vn[0].psnidn;
      this.empname = this.dbmtabtitle[0].tabdsc + vn[0].psnfnm +" "+ vn[0].psnlnm ;
  
      // this.empname = vn[0].psntth 
  });
  this.CourseAppservice.getTCourse(datan,'','','','',undefined,'').subscribe((result) => {
    this.Course = result.items;

});
//   this.CourseDetailAppservice.getCourseDetail(empid,datan,'','','','',undefined).subscribe((result) => {
//     this.coursedetail = result.items;
   
// });
  this.gatdata(empid,datan)

  }

gatdata(empid,datan){
  this.CourseDetailAppservice.getCourseDetail(empid,datan,'','','','',undefined).subscribe((result) => {
    this.coursedetail = result.items;
   
});
}


  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    $('#md').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
 
}
