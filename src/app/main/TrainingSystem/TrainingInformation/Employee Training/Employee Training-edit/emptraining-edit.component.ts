import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CourseDetailAppserviceServiceProxy, TRBUD00AppserviceServiceProxy, CourseAppserviceServiceProxy, CourseListDto, UpdateCourseDetailInput, MSMPSN00ListDto, TRBUD00ListDto, CreateCourseDetailInput, CourseDetailListDto, ThemeLayoutSettingsDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { id } from '@swimlane/ngx-charts/release/utils';

@Component({
  selector: 'emptrainingedit',
  templateUrl: './emptraining-edit.component.html',
})
export class EmptrainingUpdateComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  saving: boolean;
  updateCoursedetail: UpdateCourseDetailInput = new UpdateCourseDetailInput();
  updatecoursede1: CreateCourseDetailInput = new CreateCourseDetailInput();
  coursedetail:CourseDetailListDto[] = [];

  Course: CourseListDto[] = [];

  constructor(
    injector: Injector,
    private CourseDetailAppservice: CourseDetailAppserviceServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,




  ) {
    super(injector);

  }
  ngOnInit() {

  }
  docpro;
  hsrcsuname;
  show(eid,datan): void {

    this.active = true;
    this.modal.show();
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;
      // this.docpro = this.updateCoursedetail.id

      var vn = this.Course.filter(res => res.placod == datan)
      this.hsrcsuname = vn[0].planam;
    });
  //   this.CourseDetailAppservice.getCourseDetail(eid,datan,'','','','',undefined).subscribe((result) => {
  //     this.coursedetail = result.items;
     
  // });
   
  this.gatdata(eid,datan)
  }
  gatdata(eid,datan){

    this.CourseDetailAppservice.getCourseDetail(eid, datan, '', '', '', '', undefined).subscribe((result) => {
      this.updateCoursedetail = result.items[0];
      this.docpro = this.updateCoursedetail.id
      this.updateCoursedetail.hsrmsc = 0.00;
      this.updateCoursedetail.hsrmsC2 = 0.00;
      this.updateCoursedetail.hsrmsC3 = 0.00;
      this.updateCoursedetail.hsrfee = '0.00';
      this.updateCoursedetail.hsrasc = 0.00;
      this.updateCoursedetail.hsrasC2 = 0.00;
      this.updateCoursedetail.hsrasC3 = 0.00;
      this.updateCoursedetail.hsrdte = this.fnddmmyyyy(this.updateCoursedetail.hsrfmd)
      this.updateCoursedetail.hsrfmd = this.fnddmmyyyy(this.updateCoursedetail.hsrfmd)
      this.updateCoursedetail.hsrtod = this.fnddmmyyyy(this.updateCoursedetail.hsrtod)

    });
  }
 
  save(d1, dfm, dto, train, p1, p2, p3, sts1, sts2) {

          this.updateCoursedetail.hsrcom = '001'
          this.updateCoursedetail.hsrbrn = '001'
          this.updateCoursedetail.hsrdte = this.fnyyyymmdd(d1)
          this.updateCoursedetail.hsrfmd = this.fnyyyymmdd(dfm)
          this.updateCoursedetail.hsrtod = this.fnyyyymmdd(dto)
          this.updateCoursedetail.hsratt = train.toString()
          this.updateCoursedetail.hsrpsf = p1.toString()
          this.updateCoursedetail.hsrpsF2 = p2.toString()
          this.updateCoursedetail.hsrpsF3 = p3.toString()
          this.updateCoursedetail.hsrvf = sts1.toString()
          this.updateCoursedetail.hsrrec = sts2.toString()
          this.CourseDetailAppservice.updateCourseDetail(this.updateCoursedetail).subscribe(() => { 
            this.modalSave.emit()
            this.close()
          });
        console.log(this.updateCoursedetail)
  }

  Delete() {
    this.message.confirm(
      this.l('AreYouSureToDeleteTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.CourseDetailAppservice.deleteCourseDetail(this.updateCoursedetail.id).subscribe(() => {
            this.modalSave.emit()
            this.close()
          })

        }
      })

  }
  
  close(): void {
    $('#moda').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
}
