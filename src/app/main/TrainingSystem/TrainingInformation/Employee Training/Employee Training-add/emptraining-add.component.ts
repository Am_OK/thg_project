import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CourseDetailAppserviceServiceProxy, CreateCourseDetailInput, CourseAppserviceServiceProxy, CourseListDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'emptrainingadd',
  templateUrl: './emptraining-add.component.html',
})
export class EmptrainingAddComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  saving: boolean;

  createcoursede: CreateCourseDetailInput = new CreateCourseDetailInput();


  constructor(
    injector: Injector,
    private CourseDetailAppservice:CourseDetailAppserviceServiceProxy,



  ) {
    super(injector);

  }
  ngOnInit() {

  }
  show(): void {

    this.active = true;
    this.modal.show();
    this.createcoursede.hsrndy=0;
    this.createcoursede.hsrmsc = 0.00;
    this.createcoursede.hsrmsC2 = 0.00;
    this.createcoursede.hsrmsC3 = 0.00;
    this.createcoursede.hsrfee ='0.00';
    this.createcoursede.hsrasc = 0.00;
    this.createcoursede.hsrasC2 = 0.00;
    this.createcoursede.hsrasC3 = 0.00;
  }

  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1, fixed: true, color: 'red'
    });
  }
  close(): void {
    $('#moda').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
}
