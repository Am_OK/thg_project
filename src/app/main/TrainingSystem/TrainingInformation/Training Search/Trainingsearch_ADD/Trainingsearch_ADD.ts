import { Component, OnInit, Output, ViewChild, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CourseDetailAppserviceServiceProxy, TRBUD00AppserviceServiceProxy, CourseAppserviceServiceProxy, CourseListDto, UpdateCourseDetailInput, MSMPSN00ListDto, TRBUD00ListDto, CreateCourseDetailInput, CourseDetailListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'TrainingsearchADD',
  templateUrl: './Trainingsearch_ADD.html',
})
export class TrainingsearchADDComponent extends AppComponentBase implements OnInit {
  active: boolean = false;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  saving: boolean;
  updateCoursed: UpdateCourseDetailInput = new UpdateCourseDetailInput();
  updatecoursede1: CreateCourseDetailInput = new CreateCourseDetailInput();

  updatecoursede: CourseDetailListDto = new CourseDetailListDto();
  updatecoursedetail: CreateCourseDetailInput[] = []
  Course: CourseListDto[] = [];

  constructor(
    injector: Injector,
    private CourseDetailAppservice: CourseDetailAppserviceServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,




  ) {
    super(injector);

  }
  ngOnInit() {

  }
  docpro;
  hsrcsuname;
  show(eid,datan): void {

    this.active = true;
    this.modal.show();
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;
      this.docpro = this.updateCoursed.id

      var vn = this.Course.filter(res => res.placod == datan)
      this.hsrcsuname = vn[0].planam;
    });

    this.CourseDetailAppservice.getCourseDetail(eid, datan, '', '', '', '', undefined).subscribe((result) => {
      this.updatecoursedetail = result.items;
      this.updatecoursede = result.items[0];
      this.docpro = this.updatecoursede.id
      this.updatecoursede.hsrmsc = 0.00;
      this.updatecoursede.hsrmsC2 = 0.00;
      this.updatecoursede.hsrmsC3 = 0.00;
      this.updatecoursede.hsrfee = '0.00';
      this.updatecoursede.hsrasc = 0.00;
      this.updatecoursede.hsrasC2 = 0.00;
      this.updatecoursede.hsrasC3 = 0.00;
      this.updatecoursede.hsrdte = this.fnddmmyyyy(this.updatecoursede.hsrfmd)
      this.updatecoursede.hsrfmd = this.fnddmmyyyy(this.updatecoursede.hsrfmd)
      this.updatecoursede.hsrtod = this.fnddmmyyyy(this.updatecoursede.hsrtod)

    });

  }
  getCourseDetail(){
    
  }
  save(d1, dfm, dto, train, p1, p2, p3, sts1, sts2) {
    this.message.confirm(
      this.l('AreYouSureToSaveTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.CourseDetailAppservice.deleteCourseDetail(this.docpro).subscribe(() => {

          this.saving = true;
          this.updatecoursede.hsrcom = '001'
          this.updatecoursede.hsrbrn = '001'
          this.updatecoursede.hsrdte = this.fnyyyymmdd(d1)
          this.updatecoursede.hsrfmd = this.fnyyyymmdd(dfm)
          this.updatecoursede.hsrtod = this.fnyyyymmdd(dto)
          this.updatecoursede.hsratt = train.toString()
          this.updatecoursede.hsrpsf = p1.toString()
          this.updatecoursede.hsrpsF2 = p2.toString()
          this.updatecoursede.hsrpsF3 = p3.toString()
          this.updatecoursede.hsrvf = sts1.toString()
          this.updatecoursede.hsrrec = sts2.toString()
          this.updatecoursedetail.push(this.updatecoursede)
          this.clearP()
            this.CourseDetailAppservice.createCourseDetail(this.updatecoursedetail).subscribe(() => {
              this.notify.info(this.l('SavedSuccessfully'));
              this.modalSave.emit();
              this.close();

            });
          });
        }
   
      })
   

  }
  clearP() {

  }
  Delete() {
    this.message.confirm(
      this.l('AreYouSureToDeleteTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.CourseDetailAppservice.deleteCourseDetail(this.updatecoursede.id).subscribe(() => {
            this.modalSave.emit()
            this.close()
          })

        }
      })

  }
  close(): void {
    $('#moda').find("input").val('').end()
    this.modal.hide();
    this.active = false;
  }
}
