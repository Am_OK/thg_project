import { Component, OnInit, Injector, ViewChild, Output, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, MSMPSN00ServiceProxy, DbmtabListDto, CourseDetailListDto, CourseDetailAppserviceServiceProxy, MSMPSN00ListDto, CourseListDto, CourseAppserviceServiceProxy, CreateCourseDetailInput, UpdateCourseDetailInput } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'traininghistory',
  templateUrl: './traininghistory.component.html'
})
export class TraininghistoryComponent extends AppComponentBase implements OnInit {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  active: boolean = false;
  emp: MSMPSN00ListDto[] = [];
  Course: CourseListDto[] = [];
  filterTab: string = 'POSITION';
  dbmtab: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  dbmtabdep: DbmtabListDto[] = [];
  dbmtabpay: DbmtabListDto[] = [];
  updatecoursedetail: CreateCourseDetailInput[] = []
  updatecoursede: CourseDetailListDto = new CourseDetailListDto();

  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,
    private CourseDetailAppserviceServiceProxy: CourseDetailAppserviceServiceProxy,
    // private SetuptrainingComponent: SetuptrainingComponent


  ) {
    super(injector);


  }

  ngOnInit() {
    this.getTCourse();
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1(this.filterTab, '').subscribe((result) => {
      this.dbmtab = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
      this.dbmtabemti = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.dbmtabdep = result.items;

    });
    this._dbmtabService.getDbmtabFromTABTB1('TrainingPay', '').subscribe((result) => {
      this.dbmtabpay = result.items;

    });
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
      this.emp = result.items;

    });

  }
  psnidn;

  getTCourse(): void {
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;

    });
  }


  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }

  chk_out = 0
  show(doc): void {
    this.active = true;
    this.modal.show();
    this.docic = doc
    this.CourseAppservice.getTCourse('', doc, '', '', '', undefined, '').subscribe((result) => {
      this.Course = result.items;
      $('#expper22').val(this.Course[0].placod);
      $('#date1').val(this.fnddmmyyyy(this.Course[0].pladtefm));
      $('#tami1').val(this.Course[0].plastrtim);
      $('#date2').val(this.fnddmmyyyy(this.Course[0].pladteto));
      $('#tami2').val(this.Course[0].plaendtim);
      if(this.Course[0].platyp == 'OUT'){
        this.chk_out = 1
      }else{
        this.chk_out = 0
      }
    });
   
    // this.emp = this.SetuptrainingComponent.emp
    setTimeout(() => {
      this.CourseDetailAppserviceServiceProxy.getCourseDetail('', doc, '', '', '', '', undefined).subscribe((resultD) => {
        this.CreateCourseDetailInputData = resultD.items
       
        var name = this.emp.filter(res => res.psnidn == res.psnidn)
        this.CreateCourseDetailInputData[0].hrsunam = name[0].psnfnm + ' ' + name[0].psnlnm
      })
     
    }, 500);
   
  

  }
 
  updatecoursedetail2: CourseDetailListDto[] = []
  updatecoursed: UpdateCourseDetailInput = new UpdateCourseDetailInput;

  CreateCourseDetailInputData: CreateCourseDetailInput[] = []
  docic
  getDisplayExpr(item) {
    // console.log(item)
    if (!item) {
      return "";
    }
    // console.log(item.psnfnm + " " + item.psnlnm)
    return item.psnfnm + " " + item.psnlnm
  }
  send(e) {
    console.log(e)
    var data = e.split(",");
    for (let i = 0; i < data.length; i++) {
      this.CourseDetailAppserviceServiceProxy.getCourseDetail(data[i], this.docic, '', '', '', '', undefined).subscribe((result) => {
        this.updatecoursedetail2 = result.items;
        console.log(this.updatecoursedetail2)
          this.updatecoursed.hsradd = this.updatecoursedetail2[0].hsradd
          this.updatecoursed.hsradm = this.updatecoursedetail2[0].hsradm
          this.updatecoursed.hsramT1 = this.updatecoursedetail2[0].hsramT1
          this.updatecoursed.hsramT2 = this.updatecoursedetail2[0].hsramT2
          this.updatecoursed.hsramT3 = this.updatecoursedetail2[0].hsramT3
          this.updatecoursed.hsramT4 = this.updatecoursedetail2[0].hsramT4
          this.updatecoursed.hsramT5 = this.updatecoursedetail2[0].hsramT5
          this.updatecoursed.hsramT6 = this.updatecoursedetail2[0].hsramT6
          this.updatecoursed.hsramT7 = this.updatecoursedetail2[0].hsramT7
          this.updatecoursed.hsramT8 = this.updatecoursedetail2[0].hsramT8
          this.updatecoursed.hsramT9 = this.updatecoursedetail2[0].hsramT9
          this.updatecoursed.hsramT10 = this.updatecoursedetail2[0].hsramT10
          this.updatecoursed.hsramT11 = this.updatecoursedetail2[0].hsramT11
          this.updatecoursed.hsramT12 = this.updatecoursedetail2[0].hsramT12
          this.updatecoursed.hsrapr = this.updatecoursedetail2[0].hsrapr
          this.updatecoursed.hsrasC2 = this.updatecoursedetail2[0].hsrasC2
          this.updatecoursed.hsrasC3 = this.updatecoursedetail2[0].hsrasC3
          this.updatecoursed.hsrasc = this.updatecoursedetail2[0].hsrasc
          this.updatecoursed.hsratt = this.updatecoursedetail2[0].hsratt
          this.updatecoursed.hsrbrn = this.updatecoursedetail2[0].hsrbrn
          this.updatecoursed.hsrcer = this.updatecoursedetail2[0].hsrcer
          this.updatecoursed.hsrcom = this.updatecoursedetail2[0].hsrcom
          this.updatecoursed.hsrcrn = this.updatecoursedetail2[0].hsrcrn
          this.updatecoursed.hsrcsu = this.updatecoursedetail2[0].hsrcsu
          this.updatecoursed.hrsunam = this.updatecoursedetail2[0].hrsunam
          this.updatecoursed.hsrdte = this.updatecoursedetail2[0].hsrdte
          this.updatecoursed.hsredtm = this.updatecoursedetail2[0].hsredtm
          this.updatecoursed.hsreid = this.updatecoursedetail2[0].hsreid
          this.updatecoursed.hsreusr = this.updatecoursedetail2[0].hsreusr
          this.updatecoursed.hsrfG1 = this.updatecoursedetail2[0].hsrfG1
          this.updatecoursed.hsrfG2 = this.updatecoursedetail2[0].hsrfG2
          this.updatecoursed.hsrfG3 = this.updatecoursedetail2[0].hsrfG3
          this.updatecoursed.hsrfee = this.updatecoursedetail2[0].hsrfee
          this.updatecoursed.hsrfmd = this.updatecoursedetail2[0].hsrfmd
          this.updatecoursed.hsrfmt = this.updatecoursedetail2[0].hsrfmt
          this.updatecoursed.hsrloc = this.updatecoursedetail2[0].hsrloc
          this.updatecoursed.hsrmsC2 = this.updatecoursedetail2[0].hsrmsC2
          this.updatecoursed.hsrmsC3 = this.updatecoursedetail2[0].hsrmsC3
          this.updatecoursed.hsrmsc = this.updatecoursedetail2[0].hsrmsc
          this.updatecoursed.hsrndy = this.updatecoursedetail2[0].hsrndy
          this.updatecoursed.hsrpsF2 = this.updatecoursedetail2[0].hsrpsF2
          this.updatecoursed.hsrpsF3 = this.updatecoursedetail2[0].hsrpsF3
          this.updatecoursed.hsrpsf = this.updatecoursedetail2[0].hsrpsf
          this.updatecoursed.hrssnw = "Y"
          this.updatecoursed.hsrrec = this.updatecoursedetail2[0].hsrrec
          this.updatecoursed.hsrremarK2 = this.updatecoursedetail2[0].hsrremarK2
          this.updatecoursed.hsrremarK3 = this.updatecoursedetail2[0].hsrremarK3
          this.updatecoursed.hsrremark = this.updatecoursedetail2[0].hsrremark
          this.updatecoursed.hsrrmK2 = this.updatecoursedetail2[0].hsrrmK2
          this.updatecoursed.hsrrmk = this.updatecoursedetail2[0].hsrrmk
          this.updatecoursed.hsrsts = this.updatecoursedetail2[0].hsrsts
          this.updatecoursed.hsrtod = this.updatecoursedetail2[0].hsrtod
          this.updatecoursed.hsrtot = this.updatecoursedetail2[0].hsrtot
          this.updatecoursed.hsrtty = this.updatecoursedetail2[0].hsrtty
          this.updatecoursed.hsrvf = this.updatecoursedetail2[0].hsrvf
          this.updatecoursed.hsryea = this.updatecoursedetail2[0].hsryea
          this.updatecoursed.id = this.updatecoursedetail2[0].id
        
          this.CourseDetailAppserviceServiceProxy.updateCourseDetail(this.updatecoursed).subscribe((result) => {

            this.modalSave.emit()
            this.close2()
          });
       
      });

    }


  }
  savetrue(e) {
    console.log(e)
    var data = e.split(",");
    for (let i = 0; i < data.length; i++) {
      this.CourseDetailAppserviceServiceProxy.getCourseDetail(data[i], this.docic, '', '', '', '', undefined).subscribe((result) => {
        this.updatecoursedetail2 = result.items;
        console.log(this.updatecoursedetail2)
        if (this.docic == 'OUT') {
          this.updatecoursed.hrssnw = "Y"
        }
          this.updatecoursed.hsradd = this.updatecoursedetail2[0].hsradd
          this.updatecoursed.hsradm = this.updatecoursedetail2[0].hsradm
          this.updatecoursed.hsramT1 = this.updatecoursedetail2[0].hsramT1
          this.updatecoursed.hsramT2 = this.updatecoursedetail2[0].hsramT2
          this.updatecoursed.hsramT3 = this.updatecoursedetail2[0].hsramT3
          this.updatecoursed.hsramT4 = this.updatecoursedetail2[0].hsramT4
          this.updatecoursed.hsramT5 = this.updatecoursedetail2[0].hsramT5
          this.updatecoursed.hsramT6 = this.updatecoursedetail2[0].hsramT6
          this.updatecoursed.hsramT7 = this.updatecoursedetail2[0].hsramT7
          this.updatecoursed.hsramT8 = this.updatecoursedetail2[0].hsramT8
          this.updatecoursed.hsramT9 = this.updatecoursedetail2[0].hsramT9
          this.updatecoursed.hsramT10 = this.updatecoursedetail2[0].hsramT10
          this.updatecoursed.hsramT11 = this.updatecoursedetail2[0].hsramT11
          this.updatecoursed.hsramT12 = this.updatecoursedetail2[0].hsramT12
          this.updatecoursed.hsrapr = this.updatecoursedetail2[0].hsrapr
          this.updatecoursed.hsrasC2 = this.updatecoursedetail2[0].hsrasC2
          this.updatecoursed.hsrasC3 = this.updatecoursedetail2[0].hsrasC3
          this.updatecoursed.hsrasc = this.updatecoursedetail2[0].hsrasc
          this.updatecoursed.hsratt = this.updatecoursedetail2[0].hsratt
          this.updatecoursed.hsrbrn = this.updatecoursedetail2[0].hsrbrn
          this.updatecoursed.hsrcer = this.updatecoursedetail2[0].hsrcer
          this.updatecoursed.hsrcom = this.updatecoursedetail2[0].hsrcom
          this.updatecoursed.hsrcrn = this.updatecoursedetail2[0].hsrcrn
          this.updatecoursed.hsrcsu = this.updatecoursedetail2[0].hsrcsu
          this.updatecoursed.hrsunam = this.updatecoursedetail2[0].hrsunam
          this.updatecoursed.hsrdte = this.updatecoursedetail2[0].hsrdte
          this.updatecoursed.hsredtm = this.updatecoursedetail2[0].hsredtm
          this.updatecoursed.hsreid = this.updatecoursedetail2[0].hsreid
          this.updatecoursed.hsreusr = this.updatecoursedetail2[0].hsreusr
          this.updatecoursed.hsrfG1 = this.updatecoursedetail2[0].hsrfG1
          this.updatecoursed.hsrfG2 = this.updatecoursedetail2[0].hsrfG2
          this.updatecoursed.hsrfG3 = this.updatecoursedetail2[0].hsrfG3
          this.updatecoursed.hsrfee = this.updatecoursedetail2[0].hsrfee
          this.updatecoursed.hsrfmd = this.updatecoursedetail2[0].hsrfmd
          this.updatecoursed.hsrfmt = this.updatecoursedetail2[0].hsrfmt
          this.updatecoursed.hsrloc = this.updatecoursedetail2[0].hsrloc
          this.updatecoursed.hsrmsC2 = this.updatecoursedetail2[0].hsrmsC2
          this.updatecoursed.hsrmsC3 = this.updatecoursedetail2[0].hsrmsC3
          this.updatecoursed.hsrmsc = this.updatecoursedetail2[0].hsrmsc
          this.updatecoursed.hsrndy = this.updatecoursedetail2[0].hsrndy
          this.updatecoursed.hsrpsF2 = "Y"
          this.updatecoursed.hsrpsF3 = "Y"
          this.updatecoursed.hsrpsf = "Y"
          // this.updatecoursed.hrssnw = "Y"
          this.updatecoursed.hsrrec = this.updatecoursedetail2[0].hsrrec
          this.updatecoursed.hsrremarK2 = this.updatecoursedetail2[0].hsrremarK2
          this.updatecoursed.hsrremarK3 = this.updatecoursedetail2[0].hsrremarK3
          this.updatecoursed.hsrremark = this.updatecoursedetail2[0].hsrremark
          this.updatecoursed.hsrrmK2 = this.updatecoursedetail2[0].hsrrmK2
          this.updatecoursed.hsrrmk = this.updatecoursedetail2[0].hsrrmk
          this.updatecoursed.hsrsts = this.updatecoursedetail2[0].hsrsts
          this.updatecoursed.hsrtod = this.updatecoursedetail2[0].hsrtod
          this.updatecoursed.hsrtot = this.updatecoursedetail2[0].hsrtot
          this.updatecoursed.hsrtty = this.updatecoursedetail2[0].hsrtty
          this.updatecoursed.hsrvf = this.updatecoursedetail2[0].hsrvf
          this.updatecoursed.hsryea = this.updatecoursedetail2[0].hsryea
          this.updatecoursed.id = this.updatecoursedetail2[0].id
        
          this.CourseDetailAppserviceServiceProxy.updateCourseDetail(this.updatecoursed).subscribe((result) => {

            this.modalSave.emit()
            this.close2()
          });
       
      });

    }


  }
  close2() {
    this.modal.hide();

  }
}
