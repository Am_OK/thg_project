import { Component, OnInit, Injector, ViewChild, Output, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, MSMPSN00ServiceProxy, DbmtabListDto, CourseDetailListDto, CourseDetailAppserviceServiceProxy, MSMPSN00ListDto, CourseListDto, CourseAppserviceServiceProxy, CreateCourseDetailInput, UpdateCourseDetailInput } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_course } from '../../data_centercourse';

import { SetuptrainingComponent } from '../../SetupTrainingProgram/setuptraining.component';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'Trainingsearch',
  templateUrl: './trainingsearch.component.html'
})
export class TrainingsearchComponent extends AppComponentBase implements OnInit {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') public modal: ModalDirective;
  active: boolean = false;
  emp: MSMPSN00ListDto[] = [];
  coursec: CourseListDto[] = [];
  filterTab: string = 'POSITION';
  dbmtab: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  dbmtabdep: DbmtabListDto[] = [];
  dbmtabpay: DbmtabListDto[] = [];

  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
    private CourseAppservice: CourseAppserviceServiceProxy,
    private data_course: data_course,
    

    

  ) {
    super(injector);


  }

  ngOnInit() {
    this.getTCourse();
    

  }
  psnidn;

  getTCourse(): void {
    this.coursec = this.data_course.coursec
    this.dbmtab = this.data_course.dbmtab
    this.dbmtabemti = this.data_course.dbmtabemti
    this.dbmtabdep = this.data_course.dbmtabdep
    this.dbmtabpay = this.data_course.dbmtabpay
    this.emp = this.data_course.emp
    this.CourseAppservice.getTCourse('', '', '', '', '', undefined, '').subscribe((result) => {
      this.coursec = result.items;

    });
    
  }


  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }


 
}
