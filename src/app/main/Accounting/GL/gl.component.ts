import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
// import { PersonServiceProxy, PersonListDto, ListResultDtoOfPersonListDto, PhoneInPersonListDto, AddPhoneInput, AddPhoneInputType, PhoneInPersonListDtoType } from '@shared/service-proxies/service-proxies';
import { DxDataGridModule } from 'devextreme-angular';
import * as events from "devextreme/events";

import * as _ from 'lodash';

@Component({
    templateUrl: './gl.component.html',
    // styleUrls: ['./accInterfacedata.component.less'],
    animations: [appModuleAnimation()]
})
export class GLComponent extends AppComponentBase implements OnInit {

    //people: PersonListDto[] = [];
    filter: string = '';

    //editingPerson: PersonListDto = null;
    //newPhone: AddPhoneInput = null;

    shownLoginName: string = "";
    shownLoginDepartment: string = "";

    constructor(
        injector: Injector,
        //private _personService: PersonServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getCurrentLoginInformations();
        this.runGLLP()
    }

    getCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.shownLoginDepartment = this.appSession.getShownLoginDepartmentCode();      
    }

    runGLLP(): void {
        window.location.href = "GLLP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
    }  
}