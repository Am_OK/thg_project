import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UserServiceProxy,
         LinkedUserDto } from '@shared/service-proxies/service-proxies';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Http, HttpModule } from '@angular/http';
import { DxDataGridModule } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';

@Component({
    templateUrl: './Printngpm1.component.html',
    //styleUrls: ["./BudgetSetting.component.less"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class Printngpm1Component extends AppComponentBase implements OnInit  {

    filter: string = '';
    shownLoginName: string = "";
    shownLoginDepartment: string = "";

    constructor(
        injector: Injector,
        private _userServiceProxy: UserServiceProxy,
    ) {
        super(injector);
    }

      getCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.shownLoginDepartment = this.appSession.getShownLoginDepartmentCode();
    }
    

    ngOnInit(): void {
         this.getCurrentLoginInformations();
         //var linktoJsFiddle = $('#linktoJsFiddle').attr('href');
         //"App2:KK006KKKKKKKK001KKKKKKKKTHAKKKKKKKK0001"
         console.log("Old Detail:",this.shownLoginDepartment);
         if (this.shownLoginDepartment == "")
         {
            console.log("Detail N:",this.shownLoginDepartment); 
            window.location.href = "http://mis.mcu.ac.th:5001/Home/NGPM1?Department=61000100&Period=01&Year=2561";
         }
         else
         {
            console.log("Detail H:",this.shownLoginDepartment);  
            window.location.href = "http://mis.mcu.ac.th:5001/Home/NGPM1?Department="+this.shownLoginDepartment+"&Period=01&Year=2561"; 
         }
         //"http://202.28.108.108:5001/NGPM/Report601?Dep=61000200&EndY=2561&StartY=2560";
         //"KP02:KK006KKKKKKKK001KKKKKKKKTHAKKKKKKKK0001"
        //  "App2:KK"+this.shownLoginName;//006KKKKKKKK001KKKKKKKKTHAKKKKKKKK0001";
    }


}