import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OvertimeComponent } from '@app/main/HrSystem/OverTime/overtime/overtime.component';
import { SpecialComponent } from '@app/main/HrSystem/SpecialCost/special/special.component';
import { WorkshiftComponent } from '@app/main/HrSystem/WorkShift/workshift/workshift.component';
import { WorkwageComponent } from '@app/main/HrSystem/WorkWage/workwage/workwage.component';
import { VacationComponent } from '@app/main/HrSystem/Vacation/vacation.component';
import { JobAppComponent } from '@app/main/HrSystem/JobApplication/jobapplication.component';
import { ReportDetailOTComponent } from '@app/main/Time and Attendance/Daily Report/ReportDetailOT/report-detailot.component';
import { ReportDetailSpecialComponent } from '@app/main/Time and Attendance/Daily Report/ReportDetailSpecial/report-detailspecial.component';
import { ReportDetailWorkwageComponent } from '@app/main/Time and Attendance/Daily Report/ReportDetailWorkwage/report-detailworkwage.component';
import { ReportLeavingComponent } from '@app/main/Time and Attendance/Daily Report/ReportLeaving/report-leaving.component';
import { ReportOvertimeComponent } from '@app/main/Time and Attendance/Daily Report/ReportOvertime/report-overtime.component';
import { ReportPersonalCSComponent } from '@app/main/Time and Attendance/Daily Report/ReportPersonalCS/report-personalCS.component';
import { ReportSumEmployeeComponent } from '@app/main/Time and Attendance/Daily Report/ReportSumEmployee/report-sumemployee.component';
import { ReportSumTakeComponent } from '@app/main/Time and Attendance/Daily Report/ReportSumTake/report-sumtake.component';
import { ReportCalculateEmpComponent } from '@app/main/Time and Attendance/Daily Report/ReportCalculateEmp/report-calculatemp.component';
import { ReportDailyWorkComponent } from '@app/main/Time and Attendance/Daily Report/ReportDailyWork/report-dailywork.component';
import { ReportTimeAttendanceComponent } from '@app/main/Time and Attendance/Daily Report/ReportTime&Attendancs/report-timeattendance.component';
import { HolidayYearListComponent } from '@app/main/Time and Attendance/Attendance Setting/HolidayYearList/holiday-year-list.component';
import { LeavingcodeComponent } from '@app/main/Time and Attendance/Attendance Setting/LeavingCode/leaving-code.component';
import { TableshiftcodeComponent } from '@app/main/Time and Attendance/Attendance Setting/TableCode+ShiftCode/table-shiftcode.component';
import { LevelControlComponent } from '@app/main/Time and Attendance/Attendance Setting/LevelControl/level-control.component';
import { SettingHPComponent } from '@app/main/Time and Attendance/Attendance Setting/SettingHoursforPosition/setting-HP.component';
import { WorkshiftpermisComponent } from '@app/main/Time and Attendance/Attendance Setting/WorkShiftPermis/workshift-permis.component';
import { EmpworkingayComponent } from '@app/main/Time and Attendance/Daily Activities/EmpWorkingDay/empworkingday.component';
import { EmpbfTopayrollComponent } from '@app/main/Time and Attendance/Daily Activities/EmpData BTToPayroll/empbftopayroll.component';
import { TimeRecordEntryComponent } from '@app/main/Time and Attendance/Daily Activities/Time Record Entry/timerecord.component';
import { FingerscanComponent } from '@app/main/Time and Attendance/Daily Activities/FingerScan/fingerscan.component';
import { CalculateattendanceComponent } from '@app/main/Time and Attendance/Attendance Summary/CalculateAttendance/calculateattendance.component';
import { EmpTransfertoPayrollComponent } from '@app/main/Time and Attendance/Attendance Summary/EmpTransfertoPayroll/emptransfertopay.component';
import { PayrollworksheetComponent } from '@app/main/Time and Attendance/Attendance Summary/Payroll Work Sheet/payrollworksheet.component';
import { PrintPayrollComponent } from '@app/main/Time and Attendance/Attendance Summary/Print Payroll/printpayroll.component';
import { TransferdataftalfComponent } from '@app/main/Time and Attendance/Daily Activities/Transfer Data from Talf/transferdataftalf.component';
import { WorkingoverTimeComponent } from '@app/main/Time and Attendance/Daily Activities/Working OverTime List/workingovertime.component';
import { IncomeAndReductionCodeListComponent } from './PayrollSystem/PayrollSetting/income-and-reduction-code-list/income-and-reduction-code-list.component';
import { TaxRateSettingComponent } from './PayrollSystem/PayrollSetting/tax-rate-setting/tax-rate-setting.component';
import { ReductionRateSettingComponent } from './PayrollSystem/PayrollSetting/reduction-rate-setting/reduction-rate-setting.component';
import { SocialRateSettingComponent } from './PayrollSystem/PayrollSetting/social-rate-setting/social-rate-setting.component';
import { SpecialcostentryComponent } from '@app/main/Time and Attendance/Daily Activities/SpecialCostEntry/specialcostentry.component';
import { PrintTaxRateComponent } from './PayrollSystem/PayrollSetting/print-tax-rate/print-tax-rate.component';
import { PrintReductionRateComponent } from './PayrollSystem/PayrollSetting/print-reduction-rate/print-reduction-rate.component';
import { PrintSocialRateComponent } from './PayrollSystem/PayrollSetting/print-social-rate/print-social-rate.component';
import { PrintIncomeAndReductionCodeListComponent } from './PayrollSystem/PayrollSetting/print-income-and-reduction-code-list/print-income-and-reduction-code-list.component';
import { DataEntryForCalculatePayrollComponent } from './payrollSystem/dataentryforcalculate/data-entry-for-calculate-payroll/data-entry-for-calculate-payroll.component';
import { SummaryAttendanceByYearComponent } from './payrollSystem/dataentryforcalculate/summary-attendance-by-year/summary-attendance-by-year.component';
import { IncomeAndReduceOfPeriodComponent } from './payrollSystem/dataentryforcalculate/income-and-reduce-of-period/income-and-reduce-of-period.component';
import { SummaryIncomeAndReduceByYearComponent } from './payrollSystem/dataentryforcalculate/summary-income-and-reduce-by-year/summary-income-and-reduce-by-year.component';
import { PrintSummaryAttendanceByYearComponent } from './payrollSystem/dataentryforcalculate/print-summary-attendance-by-year/print-summary-attendance-by-year.component';
import { PrintAttendanceDataComponent } from './payrollSystem/dataentryforcalculate/print-attendance-data/print-attendance-data.component';
import { EmpComponent } from '@app/main/EmpMaster/Employee/emp.component';
import { PrintPayrollWorkSheetReportComponent } from './PayrollSystem/PeriodReport/print-payroll-work-sheet-report/print-payroll-work-sheet-report.component';
import { CancelCalculatedPayrollOfPeriodComponent } from './PayrollSystem/CalculateandClose/cancel-calculated-payroll-of-period/cancel-calculated-payroll-of-period.component';
import { CreateEmpModalComponent } from './EmpMaster/Employee/AddEmp/create-emp.component';
import { TransferPayrollDataToBankComponent } from './PayrollSystem/Transfer_Data/TransferPayrollDataToBank/TransferPayrollDataToBank';
import { TranferPayrollDataToSocialDepartmentComponent } from './PayrollSystem/Transfer_Data/tranfer-payroll-data-to-social-department/tranfer-payroll-data-to-social-department.component';
import { SetupbudgetComponent } from './TrainingSystem/SetupBudget/setupbudget.component';
import { PrintMonthlyReportForSocialDepartmentComponent } from './PayrollSystem/Transfer_Data/print-monthly-report-for-social-department/print-monthly-report-for-social-department.component';
import { SetuptrainingComponent } from './TrainingSystem/SetupTrainingProgram/setuptraining.component';
import { CalculatePayrollPeriodComponent } from './PayrollSystem/CalculateandClose/Calculate_Payroll_Period/Calculate_Payroll_Period';
import { EmpP1ModalComponent } from './EmpMaster/Employee/EditEMP/empp1.component';
import { GovemmentReportComponent } from './PayrollSystem/MonthlyRepot/Govemment_Report/Govemment_Report';
import { ClosedpayrollComponent } from './PayrollSystem/CalculateandClose/Closed_payroll/Closed_payroll';
import { TraininginformationComponent } from './TrainingSystem/TrainingInformation/Add-Training/traininginformation-add.component';
import { TimeandAttendanceHRComponent } from './HR/timeadndAttendanceHR.component';
import { TimeAndAttendanceComponent } from './HR/timeandAttendance.component';
import { PayrollComponent } from './HR/payroll.component';
import { TrainingComponent } from './HR/training.component';
import { PFComponent } from './HR/pf.component';
import { AppraisalComponent } from './HR/appraisal.component';
import { InterfaceDFComponent } from './DF/interfaceDF.component';
import { DoctorMasterComponent } from './DF/doctorMaster.component';
import { DoctorFeeComponent } from './DF/doctorfee.component';
import { DoctorPayComponent } from './DF/doctorpay.component';
import { VersionCheckComponent } from './VersionCheck/versioncheck.component';
import { UserManualComponent } from './UserManual/usermanual.component';
import { PhoneBookComponent } from './people/phonebook.component';

import { DepartmentMasterComponent } from './DepartmentMaster/DepartmentMaster.component';



import { Printngpm1Component } from './Printngpm1/Printngpm1.component';


import { AccInterfaceDataComponent } from './Accounting/accInterfacedata.component';
import { TransactionReceivingComponent } from './Accounting/AR/transactionReceiving.component';
import { CustomerMasterComponent } from './Accounting/AR/customerMaster.component';
import { VendorMasterWebComponent } from './Accounting/AP/Vendor/vendorMasterWeb.component';
import { TaxCenterComponent } from './Accounting/AP/Tax/taxCenter.component';
import { AssetsManagementComponent } from './Accounting/AP/Asset/assetsManagement.component';
import { PaymentComponent } from './Accounting/AP/Payment/payment.component';
import { InventoryComponent } from './Accounting/AP/Inventory/inventory.component';
import { ChqueAndBankComponent } from './Accounting/AP/ChqueandBank/chqueAndBank.component';
import { GLComponent } from './Accounting/GL/gl.component';
import { GlobalSettingComponent } from './Accounting/globalSetting.component';
import { EmployeeMasterComponent } from './HR/employeeMaster.component';

import { DashboardclientipdComponent } from './dashboard/dashboardclientipd/dashboardclientipd.component';
import { DashboardclientopdComponent } from './dashboard/dashboardclientopd/dashboardclientopd.component';
import { TrainingsearchComponent } from './TrainingSystem/TrainingInformation/Training Search/trainingsearch.component';
import { ReportregisterComponent } from './TrainingSystem/SummaryReport/ReportRegister/report-register.component';
import { ReportEmpHistoryComponent } from './TrainingSystem/SummaryReport/ReportEmpHistory/report-emphistory.component';
import { ReportPlantrainingComponent } from './TrainingSystem/SummaryReport/ReportPlanTraining/report-plantraining.component';
import { ReportHistoryOJTComponent } from './TrainingSystem/SummaryReport/ReportEmpHistoryOJT/report-historyojt.component';
import { ReportTrainingempComponent } from './TrainingSystem/SummaryReport/ReportTrainingEmp/report-trainingemp.component';
import { ReportsumpaytrainComponent } from './TrainingSystem/SummaryReport/ReportSumpayTraining/report-sumpaytrain.component';
import { DashboardbalaComponent } from './dashboard/dashboardbala/dashboardbala.component';
import { ReportHrstrainyearComponent } from './TrainingSystem/SummaryReport/ReportHrsTrainyear/report-hrstrainingyear.component';
import { ReporttrainingpayComponent } from './TrainingSystem/SummaryReport/ReportTrainingpay/report-trainingpay.component';
import { ReportCoursemonthComponent } from './TrainingSystem/SummaryReport/ReportCoursemonth/report-coursemonth.component';
import { ReportTakereserveComponent } from './TrainingSystem/SummaryReport/ReportTakereserve/report-takereserve.component';
import { ReportTrainingmonthComponent } from './TrainingSystem/SummaryReport/ReportTrainingmonth/report-trainingmonth.component';
import { ReportDetailpaymonthComponent } from './TrainingSystem/SummaryReport/ReportDetailpaymonth/report-detailpaymonth.component';
import { ReportnumEmptrainComponent } from './TrainingSystem/SummaryReport/ReportNumEmpTraining/report-numemptrain.component';
import { TrainingregisterComponent } from './TrainingSystem/TrainingInformation/Register-Training-add/trainingregister.component';
import { SetcodepositionComponent } from './EmpMaster/Position/Set-Code-Position/setcode-position.component';
import { RecruitmentComponent } from './EmpMaster/Recruitment/recruitment.component';
import { EmployeeresignComponent } from './EmpMaster/EmployeeResign/employeeresign.component';
import { empmoveComponent } from './EmpMaster/Empmove/empmove.component';
import { CommendationletterComponent } from './EmpMaster/CommendationLetter/commendationletter.component';
import { EmployeenoticeComponent } from './EmpMaster/Employee Notice/employeenotice.component';
import { EmployeenoticeaddComponent } from './EmpMaster/Employee Notice/EmployeeNotice-add/employeenotice-add.component';
import { importantletterComponent } from './EmpMaster/Important Letter/importantletter.component';
import { ManpowerComponent } from './EmpMaster/Manpower/manpower.component';
import { ReportsumEmpComponent } from './EmpMaster/EmployeeMasterIn/ReportsumEmp/reportsumemp.component';
import { ReportsumEmppositionComponent } from './EmpMaster/EmployeeMasterIn/ReportSumEmpPosition/report-sumempposition.component';
import { ReportProbationComponent } from './EmpMaster/EmployeeMasterIn/Reportprobation/report-probation.component';
import { ReportRegistrationComponent } from './EmpMaster/EmployeeMasterIn/ReportRegistration/report-registration.component';
import { ReportEmpRegisterComponent } from './EmpMaster/EmployeeMasterIn/ReportEmpRegister/report-empregister.component';
import { ReportEmpListingComponent } from './EmpMaster/EmployeeMasterIn/ReportEmplisting/report-emplisting.component';
import { ReportCertificateComponent } from './EmpMaster/EmployeeMasterIn/ReportCertificate/report-certificate.component';
import { ReportManpowerComponent } from './EmpMaster/EmployeeMasterIn/ReportManpower/report-manpower.component';
import { PrintrecruitmentComponent } from './EmpMaster/Recruitment/Print-Recruiment/print-recruitment.component';
import { PrintEmpResignComponent } from './EmpMaster/EmployeeResign/PrintEmployeeResign/printempresign.component';
import { PrintEmpMoveComponent } from './EmpMaster/Empmove/PrintEmpmove/printempmovement.component';
import { BenefitsReportComponent } from './EmpMaster/Employee Benefit/Benefits Report/benefitreport.component';
import { WorkplanComponent } from './HrSystem/Workplan/workplan.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    { path: 'dashboard', component: DashboardComponent },
                    { path: 'dashboardclientipd', component: DashboardclientipdComponent },
                    { path: 'dashboardclientopd', component: DashboardclientopdComponent },
                    { path: 'dashboardbala', component: DashboardbalaComponent },

                    { path: 'phonebook', component: PhoneBookComponent },


                    { path: 'DepartmentMaster', component: DepartmentMasterComponent },




                    { path: 'Printngpm1', component: Printngpm1Component },



                    { path: 'AccInterfaceData', component: AccInterfaceDataComponent },
                    { path: 'TransactionReceiving', component: TransactionReceivingComponent },
                    { path: 'CustomerMaster', component: CustomerMasterComponent },
                    { path: 'VendorMasterWeb', component: VendorMasterWebComponent },
                    { path: 'TaxCenter', component: TaxCenterComponent },
                    { path: 'AssetManagement', component: AssetsManagementComponent },
                    { path: 'PaymentandPTT', component: PaymentComponent },
                    { path: 'InventoryandWarehouse', component: InventoryComponent },
                    { path: 'ChqueAndBank', component: ChqueAndBankComponent },
                    { path: 'GL', component: GLComponent },
                    { path: 'GlobalSetting', component: GlobalSettingComponent },
                    // { path: 'EmployeeMaster', component: EmployeeMasterComponent },
                    { path: 'Workplan', component: WorkplanComponent },
                    { path: 'TimeandAttendanceHR', component: TimeandAttendanceHRComponent },
                    { path: 'TimeandAttendance', component: TimeAndAttendanceComponent },
                    { path: 'Payroll', component: PayrollComponent },
                    { path: 'TrainingandDevelopment', component: TrainingComponent },
                    { path: 'PerformanceAppraisal', component: PFComponent },
                    { path: 'AppraisalSystem', component: AppraisalComponent },
                    { path: 'InterfaceDF', component: InterfaceDFComponent },
                    { path: 'DoctorMaster', component: DoctorMasterComponent },
                    { path: 'DoctorFee', component: DoctorFeeComponent },
                    { path: 'DoctorPay', component: DoctorPayComponent },
                    { path: 'VersionCheck', component: VersionCheckComponent },
                    { path: 'UserManual', component: UserManualComponent },
                    { path: 'Setupbudget', component: SetupbudgetComponent },
                    { path: 'Setuptraining', component: SetuptrainingComponent },
                    { path: 'Traininginformation', component: TraininginformationComponent },
                    { path: 'Trainingsearch', component: TrainingsearchComponent },
                    { path: 'Reportregister', component: ReportregisterComponent },
                    { path: 'ReportEmpHistory', component: ReportEmpHistoryComponent },
                    { path: 'ReportPlantraining', component: ReportPlantrainingComponent },
                    { path: 'ReportHistoryOJT', component: ReportHistoryOJTComponent },
                    { path: 'ReportTrainingemp', component: ReportTrainingempComponent },
                    { path: 'Reportsumpaytrain', component: ReportsumpaytrainComponent },
                    { path: 'ReportHrstrainyear', component: ReportHrstrainyearComponent },
                    { path: 'Reporttrainingpay', component: ReporttrainingpayComponent },
                    { path: 'ReportCoursemonth', component: ReportCoursemonthComponent },
                    { path: 'ReportTakereserve', component: ReportTakereserveComponent },
                    { path: 'ReportTrainingmonth', component: ReportTrainingmonthComponent },
                    { path: 'ReportDetailpaymonth', component: ReportDetailpaymonthComponent },
                    { path: 'ReportnumEmptrain', component: ReportnumEmptrainComponent },
                    { path: 'Trainingregister', component: TrainingregisterComponent },
                    { path: 'EmployeeMaster', component: EmpComponent },
                    { path: 'EditEmp/:doc', component: EmpP1ModalComponent },
                    { path: 'CreateEmp', component: CreateEmpModalComponent },
                    { path: 'Setcodeposition', component: SetcodepositionComponent },
                    { path: 'Recruitment', component: RecruitmentComponent },
                    { path: 'Employeeresign', component: EmployeeresignComponent },
                    { path: 'empmove', component: empmoveComponent },
                    { path: 'Commendationletter', component: CommendationletterComponent },
                    { path: 'Employeenotice', component: EmployeenoticeComponent },
                    { path: 'importantletter', component: importantletterComponent },
                    { path: 'Manpower', component: ManpowerComponent },
                    { path: 'ReportsumEmp', component: ReportsumEmpComponent },
                    { path: 'ReportsumEmpposition', component: ReportsumEmppositionComponent },
                    { path: 'ReportProbation', component: ReportProbationComponent },
                    { path: 'ReportRegistration', component: ReportRegistrationComponent },
                    { path: 'ReportEmpRegister', component: ReportEmpRegisterComponent },
                    { path: 'ReportEmpListing', component: ReportEmpListingComponent },
                    { path: 'ReportCertificate', component: ReportCertificateComponent },
                    { path: 'ReportManpower', component: ReportManpowerComponent },
                    { path: 'Printrecruitment', component: PrintrecruitmentComponent },
                    { path: 'PrintEmpResign', component: PrintEmpResignComponent },
                    { path: 'PrintEmpMove', component: PrintEmpMoveComponent },
                    { path: 'BenefitsReport', component: BenefitsReportComponent },

                    
                    
                    
                    
                    

                    
                    
   
                    
    
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule { }
