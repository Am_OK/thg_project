import { CommonModule } from '@angular/common';
import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import { ModalModule, TabsModule, TooltipModule, BsDropdownModule, PopoverModule } from 'ngx-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { BsDatepickerModule, BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';

import { DevExtremeModule, DxLoadPanelModule, DxChartModule } from 'devextreme-angular';
import { JobAppComponent } from '@app/main/HrSystem/JobApplication/jobapplication.component';
import { JobAppaddComponent } from '@app/main/HrSystem/JobApplication/jobapplication-add/jobapplication-add.component';
import { OvertimeComponent } from '@app/main/HrSystem/OverTime/overtime/overtime.component';
import { OvertimeAddComponent } from '@app/main/HrSystem/OverTime/overtime-add/overtime-add.component';
import { OvertimeEditComponent } from '@app/main/HrSystem/OverTime/overtime-edit/overtime-edit.component';
import { SpecialComponent } from '@app/main/HrSystem/SpecialCost/special/special.component';
import { SpecialAddComponent } from '@app/main/HrSystem/SpecialCost/special-add/special-add.component';
import { SpecialEditComponent } from '@app/main/HrSystem/SpecialCost/special-edit/special-edit.component';
import { VacationComponent } from '@app/main/HrSystem/Vacation/vacation.component';
import { VacationAddComponent } from '@app/main/HrSystem/Vacation/vacation-add/vacation-add.component';
import { VacationEditComponent } from '@app/main/HrSystem/Vacation/vacation-edit/vacation-edit.component';
import { WorkshiftComponent } from '@app/main/HrSystem/WorkShift/workshift/workshift.component';
import { WorkshiftAddComponent } from '@app/main/HrSystem/WorkShift/workshift-add/workshift-add.component';
import { WorkshiftEditComponent } from '@app/main/HrSystem/WorkShift/workshift-edit/workshift-edit.component';
import { WorkwageComponent } from '@app/main/HrSystem/WorkWage/workwage/workwage.component';
import { WorkwageAddComponent } from '@app/main/HrSystem/WorkWage/workwage-add/workwage-add.component';
import { WorkwageEditComponent } from '@app/main/HrSystem/WorkWage/workwage-edit/workwage-edit.component';
import { ReportCalculateEmpComponent } from '@app/main/Time and Attendance/Daily Report/ReportCalculateEmp/report-calculatemp.component';
import { ReportDailyWorkComponent } from '@app/main/Time and Attendance/Daily Report/ReportDailyWork/report-dailywork.component';
import { ReportDetailOTComponent } from '@app/main/Time and Attendance/Daily Report/ReportDetailOT/report-detailot.component';
import { ReportDetailSpecialComponent } from '@app/main/Time and Attendance/Daily Report/ReportDetailSpecial/report-detailspecial.component';
import { ReportDetailWorkwageComponent } from '@app/main/Time and Attendance/Daily Report/ReportDetailWorkwage/report-detailworkwage.component';
import { ReportLeavingComponent } from '@app/main/Time and Attendance/Daily Report/ReportLeaving/report-leaving.component';
import { ReportOvertimeComponent } from '@app/main/Time and Attendance/Daily Report/ReportOvertime/report-overtime.component';
import { ReportPersonalCSComponent } from '@app/main/Time and Attendance/Daily Report/ReportPersonalCS/report-personalCS.component';
import { ReportSumEmployeeComponent } from '@app/main/Time and Attendance/Daily Report/ReportSumEmployee/report-sumemployee.component';
import { ReportSumTakeComponent } from '@app/main/Time and Attendance/Daily Report/ReportSumTake/report-sumtake.component';
import { ReportTimeAttendanceComponent } from '@app/main/Time and Attendance/Daily Report/ReportTime&Attendancs/report-timeattendance.component';
import { EmpbfTopayrollComponent } from '@app/main/Time and Attendance/Daily Activities/EmpData BTToPayroll/empbftopayroll.component';
import { EmpBtoPeditComponent } from '@app/main/Time and Attendance/Daily Activities/EmpData BTToPayroll/EmpDataBtoP-edit/empbftopayroll-edit.component';
import { EmpworkingayComponent } from '@app/main/Time and Attendance/Daily Activities/EmpWorkingDay/empworkingday.component';
import { FingerscanComponent } from '@app/main/Time and Attendance/Daily Activities/FingerScan/fingerscan.component';
import { TimeRecordEntryComponent } from '@app/main/Time and Attendance/Daily Activities/Time Record Entry/timerecord.component';
import { TimerecordentryAddComponent } from '@app/main/Time and Attendance/Daily Activities/Time Record Entry/TimeRecordEntry-add/timerecord-add.component';
import { HolidayYearListComponent } from '@app/main/Time and Attendance/Attendance Setting/HolidayYearList/holiday-year-list.component';
import { HolidayyearAddComponent } from '@app/main/Time and Attendance/Attendance Setting/HolidayYearList/Holiday-add/holiday-add.component';
import { HolidayyearEditComponent } from '@app/main/Time and Attendance/Attendance Setting/HolidayYearList/Holiday-edit/holiday-edit.component';
import { LeavingcodeComponent } from '@app/main/Time and Attendance/Attendance Setting/LeavingCode/leaving-code.component';
import { LeavingcodeAddComponent } from '@app/main/Time and Attendance/Attendance Setting/LeavingCode/LeavingCode-add/leavingcode-add.component';
import { LevelControlComponent } from '@app/main/Time and Attendance/Attendance Setting/LevelControl/level-control.component';
import { LevelControlAddComponent } from '@app/main/Time and Attendance/Attendance Setting/LevelControl/Levelcontrol-add/levelcontrol-add.component';
import { SettingHPComponent } from '@app/main/Time and Attendance/Attendance Setting/SettingHoursforPosition/setting-HP.component';
import { SettingHPAddComponent } from '@app/main/Time and Attendance/Attendance Setting/SettingHoursforPosition/SettingHP-add/settingHP-add.component';
import { TableshiftcodeComponent } from '@app/main/Time and Attendance/Attendance Setting/TableCode+ShiftCode/table-shiftcode.component';
import { TableShiftcodeEditComponent } from '@app/main/Time and Attendance/Attendance Setting/TableCode+ShiftCode/TableShift-edit/tableshiftcode-edit.component';
import { TableShiftcodeAddComponent } from '@app/main/Time and Attendance/Attendance Setting/TableCode+ShiftCode/TableShift-add/tableshiftcode-add.component';
import { WorkshiftpermisComponent } from '@app/main/Time and Attendance/Attendance Setting/WorkShiftPermis/workshift-permis.component';
import { WorkshiftpermissAddComponent } from '@app/main/Time and Attendance/Attendance Setting/WorkShiftPermis/Workshiftpermis-add/workshiftpermis-add.component';
import { CalculateattendanceComponent } from '@app/main/Time and Attendance/Attendance Summary/CalculateAttendance/calculateattendance.component';
import { PayrollcolumnconfigComponent } from '@app/main/Time and Attendance/Attendance Summary/CalculateAttendance/Payroll Column Config/payrollcolumnconfig.component';
import { EmpTransfertoPayrollComponent } from '@app/main/Time and Attendance/Attendance Summary/EmpTransfertoPayroll/emptransfertopay.component';
import { PayrollworksheetComponent } from '@app/main/Time and Attendance/Attendance Summary/Payroll Work Sheet/payrollworksheet.component';
import { PrintPayrollComponent } from '@app/main/Time and Attendance/Attendance Summary/Print Payroll/printpayroll.component';
import { TransferdataftalfComponent } from '@app/main/Time and Attendance/Daily Activities/Transfer Data from Talf/transferdataftalf.component';
import { WorkingoverTimeComponent } from '@app/main/Time and Attendance/Daily Activities/Working OverTime List/workingovertime.component';
import { IncomeAndReductionCodeListComponent } from './PayrollSystem/PayrollSetting/income-and-reduction-code-list/income-and-reduction-code-list.component';
import { WorkingovertimeAddComponent } from '@app/main/Time and Attendance/Daily Activities/Working OverTime List/WorkingOvertime-add/workingovertime-add.component';
import { AddIncomeAndReductionCodeListComponent } from './PayrollSystem/PayrollSetting/income-and-reduction-code-list/add-income-and-reduction-code-list/add-income-and-reduction-code-list.component';
import { TaxRateSettingComponent } from './PayrollSystem/PayrollSetting/tax-rate-setting/tax-rate-setting.component';
import { AddTaxRateSettingComponent } from './PayrollSystem/PayrollSetting/tax-rate-setting/add-tax-rate-setting/add-tax-rate-setting.component';
import { EditTaxRateSettingComponent } from './PayrollSystem/PayrollSetting/tax-rate-setting/edit-tax-rate-setting/edit-tax-rate-setting.component';
import { ReductionRateSettingComponent } from './PayrollSystem/PayrollSetting/reduction-rate-setting/reduction-rate-setting.component';
import { SocialRateSettingComponent } from './PayrollSystem/PayrollSetting/social-rate-setting/social-rate-setting.component';
import { SpecialcostentryComponent } from '@app/main/Time and Attendance/Daily Activities/SpecialCostEntry/specialcostentry.component';
import { AddReductionRateSettingComponent } from './PayrollSystem/PayrollSetting/reduction-rate-setting/add-reduction-rate-setting/add-reduction-rate-setting.component';
import { EditReductionRateSettingComponent } from './PayrollSystem/PayrollSetting/reduction-rate-setting/edit-reduction-rate-setting/edit-reduction-rate-setting.component';
import { AddSocialRateSettingComponent } from './PayrollSystem/PayrollSetting/social-rate-setting/add-social-rate-setting/add-social-rate-setting.component';
import { EditSocialRateSettingComponent } from './PayrollSystem/PayrollSetting/social-rate-setting/edit-social-rate-setting/edit-social-rate-setting.component';
import { SpecialcostentryAddComponent } from '@app/main/Time and Attendance/Daily Activities/SpecialCostEntry/SpecialCostEntry-add/specialcostentry-add.component';
import { PrintTaxRateComponent } from './PayrollSystem/PayrollSetting/print-tax-rate/print-tax-rate.component';
import { PrintReductionRateComponent } from './PayrollSystem/PayrollSetting/print-reduction-rate/print-reduction-rate.component';
import { PrintSocialRateComponent } from './PayrollSystem/PayrollSetting/print-social-rate/print-social-rate.component';
import { PrintIncomeAndReductionCodeListComponent } from './PayrollSystem/PayrollSetting/print-income-and-reduction-code-list/print-income-and-reduction-code-list.component';
import { DataEntryForCalculatePayrollComponent } from './payrollSystem/dataentryforcalculate/data-entry-for-calculate-payroll/data-entry-for-calculate-payroll.component';
import { SummaryAttendanceByYearComponent } from './payrollSystem/dataentryforcalculate/summary-attendance-by-year/summary-attendance-by-year.component';
import { IncomeAndReduceOfPeriodComponent } from './payrollSystem/dataentryforcalculate/income-and-reduce-of-period/income-and-reduce-of-period.component';
import { SummaryIncomeAndReduceByYearComponent } from './payrollSystem/dataentryforcalculate/summary-income-and-reduce-by-year/summary-income-and-reduce-by-year.component';
import { PrintSummaryAttendanceByYearComponent } from './payrollSystem/dataentryforcalculate/print-summary-attendance-by-year/print-summary-attendance-by-year.component';
import { PrintAttendanceDataComponent } from './payrollSystem/dataentryforcalculate/print-attendance-data/print-attendance-data.component';
import { EmpComponent } from '@app/main/EmpMaster/Employee/emp.component';
import { CreateEmpModalComponent } from '@app/main/EmpMaster/Employee/AddEmp/create-emp.component';
import { EmpP1ModalComponent } from '@app/main/EmpMaster/Employee/EditEMP/empp1.component';
import { AddSummaryIncomeAndReduceByYearComponent } from './PayrollSystem/DataEntryforCalculate/summary-income-and-reduce-by-year/add-summary-income-and-reduce-by-year/add-summary-income-and-reduce-by-year.component';
import { EditSummaryIncomeAndReduceByYearComponent } from './PayrollSystem/DataEntryforCalculate/summary-income-and-reduce-by-year/edit-summary-income-and-reduce-by-year/edit-summary-income-and-reduce-by-year.component';
import { EditDataEntryForCalculatePayrollComponent } from './PayrollSystem/DataEntryforCalculate/data-entry-for-calculate-payroll/edit-data-entry-for-calculate-payroll/edit-data-entry-for-calculate-payroll.component';
import { PrintPayrollWorkSheetReportComponent } from './PayrollSystem/PeriodReport/print-payroll-work-sheet-report/print-payroll-work-sheet-report.component';
import { CancelCalculatedPayrollOfPeriodComponent } from './PayrollSystem/CalculateandClose/cancel-calculated-payroll-of-period/cancel-calculated-payroll-of-period.component';
import { TransferPayrollDataToBankComponent } from './PayrollSystem/Transfer_Data/TransferPayrollDataToBank/TransferPayrollDataToBank';
import { TranferPayrollDataToSocialDepartmentComponent } from './PayrollSystem/Transfer_Data/tranfer-payroll-data-to-social-department/tranfer-payroll-data-to-social-department.component';
import { SetupbudgetComponent } from './TrainingSystem/SetupBudget/setupbudget.component';
import { SetupbudgetAddComponent } from './TrainingSystem/SetupBudget/SetupBudget-add/setupbudget-add.component';
import { PrintMonthlyReportForSocialDepartmentComponent } from './PayrollSystem/Transfer_Data/print-monthly-report-for-social-department/print-monthly-report-for-social-department.component';
import { SetuptrainingComponent } from './TrainingSystem/SetupTrainingProgram/setuptraining.component';
import { CalculatePayrollPeriodComponent } from './PayrollSystem/CalculateandClose/Calculate_Payroll_Period/Calculate_Payroll_Period';
import { SetuptrainingAddComponent } from './TrainingSystem/SetupTrainingProgram/SetupTrainingProgram-add/setuptraining-add.component';
import { GovemmentReportComponent } from './PayrollSystem/MonthlyRepot/Govemment_Report/Govemment_Report';
import { ClosedpayrollComponent } from './PayrollSystem/CalculateandClose/Closed_payroll/Closed_payroll';
import { personalModalComponent } from './EmpMaster/Employee/EmpDetailModal/Personal/personal.component';
import { TraininginformationComponent } from './TrainingSystem/TrainingInformation/Add-Training/traininginformation-add.component';
import { AddinstituteinformationComponent } from './TrainingSystem/InstituteInformation/Add_ISI/Addinstituteinformation';
import { MemberOFFamilyComponent } from './EmpMaster/Employee/EmpDetailModal/MemberOFFamily/MemberOFFamily.component';
import { EducationComponent } from './EmpMaster/Employee/EmpDetailModal/education/education.component';
import { EmpTrainingComponent } from './TrainingSystem/TrainingInformation/Employee Training/emptraining.component';
import { EmptrainingAddComponent } from './TrainingSystem/TrainingInformation/Employee Training/Employee Training-add/emptraining-add.component';
import { SetupBudgeteditComponent } from './TrainingSystem/SetupBudget/SetupBudget-edit/SetupBudget-edit';
import { TimeandAttendanceHRComponent } from './HR/timeadndAttendanceHR.component';
import { TimeAndAttendanceComponent } from './HR/timeandAttendance.component';
import { PayrollComponent } from './HR/payroll.component';
import { TrainingComponent } from './HR/training.component';
import { PFComponent } from './HR/pf.component';
import { AppraisalComponent } from './HR/appraisal.component';
import { InterfaceDFComponent } from './DF/interfaceDF.component';
import { DoctorMasterComponent } from './DF/doctorMaster.component';
import { DoctorFeeComponent } from './DF/doctorfee.component';
import { DoctorPayComponent } from './DF/doctorpay.component';
import { VersionCheckComponent } from './VersionCheck/versioncheck.component';
import { UserManualComponent } from './UserManual/usermanual.component';
import { PhoneBookComponent } from './people/phonebook.component';

import { CreatePersonModalComponent } from './people/create-person-modal.component';
import { EditPersonModalComponent } from './people/edit-person-modal.component';

import { VendorMasterWebComponent } from './Accounting/AP/Vendor/vendorMasterWeb.component';

import { DepartmentMasterComponent } from './DepartmentMaster/DepartmentMaster.component';


import { Printngpm1Component } from './Printngpm1/Printngpm1.component';




import { AccInterfaceDataComponent } from './Accounting/accInterfacedata.component';
import { TransactionReceivingComponent } from './Accounting/AR/transactionReceiving.component';
import { CustomerMasterComponent } from './Accounting/AR/customerMaster.component';

import { TaxCenterComponent } from './Accounting/AP/Tax/taxCenter.component';
import { AssetsManagementComponent } from './Accounting/AP/Asset/assetsManagement.component';
import { PaymentComponent } from './Accounting/AP/Payment/payment.component';
import { InventoryComponent } from './Accounting/AP/Inventory/inventory.component';
import { ChqueAndBankComponent } from './Accounting/AP/ChqueandBank/chqueAndBank.component';
import { GLComponent } from './Accounting/GL/gl.component';
import { GlobalSettingComponent } from './Accounting/globalSetting.component';
import { EmployeeMasterComponent } from './HR/employeeMaster.component';

import { DashboardclientipdComponent } from './dashboard/dashboardclientipd/dashboardclientipd.component';
import { DashboardclientopdComponent } from './dashboard/dashboardclientopd/dashboardclientopd.component';
import { TrainingsearchComponent } from './TrainingSystem/TrainingInformation/Training Search/trainingsearch.component';
import { ReportregisterComponent } from './TrainingSystem/SummaryReport/ReportRegister/report-register.component';
import { ReportEmpHistoryComponent } from './TrainingSystem/SummaryReport/ReportEmpHistory/report-emphistory.component';
import { ReportPlantrainingComponent } from './TrainingSystem/SummaryReport/ReportPlanTraining/report-plantraining.component';
import { ReportHistoryOJTComponent } from './TrainingSystem/SummaryReport/ReportEmpHistoryOJT/report-historyojt.component';
import { SetuptrainingEditComponent } from './TrainingSystem/SetupTrainingProgram/SetupTrainingProgram-edit/setuptraining-edit.component';
import { ReportTrainingempComponent } from './TrainingSystem/SummaryReport/ReportTrainingEmp/report-trainingemp.component';
import { SetupEmptrainingaddComponent } from './TrainingSystem/SetupTrainingProgram/SetupEmptraining_add/SetupEmptraining_add';
import { ReportsumpaytrainComponent } from './TrainingSystem/SummaryReport/ReportSumpayTraining/report-sumpaytrain.component';
import { DashboardbalaComponent } from './dashboard/dashboardbala/dashboardbala.component';
import { ReportHrstrainyearComponent } from './TrainingSystem/SummaryReport/ReportHrsTrainyear/report-hrstrainingyear.component';
import { ReporttrainingpayComponent } from './TrainingSystem/SummaryReport/ReportTrainingpay/report-trainingpay.component';
import { ReportCoursemonthComponent } from './TrainingSystem/SummaryReport/ReportCoursemonth/report-coursemonth.component';
import { ReportTakereserveComponent } from './TrainingSystem/SummaryReport/ReportTakereserve/report-takereserve.component';
import { ReportTrainingmonthComponent } from './TrainingSystem/SummaryReport/ReportTrainingmonth/report-trainingmonth.component';
import { ReportDetailpaymonthComponent } from './TrainingSystem/SummaryReport/ReportDetailpaymonth/report-detailpaymonth.component';
import { ReportnumEmptrainComponent } from './TrainingSystem/SummaryReport/ReportNumEmpTraining/report-numemptrain.component';
import { EmptrainingUpdateComponent } from './TrainingSystem/TrainingInformation/Employee Training/Employee Training-edit/emptraining-edit.component';
import { SetnumberDirective } from './setnumber.directive';
import { TrainingregisterComponent } from './TrainingSystem/TrainingInformation/Register-Training-add/trainingregister.component';
import { RegisteraddComponent } from './TrainingSystem/TrainingInformation/Register-Training-add/Register-add/register-add.component';
import { checktrainingComponent } from './TrainingSystem/TrainingInformation/Register-Training-add/Check-Training/check-training.component';
import { FileUploadModule } from 'ng2-file-upload';
import { FileUploadModule as PrimeNgFileUploadModule } from 'primeng/primeng';
import { IncomAndDeductibleComponent } from './EmpMaster/Employee/EmpDetailModal/IncomAndDeductible/IncomAndDeductible';
import { EmployeeTrainingRecordComponent } from './EmpMaster/Employee/EmpDetailModal/EmployeeTrainingRecord/EmployeeTrainingRecord';
import { EmployeeExperienceComponent } from './EmpMaster/Employee/EmpDetailModal/EmployeeExperience/EmployeeExperience';
import { CommendationRecordComponent } from './EmpMaster/Employee/EmpDetailModal/CommendationRecord/CommendationRecord';
import { MisconductRecordComponent } from './EmpMaster/Employee/EmpDetailModal/MisconductRecord/MisconductRecord';
import { PaymentInquiryComponent } from './EmpMaster/Employee/EmpDetailModal/PaymentInquiry/PaymentInquiry';
import { BenefitInquiryComponent } from './EmpMaster/Employee/EmpDetailModal/BenefitInquiry/BenefitInquiry';
import { LoanInquiryComponent } from './EmpMaster/Employee/EmpDetailModal/LoanInquiry/LoanInquiry';
import { AppraisalInquiryComponent } from './EmpMaster/Employee/EmpDetailModal/AppraisalInquiry/AppraisalInquiry';
import { EmployeeAddressListComponent } from './EmpMaster/Employee/EmpDetailModal/EmployeeAddressList/EmployeeAddressList';
import { GuarrantorListComponent } from './EmpMaster/Employee/EmpDetailModal/GuarrantorList/GuarrantorList';
import { EmergencyContactComponent } from './EmpMaster/Employee/EmpDetailModal/EmergencyContact/EmergencyContact';
import { AssetOnHandComponent } from './EmpMaster/Employee/EmpDetailModal/AssetOnHand/AssetOnHand';
import { PromotionRecordComponent } from './EmpMaster/Employee/EmpDetailModal/PromotionRecord/PromotionRecord';
import { BonusHistoryComponent } from './EmpMaster/Employee/EmpDetailModal/BonusHistory/BonusHistory';
import { EmployeeMovementRecComponent } from './EmpMaster/Employee/EmpDetailModal/EmployeeMovementRec/EmployeeMovementRec';
import { AttendenceHistoryComponent } from './EmpMaster/Employee/EmpDetailModal/AttendenceHistory/AttendenceHistory';
import { LeavingDocumentListComponent } from './EmpMaster/Employee/EmpDetailModal/LeavingDocumentList/LeavingDocumentList';
import { SummaryArrendnaceListComponent } from './EmpMaster/Employee/EmpDetailModal/SummaryArrendnaceList/SummaryArrendnaceList';
import { LeavingProfileComponent } from './EmpMaster/Employee/EmpDetailModal/LeavingDocumentList/leaving-profile/leaving-profile.component';
import { SetcodepositionComponent } from './EmpMaster/Position/Set-Code-Position/setcode-position.component';
import { SetcodepositionaddComponent } from './EmpMaster/Position/Set-Code-Position/SetCodePosition-add/setcodeposition-add.component';
import { RecruitmentComponent } from './EmpMaster/Recruitment/recruitment.component';
import { recruitmentaddComponent } from './EmpMaster/Recruitment/Recruitment-add/recruitment-add.component';
import { EmployeeresignComponent } from './EmpMaster/EmployeeResign/employeeresign.component';
import { EmployeeresignaddComponent } from './EmpMaster/EmployeeResign/EmployeeResign-add/employeeresign-add.component';
import { empmoveComponent } from './EmpMaster/Empmove/empmove.component';
import { empmoveaddComponent } from './EmpMaster/Empmove/empmove-add/empmove-add.component';
import { CommendationletterComponent } from './EmpMaster/CommendationLetter/commendationletter.component';
import { commendationletteraddComponent } from './EmpMaster/CommendationLetter/CommendationLetter-add/commendationletter-add.component';
import { EmployeenoticeComponent } from './EmpMaster/Employee Notice/employeenotice.component';
import { EmployeenoticeaddComponent } from './EmpMaster/Employee Notice/EmployeeNotice-add/employeenotice-add.component';
import { importantletterComponent } from './EmpMaster/Important Letter/importantletter.component';
import { importantletteraddComponent } from './EmpMaster/Important Letter/ImportantLetter-add/importantletter-add.component';
import { ManpowerComponent } from './EmpMaster/Manpower/manpower.component';
import { ManpoweraddComponent } from './EmpMaster/Manpower/Manpower-add/manpower-add.component';
import { CodeListingComponent } from './EmpMaster/Employee/EmpDetailModal/CodeListing/CodeListing';
import { PersonListComponent } from './EmpMaster/Employee/EmpDetailModal/personlist/personlist.component';
import { AddpersonlistComponent } from './EmpMaster/Employee/EmpDetailModal/personlist/addpersonlist/addpersonlist.component';
import { PhoneComponent } from './EmpMaster/Employee/EmpDetailModal/Phone/Phone';
import { TrainingsearchADDComponent } from './TrainingSystem/TrainingInformation/Training Search/Trainingsearch_ADD/Trainingsearch_ADD';
import { PictureComponent } from './EmpMaster/Employee/EmpDetailModal/Picture/Picture';
import { CodeTBListingComponent } from './EmpMaster/Employee/EmpDetailModal/CodeTBListing/CodeTBListing';
import { TraininghistoryComponent } from './TrainingSystem/TrainingInformation/Training Search/Training History/traininghistory.component';
import { selectkpiComponent } from './TrainingSystem/SetupTrainingProgram/SelectKPI/selectkpi.component';
import { ReportsumEmpComponent } from './EmpMaster/EmployeeMasterIn/ReportsumEmp/reportsumemp.component';
import { ReportsumEmppositionComponent } from './EmpMaster/EmployeeMasterIn/ReportSumEmpPosition/report-sumempposition.component';
import { ReportProbationComponent } from './EmpMaster/EmployeeMasterIn/Reportprobation/report-probation.component';
import { ReportRegistrationComponent } from './EmpMaster/EmployeeMasterIn/ReportRegistration/report-registration.component';
import { ReportEmpRegisterComponent } from './EmpMaster/EmployeeMasterIn/ReportEmpRegister/report-empregister.component';
import { ReportEmpListingComponent } from './EmpMaster/EmployeeMasterIn/ReportEmplisting/report-emplisting.component';
import { ReportCertificateComponent } from './EmpMaster/EmployeeMasterIn/ReportCertificate/report-certificate.component';
import { ReportManpowerComponent } from './EmpMaster/EmployeeMasterIn/ReportManpower/report-manpower.component';
import { PrintrecruitmentComponent } from './EmpMaster/Recruitment/Print-Recruiment/print-recruitment.component';
import { PrintEmpResignComponent } from './EmpMaster/EmployeeResign/PrintEmployeeResign/printempresign.component';
import { PrintEmpMoveComponent } from './EmpMaster/Empmove/PrintEmpmove/printempmovement.component';
import { BenefitsReportComponent } from './EmpMaster/Employee Benefit/Benefits Report/benefitreport.component';
import { EmployeemasterlistComponent } from './EmpMaster/EmployeeMasterIn/Empmaster/empmasterlist.component';
import { EmployeemovementlistComponent } from './EmpMaster/EmployeeMasterIn/EmpmovementList/empmovement-list.component';
import { SetpositionbycerComponent } from './EmpMaster/EmployeeMasterIn/ReportCertificate/Setpositionbycer/setpositionbycer.component';
import { PositionCFComponent } from './EmpMaster/Position/Set-Code-Position/PositionCF/PositionCF';
import { EmplistComponent } from './TrainingSystem/SummaryReport/Emplist/emplist.component';
import { WorkplanComponent } from './HrSystem/Workplan/workplan.component';
import { WorkplanAddComponent } from './HrSystem/Workplan/workplan-add/workplan-add.component';
import { WorkplanAdd2Component } from './HrSystem/Workplan/workplan-add2/workplan-add2.component';
import { WorkplanEditComponent } from './HrSystem/Workplan/workplan-edit/workplanedit.component';
import { LeavingSettingComponent } from './EmpMaster/Employee/EmpDetailModal/LeavingSetting/LeavingSetting';
NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@Pipe({name: 'convertCheckbox'})
export class ConvertCheckboxFromStringpipe implements PipeTransform {
      transform(value: string): boolean {
        return value === 'true';
      }
}  

@NgModule({
    imports: [
        DevExtremeModule,
        DxLoadPanelModule,
        DxChartModule,
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        MainRoutingModule,
        CountoModule,
        NgxChartsModule,
        FileUploadModule,
        PrimeNgFileUploadModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot()
    ],
    declarations: [
        DashboardComponent,
       
        PhoneBookComponent,
        CreatePersonModalComponent,
        EditPersonModalComponent,
       
        VendorMasterWebComponent,
 
        DepartmentMasterComponent,
       
        ConvertCheckboxFromStringpipe,
        TableshiftcodeComponent,
        TableShiftcodeEditComponent,
        TableShiftcodeAddComponent,
        Printngpm1Component,
       
      
       
       
        AccInterfaceDataComponent,
        TransactionReceivingComponent,
        CustomerMasterComponent,
       
        TaxCenterComponent,
        AssetsManagementComponent,
        PaymentComponent,
        InventoryComponent,
        ChqueAndBankComponent,
        GLComponent,
        GlobalSettingComponent,
        EmployeeMasterComponent,

        TimeandAttendanceHRComponent,
        TimeAndAttendanceComponent,
        PayrollComponent,
        TrainingComponent,
        PFComponent,
        AppraisalComponent,
        InterfaceDFComponent,
        DoctorMasterComponent,
        DoctorFeeComponent,
        DoctorPayComponent,
        VersionCheckComponent,
        UserManualComponent,

        AddinstituteinformationComponent,
        SetupbudgetAddComponent,
        SetupBudgeteditComponent,
        SetupbudgetComponent,
        SetuptrainingAddComponent,
        SetuptrainingComponent,
        TraininginformationComponent,
        EmptrainingAddComponent,
        EmpTrainingComponent,
        DashboardclientipdComponent,
        DashboardclientopdComponent,
        TrainingsearchComponent,
        ReportregisterComponent,
        ReportEmpHistoryComponent,
        ReportPlantrainingComponent,
        ReportHistoryOJTComponent,
        ReportTrainingempComponent,
        SetuptrainingEditComponent,
        SetupEmptrainingaddComponent,
        ReportsumpaytrainComponent,
        DashboardbalaComponent,
        ReportHrstrainyearComponent,
        ReporttrainingpayComponent,
        ReportCoursemonthComponent,
        ReportTakereserveComponent,
        ReportTrainingmonthComponent,
        ReportDetailpaymonthComponent,
        ReportnumEmptrainComponent,
        EmptrainingUpdateComponent,
        SetnumberDirective,
        TrainingregisterComponent,
        RegisteraddComponent,
        EmpComponent,
        CreateEmpModalComponent,
        EmpP1ModalComponent,
        MemberOFFamilyComponent,
        CodeListingComponent,
        PhoneComponent,
        personalModalComponent,
        IncomAndDeductibleComponent,
        EducationComponent,
        checktrainingComponent,
        EmployeeTrainingRecordComponent,
        EmployeeExperienceComponent,
        CommendationRecordComponent,
        MisconductRecordComponent,
        PaymentInquiryComponent,
        BenefitInquiryComponent,
        LoanInquiryComponent,
        AppraisalInquiryComponent,
        EmployeeAddressListComponent,
        GuarrantorListComponent,
        EmergencyContactComponent,
        AssetOnHandComponent,
        PromotionRecordComponent,
        BonusHistoryComponent,
        EmployeeMovementRecComponent,
        AttendenceHistoryComponent,
        LeavingDocumentListComponent,
        LeavingProfileComponent,
        SummaryArrendnaceListComponent,
        SetcodepositionComponent,
        SetcodepositionaddComponent,
        RecruitmentComponent,
        recruitmentaddComponent,
        EmployeeresignComponent,
        EmployeeresignaddComponent,
        empmoveComponent,
        empmoveaddComponent,
        CommendationletterComponent,
        commendationletteraddComponent,
        EmployeenoticeComponent,
        EmployeenoticeaddComponent,
        importantletterComponent,
        importantletteraddComponent,
        ManpowerComponent,
        ManpoweraddComponent,
        PersonListComponent,
        TrainingsearchADDComponent,
        AddpersonlistComponent,
        PictureComponent,
        CodeTBListingComponent,
        TraininghistoryComponent,
        selectkpiComponent,
        ReportsumEmpComponent,
        ReportsumEmppositionComponent,
        ReportProbationComponent,
        ReportRegistrationComponent,
        ReportEmpRegisterComponent,
        ReportEmpListingComponent,
        ReportCertificateComponent,
        ReportManpowerComponent,
        PrintrecruitmentComponent,
        PrintEmpResignComponent,
        PrintEmpMoveComponent,
        BenefitsReportComponent,
        EmployeemasterlistComponent,
        EmployeemovementlistComponent,
        SetpositionbycerComponent,
        PositionCFComponent,
        EmplistComponent,
        WorkplanComponent,
        WorkplanAddComponent,
        WorkplanAdd2Component,
        WorkplanEditComponent,
        LeavingSettingComponent
    ],
    providers: [
        { provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig },
        { provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig },
        { provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale }
    ]
})
export class MainModule { }
