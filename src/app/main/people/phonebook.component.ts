import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DxDataGridModule } from 'devextreme-angular';
import * as events from "devextreme/events";

import * as _ from 'lodash';

@Component({
    templateUrl: './phonebook.component.html',
    styleUrls: ['./phonebook.component.less'],
    animations: [appModuleAnimation()]
})
export class PhoneBookComponent extends AppComponentBase implements OnInit {

    // people: PersonListDto[] = [];
    // filter: string = '';

    // editingPerson: PersonListDto = null;
    // newPhone: AddPhoneInput = null;

    shownLoginName: string = "";
    shownLoginDepartment: string = "";

    constructor(
        injector: Injector,
        // private _personService: PersonServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        // this.getPeople();
        this.getCurrentLoginInformations();
    }

    // getPeople(): void {
    //     this._personService.getPeople(this.filter).subscribe((result) => {
    //         this.people = result.items;
    //     });
    // }

    // deletePerson(person: PersonListDto): void {
    //     console.log(person);
    //     this.message.confirm(
    //         this.l('AreYouSureToDeleteThePerson', person.name),
    //         isConfirmed => {
    //             if (isConfirmed) {
    //                 this._personService.deletePerson(person.id).subscribe(() => {
    //                     this.notify.info(this.l('SuccessfullyDeleted'));
    //                     _.remove(this.people, person);
    //                 });
    //             }
    //         }
    //     );
    // }

    // editPerson(person: PersonListDto): void {
    //     if (person === this.editingPerson) {
    //         this.editingPerson = null;
    //     } else {
    //         this.editingPerson = person;

    //         this.newPhone = new AddPhoneInput();
    //         this.newPhone.type = AddPhoneInputType._0;
    //         this.newPhone.personId = person.id;
    //     }
    // };

    // getPhoneTypeAsString(phoneType: PhoneInPersonListDtoType): string {
    //     switch (phoneType) {
    //         case PhoneInPersonListDtoType._0:
    //             return this.l('PhoneType_Mobile');
    //         case PhoneInPersonListDtoType._1:
    //             return this.l('PhoneType_Home');
    //         case PhoneInPersonListDtoType._2:
    //             return this.l('PhoneType_Business');
    //         default:
    //             return '?';
    //     }
    // };

    // deletePhone(phone, person): void {
    //     this._personService.deletePhone(phone.id).subscribe(() => {
    //         this.notify.success(this.l('SuccessfullyDeleted'));
    //         _.remove(person.phones, phone);
    //     });
    // };

    // savePhone(): void {
    //     if (!this.newPhone.number) {
    //         this.message.warn('Please enter a number!');
    //         return;
    //     }
    //     console.log(this.newPhone);
    //     this._personService.addPhone(this.newPhone).subscribe(result => {
    //         this.editingPerson.phones.push(result);
    //         this.newPhone.number = '';

    //         this.notify.success(this.l('SavedSuccessfully'));
    //     });
    // };

    onCellPrepared (e) {
        if (e.rowType == "data" && e.column.command == "edit") {
            let cellElement = e.cellElement,
                editLink = cellElement.querySelector(".dx-link-edit");
            events.off(editLink); 
            events.on(editLink, "dxclick", (args) => {
                // Implement your logic here
                //editPersonModal.show(person.id);
                
            });
        }
    };

    rowClickEvent(e) {
        //debugger;
        console.log(e);
        alert('in rowClickEvent');
        
      };

      getCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.shownLoginDepartment = this.appSession.getShownLoginDepartmentCode();      
    }

      runINFA(): void {
        window.location.href = "INFA:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }  

       runRCCP(): void {
        window.location.href = "RCCP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }      
       
       runCUSP(): void {
        window.location.href = "CUSP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       } 

       runVENP(): void {
        window.location.href = "VENP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }  
       
       runVATP(): void {
        window.location.href = "VATP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }      
       
       runASMP(): void {
        window.location.href = "ASMP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     
       
       runPTTP(): void {
        window.location.href = "PTTP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }  
       
       runINVP(): void {
        window.location.href = "INVP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     
       
       runCBKP(): void {
        window.location.href = "CBKP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       } 
       
       runGLLP(): void {
        window.location.href = "GLLP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }   
       
       runAPSP(): void {
        window.location.href = "APSP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     
       
       runPAFP(): void {
        window.location.href = "PAFP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runAPPP(): void {
        window.location.href = "APPP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runMASP(): void {
        window.location.href = "MASP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runTRNP(): void {
        window.location.href = "TRNP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runATTP(): void {
        window.location.href = "ATTP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runWRKP(): void {
        window.location.href = "WRKP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runATLP(): void {
        window.location.href = "ATLP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runPAYP(): void {
        window.location.href = "PAYP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runINDP(): void {
        window.location.href = "INDP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runDTYP(): void {
        window.location.href = "DTYP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runDTFP(): void {
        window.location.href = "DTFP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     

       runDMSP(): void {
        window.location.href = "DMSP:KK001KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
       }     
}