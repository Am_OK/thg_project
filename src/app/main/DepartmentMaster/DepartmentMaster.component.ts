import { Component, Injector, OnInit, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UserServiceProxy,
         LinkedUserDto,
         DbmtabServiceProxy,
         DbmtabListDto,
         CreateDbmtabInput,
         UpdateDbmtabInput} from '@shared/service-proxies/service-proxies';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Http, HttpModule } from '@angular/http';
import { DxDataGridModule } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { finalize } from 'rxjs/operators';

@Component({
    templateUrl: './DepartmentMaster.component.html',
    styleUrls: ["./DepartmentMaster.component.less"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DepartmentMasterComponent extends AppComponentBase implements OnInit  {

    filter: string = '';
    shownLoginName: string = "";
    filterTab : string = 'SECTORIBUDGET';
    dbmtab : DbmtabListDto[] = [];

    create_dbmtab : CreateDbmtabInput = null;
    update_dbmtab : UpdateDbmtabInput = null;    
    saving: boolean = false;    

    shownLoginDepartment: string = "";
    typeofSection : string = "";

    constructor(
        injector: Injector,
        private _userServiceProxy: UserServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    
      getCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        this.shownLoginDepartment = this.appSession.getShownLoginDepartmentCode();

        if (this.shownLoginDepartment =="61000600")
        {
            this.typeofSection = "Master";
        }
        else
        {
            this.typeofSection = "";
        }
    }
    

    ngOnInit(): void {
         this.getCurrentLoginInformations();
         this.getDbmtab00();
         console.log("LogIn Info:",this.shownLoginName);
         //window.location.href = "KP03:KK006KKKKKKKK001KKKKKKKKTHAKKKKKKKK"+this.shownLoginName;
    }

    getDbmtab00(): void {
        this._dbmtabService.getDbmtabFromTABTB1(this.filterTab,'').subscribe((result) => {
            this.dbmtab = result.items;            
        });
    } 
onValueChanged(evt: any, data: any): void {
    data.setValue(evt.value);
    //console.log("onValueChanged:",evt.value,data);
}

insertDbmtab(e: any): void {
   this.create_dbmtab = new CreateDbmtabInput();
   for (var prop in e.data) {
            this.create_dbmtab[prop] = e.data[prop]
    }
    // if (this.create_dbmtab.budgetHeader == null || this.create_dbmtab.budgetHeader == "" )  
    // {
    //    //console.log("Not set glIsHeader:",this.create_glmaster.glIsHeader);
    //    this.create_budgetmaster.budgetHeader = 'false';
    // }
    this.create_dbmtab.tabtB1 ="SECTORIBUDGET";
    console.log("GL Infor:",this.create_dbmtab);    
        this._dbmtabService.createDbmtab(this.create_dbmtab)
            // .finally(() => this.saving = false)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.getDbmtab00();
                //this.close();
                //this.modalSave.emit(this.person);
                // this.getGroupHeader();
                 //console.log("Start Refresh Group Header:");
            });
}    
updateDbmtab(e: any): void {
//    this.update_glmaster = new UpdateGLInput();
//    var keyValue = e.oldData.id;
//    for (var prop in e.data) {
//             this.update_glmaster[prop] = e.data[prop]
//     }
    this.update_dbmtab = new UpdateDbmtabInput();
    var keyValue = e.oldData.id;
    var elementToUpdate = this.dbmtab.find(e => e["id"] == keyValue);
    for (var prop in e.newData) {
           elementToUpdate[prop] = e.newData[prop]
    }
    this.update_dbmtab = elementToUpdate;

        console.log("Update:",this.update_dbmtab);
        this._dbmtabService.updateDbmtab(this.update_dbmtab)
            // .finally(() => this.saving = false)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.getDbmtab00();
            });
    
}    

deleteDbmtab(e:any): void {    
        console.log("Phone ID:",e);
        this.message.confirm(
            this.l('AreYouSureToDeleteGLCode', e.data.name),
            isConfirmed => {
                if (isConfirmed) {
                    this._dbmtabService.deleteDbmtab(e.data.id).subscribe(() => {
                        this.notify.info(this.l('SuccessfullyDeleted'));
                        //_.remove(this.people, person);
                    });
                }
            }
        );        
}       

onContentReady(e) {
         e.component.columnOption("command:edit", {
            visibleIndex: -1,
            width: 80
        });
}

onCellPrepared(e) {
        if (e.rowType === "data" && e.column.command === "edit") {
            var isEditing = e.row.isEditing,
                $links = e.cellElement.find(".dx-link");

            $links.text("");

            if (this.typeofSection == "Master")
            {
                if (isEditing) {
                    $links.filter(".dx-link-save").addClass("dx-icon-save");
                    $links.filter(".dx-link-cancel").addClass("dx-icon-revert");
                } else {
                    $links.filter(".dx-link-edit").addClass("dx-icon-edit");
                    $links.filter(".dx-link-delete").addClass("dx-icon-trash");
                }
            }
        }
}        

}