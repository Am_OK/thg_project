import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, MSMPSN00ListDto, EmployeeMovmentsListDto, EmployeeMovmentsServiceProxy, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { empmoveComponent } from '../empmove.component';
import { DxDataGridComponent } from 'devextreme-angular';
import { data_Empmas } from '../../data_center';


@Component({
    selector: 'empmoveadd',
    templateUrl: './empmove-add.component.html',
})
export class empmoveaddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal5') modal5: ModalDirective;
    active: boolean;
    POSITION: DbmtabListDto[] = []
    DEPARTMENT: DbmtabListDto[] = []
    dbmtabemti: DbmtabListDto[] = []
    MOVERESON: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    dbmtab: DbmtabListDto[] = [];
    DIVISION: DbmtabListDto[] = []
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private data_Empmas: data_Empmas,
        private empmoveComponent: empmoveComponent,
        private EmployeeMovmentsServiceProxy: EmployeeMovmentsServiceProxy,
        private KPShareServiceProxy: KPShareServiceProxy
    ) {
        super(injector);
    }
    EmployeeMovmentsObject: EmployeeMovmentsListDto = new EmployeeMovmentsListDto()
    getItemMaster() {
        this.POSITION = this.empmoveComponent.POSITION
        this.DEPARTMENT = this.empmoveComponent.DEPARTMENT
        this.dbmtabemti = this.empmoveComponent.dbmtabemti
        this.MOVERESON = this.empmoveComponent.MOVERESON
        this.DIVISION = this.empmoveComponent.DIVISION
        this.emp = this.empmoveComponent.emp
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
    }
    show(): void {
        this.getItemMaster()
        this.dbmtab = this.data_Empmas.dbmtab
        this.modal.show();
        this.active = true;
        this.EmployeeMovmentsObject = new EmployeeMovmentsListDto()
        this.EmployeeMovmentsObject.movfL1 = '001'
        this.EmployeeMovmentsObject.movfL2 = '001'
        this.EmployeeMovmentsObject.movtL1 = '001'
        this.EmployeeMovmentsObject.movtL2 = '001'
        this.EmployeeMovmentsObject.movcsa = 0
        this.EmployeeMovmentsObject.movnsa = 0
        
    }
    close(): void {
        this.modal.hide();
        this.active = false;
        this.Editing = 0
        $('#empname').val('')
        $('#autname').val('')
    }
    fnnum() {
        $('#movcsa').val(this.fnformat($('#movcsa').val()))
        $('#movnsa').val(this.fnformat($('#movnsa').val()))
    }
    showedit(e) {
        this.getItemMaster()
        this.Editing = 1
        this.EmployeeMovmentsObject = new EmployeeMovmentsListDto()
        this.EmployeeMovmentsObject.id = e.data.id
        this.EmployeeMovmentsObject.movdte = this.fnddmmyyyy(e.data.movdte)
        this.EmployeeMovmentsObject.movdoc = e.data.movdoc
        this.EmployeeMovmentsObject.movefd = this.fnddmmyyyy(e.data.movefd)
        this.EmployeeMovmentsObject.movidn = e.data.movidn
        this.EmployeeMovmentsObject.movaut = e.data.movaut
        this.EmployeeMovmentsObject.movrea = e.data.movrea
        this.EmployeeMovmentsObject.movsts = e.data.movsts
        this.EmployeeMovmentsObject.movstd = this.fnddmmyyyy(e.data.movstd)
        this.EmployeeMovmentsObject.movfL1 = e.data.movfL1
        this.EmployeeMovmentsObject.movfL2 = e.data.movfL2
        this.EmployeeMovmentsObject.movfL3 = e.data.movfL3
        this.EmployeeMovmentsObject.movfL4 = e.data.movfL4
        this.EmployeeMovmentsObject.movtL1 = e.data.movtL1
        this.EmployeeMovmentsObject.movtL2 = e.data.movtL2
        this.EmployeeMovmentsObject.movtL3 = e.data.movtL3
        this.EmployeeMovmentsObject.movtL4 = e.data.movtL4
        this.EmployeeMovmentsObject.movcps = e.data.movcps
        this.EmployeeMovmentsObject.movnps = e.data.movnps
        this.EmployeeMovmentsObject.movcsa = e.data.movcsa
        this.EmployeeMovmentsObject.movnsa = e.data.movnsa

        var empname = this.emp.filter(res => res.psnidn == this.EmployeeMovmentsObject.movidn)
        var autname = this.emp.filter(res => res.psnidn == this.EmployeeMovmentsObject.movaut)
        setTimeout(() => {
            this.fnnum()
            empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            autname.length > 0 ? $('#autname').val(autname[0].psnfnm + ' ' + autname[0].psnlnm) : $('#autname').val('')
        }, 250);
        this.modal.show();
    }

    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var movdte = $('#movdte').val() != null && $('#movdte').val() != undefined ? movdte = this.fnyyyymmdd($('#movdte').val()) : movdte = ''
                    var movefd = $('#movefd').val() != null && $('#movefd').val() != undefined ? movefd = this.fnyyyymmdd($('#movefd').val()) : movefd = ''
                    var movstd = $('#movstd').val() != null && $('#movstd').val() != undefined ? movstd = this.fnyyyymmdd($('#movstd').val()) : movstd = ''
                    var movcsa = $('#movcsa').val() != null && $('#movcsa').val() != undefined ? movcsa = this.fnreplace($('#movcsa').val()) : movcsa = 0
                    var movnsa = $('#movnsa').val() != null && $('#movnsa').val() != undefined ? movnsa = this.fnreplace($('#movnsa').val()) : movnsa = 0

                    if ($('#movfL1').val() != null) {
                        this.EmployeeMovmentsObject.movfL1 = $('#movfL1').val().toString()
                    }
                    if ($('#movfL2').val() != null) {
                        this.EmployeeMovmentsObject.movfL2 = $('#movfL2').val().toString()
                    }
                    if ($('#movfL3').val() != null) {
                        this.EmployeeMovmentsObject.movfL3 = $('#movfL3').val().toString()
                    }
                    if ($('#movfL4').val() != null) {
                        this.EmployeeMovmentsObject.movfL4 = $('#movfL4').val().toString()
                    }
                    if ($('#movcps').val() != null) {
                        this.EmployeeMovmentsObject.movcps = $('#movcps').val().toString()
                    }
                    if ($('#movtL1').val() != null) {
                        this.EmployeeMovmentsObject.movfL1 = $('#movtL1').val().toString()
                    }
                    if ($('#movtL2').val() != null) {
                        this.EmployeeMovmentsObject.movtL2 = $('#movtL2').val().toString()
                    }
                    if ($('#movtL3').val() != null) {
                        this.EmployeeMovmentsObject.movtL3 = $('#movtL3').val().toString()
                    }
                    if ($('#movtL4').val() != null) {
                        this.EmployeeMovmentsObject.movtL4 = $('#movtL4').val().toString()
                    }
                    if ($('#movnps').val() != null) {
                        this.EmployeeMovmentsObject.movnps = $('#movnps').val().toString()
                    }
                    if ($('#movrea').val() != null) {
                        this.EmployeeMovmentsObject.movrea = $('#movrea').val().toString()
                    }
                    
                    this.EmployeeMovmentsObject.movdte = movdte
                    this.EmployeeMovmentsObject.movefd = movefd
                    this.EmployeeMovmentsObject.movstd = movstd

                    this.EmployeeMovmentsObject.movcsa = movcsa
                    this.EmployeeMovmentsObject.movnsa = movnsa

                    if (this.Editing == 1) {
                        this.EmployeeMovmentsServiceProxy.updateEmployeeMovments(this.EmployeeMovmentsObject).subscribe(() => {
                            this.close()
                            this.modalSave.emit()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.EmployeeMovmentsObject.movdoc != '' && this.EmployeeMovmentsObject.movdoc != null && this.EmployeeMovmentsObject.movdoc != undefined) {
                            this.EmployeeMovmentsServiceProxy.createEmployeeMovments(this.EmployeeMovmentsObject).subscribe(() => {
                                this.close()
                                this.modalSave.emit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.KPShareServiceProxy.getDocumentNumber('MSN', this.EmployeeMovmentsObject.movdte, 'THI').subscribe((result) => {
                                this.EmployeeMovmentsObject.movdoc = result[0].documentNumber
                                this.EmployeeMovmentsServiceProxy.createEmployeeMovments(this.EmployeeMovmentsObject).subscribe(() => {
                                    this.close()
                                    this.modalSave.emit()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }
                    }

                }
            })
    }

    removed() {
        this.EmployeeMovmentsServiceProxy.deleteEmployeeMovments(this.EmployeeMovmentsObject.id).subscribe(() => {
            this.modalSave.emit()
        })
    }
    Editing = 0
    @ViewChild("gEmployeea") gEmployeea: DxDataGridComponent
    OpMdl
    show5(vl) {
        this.OpMdl = vl
        this.modal5.show();
    }
    closeemp() {
        this.gEmployeea.instance.clearSelection()
        this.modal5.hide();
    }
    get_datan(eid) {
        if (this.OpMdl == 'Emp') {
            this.EmployeeMovmentsObject.movidn = eid
            var empname = this.emp.filter(res => res.psnidn == this.EmployeeMovmentsObject.movidn)
            setTimeout(() => {
                empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            }, 250);
            this.closeemp()
        }
        if (this.OpMdl == 'Aut') {
            this.EmployeeMovmentsObject.movaut = eid
            var empname = this.emp.filter(res => res.psnidn == this.EmployeeMovmentsObject.movaut)
            setTimeout(() => {
                empname.length > 0 ? $('#autname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#autname').val('')
            }, 250);
            this.closeemp()
        }
    }

}

