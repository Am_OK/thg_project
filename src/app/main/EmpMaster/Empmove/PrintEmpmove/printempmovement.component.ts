import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './printempmovement.component.html',
    animations: [appModuleAnimation()]
})
export class PrintEmpMoveComponent extends AppComponentBase implements OnInit {
    emp: MSMPSN00ListDto[] = [];

    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
        

    }
    chkexcel(){
        $('#typmove1').val('True')
    }
    
    prints(movdtefm,movdteto,docfm,docto,typmove1) {
        if (movdteto != "" && docto != "" ) {
            if(docto == 'ZZZZ'){docfm == ''}
            if (movdteto == "" || movdteto == null || movdteto == undefined || docto == "" || docto == null || docto == undefined) { movdteto = '' ,docto =''}
            http://27.254.140.187:5433/WebForms/EmployeeMaster/WebEmployeeMoveMent.aspx?COM=001&BRN=001&STRDTE=20180601&ENDDTE=20190618&STRDOC=&ENDDOC=&Excel=False       
            window.open(" http://27.254.140.187:5433/WebForms/EmployeeMaster/WebEmployeeMoveMent.aspx?COM=001&BRN=001" + "&STRDTE=" + this.fnyyyymmdd(movdtefm) + "&ENDDTE=" + this.fnyyyymmdd(movdteto)+ "&STRDOC=" + docfm + "&ENDDOC=" + docto + "&Excel=" + typmove1 )
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
    
}
