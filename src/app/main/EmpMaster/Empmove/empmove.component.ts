import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto ,MSMPSN00ServiceProxy, EmployeeMovmentsServiceProxy, EmployeeMovmentsListDto, MSMPSN00ListDto} from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'empmove',
    templateUrl: './empmove.component.html',
})
export class empmoveComponent extends AppComponentBase implements OnInit{
   
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private MSMPSN00ServiceProxy:MSMPSN00ServiceProxy,
        private EmployeeMovmentsServiceProxy:EmployeeMovmentsServiceProxy
    ) {
        super(injector);
    }
    EmployeeMovmentsListDto:EmployeeMovmentsListDto[]=[]
    POSITION:DbmtabListDto[]=[]
    DEPARTMENT:DbmtabListDto[]=[]
    dbmtabemti:DbmtabListDto[]=[]
    emp: MSMPSN00ListDto[] = []
    MOVERESON:DbmtabListDto[]=[]
    DIVISION:DbmtabListDto[]=[]
    getDisplayExpr(item) {
        // console.log(item)
        if (!item) {
          return "";
        }
        // console.log(item.psnfnm + " " + item.psnlnm)
        return item.psnfnm + " " + item.psnlnm
      }
    ngOnInit(): void {
        $('#to').val('ALL')
        this.getMovement('','ALL')
        this.getmaster()
        this.getEmployees();
    }
    
    getEmployees(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
    }
    getmaster() {
        this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
            this.dbmtabemti = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('POSITION', '').subscribe((result) => {
            this.POSITION = result.items

        })
        this._dbmtabService.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;
          });
          this._dbmtabService.getDbmtabFromTABFG1('DIVISION', '').subscribe((result) => {
            this.DIVISION = result.items;
          });
          this._dbmtabService.getDbmtabFromTABFG1('MOVERESON', '').subscribe((result) => {
            this.MOVERESON = result.items;
          });

    }
    getMovement(frm,to) {
        this.EmployeeMovmentsServiceProxy.getEmployeeMovmentsFromToEid(frm,to).subscribe((result) => {
            this.EmployeeMovmentsListDto = result.items
        })
    }
    
}

