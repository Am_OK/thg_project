import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, MSMPSN00ServiceProxy, MSMPSN00ListDto, PunishmentInformationListDto, PunishmentInformationServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'employeenotice',
    templateUrl: './employeenotice.component.html',
})
export class EmployeenoticeComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private PunishmentInformationServiceProxy: PunishmentInformationServiceProxy
    ) {
        super(injector);
    }
    
    show(): void {
        this.modal.show();
        this.active = true;
    }
    ngOnInit(): void {
        $('#to').val('ALL')
        this.getEmployees()
        this.getmaster()
        this.getPunish('','ALL')
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    PunishmentInformationListDto: PunishmentInformationListDto[] = []
    PunishmentInformationObject: PunishmentInformationListDto = new PunishmentInformationListDto()
    dbmtabemti: DbmtabListDto[] = []
    REASON: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    NOTICEREASON:DbmtabListDto[]=[]
  WARNINGISSUE:DbmtabListDto[]=[]
  NOTICERESULT:DbmtabListDto[]=[]
  NOTICELEVEL: DbmtabListDto[] = []
    getPunish(frm,to) {
        
        this.PunishmentInformationServiceProxy.getPunishmentInformationFromTo(frm,to).subscribe((result) => {
            this.PunishmentInformationListDto = result.items
        })
    }
    getEmployees(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
    }
    getmaster() {
       
        this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
            this.dbmtabemti = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('NOTICEREASON', '').subscribe((result) => {
            this.NOTICEREASON = result.items
      
          })
          this._dbmtabService.getDbmtabFromTABFG1('WARNINGISSUE', '').subscribe((result) => {
            this.WARNINGISSUE = result.items
      
          })
          this._dbmtabService.getDbmtabFromTABFG1('NOTICERESULT', '').subscribe((result) => {
            this.NOTICERESULT = result.items
      
          })
          this._dbmtabService.getDbmtabFromTABTB1('NOTICELEVEL', '').subscribe((result) => {
            this.NOTICELEVEL = result.items;
          });
    }
    getDisplayExpr(item) {
        // console.log(item)
        if (!item) {
            return "";
        }
        // console.log(item.psnfnm + " " + item.psnlnm)
        return item.psnfnm + " " + item.psnlnm
    }
}

