import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, PunishmentInformationServiceProxy, PunishmentInformationListDto, MSMPSN00ListDto, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { EmployeenoticeComponent } from '../employeenotice.component';
import { DxDataGridComponent } from 'devextreme-angular';


@Component({
    selector: 'employeenoticeadd',
    templateUrl: './employeenotice-add.component.html',
})
export class EmployeenoticeaddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal5') modal5: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private PunishmentInformationServiceProxy: PunishmentInformationServiceProxy,
        private EmployeenoticeComponent: EmployeenoticeComponent,
        private KPShareServiceProxy: KPShareServiceProxy
    ) {
        super(injector);
    }
    dbmtabemti: DbmtabListDto[] = []
    NOTICEREASON: DbmtabListDto[] = []
    WARNINGISSUE: DbmtabListDto[] = []
    NOTICERESULT: DbmtabListDto[] = []
    NOTICELEVEL: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    PunishmentInformationObject: PunishmentInformationListDto = new PunishmentInformationListDto()
    getItemMaster() {
        this.NOTICEREASON = this.EmployeenoticeComponent.NOTICEREASON
        this.WARNINGISSUE = this.EmployeenoticeComponent.WARNINGISSUE
        this.NOTICERESULT = this.EmployeenoticeComponent.NOTICERESULT
        this.emp = this.EmployeenoticeComponent.emp
        this.dbmtabemti = this.EmployeenoticeComponent.dbmtabemti
        this.NOTICELEVEL = this.EmployeenoticeComponent.NOTICELEVEL
    }
    show(): void {
        this.getItemMaster()
        this.modal.show();
        this.active = true;
        this.PunishmentInformationObject = new PunishmentInformationListDto()
        this.PunishmentInformationObject.notdsl = 0
        setTimeout(() => {
            this.fnnum()
        }, 200);

    }
    close(): void {
        this.modal.hide();
        this.active = false;
        this.Editing = 0
        $('#empname').val('')
        $('#autname').val('')
    }
    fnnum() {
        $('#notdsl').val(this.fnformat($('#notdsl').val()))
    }
    showedit(e) {
        this.getItemMaster()
        this.Editing = 1
        this.PunishmentInformationObject = new PunishmentInformationListDto()
        this.PunishmentInformationObject.id = e.data.id
        this.PunishmentInformationObject.notdte = this.fnddmmyyyy(e.data.notdte)
        this.PunishmentInformationObject.notdoc = e.data.notdoc
        this.PunishmentInformationObject.notefd = this.fnddmmyyyy(e.data.notefd)
        this.PunishmentInformationObject.notlev = e.data.notlev
        this.PunishmentInformationObject.noteid = e.data.noteid
        this.PunishmentInformationObject.notaut = e.data.notaut
        this.PunishmentInformationObject.notrea = e.data.notrea
        this.PunishmentInformationObject.notiss = e.data.notiss
        this.PunishmentInformationObject.notres = e.data.notres
        this.PunishmentInformationObject.notsts = e.data.notsts
        this.PunishmentInformationObject.notstd = this.fnddmmyyyy(e.data.notstd)
        this.PunishmentInformationObject.notrM1 = e.data.notrM1
        this.PunishmentInformationObject.notrM2 = e.data.notrM2
        this.PunishmentInformationObject.notrM3 = e.data.notrM3
        this.PunishmentInformationObject.notdsl = e.data.notdsl
        this.PunishmentInformationObject.notfmd = this.fnddmmyyyy(e.data.notfmd)
        this.PunishmentInformationObject.nottod = this.fnddmmyyyy(e.data.nottod)
        var empname = this.emp.filter(res => res.psnidn == this.PunishmentInformationObject.noteid)
        var autname = this.emp.filter(res => res.psnidn == this.PunishmentInformationObject.notaut)
        setTimeout(() => {
            this.fnnum()
            empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            autname.length > 0 ? $('#autname').val(autname[0].psnfnm + ' ' + autname[0].psnlnm) : $('#autname').val('')
        }, 250);
        this.modal.show();
    }

    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var notdte = $('#notdte').val() != null && $('#notdte').val() != undefined ? notdte = this.fnyyyymmdd($('#notdte').val()) : notdte = ''
                    var notefd = $('#notefd').val() != null && $('#notefd').val() != undefined ? notefd = this.fnyyyymmdd($('#notefd').val()) : notefd = ''
                    var notstd = $('#notstd').val() != null && $('#notstd').val() != undefined ? notstd = this.fnyyyymmdd($('#notstd').val()) : notstd = ''
                    var notfmd = $('#notfmd').val() != null && $('#notfmd').val() != undefined ? notfmd = this.fnyyyymmdd($('#notfmd').val()) : notfmd = ''
                    var nottod = $('#nottod').val() != null && $('#nottod').val() != undefined ? nottod = this.fnyyyymmdd($('#nottod').val()) : nottod = ''
                    var notdsl = $('#notdsl').val() != null && $('#notdsl').val() != undefined ? notdsl = this.fnreplace($('#notdsl').val()) : notdsl = 0
                    this.PunishmentInformationObject.notdte = notdte
                    this.PunishmentInformationObject.notefd = notefd
                    this.PunishmentInformationObject.notstd = notstd
                    this.PunishmentInformationObject.notfmd = notfmd
                    this.PunishmentInformationObject.nottod = nottod
                    this.PunishmentInformationObject.notdsl = notdsl
                    if (this.Editing == 1) {
                        this.PunishmentInformationServiceProxy.updatePunishmentInformation(this.PunishmentInformationObject).subscribe(() => {
                            this.close()
                            this.modalSave.emit()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.PunishmentInformationObject.notdoc != '' && this.PunishmentInformationObject.notdoc != null && this.PunishmentInformationObject.notdoc != undefined) {
                            this.PunishmentInformationServiceProxy.createPunishmentInformation(this.PunishmentInformationObject).subscribe(() => {
                                this.close()
                                this.modalSave.emit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.KPShareServiceProxy.getDocumentNumber('MSN', this.PunishmentInformationObject.notdte, 'THI').subscribe((result) => {
                                this.PunishmentInformationObject.notdoc = result[0].documentNumber
                                this.PunishmentInformationServiceProxy.createPunishmentInformation(this.PunishmentInformationObject).subscribe(() => {
                                    this.close()
                                    this.modalSave.emit()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }
                    }

                }
            })
    }

    removed() {
        this.message.confirm(
            this.l('AreYouSureToDeleteTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    this.PunishmentInformationServiceProxy.deletePunishmentInformation(this.PunishmentInformationObject.id).subscribe(() => {
                        this.modalSave.emit()
                        this.close()
                    })
                }
            })
    }
    Editing = 0
    @ViewChild("gEmployeea") gEmployeea: DxDataGridComponent
    OpMdl
    show5(vl) {
        this.OpMdl = vl
        this.modal5.show();
    }
    closeemp() {
        this.gEmployeea.instance.clearSelection()
        this.modal5.hide();
    }
    get_datan(eid) {
        if (this.OpMdl == 'Emp') {
            this.PunishmentInformationObject.noteid = eid
            var empname = this.emp.filter(res => res.psnidn == this.PunishmentInformationObject.noteid)
            setTimeout(() => {
                empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            }, 250);
            this.closeemp()
        }
        if (this.OpMdl == 'Aut') {
            this.PunishmentInformationObject.notaut = eid
            var empname = this.emp.filter(res => res.psnidn == this.PunishmentInformationObject.notaut)
            setTimeout(() => {
                empname.length > 0 ? $('#autname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#autname').val('')
            }, 250);
            this.closeemp()
        }
    }


}

