import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DbmtabListDto, DbmtabServiceProxy, MSMPSN00ServiceProxy, KPShareServiceProxy } from "@shared/service-proxies/service-proxies";
import { DxDataGridModule } from 'devextreme-angular';
import { Http, HttpModule } from '@angular/http';
import * as events from "devextreme/events";
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { data_Empmas } from '../data_center';

@Component({
    templateUrl: './emp.component.html',
    styleUrls: ['./emp.component.less'],
    animations: [appModuleAnimation()]
})
// class AddEmpMaster{}
// class UpdateEmpMaster{}
// class EmpMasterList{}
export class EmpComponent extends AppComponentBase implements OnInit {

    // create_emp : AddEmpMaster = null;
    // update_emp : UpdateEmpMaster = null;
    // emp: EmpMasterList[] = [];
    create_emp = null;
    update_emp = null;
    emp = [];
    dbmtab: DbmtabListDto[] = [];
    dbmtabemti: DbmtabListDto[] = [];
    dbmtabenen: DbmtabListDto[] = [];
    dbmtabfna: DbmtabListDto[] = [];
    dbmtabsna: DbmtabListDto[] = [];
    dbmtabamo: DbmtabListDto[] = [];
    hd = [{ 'code': 'PSNIDN', 'val': 'รหัสพนักงาน' }, { 'code': 'PSNFNM', 'val': 'ชื่อ' }, { 'code': 'PSLFNM', 'val': 'นามสกุล' }, { 'code': 'PSLDEP', 'val': 'แผนก' }]
    // , { 'code': 'PSLPOS', 'val': 'ตำแหน่ง' }
    filter: string = '';
    saving: boolean = false;

    shownLoginName: string = "";
    shownLoginDepartment: string = "";

    constructor(
        injector: Injector,
        //private _empMasterService : EmpMasterServiceProxy,
        private _dbmtabService: DbmtabServiceProxy,
        private router: Router,
        private data_Empmas: data_Empmas,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private KPShareServiceProxy:KPShareServiceProxy
    ) {
        super(injector);
    }

    // refreshpage(): void {
    //     window.location.reload();
    // }

    ngOnInit(): void {
        if (this.data_Empmas.empmasterl != 1) {
            this.data_Empmas.Empmaster()
        }

        var PSNIDN ='PSNIDN'
        var from =''
        var to ='ALL'
        this.getEmployeesData(PSNIDN, from, to)
        $('#to').val('ALL')
        // this.getEmployees();
        this.getDbmtabFromTABTB1();

    }

    getCurrentLoginInformations(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
        // this.shownLoginDepartment = this.appSession.getShownLoginDepartmentCode();
    }

    // getEmployees(): void {
    //     this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
    //         this.emp = result.items;

    //     });
    // }
    getEmployeesData(head, from, to) {
        from != '' && from != null && from != '' ? '' : from = ''
        to != '' && to != null && to != '' ? '' : to = ''
        this.KPShareServiceProxy.getEmployeeList(head, from, to).subscribe((result)=>{
            this.emp = result;
        })
    }
    DEPARTMENT: DbmtabListDto[] = [];
    getDbmtabFromTABTB1(): void {
        if (this.data_Empmas.dbmtab.length > 0 && this.data_Empmas.dbmtabemti.length > 0 && this.data_Empmas.dbmtabenen.length > 0 && this.data_Empmas.dbmtabfna.length > 0 && this.data_Empmas.dbmtabsna.length > 0 && this.data_Empmas.dbmtabamo.length > 0) {
            this.dbmtab = this.data_Empmas.dbmtab
            this.dbmtabemti = this.data_Empmas.dbmtabemti
            this.dbmtabenen = this.data_Empmas.dbmtabenen
            this.dbmtabfna = this.data_Empmas.dbmtabfna
            this.dbmtabsna = this.data_Empmas.dbmtabsna
            this.dbmtabamo = this.data_Empmas.dbmtabamo
            this.DEPARTMENT = this.data_Empmas.DEPARTMENT
        } else {
            this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
                this.dbmtab = result.items;
            });
            this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
                this.dbmtabemti = result.items;
            });

            this._dbmtabService.getDbmtabFromTABTB1('ENGTITLE', '').subscribe((result) => {
                this.dbmtabenen = result.items;
            });

            this._dbmtabService.getDbmtabFromTABTB1('NATIONALITY', '').subscribe((result) => {
                this.dbmtabfna = result.items;
            });

            this._dbmtabService.getDbmtabFromTABTB1('CITIZENT', '').subscribe((result) => {
                this.dbmtabsna = result.items;
            });

            this._dbmtabService.getDbmtabFromTABTB1('REGION', '').subscribe((result) => {
                this.dbmtabamo = result.items;
            });
            this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
                this.DEPARTMENT = result.items;
            });

        }



    }





    rowClickEvent(e) {
        //debugger;
        //console.log(e);
        alert('in rowClickEvent');
    };

    onContentReady(e) {
        e.component.columnOption("command:edit", {
            visibleIndex: -1,
            width: 70
        });
    }

    createEmpModal() {
        this.router.navigate(['/app/main/CreateEmp']);
    }

    EditEmpModal(prodoc) {
        //console.log(prodoc)
        this.router.navigate(['/app/main/EditEmp/' + prodoc]);
    }
}
