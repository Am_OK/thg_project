import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeInformationsListDto, EmployeeInformationsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'IncomAndDeductible',
    templateUrl: './IncomAndDeductible.html',
})
export class IncomAndDeductibleComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active:boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeInformationsServiceProxy:EmployeeInformationsServiceProxy
    ) {
        super(injector);
    }
    EmployeeInformationsListDto: EmployeeInformationsListDto = new EmployeeInformationsListDto()
    UpdatingS = 0
    RefEID;
    nameID
    show(id,psnfnm,psnlnm): void {
        this.active = true;
        this.nameID = psnfnm+' '+psnlnm
        this.RefEID = id
        this.EmployeeInformationsListDto = new EmployeeInformationsListDto()
        setTimeout(() => {
        this.modal.show();            
        }, 100);
        this.EmployeeInformationsServiceProxy.getEmployeeInformationsByEmpId(this.RefEID).subscribe((result) => {

            if (result.items.length <= 0) {
                
                this.EmployeeInformationsListDto.pswidn = this.RefEID
                this.UpdatingS = 0
            }else{
                this.EmployeeInformationsListDto = result.items[0]
                this.UpdatingS = 1
            }
        })
    }

    save() {
        if (this.UpdatingS == 0) {
            this.EmployeeInformationsServiceProxy.createEmployeeInformations(this.EmployeeInformationsListDto).subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close()
            })
        } else if (this.UpdatingS == 1) {
            this.EmployeeInformationsServiceProxy.updateEmployeeInformations(this.EmployeeInformationsListDto).subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close()
            })
        }
    }
    
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}

