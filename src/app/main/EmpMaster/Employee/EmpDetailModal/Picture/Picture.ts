import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeePicturesServiceProxy, EmployeePicturesListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'Picture',
    templateUrl: './Picture.html',
})
export class PictureComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
   
    active:boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeePicturesServiceProxy:EmployeePicturesServiceProxy
    ) {
        super(injector);
    }
    disdsc=[{'code':'001','dsc':'รูปภาพพนักงาน'},{'code':'002','dsc':'บัตรประชาชน'},{'code':'003','dsc':'ทะเบียนบ้าน'},{'code':'004','dsc':'บัตรสมาชิกสภาการพยาบาล'},{'code':'005','dsc':'ใบประกอบวิชาชีพ'}]
    picture:EmployeePicturesListDto[]=[]
    pictureOrg:EmployeePicturesListDto[]=[]
    REFEMPID = ''
    show(id): void {
        this.active = true;    
        setTimeout(() => {
        this.modal.show();            
        }, 100);    
        this.REFEMPID = id
        this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(id).subscribe((result)=>{
            this.pictureOrg = result.items
            this.picture = result.items
        })
    }
    chgdis(v){
        if(v=='all'){
            this.picture = this.pictureOrg
        }else{
            this.picture = this.pictureOrg.filter(res=>res.picdsc == v)
        }
    }
    showpic(path){
        window.open(path,'_blank')
    }
   
    close(): void {
        this.modal.hide();
        this.active = false;
    }

    delete(e,v) {

        this.EmployeePicturesServiceProxy.deleteEmployeePictures(e.data.id).subscribe(() => {
            this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(this.REFEMPID).subscribe((result) => {
                this.pictureOrg = result.items
                if(v=='all'){
                    this.picture = this.pictureOrg
                }else{
                    this.picture = this.pictureOrg.filter(res=>res.picdsc == v)
                }
            })
        })

    }
    onContentReady(e) {
        e.component.columnOption("command:edit", {
            visibleIndex: -1, width: 50, fixed: true
        });
    }
   
}

