import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, PhoneNumberListDto, CreatePhoneNumberInput, EmployeePhoneNumbersAppserviceServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';



@Component({
    selector: 'Phone',
    templateUrl: './Phone.html',
})
export class PhoneComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    phonelist: PhoneNumberListDto[] = []
    active: boolean;
    CreatePhoneNumberInput: CreatePhoneNumberInput = new CreatePhoneNumberInput()
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeePhoneNumbersAppserviceServiceProxy: EmployeePhoneNumbersAppserviceServiceProxy
    ) {
        super(injector);
    }
    onContentReady(e) {
        e.component.columnOption("command:edit", {
            visibleIndex: -1, width: 50, fixed: true
        });
    }

    show(id,psnfnm,psnlnm): void {
       this.active = true;   
       this.EmpName= psnfnm+' '+psnlnm
       setTimeout(() => {
                 this.modal.show();  
       }, 100);     

 
        this.REFEMPID = id
        this.CreatePhoneNumberInput.dtleid = id
        this.EmployeePhoneNumbersAppserviceServiceProxy.getPhoneNumberByEmpId(id).subscribe((result) => {
            this.phonelist = result.items
        })

    }

    EmpName = ''
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    REFEMPID = ''
    save() {
        this.message.confirm(
            this.l('คุณแน่ใจที่จะบันทึกข้อมูลหรือไม่'),
            isConfirmed => {
                if (isConfirmed) {
                    
                    this.EmployeePhoneNumbersAppserviceServiceProxy.createPhoneNumber(this.CreatePhoneNumberInput).subscribe(() => {
                        this.EmployeePhoneNumbersAppserviceServiceProxy.getPhoneNumberByEmpId(this.REFEMPID).subscribe((result) => {
                            this.phonelist = result.items
                            this.CreatePhoneNumberInput = new CreatePhoneNumberInput()
                            this.CreatePhoneNumberInput.dtleid = this.REFEMPID
                        })
                    })
                }
            }

        );
    }

    delete(e) {

        this.EmployeePhoneNumbersAppserviceServiceProxy.deletePhoneNumber(e.data.id).subscribe(() => {
            this.EmployeePhoneNumbersAppserviceServiceProxy.getPhoneNumberByEmpId(this.REFEMPID).subscribe((result) => {
                this.phonelist = result.items
            })
        })

    }
}

