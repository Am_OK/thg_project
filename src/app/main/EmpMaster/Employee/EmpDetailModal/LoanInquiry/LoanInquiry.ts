import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeLoanAccountListDto, EmployeeLoanAccountServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';

@Component({
    selector: 'LoanInquiry',
    templateUrl: './LoanInquiry.html',
})
export class LoanInquiryComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private EmployeeLoanAccountServiceProxy: EmployeeLoanAccountServiceProxy,
        private data_Empmas:data_Empmas,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    REFID;
    LOANREASON: DbmtabListDto[];
    LOANSUBREASON: DbmtabListDto[];
    PAYDAY: DbmtabListDto[];
    RECTYPE:DbmtabListDto[];
    PAYTYPE:DbmtabListDto[];
    INTEREST:DbmtabListDto[];
    INTERESTTYPE:DbmtabListDto[];
    BNKCOD: DbmtabListDto[];
    EmployeeLoanAccountListDto: EmployeeLoanAccountListDto[] = []
    EmployeeLoanAccountObject: EmployeeLoanAccountListDto = new EmployeeLoanAccountListDto()
    getCommendation() {
        this.EmployeeLoanAccountServiceProxy.getEmployeeLoanAccountByEmpId(this.REFID).subscribe((result) => {
            this.EmployeeLoanAccountListDto = result.items
        })
    }
    nameLQ
    show(id,psnfnm,psnlnm): void {
        this.active = true;
        setTimeout(() => {
        this.modal.show();            
        }, 100);

        this.nameLQ = psnfnm+''+psnlnm
        this.REFID = id
        this.LOANREASON = this.data_Empmas.LOANREASON
        this.LOANSUBREASON = this.data_Empmas.LOANSUBREASON
        this.PAYDAY = this.data_Empmas.PAYDAY
        this.RECTYPE = this.data_Empmas.RECTYPE
        this.PAYTYPE = this.data_Empmas.PAYTYPE
        this.INTEREST = this.data_Empmas.INTEREST
        this.INTERESTTYPE = this.data_Empmas.INTERESTTYPE
        this.BNKCOD = this.data_Empmas.BNKCOD;

        this.getCommendation()
    }
    fnnum() {
        $('#acccrm').val(this.fnformat($('#acccrm').val()))
        $('#accpam').val(this.fnformat($('#accpam').val()))
        $('#accout').val(this.fnformat($('#accout').val()))
        $('#accnpr').val(this.fnformat($('#accnpr').val()))
    }
    Editing = 0
    showedit(e) {
        this.Editing = 1
        this.EmployeeLoanAccountObject = new EmployeeLoanAccountListDto()
        this.EmployeeLoanAccountObject.id = e.data.id
        this.EmployeeLoanAccountObject.acccod = e.data.acccod
        this.EmployeeLoanAccountObject.acceid = e.data.acceid
        this.EmployeeLoanAccountObject.accrea = e.data.accrea
        this.EmployeeLoanAccountObject.accfG1 = e.data.accfG1
        this.EmployeeLoanAccountObject.accsts = e.data.accsts
        this.EmployeeLoanAccountObject.accstd = this.fnddmmyyyy(e.data.accstd)
        this.EmployeeLoanAccountObject.acccrm = e.data.acccrm
        this.EmployeeLoanAccountObject.accaam = e.data.accaam
        this.EmployeeLoanAccountObject.accpam = e.data.accpam
        this.EmployeeLoanAccountObject.accout = e.data.accout
        this.EmployeeLoanAccountObject.accnpr = e.data.accnpr
        this.EmployeeLoanAccountObject.accins = e.data.accins
        this.EmployeeLoanAccountObject.accday = e.data.accday
        this.EmployeeLoanAccountObject.accpdt = this.fnddmmyyyy(e.data.accpdt)
        this.EmployeeLoanAccountObject.accpay = e.data.accpay
        this.EmployeeLoanAccountObject.accrec = e.data.accrec
        this.EmployeeLoanAccountObject.accrty = e.data.accrty
        this.EmployeeLoanAccountObject.acccty = e.data.acccty
        this.EmployeeLoanAccountObject.accbnk = e.data.accbnk
        this.EmployeeLoanAccountObject.accrte = e.data.accrte
        this.EmployeeLoanAccountObject.accrre = e.data.accrre
        this.EmployeeLoanAccountObject.acccar = e.data.acccar
        this.EmployeeLoanAccountObject.accpro = e.data.accpro
        this.EmployeeLoanAccountObject.accdow = e.data.accdow
        this.EmployeeLoanAccountObject.accapr = e.data.accapr
        this.EmployeeLoanAccountObject.accapd = this.fnddmmyyyy(e.data.accapd)
        this.EmployeeLoanAccountObject.accrmk = e.data.accrmk
        this.EmployeeLoanAccountObject.accgA1 = e.data.accgA1
        this.EmployeeLoanAccountObject.accgA2 = e.data.accgA2
        this.EmployeeLoanAccountObject.accgA3 = e.data.accgA3
        setTimeout(() => {
            // $('#godprc').val(this.fnformat(this.EmployeeLoanAccountObject.godprc))
            $('#acccrm').val(this.fnformat(this.EmployeeLoanAccountObject.acccrm))
            $('#accpam').val(this.fnformat(this.EmployeeLoanAccountObject.accpam))
            $('#accout').val(this.fnformat(this.EmployeeLoanAccountObject.accout))
            $('#accnpr').val(this.fnformat(this.EmployeeLoanAccountObject.accnpr))
        }, 250);
        this.modal2.show();
    }

    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    if (this.EmployeeLoanAccountObject.acccod != '' && this.EmployeeLoanAccountObject.acccod != null && this.EmployeeLoanAccountObject.acccod != undefined) {
                        var accstd = $('#accstd').val() != null && $('#accstd').val() != undefined ? accstd = this.fnyyyymmdd($('#accstd').val()) : accstd = ''
                        var accpdt = $('#accpdt').val() != null && $('#accpdt').val() != undefined ? accpdt = this.fnyyyymmdd($('#accpdt').val()) : accpdt = ''
                        var accapd = $('#accapd').val() != null && $('#accapd').val() != undefined ? accapd = this.fnyyyymmdd($('#accapd').val()) : accapd = ''
                        var acccrm = $('#acccrm').val() != null && $('#acccrm').val() != undefined ? acccrm = this.fnreplace($('#acccrm').val()) : acccrm = 0
                        var accpam = $('#accpam').val() != null && $('#accpam').val() != undefined ? accpam = this.fnreplace($('#accpam').val()) : accpam = 0
                        var accout = $('#accout').val() != null && $('#accout').val() != undefined ? accout = this.fnreplace($('#accout').val()) : accout = 0
                        var accnpr = $('#accnpr').val() != null && $('#accnpr').val() != undefined ? accnpr = this.fnreplace($('#accnpr').val()) : accnpr = 0

                        this.EmployeeLoanAccountObject.accstd = accstd
                        this.EmployeeLoanAccountObject.accpdt = accpdt
                        this.EmployeeLoanAccountObject.accapd = accapd
                        this.EmployeeLoanAccountObject.acccrm = acccrm
                        this.EmployeeLoanAccountObject.accpam = accpam
                        this.EmployeeLoanAccountObject.accout = accout
                        this.EmployeeLoanAccountObject.accnpr = accnpr



                        if (this.Editing == 1) {
                            this.EmployeeLoanAccountServiceProxy.updateEmployeeLoanAccount(this.EmployeeLoanAccountObject).subscribe(() => {
                                this.close2()
                                this.getCommendation()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.EmployeeLoanAccountServiceProxy.createEmployeeLoanAccount(this.EmployeeLoanAccountObject).subscribe(() => {
                                this.close2()
                                this.getCommendation()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        }
                    } else {
                        this.message.warn('กรุณาระบุ Document No.')
                    }
                }
            })
    }


    show2(): void {
        this.modal.hide();
        this.modal2.show();
        this.EmployeeLoanAccountObject = new EmployeeLoanAccountListDto()
        this.EmployeeLoanAccountObject.acceid = this.REFID
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close2(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    removed(e) {
        this.EmployeeLoanAccountServiceProxy.deleteEmployeeLoanAccountByEid(e.data.id).subscribe(() => {
            this.getCommendation()
        })
    }
}

