import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeGuarantorServiceProxy, EmployeeAddressListDto, EmployeeGuarantorListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'GuarrantorList',
    templateUrl: './GuarrantorList.html',
})
export class GuarrantorListComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private EmployeeGuarantorServiceProxy: EmployeeGuarantorServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    EmployeeGuarantorListDto: EmployeeGuarantorListDto[] = []
    EmployeeGuarantorObject: EmployeeGuarantorListDto = new EmployeeGuarantorListDto()

    getCommendation() {
        this.EmployeeGuarantorServiceProxy.getEmployeeGuarantorByEmpId(this.REFID).subscribe((result) => {
            this.EmployeeGuarantorListDto = result.items
        })
    }
    REFID
    show(id): void {
        this.active = true;
        this.REFID = id
        setTimeout(() => {
        this.modal.show();    
        }, 100);


        this.getCommendation()
    }
    fnnum() {
        $('#gpacrt').val(this.fnformat($('#gpacrt').val()))
        $('#gpasal').val(this.fnformat($('#gpasal').val()))
    }

    showedit(e) {
        this.Editing = 1
        this.EmployeeGuarantorObject = new EmployeeGuarantorListDto()
        this.EmployeeGuarantorObject.id = e.data.id
        this.EmployeeGuarantorObject.gpapid =e.data.gpapid
        this.EmployeeGuarantorObject.gpaidn=e.data.gpaidn
        this.EmployeeGuarantorObject.gpanam=e.data.gpanam
        this.EmployeeGuarantorObject.gpaaD1=e.data.gpaaD1
        this.EmployeeGuarantorObject.gpaaD2=e.data.gpaaD2
        this.EmployeeGuarantorObject.gpasta=e.data.gpasta
        this.EmployeeGuarantorObject.gpacon=e.data.gpacon
        this.EmployeeGuarantorObject.gpazip=e.data.gpazip
        this.EmployeeGuarantorObject.gpatel=e.data.gpatel
        this.EmployeeGuarantorObject.gpafax=e.data.gpafax
        this.EmployeeGuarantorObject.gpambl=e.data.gpambl
        this.EmployeeGuarantorObject.gpapag=e.data.gpapag
        this.EmployeeGuarantorObject.gpaeml=e.data.gpaeml
        this.EmployeeGuarantorObject.gpaurl=e.data.gpaurl
        this.EmployeeGuarantorObject.gpacmn=e.data.gpacmn
        this.EmployeeGuarantorObject.gpasal=e.data.gpasal
        this.EmployeeGuarantorObject.gpasal=e.data.gpasal
        this.EmployeeGuarantorObject.gpacrt=e.data.gpacrt
        setTimeout(() => {
            // $('#godprc').val(this.fnformat(this.EmployeeGuarantorObject.godprc))
            $('#gpacrt').val(this.fnformat(this.EmployeeGuarantorObject.gpacrt))
            $('#gpasal').val(this.fnformat(this.EmployeeGuarantorObject.gpasal))
        }, 250);
        this.modal2.show();
    }


    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    var gpacrt = $('#gpacrt').val() != null && $('#gpacrt').val() != undefined ? gpacrt = this.fnreplace($('#gpacrt').val()) : gpacrt = 0
                    var gpasal = $('#gpasal').val() != null && $('#gpasal').val() != undefined ? gpasal = this.fnreplace($('#gpasal').val()) : gpasal = 0
                    this.EmployeeGuarantorObject.gpacrt = gpacrt
                    this.EmployeeGuarantorObject.gpasal = gpasal

                    if (this.Editing == 1) {
                        this.EmployeeGuarantorServiceProxy.updateEmployeeGuarantor(this.EmployeeGuarantorObject).subscribe(() => {
                            this.close4()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        this.EmployeeGuarantorServiceProxy.createEmployeeGuarantor(this.EmployeeGuarantorObject).subscribe(() => {
                            this.close4()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    }
                }

            })
    }
    Editing = 0
    show2(): void {
        this.modal2.show();
        this.EmployeeGuarantorObject = new EmployeeGuarantorListDto()
        this.EmployeeGuarantorObject.gpaidn = this.REFID
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close4(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    removed(e) {
        this.EmployeeGuarantorServiceProxy.deleteEmployeeGuarantorByEid(e.data.id).subscribe(() => {
            this.getCommendation()
        })
    }
}

