import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'LeavingDocumentList',
    templateUrl: './LeavingDocumentList.html',
})
export class LeavingDocumentListComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active:boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    show(): void {
        this.active = true;   
        setTimeout(() => {
        this.modal.show();            
        }, 100); 

    }
    show2(): void {
        this.modal.hide();
        this.modal2.show();
      }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    onContentReady(e) {
        e.component.columnOption("command:edit", {
            visibleIndex: -1,
            width: 70
        });
    }
}

