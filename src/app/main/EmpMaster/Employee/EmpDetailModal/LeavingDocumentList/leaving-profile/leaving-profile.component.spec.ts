import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavingProfileComponent } from './leaving-profile.component';

describe('LeavingProfileComponent', () => {
  let component: LeavingProfileComponent;
  let fixture: ComponentFixture<LeavingProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavingProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavingProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
