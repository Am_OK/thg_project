import { Component, OnInit, Injector, Output, EventEmitter, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'appleavingprofile',
  templateUrl: './leaving-profile.component.html',
  styleUrls: ['./leaving-profile.component.css']
})
export class LeavingProfileComponent extends AppComponentBase  {
  

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  Week: { weekCount: string; bulletsData: { value: number; target: number; color: string; }[]; }[];
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }
  
  show():void{
    this.Week = [{
      weekCount: "ลากิจ",
      bulletsData: [{
          value: 45,
          target: 45,
          color: "#ebdd8f"
      }]
  }, {
      weekCount: "ลาป่วย",
      bulletsData: [{
          value: 30,
          target: 26,
          color: "#BDAC65"
      }]
  }, {
      weekCount: "ลาพักร้อน",
      bulletsData: [{
          value: 3,
          target: 3,
          color: "#837129"
      }]
  }, {
      weekCount: "ชั่วโมงสะสม",
      bulletsData: [{
          value: 17,
          target: 17,
          color: "#22BBC8"
      }]
  }];

    this.modal.show()
  }
  close(){
    this.modal.hide()
  }

}
