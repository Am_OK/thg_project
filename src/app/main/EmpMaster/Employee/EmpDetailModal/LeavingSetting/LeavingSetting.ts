import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, PhoneNumberListDto, CreatePhoneNumberInput, EmployeePhoneNumbersAppserviceServiceProxy, LeaveInformationServiceProxy, LeaveInformationListDto, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';



@Component({
    selector: 'LeavingSetting',
    templateUrl: './LeavingSetting.html',
})
export class LeavingSettingComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    LeaveInformationListDto: LeaveInformationListDto[] = []
    active: boolean;

    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,

        private LeaveInformationServiceProxy: LeaveInformationServiceProxy,
        private KPShareServiceProxy: KPShareServiceProxy
    ) {
        super(injector);
    }
    onContentReady(e) {
        e.component.columnOption("command:edit", {
            visibleIndex: -1, width: 50, fixed: true
        });
    }
    yearData
    pipe = new DatePipe('en-US');
    show(id,fm,lm): void {
        this.active = true;
        this.EmpName= fm+' '+lm
        this.REFEMPID = id
        this.yearData = this.fnyyyy543(this.pipe.transform(Date(), 'yyyy'))
        setTimeout(() => {
            this.modal.show();
        }, 100);

        this.getLeaveInfor(this.REFEMPID, this.yearData)



    }
    getLeaveInfor(id, yea) {
        this.LeaveInformationServiceProxy.getLeaveInformationByEidYear(id, this.fnyyyy(yea).toString()).subscribe((result) => {
            this.LeaveInformationListDto = result.items
            if (this.LeaveInformationListDto.length <= 0) {
                this.KPShareServiceProxy.insertLeaveCodeByEmpId(id, this.fnyyyy(yea).toString()).subscribe(() => {
                    this.LeaveInformationServiceProxy.getLeaveInformationByEidYear(id, this.fnyyyy(yea).toString()).subscribe((result) => {
                        this.LeaveInformationListDto = result.items
                    })
                })
            }

        })
    }

    EmpName = ''
    close(): void {
        this.modal.hide();
        this.active = false;
        this.modalSave.emit()
    }
    REFEMPID = ''
    save() {
        this.message.confirm(
            this.l('คุณแน่ใจที่จะบันทึกข้อมูลหรือไม่'),
            isConfirmed => {
                if (isConfirmed) {

                    this.LeaveInformationServiceProxy.updateLeaveInformation(this.DataLeavObject).subscribe(() => {
                        this.getLeaveInfor(this.REFEMPID, this.yearData)
                        this.notify.info(this.l('SavedSuccessfully'));
                        this.DataLeavObject = new LeaveInformationListDto()
                        this.saved = 0
                        this.ofymaxD = 0
                        this.ofyuseD = 0
                        this.ofywkpD = 0
                        this.ofybalD = 0
                    })

                }
            }

        );
    }
    ofymaxD = 0
    ofyuseD = 0
    ofywkpD = 0
    ofybalD = 0
    DataLeavObject = new LeaveInformationListDto()
    DataLeavObjectSH = []
    saved = 0
    selectData(e) {
        // console.log(e)
        if(e.selectedRowsData.length > 0){
            this.saved = 1
            this.DataLeavObjectSH = []
            this.DataLeavObject = new LeaveInformationListDto()
            this.DataLeavObjectSH.push(e.selectedRowsData[0])
            // console.log(this.DataLeavObject)
            this.DataLeavObject.id = this.DataLeavObjectSH[0].id
            this.DataLeavObject.ofycod = this.DataLeavObjectSH[0].ofycod
            this.DataLeavObject.ofyeid = this.DataLeavObjectSH[0].ofyeid
            this.DataLeavObject.ofyfmd = this.DataLeavObjectSH[0].ofyfmd
            this.DataLeavObject.ofytod = this.DataLeavObjectSH[0].ofytod
            this.DataLeavObject.ofyyea = this.DataLeavObjectSH[0].ofyyea
            this.DataLeavObject.ofymax = this.DataLeavObjectSH[0].ofymax
            this.DataLeavObject.ofyuse = this.DataLeavObjectSH[0].ofyuse
            this.DataLeavObject.ofywkp = this.DataLeavObjectSH[0].ofywkp
            this.DataLeavObject.ofybal = this.DataLeavObjectSH[0].ofybal
            this.ofymaxD = this.DataLeavObject.ofymax / 8
            this.ofyuseD = this.DataLeavObject.ofyuse / 8
            this.ofywkpD = this.DataLeavObject.ofywkp / 8
            this.ofybalD = this.DataLeavObject.ofybal / 8
        }
       
    }
    delete(e) {

        this.LeaveInformationServiceProxy.deleteLeaveInformation(e.data.id).subscribe(() => {
            this.LeaveInformationServiceProxy.getLeaveInformationByEid(this.REFEMPID).subscribe((result) => {
                this.LeaveInformationListDto = result.items

            })
        })

    }
    chgtyp = ''
    chgtyp2 = ''
    chgday(v) {

        if (this.chgtyp == 'H') {
            this.chgtyp = ''
        } else {
            // console.log('day')
            this.chgtyp2 = 'D'
            v == 'ofymax' ? this.DataLeavObject.ofymax = this.ofymaxD * 8 :
                v == 'ofyuse' ? this.DataLeavObject.ofyuse = this.ofyuseD * 8 :
                    v == 'ofywkp' ? this.DataLeavObject.ofywkp = this.ofywkpD * 8 :
                        v == 'ofybal' ? this.DataLeavObject.ofybal = this.ofybalD * 8 : ''
            setTimeout(() => {
                this.chgtyp2 = ''
            }, 500);
        }
    }
    chgHour(v) {

        if (this.chgtyp2 == 'D') {
            this.chgtyp2 = ''
        } else {
            // console.log('hour')
            this.chgtyp = 'H'
            v == 'ofymax' ? this.ofymaxD = this.DataLeavObject.ofymax / 8 :
                v == 'ofyuse' ? this.ofyuseD = this.DataLeavObject.ofyuse / 8 :
                    v == 'ofywkp' ? this.ofywkpD = this.DataLeavObject.ofywkp / 8 :
                        v == 'ofybal' ? this.ofybalD = this.DataLeavObject.ofybal / 8 : ''
            setTimeout(() => {
                this.chgtyp = ''
            }, 500);
        }
    }
}

