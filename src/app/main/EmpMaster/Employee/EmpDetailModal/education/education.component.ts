import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeEducationsListDto, EmployeeEducationsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { now } from 'moment';
import { data_Empmas } from '@app/main/EmpMaster/data_center';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'educationModal',
  templateUrl: './education.component.html',
  styles: []
})
export class EducationComponent extends AppComponentBase {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('modal2') modal2: ModalDirective;
  active: boolean;
  MAJOR: DbmtabListDto[];
  EDUCATION: DbmtabListDto[];
  CETIFICATED: DbmtabListDto[];
  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private data_Empmas: data_Empmas,
    private EmployeeEducationsServiceProxy: EmployeeEducationsServiceProxy
  ) {
    super(injector);
  }
  Education = []
  RefEmp;
  show(id): void {
    this.active = true;
    this.RefEmp = id
    this.MAJOR = this.data_Empmas.MAJOR
    this.EDUCATION = this.data_Empmas.EDUCATION
    this.CETIFICATED = this.data_Empmas.CETIFICATED
    this.EmployeeEducationsServiceProxy.getEmployeeEducationsByEmpId(this.RefEmp).subscribe((result) => {
      this.EmployeeEducationsListDto = result.items
    })
    setTimeout(() => {
      this.modal.show();
    }, 100);
    
    

  }
  save() {
    this.message.confirm(
      this.l('AreYouSureToSaveTheData'),
      isConfirmed => {
        if (isConfirmed) {
          this.EmployeeEducationsServiceProxy.deleteEmployeeEducationsByEid(this.RefEmp).subscribe(() => {
            for (let index = 0; index < this.EmployeeEducationsListDto.length; index++) {
              setTimeout(() => {
                this.EmployeeEducationsServiceProxy.createEmployeeEducations(this.EmployeeEducationsListDto[index]).subscribe(() => {

                })
              }, 180 * index);
              if (this.EmployeeEducationsListDto.length <= (index + 1 * 1)) {
                this.close()
                this.notify.info(this.l('SavedSuccessfully'));
              }

            }
          })
        }
      })
  }
  editing = 0
  editmem(e) {
    console.log(e)
    this.editing = 1
    this.inxseq = this.EmployeeEducationsListDto.findIndex(res => res.eduseq == e.data.eduseq)
    this.createEdu = new EmployeeEducationsListDto()
    this.createEdu.eduidn = e.data.eduidn
    this.createEdu.eduseq = e.data.eduseq
    this.createEdu.educdu = e.data.educdu
    this.createEdu.edusma = e.data.edusma
    this.createEdu.edusdu = e.data.edusdu
    this.createEdu.edusin = e.data.edusin
    this.createEdu.edusot = e.data.edusot
    this.createEdu.edumag = e.data.edumag
    this.createEdu.edumin = e.data.edumin
    e.data.edufG1 == '0' ? this.edufG1 = undefined : this.edufG1 = true
    e.data.edufG2 == '0' ? this.edufG2 = undefined : this.edufG2 = true
    this.modal2.show();
    $('.m_select2_1a').select2({
      placeholder: "Please Select.."
    });




  }
  close(): void {
    this.modal.hide();
    this.active = false;
  }
  inxseq;
  edufG1;
  edufG2;
  //////////////modal2

  EmployeeEducationsListDto: EmployeeEducationsListDto[] = []
  createEdu: EmployeeEducationsListDto = new EmployeeEducationsListDto()
  AddEduShow() {
    this.modal2.show();
    this.createEdu = new EmployeeEducationsListDto()
    this.edufG1 = undefined
    this.edufG2 = undefined
    $('.m_select2_1a').select2({
      placeholder: "Please Select.."
    });
  }
  closeAddEdu() {
    this.editing = 0
    this.modal2.hide()

  }
  saveEdu() {
    if (this.createEdu.eduseq != NaN && this.createEdu.eduseq != null && this.createEdu.eduseq != undefined) {
      var indx = this.EmployeeEducationsListDto.filter(res => res.eduseq == this.createEdu.eduseq)

      var code = $('#code').val() != null && $('#code').val() != undefined ? code = $('#code').val().toString() : code = ''
      var fac = $('#fac').val() != null && $('#fac').val() != undefined ? fac = $('#fac').val().toString() : fac = ''
      var major = $('#major').val() != null && $('#major').val() != undefined ? major = $('#major').val().toString() : major = ''
      var minor = $('#minor').val() != null && $('#minor').val() != undefined ? minor = $('#minor').val().toString() : minor = ''

      this.createEdu.educdu = code
      this.createEdu.edusma = fac
      this.createEdu.edumag = major
      this.createEdu.edumin = minor
      this.createEdu.eduidn = this.RefEmp

      this.edufG1 == undefined || this.edufG1 == false ? this.createEdu.edufG1 = '0' : this.createEdu.edufG1 = '1'
      this.edufG2 == undefined || this.edufG2 == false ? this.createEdu.edufG2 = '0' : this.createEdu.edufG2 = '1'
      if (this.editing == 0) {
        if (indx.length <= 0) {
          this.EmployeeEducationsListDto.push(this.createEdu)
          this.closeAddEdu()
        } else this.message.warn('พบ Items นี้ อยู่ในระบบแล้ว')

      } else {
       
        
        // var indx2 = this.EmployeeEducationsListDto.filter(res => res.eduseq == this.createEdu.eduseq)
        if (indx.length <= 0 || this.EmployeeEducationsListDto[this.inxseq].eduseq == this.createEdu.eduseq) {
         
          this.EmployeeEducationsListDto[this.inxseq].eduidn = this.RefEmp
          this.EmployeeEducationsListDto[this.inxseq].eduseq = this.createEdu.eduseq
          this.EmployeeEducationsListDto[this.inxseq].educdu = this.createEdu.educdu
          this.EmployeeEducationsListDto[this.inxseq].edusma = this.createEdu.edusma
          this.EmployeeEducationsListDto[this.inxseq].edusdu = this.createEdu.edusdu
          this.EmployeeEducationsListDto[this.inxseq].edusin = this.createEdu.edusin
          this.EmployeeEducationsListDto[this.inxseq].edusot = this.createEdu.edusot
          this.EmployeeEducationsListDto[this.inxseq].edumag = this.createEdu.edumag
          this.EmployeeEducationsListDto[this.inxseq].edumin = this.createEdu.edumin
          this.EmployeeEducationsListDto[this.inxseq].edufG1 = this.createEdu.edufG1
          this.EmployeeEducationsListDto[this.inxseq].edufG2 = this.createEdu.edufG2
          this.closeAddEdu()
        } else this.message.warn('พบ Items นี้ อยู่ในระบบแล้ว')
      }



    } else {
      this.message.warn('กรุณาระบุ Items ให้ถูกต้อง')
    }


  }
}

