import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-addpersonlist',
  templateUrl: './addpersonlist.component.html',
  styles: []
})
export class AddpersonlistComponent extends AppComponentBase {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;

  constructor(
    injector: Injector,
  ) {
    super(injector);
  }
  show(id): void {
    console.log(id)
    this.modal.show();
  }
  person_data = []
  perid
  perna
  perad
  perst
  perco
  perzi
  perte
  perfa
  permo
  perpa
  perem
  perur
  perrel
  perpe
  perrec
  add() {
    this.person_data.push({
      "perid": this.perid,
      "perna": this.perna,
      "perad": this.perad,
      "perst": this.perst,
      "perco": this.perco,
      "perzi": this.perzi,
      "perte": this.perte,
      "perfa": this.perfa,
      "permo": this.permo,
      "perpa": this.perpa,
      "perem": this.perem,
      "perur": this.perur,
      "perrel": this.perrel,
      "perpe": this.perpe,
      "perrec": this.perrec
    })
    console.log(this.person_data)
    this.modalSave.emit(this.person_data);
  }
  close(): void {
    this.modal.hide();
  }
}