import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { PersonnalInformationsServiceProxy, PersonnalInformationsListDto, CreatePersonnalInformationsInput } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-personlist',
  templateUrl: './personlist.component.html',
  styles: []
})
export class PersonListComponent extends AppComponentBase {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  @ViewChild('modal2') modal2: ModalDirective;
  active: boolean;
  constructor(
    injector: Injector,
    private PersonnalInformationsServiceProxy: PersonnalInformationsServiceProxy
  ) {
    super(injector);
  }
  PersonnalInformationsListDto: PersonnalInformationsListDto[] = []
  REFEID;
  show(id): void {
    this.REFEID = id
    this.active = true;   
    setTimeout(() => {
    this.modal.show();      
    }, 100);
    this.PersonnalInformationsServiceProxy.getPersonnalInformationsByEmpId(id).subscribe((result) => {
      this.PersonnalInformationsListDto = result.items
      this.person_data = []
      for (let i = 0; i < this.PersonnalInformationsListDto.length; i++) {
        this.add_id = this.add_id + 1
        this.person_data.push({
          "id": this.add_id,
          "perid": this.PersonnalInformationsListDto[i].gpapid,
          "perna": this.PersonnalInformationsListDto[i].gpanam,
          "perad": this.PersonnalInformationsListDto[i].gpaaD1,
          "perst": this.PersonnalInformationsListDto[i].gpasta,
          "perco": this.PersonnalInformationsListDto[i].gpacon,
          "perzi": this.PersonnalInformationsListDto[i].gpazip,
          "perte": this.PersonnalInformationsListDto[i].gpatel,
          "perfa": this.PersonnalInformationsListDto[i].gpafax,
          "permo": this.PersonnalInformationsListDto[i].gpambl,
          "perpa": this.PersonnalInformationsListDto[i].gpapag,
          "perem": this.PersonnalInformationsListDto[i].gpaeml,
          "perur": this.PersonnalInformationsListDto[i].gpaurl,
          "perrel": this.PersonnalInformationsListDto[i].gpacmn,
          "perpe": this.PersonnalInformationsListDto[i].gpasal,
          "perrec": this.PersonnalInformationsListDto[i].gpacrt

        })
      }
    })
  }

  show_add() {
    this.modal.hide()
    this.modal2.show();
  }
  person_data = []
  add_id = 0
  perid
  perna
  perad
  perst
  perco
  perzi
  perte
  perfa
  permo
  perpa
  perem
  perur
  perrel
  perpe
  perrec
  add() {
    if (this.status_edit == 0) {
      this.add_id = this.add_id + 1
      this.person_data.push({
        "id": this.add_id,
        "perid": this.perid,
        "perna": this.perna,
        "perad": this.perad,
        "perst": this.perst,
        "perco": this.perco,
        "perzi": this.perzi,
        "perte": this.perte,
        "perfa": this.perfa,
        "permo": this.permo,
        "perpa": this.perpa,
        "perem": this.perem,
        "perur": this.perur,
        "perrel": this.perrel,
        "perpe": this.perpe,
        "perrec": this.perrec
      })
    } else {
      var edit = this.person_data.findIndex(result => result.id == this.edit_id)
      this.person_data[edit].perna = this.perna
      this.person_data[edit].perna = this.perna
      this.person_data[edit].perad = this.perad
      this.person_data[edit].perst = this.perst
      this.person_data[edit].perco = this.perco
      this.person_data[edit].perzi = this.perzi
      this.person_data[edit].perte = this.perte
      this.person_data[edit].perfa = this.perfa
      this.person_data[edit].permo = this.permo
      this.person_data[edit].perpa = this.perpa
      this.person_data[edit].perem = this.perem
      this.person_data[edit].perur = this.perur
      this.person_data[edit].perrel = this.perrel
      this.person_data[edit].perpe = this.perpe
      this.person_data[edit].perrec = this.perrec
      this.status_edit = 0
    }
    this.clear()
    this.modal2.hide();
    this.modal.show()
  }
  status_edit = 0
  edit_id
  show_edit(id) {
    this.status_edit = 1
    this.modal.hide()
    this.modal2.show()
    var show_person_data = this.person_data.filter(result => result.id == id)
    this.edit_id = id
    this.perid = show_person_data[0].perid
    this.perna = show_person_data[0].perna
    this.perad = show_person_data[0].perad
    this.perst = show_person_data[0].perst
    this.perco = show_person_data[0].perco
    this.perzi = show_person_data[0].perzi
    this.perte = show_person_data[0].perte
    this.perfa = show_person_data[0].perfa
    this.permo = show_person_data[0].permo
    this.perpa = show_person_data[0].perpa
    this.perem = show_person_data[0].perem
    this.perur = show_person_data[0].perur
    this.perrel = show_person_data[0].perrel
    this.perpe = show_person_data[0].perpe
    this.perrec = show_person_data[0].perrec
  }
  CreatePersonnalInformationsInput: CreatePersonnalInformationsInput = new CreatePersonnalInformationsInput()
  save() {
    this.PersonnalInformationsServiceProxy.deletePersonnalInformationsEmpId(this.REFEID).subscribe(() => {
      for (let i = 0; i < this.person_data.length; i++) {
        setTimeout(() => {
          this.CreatePersonnalInformationsInput = new CreatePersonnalInformationsInput()
          this.CreatePersonnalInformationsInput.gpaidn = this.REFEID
          this.CreatePersonnalInformationsInput.gpapid = this.person_data[i].perid
          this.CreatePersonnalInformationsInput.gpanam = this.person_data[i].perna
          this.CreatePersonnalInformationsInput.gpaaD1 = this.person_data[i].perad
          this.CreatePersonnalInformationsInput.gpasta = this.person_data[i].perst
          this.CreatePersonnalInformationsInput.gpacon = this.person_data[i].perco
          this.CreatePersonnalInformationsInput.gpazip = this.person_data[i].perzi
          this.CreatePersonnalInformationsInput.gpatel = this.person_data[i].perte
          this.CreatePersonnalInformationsInput.gpafax = this.person_data[i].perfa
          this.CreatePersonnalInformationsInput.gpambl = this.person_data[i].permo
          this.CreatePersonnalInformationsInput.gpapag = this.person_data[i].perpa
          this.CreatePersonnalInformationsInput.gpaeml = this.person_data[i].perem
          this.CreatePersonnalInformationsInput.gpaurl = this.person_data[i].perur
          this.CreatePersonnalInformationsInput.gpacmn = this.person_data[i].perrel
          this.CreatePersonnalInformationsInput.gpasal = this.person_data[i].perpe
          this.CreatePersonnalInformationsInput.gpacrt = this.person_data[i].perrec
          this.PersonnalInformationsServiceProxy.createPersonnalInformations(this.CreatePersonnalInformationsInput).subscribe(() => {

          })
          if(this.person_data.length<=i+1*1){
            this.notify.info(this.l('SavedSuccessfully'));
            this.close('1')
          }
        }, 180 * i);

      }

    })
  }
  clear() {
    this.perid = ""
    this.perna = ""
    this.perad = ""
    this.perst = ""
    this.perco = ""
    this.perzi = ""
    this.perte = ""
    this.perfa = ""
    this.permo = ""
    this.perpa = ""
    this.perem = ""
    this.perur = ""
    this.perrel = ""
    this.perpe = ""
    this.perrec = ""
  }

  close(data): void {
    if (data == "1") {
      this.modal.hide();
      this.active = false;
    } else {
      this.modal2.hide();
      this.modal.show()
    }
  }
}