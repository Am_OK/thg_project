import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeCommendationServiceProxy, EmployeeCommendationListDto, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'CommendationRecord',
    templateUrl: './CommendationRecord.html',
})
export class CommendationRecordComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeCommendationServiceProxy: EmployeeCommendationServiceProxy,
        private data_Empmas: data_Empmas,
        private KPShareServiceProxy: KPShareServiceProxy
    ) {
        super(injector);
    }
    namecr;
    REFID;
    EmployeeCommendationListDto: EmployeeCommendationListDto[] = []
    EmployeeCommendationObject: EmployeeCommendationListDto = new EmployeeCommendationListDto()
    REASON: DbmtabListDto[] = []
    show(id,psnfnm,psnlnm): void {
        this.active = true;
        this.REFID = id
        this.namecr = psnfnm+' '+psnlnm
        this.REASON = this.data_Empmas.REASON
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getCommendation()
    }
    getCommendation() {
        this.EmployeeCommendationServiceProxy.getEmployeeCommendationByEmpId(this.REFID).subscribe((result) => {
            this.EmployeeCommendationListDto = result.items
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    fnnum() {
        $('#godprc').val(this.fnformat($('#godprc').val()))
    }
    showedit(e) {
        this.Editing = 1
        this.EmployeeCommendationObject = new EmployeeCommendationListDto()
        this.EmployeeCommendationObject.id = e.data.id
        this.EmployeeCommendationObject.goddoc = e.data.goddoc
        this.EmployeeCommendationObject.goddte = this.fnddmmyyyy(e.data.goddte)
        this.EmployeeCommendationObject.godefd = this.fnddmmyyyy(e.data.godefd)
        this.EmployeeCommendationObject.godeid = e.data.godeid
        this.EmployeeCommendationObject.godaut = e.data.godaut
        this.EmployeeCommendationObject.godrea = e.data.godrea
        this.EmployeeCommendationObject.godsts = e.data.godsts
        this.EmployeeCommendationObject.godstd = this.fnddmmyyyy(e.data.godstd)
        this.EmployeeCommendationObject.godrM1 = e.data.godrM1
        this.EmployeeCommendationObject.godrM2 = e.data.godrM2
        this.EmployeeCommendationObject.godrM3 = e.data.godrM3
        this.EmployeeCommendationObject.godprc = e.data.godprc
        setTimeout(() => {
            $('#godprc').val(this.fnformat(this.EmployeeCommendationObject.godprc))
        }, 250);
        this.modal2.show();

    }
    show2(): void {
        this.modal2.show();
        this.EmployeeCommendationObject = new EmployeeCommendationListDto()
        this.EmployeeCommendationObject.godeid = this.REFID
    }
    close2(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    Editing = 0
    pipe = new DatePipe('en-US');
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    var goddte = $('#goddte').val() != null && $('#goddte').val() != undefined ? goddte = this.fnyyyymmdd($('#goddte').val()) : goddte = ''
                    var godefd = $('#godefd').val() != null && $('#godefd').val() != undefined ? godefd = this.fnyyyymmdd($('#godefd').val()) : godefd = ''
                    var godstd = $('#godstd').val() != null && $('#godstd').val() != undefined ? godstd = this.fnyyyymmdd($('#godstd').val()) : godstd = ''
                    var godprc = $('#godprc').val() != null && $('#godprc').val() != undefined ? godprc = this.fnreplace($('#godprc').val()) : godprc = 0


                    this.EmployeeCommendationObject.goddte = goddte
                    this.EmployeeCommendationObject.godefd = godefd
                    this.EmployeeCommendationObject.godstd = godstd
                    this.EmployeeCommendationObject.godprc = godprc
                    if (this.Editing == 1) {
                        this.EmployeeCommendationServiceProxy.updateEmployeeCommendation(this.EmployeeCommendationObject).subscribe(() => {
                            this.close2()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.EmployeeCommendationObject.goddoc != '' && this.EmployeeCommendationObject.goddoc != null && this.EmployeeCommendationObject.goddoc != undefined) {
                            this.EmployeeCommendationServiceProxy.createEmployeeCommendation(this.EmployeeCommendationObject).subscribe(() => {
                                this.close2()
                                this.getCommendation()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        }else{
                            this.KPShareServiceProxy.getDocumentNumber('CM', this.EmployeeCommendationObject.goddte,'THI').subscribe((result)=>{
                                this.EmployeeCommendationObject.goddoc = result[0].documentNumber
                                this.EmployeeCommendationServiceProxy.createEmployeeCommendation(this.EmployeeCommendationObject).subscribe(() => {
                                    this.close2()
                                    this.getCommendation()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }

                    }

                }
            })
    }
    removed(e) {
        this.EmployeeCommendationServiceProxy.deleteEmployeeCommendation(e.data.id).subscribe(() => {
            this.getCommendation()
        })
    }
}

