import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, BonusInformationServiceProxy, BonusInformationListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'BonusHistory',
    templateUrl: './BonusHistory.html',
})
export class BonusHistoryComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active:boolean;
    constructor(
        injector: Injector,
        private BonusInformationServiceProxy: BonusInformationServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    REFID;
    BonusInformationListDto: BonusInformationListDto[] = []
    BonusInformationObject: BonusInformationListDto = new BonusInformationListDto()
    getCommendation() {
        this.BonusInformationServiceProxy.getBonusInformationByEmpId(this.REFID).subscribe((result) => {
            this.BonusInformationListDto = result.items
        })
    }
    show(id): void {
        this.active = true;
        this.REFID = id
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getCommendation() 
    }
    show2(): void {
        this.modal.hide();
        this.modal2.show();
      }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close2(): void {
        this.modal2.hide();
       
    }
}

