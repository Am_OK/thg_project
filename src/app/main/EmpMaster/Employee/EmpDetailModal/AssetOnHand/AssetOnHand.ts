import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeAssetonHandServiceProxy, EmployeeAssetonHandListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'AssetOnHand',
    templateUrl: './AssetOnHand.html',
})
export class AssetOnHandComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private EmployeeAssetonHandServiceProxy: EmployeeAssetonHandServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    REFID
    EmployeeAssetonHandListDto: EmployeeAssetonHandListDto[] = []
    EmployeeAssetonHandObject: EmployeeAssetonHandListDto = new EmployeeAssetonHandListDto()
    getCommendation() {
        this.EmployeeAssetonHandServiceProxy.getEmployeeAssetonHandByEmpId(this.REFID).subscribe((result) => {
            this.EmployeeAssetonHandListDto = result.items
        })
    }
    fnnum() {
        $('#astprc').val(this.fnformat($('#astprc').val()))
    }

    show(id): void {
        this.active = true;
        this.REFID = id
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getCommendation()
    }
    //     EmployeeAssetonHandObject.astasc
    // EmployeeAssetonHandObject.astdsc
    // EmployeeAssetonHandObject.astaty
    // EmployeeAssetonHandObject.astser
    // EmployeeAssetonHandObject.aststs
    // EmployeeAssetonHandObject.astfmd
    // EmployeeAssetonHandObject.aststd
    // EmployeeAssetonHandObject.asttod
    // EmployeeAssetonHandObject.astprc
    Editing = 0
    showedit(e) {
        this.Editing = 1
        this.EmployeeAssetonHandObject = new EmployeeAssetonHandListDto()
        this.EmployeeAssetonHandObject.id = e.data.id
        this.EmployeeAssetonHandObject.astidn = e.data.astidn
        this.EmployeeAssetonHandObject.astasc =e.data.astasc
        this.EmployeeAssetonHandObject.astdsc =e.data.astdsc
        this.EmployeeAssetonHandObject.astaty =e.data.astaty
        this.EmployeeAssetonHandObject.astser =e.data.astser
        this.EmployeeAssetonHandObject.aststs =e.data.aststs
        this.EmployeeAssetonHandObject.astfmd = this.fnddmmyyyy(e.data.astfmd)
        this.EmployeeAssetonHandObject.aststd =this.fnddmmyyyy(e.data.aststd)
        this.EmployeeAssetonHandObject.asttod =this.fnddmmyyyy(e.data.asttod)
        this.EmployeeAssetonHandObject.astprc = e.data.astprc
        setTimeout(() => {
            $('#astprc').val(this.fnformat(this.EmployeeAssetonHandObject.astprc))
        }, 250);
        this.modal2.show();
    }
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    var astfmd = $('#astfmd').val() != null && $('#astfmd').val() != undefined ? astfmd = this.fnyyyymmdd($('#astfmd').val()) : astfmd = ''
                    var aststd = $('#aststd').val() != null && $('#aststd').val() != undefined ? aststd = this.fnyyyymmdd($('#aststd').val()) : aststd = ''
                    var asttod = $('#asttod').val() != null && $('#asttod').val() != undefined ? asttod = this.fnyyyymmdd($('#asttod').val()) : asttod = ''
                    var astprc = $('#astprc').val() != null && $('#astprc').val() != undefined ? astprc = this.fnreplace($('#astprc').val()) : astprc = 0

                    this.EmployeeAssetonHandObject.astfmd = astfmd
                    this.EmployeeAssetonHandObject.aststd = aststd
                    this.EmployeeAssetonHandObject.asttod = asttod
                    this.EmployeeAssetonHandObject.astprc = astprc

                    if (this.Editing == 1) {
                        this.EmployeeAssetonHandServiceProxy.updateEmployeeAssetonHand(this.EmployeeAssetonHandObject).subscribe(() => {
                            this.close4()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        this.EmployeeAssetonHandServiceProxy.createEmployeeAssetonHand(this.EmployeeAssetonHandObject).subscribe(() => {
                            this.close4()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    }
                }
            })
    }
    show2(): void {
        this.modal2.show();
        this.EmployeeAssetonHandObject = new EmployeeAssetonHandListDto()
        this.EmployeeAssetonHandObject.astidn = this.REFID
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close4(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    removed(e) {
        this.EmployeeAssetonHandServiceProxy.deleteEmployeeAssetonHandByEmpId(e.data.id).subscribe(() => {
            this.getCommendation()
        })
    }
}

