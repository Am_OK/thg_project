import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, CreateCourseDetailInput, CourseDetailAppserviceServiceProxy, CourseDetailListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';



@Component({
    selector: 'EmployeeTrainingRecord',
    templateUrl: './EmployeeTrainingRecord.html',
})
export class EmployeeTrainingRecordComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    createcoursede: CreateCourseDetailInput = new CreateCourseDetailInput();
    CourseDetailListDto:CourseDetailListDto[]=[]
    CourseDetailListDtoOrg:CourseDetailListDto[]=[]
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private CourseDetailAppserviceServiceProxy : CourseDetailAppserviceServiceProxy
    ) {
        super(injector);
    }
    REFEID;
    show(id): void {
        this.active = true;
        this.REFEID = id
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this._dbmtabService.getDbmtabFromTABTB1('TrainingCourse', '').subscribe((result) => {
            this.TrainingCourse = result.items;
           
          });
        this.CourseDetailAppserviceServiceProxy.getCourseDetail(this.REFEID,'','','','','',undefined).subscribe((result)=>{
            this.CourseDetailListDto = result.items
            this.CourseDetailListDtoOrg = result.items
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    chgdis(v){
        if(v=='all'){
            this.CourseDetailListDto = this.CourseDetailListDtoOrg
        }else{
            this.CourseDetailListDto = this.CourseDetailListDtoOrg.filter(res=>res.hsrtty == v)
        }
    }
    TrainingCourse:DbmtabListDto[]=[]
    AddTrnShow(): void {
        this.modal.hide();
        this.modal2.show();

        this.createcoursede.hsrndy = 0;
        this.createcoursede.hsrmsc = 0.00;
        this.createcoursede.hsrmsC2 = 0.00;
        this.createcoursede.hsrmsC3 = 0.00;
        this.createcoursede.hsrfee = '0.00';
        this.createcoursede.hsrasc = 0.00;
        this.createcoursede.hsrasC2 = 0.00;
        this.createcoursede.hsrasC3 = 0.00;
    }
    closeAdd() {
        this.modal2.hide();
        this.modal.show()
    }
}

