import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeUpSaralyServiceProxy, EmployeeUpSaralyListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';

@Component({
    selector: 'AppraisalInquiry',
    templateUrl: './AppraisalInquiry.html',
})
export class AppraisalInquiryComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private EmployeeUpSaralyServiceProxy: EmployeeUpSaralyServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
nameAL
    REFID;
    Editing = 0
    EmployeeUpSaralyListDto: EmployeeUpSaralyListDto[] = []
    EmployeeUpSaralyObject: EmployeeUpSaralyListDto = new EmployeeUpSaralyListDto()
    getCommendation() {
        this.EmployeeUpSaralyServiceProxy.getEmployeeUpSaralyByEmpId(this.REFID).subscribe((result) => {
            this.EmployeeUpSaralyListDto = result.items
        })
    }
    show(id,psnfnm,psnlnm): void {
        this.active = true;
        this.nameAL = psnfnm+' '+psnlnm
        this.REFID = id
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getCommendation()
    }
    fnnum() {
        $('#salosl').val(this.fnformat($('#salosl').val()))
        $('#salper').val(this.fnformat($('#salper').val()))
        $('#salamt').val(this.fnformat($('#salamt').val()))
        $('#salnsl').val(this.fnformat($('#salnsl').val()))
        $('#saladj').val(this.fnformat($('#saladj').val()))
        $('#salsco').val(this.fnformat($('#salsco').val()))
    }

    showedit(e) {
        this.Editing = 1
        this.EmployeeUpSaralyObject = new EmployeeUpSaralyListDto()
        this.EmployeeUpSaralyObject.id = e.data.id
        this.EmployeeUpSaralyObject.saleid = e.data.saleid
        this.EmployeeUpSaralyObject.salyea = e.data.salyea
        this.EmployeeUpSaralyObject.salprd = e.data.salprd
        this.EmployeeUpSaralyObject.salsts = e.data.salsts
        this.EmployeeUpSaralyObject.salstd = this.fnddmmyyyy(e.data.salstd)
        this.EmployeeUpSaralyObject.salefd = this.fnddmmyyyy(e.data.salefd)
        this.EmployeeUpSaralyObject.salosl = e.data.salosl
        this.EmployeeUpSaralyObject.salfG2 = e.data.salfG2
        this.EmployeeUpSaralyObject.salper = e.data.salper
        this.EmployeeUpSaralyObject.salsco = e.data.salsco
        this.EmployeeUpSaralyObject.salamt = e.data.salamt
        this.EmployeeUpSaralyObject.saladj = e.data.saladj
        this.EmployeeUpSaralyObject.salnsl = e.data.salnsl
        setTimeout(() => {
            // $('#godprc').val(this.fnformat(this.EmployeeUpSaralyObject.godprc))
            $('#salosl').val(this.fnformat(this.EmployeeUpSaralyObject.salosl))
            $('#salper').val(this.fnformat(this.EmployeeUpSaralyObject.salper))
            $('#salamt').val(this.fnformat(this.EmployeeUpSaralyObject.salamt))
            $('#salnsl').val(this.fnformat(this.EmployeeUpSaralyObject.salnsl))
            $('#saladj').val(this.fnformat(this.EmployeeUpSaralyObject.saladj))
            $('#salsco').val(this.fnformat(this.EmployeeUpSaralyObject.salsco))
        }, 250);
        this.modal2.show();
    }


    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    if (this.EmployeeUpSaralyObject.saleid != '' && this.EmployeeUpSaralyObject.saleid != null && this.EmployeeUpSaralyObject.saleid != undefined) {
                        var salstd = $('#salstd').val() != null && $('#salstd').val() != undefined ? salstd = this.fnyyyymmdd($('#salstd').val()) : salstd = ''
                        var salefd = $('#salefd').val() != null && $('#salefd').val() != undefined ? salefd = this.fnyyyymmdd($('#salefd').val()) : salefd = ''
                        // var accapd = $('#accapd').val() != null && $('#accapd').val() != undefined ? accapd = this.fnyyyymmdd($('#accapd').val()) : accapd = ''


                        var salosl = $('#salosl').val() != null && $('#salosl').val() != undefined ? salosl = this.fnreplace($('#salosl').val()) : salosl = 0
                        var salper = $('#salper').val() != null && $('#salper').val() != undefined ? salper = this.fnreplace($('#salper').val()) : salper = 0
                        var salamt = $('#salamt').val() != null && $('#salamt').val() != undefined ? salamt = this.fnreplace($('#salamt').val()) : salamt = 0
                        var salnsl = $('#salnsl').val() != null && $('#salnsl').val() != undefined ? salnsl = this.fnreplace($('#salnsl').val()) : salnsl = 0
                        var saladj = $('#saladj').val() != null && $('#saladj').val() != undefined ? saladj = this.fnreplace($('#saladj').val()) : saladj = 0
                        var salsco = $('#salsco').val() != null && $('#salsco').val() != undefined ? salsco = this.fnreplace($('#salsco').val()) : salsco = 0

                        this.EmployeeUpSaralyObject.salstd = salstd
                        this.EmployeeUpSaralyObject.salefd = salefd
                        // this.EmployeeUpSaralyObject.accapd = accapd


                        this.EmployeeUpSaralyObject.salosl = salosl
                        this.EmployeeUpSaralyObject.salper = salper
                        this.EmployeeUpSaralyObject.salamt = salamt
                        this.EmployeeUpSaralyObject.salnsl = salnsl
                        this.EmployeeUpSaralyObject.saladj = saladj
                        this.EmployeeUpSaralyObject.salsco = salsco


                        if (this.Editing == 1) {
                            this.EmployeeUpSaralyServiceProxy.updateEmployeeUpSaraly(this.EmployeeUpSaralyObject).subscribe(() => {
                                this.close4()
                                this.getCommendation()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.EmployeeUpSaralyServiceProxy.createEmployeeUpSaraly(this.EmployeeUpSaralyObject).subscribe(() => {
                                this.close4()
                                this.getCommendation()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        }
                    } else {
                        this.message.warn('กรุณาระบุ Employee Code.')
                    }
                }
            })
    }



    show2(): void {
        this.modal2.show();
        this.EmployeeUpSaralyObject = new EmployeeUpSaralyListDto()
        this.EmployeeUpSaralyObject.saleid = this.REFID
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close4(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    removed(e) {
        this.EmployeeUpSaralyServiceProxy.deleteEmployeeUpSaralyByEid(e.data.id).subscribe(() => {
            this.getCommendation()
        })
    }
}

