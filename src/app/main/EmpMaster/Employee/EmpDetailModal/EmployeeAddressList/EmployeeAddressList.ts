import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeAddressServiceProxy, EmployeeAddressListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';

@Component({
    selector: 'EmployeeAddressList',
    templateUrl: './EmployeeAddressList.html',
})
export class EmployeeAddressListComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active:boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeAddressServiceProxy:EmployeeAddressServiceProxy,
        private data_Empmas:data_Empmas
    ) {
        super(injector);
    }
    REFID;
    EmployeeAddressListDto:EmployeeAddressListDto[]=[]
    EmployeeAddressObject:EmployeeAddressListDto = new EmployeeAddressListDto()
    show(id): void {
        this.active = true;
        this.REFID = id
        this.EMPADDRTYPE = this.data_Empmas.EMPADDRTYPE
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getAddress()
    }
    EMPADDRTYPE:DbmtabListDto[]=[]
    Editing = 0
    showedit(e) {
        this.Editing = 1
        this.EmployeeAddressObject = new EmployeeAddressListDto()
        this.EmployeeAddressObject.id = e.data.id
        this.EmployeeAddressObject.addidn = e.data.addidn
        this.EmployeeAddressObject.addtyp = e.data.addtyp
        this.EmployeeAddressObject.addaD1 = e.data.addaD1
        this.EmployeeAddressObject.addaD2 = e.data.addaD2
        this.EmployeeAddressObject.addsta = e.data.addsta   
        this.EmployeeAddressObject.addcon = e.data.addcon
        this.EmployeeAddressObject.addzip = e.data.addzip
        this.EmployeeAddressObject.addtel = e.data.addtel
        this.EmployeeAddressObject.addfax = e.data.addfax
        this.EmployeeAddressObject.addurl = e.data.addurl
        this.EmployeeAddressObject.addeml = e.data.addeml
       
        this.modal2.show();
        
    }
    getAddress(){
        this.EmployeeAddressServiceProxy.getEmployeeAddressByEmpId(this.REFID).subscribe((result)=>{
            this.EmployeeAddressListDto = result.items
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }

    show2(): void {
        this.modal2.show();
        this.EmployeeAddressObject = new EmployeeAddressListDto()
        this.EmployeeAddressObject.addidn = this.REFID
      }
    close2(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    
                        if (this.Editing == 1) {
                            this.EmployeeAddressServiceProxy.updateEmployeeAddress(this.EmployeeAddressObject).subscribe(() => {
                                this.close2()
                                this.getAddress()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.EmployeeAddressServiceProxy.createEmployeeAddress(this.EmployeeAddressObject).subscribe(() => {
                                this.close2()
                                this.getAddress()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        }
                    
                }
            })
    }
    removed(e){
        this.EmployeeAddressServiceProxy.deleteEmployeeAddress(e.data.id).subscribe(()=>{
            this.getAddress()
        })
    }
}

