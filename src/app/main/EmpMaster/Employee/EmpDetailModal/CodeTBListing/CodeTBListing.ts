import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, CreateDbmtabInput } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';



@Component({
    selector: 'CodeTBListing',
    templateUrl: './CodeTBListing.html',
})
export class CodeTBListingComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    DEPARTMENT: DbmtabListDto[] = [];
    LevelS = [{'tabtB2':'HD','tabdsc':'หัวหน้า'},{'tabtB2':'MG','tabdsc':'ผู้จัดการ'}]
    active: boolean;
    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private DbmtabServiceProxy: DbmtabServiceProxy
    ) {
        super(injector);
    }
    EmpID;
    nameEmpID;
    DBMTABMDEPCONT: DbmtabListDto[] = []
    CreateDbmtabInput: CreateDbmtabInput = new CreateDbmtabInput()
    show(id,psnfnm,psnlnm): void {
        this.active = true;
        this.nameEmpID =psnfnm+' '+psnlnm
        this.EmpID = id
        this.DEPARTMENT = this.data_Empmas.DEPARTMENT
        this.get_MDEPCONT()
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
    }
    get_MDEPCONT() {
        this.DbmtabServiceProxy.getDbmtabFromTABFG1('MDEPCONT' + this.EmpID, '').subscribe((result) => {
            this.DBMTABMDEPCONT = result.items

        })
    }
    save() {
        if ($('#level').val() != null && $('#level').val() != '' && $('#level').val() != undefined && $('#dep').val() != null && $('#dep').val() != '' && $('#dep').val() != undefined) {
            var dep = $('#dep').val().toString()
            var level = $('#level').val().toString()
            if (this.DBMTABMDEPCONT.filter(res => res.tabtB2 == dep).length <= 0) {
                this.CreateDbmtabInput = new CreateDbmtabInput()
                this.CreateDbmtabInput.tabaM1 = 0
                this.CreateDbmtabInput.tabaM2 = 0
                this.CreateDbmtabInput.tabrT1 = 0
                this.CreateDbmtabInput.tabrT2 = 0
                this.CreateDbmtabInput.tabtB1 = 'MDEPCONT' + this.EmpID
                this.CreateDbmtabInput.tabdsc = this.DEPARTMENT.filter(res => res.tabtB2 == dep)[0].tabdsc
                this.CreateDbmtabInput.tabtB2 = dep
                this.CreateDbmtabInput.tabtB3 = level
                this.DbmtabServiceProxy.createDbmtab(this.CreateDbmtabInput).subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.get_MDEPCONT()
                })
            }else{
                this.message.warn('คุณสามารถดูแลแผนกนี้ได้ตำแหน่งเดียว', 'พบข้อผิดพลาด')
            }
        } else {
            this.message.warn('กรุณากรอกข้อมูลให้ครบถ้วน', 'พบข้อผิดพลาด')
        }
    }
    deleted(e) {
        this.DbmtabServiceProxy.deleteDbmtab(e.data.id).subscribe(() => {
            this.notify.info(this.l('DeleteSuccessfully'));
            this.get_MDEPCONT()
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }

}

