import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeInformationsServiceProxy, EmployeeInformationsListDto, MoreIncomesServiceProxy, MoreBankAccountsServiceProxy, MoreIncomesListDto, MoreBankAccountsListDto, CreateMoreBankAccountsInput } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { now } from 'moment';
import { data_Empmas } from '@app/main/EmpMaster/data_center';

@Component({
    selector: 'personalModal',
    templateUrl: './personal.component.html',
})
export class personalModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeInformationsServiceProxy: EmployeeInformationsServiceProxy,
        private MoreIncomesServiceProxy: MoreIncomesServiceProxy,
        private MoreBankAccountsServiceProxy: MoreBankAccountsServiceProxy,
        private data_Empmas: data_Empmas
    ) {
        super(injector);
    }
    RefEID;
    EmployeeInformationsListDto: EmployeeInformationsListDto = new EmployeeInformationsListDto()
    UpdatingS = 0
    MoreIncomesListDto: MoreIncomesListDto[] = []
    MoreBankAccountsListDto: MoreBankAccountsListDto[] = []
    BNKBRN: DbmtabListDto[];
    BNKCOD: DbmtabListDto[];
    SHIFTCODE: DbmtabListDto[];

    PROVIDENTCOM: DbmtabListDto[];
    POVIDENTTYPE: DbmtabListDto[];
    EMPTYPE: DbmtabListDto[];
    EMPSUBTYPE: DbmtabListDto[];
    PAYSALARYTYPE: DbmtabListDto[];
    OvertimeTyp=[{'tabtB2':1,'tabdsc':'1: ปกติ (1 เท่า,1.5 เท่า,3 เท่า)'},{'tabtB2':2,'tabdsc':'2: Fix OT'},{'tabtB2':3,'tabdsc':'3: เวรแต่เบิกแต่เป็นเท่า'},{'tabtB2':4,'tabdsc':'4: เวรแต่เบิกแต่เป็นเท่า+เวรเหมา'}]
    OvertimeP=[{'tabtB2':1,'tabdsc':'1: 40 ชม.'},{'tabtB2':2,'tabdsc':'2: 44 ชม.'},{'tabtB2':3,'tabdsc':'3: 48 ชม.'}]
   nameps
    show(id, idc, agewrk,psnfnm,psnlnm): void {
        this.RefEID = id
        this.active = true;
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        this.nameps = psnfnm+' '+psnlnm
        this.BNKBRN = this.data_Empmas.BNKBRN
        this.BNKCOD = this.data_Empmas.BNKCOD
        this.SHIFTCODE = this.data_Empmas.SHIFTCODE
        this.PROVIDENTCOM = this.data_Empmas.PROVIDENTCOM
        this.POVIDENTTYPE = this.data_Empmas.POVIDENTTYPE
        this.EMPTYPE = this.data_Empmas.EMPTYPE
        this.EMPSUBTYPE =this.data_Empmas.EMPSUBTYPE
        this.PAYSALARYTYPE = this.data_Empmas.PAYSALARYTYPE
        $(document).ready(function () {
            $("#pswtax").inputmask({
                "mask": "9-9999-99999-99-9",
                placeholder: "" // remove underscores from the input mask
            });
            $("#pswsoc").inputmask({
                "mask": "9-9999-99999-99-9",
                placeholder: "" // remove underscores from the input mask
            });
        });
        this.EmployeeInformationsServiceProxy.getEmployeeInformationsByEmpId(this.RefEID).subscribe((result) => {

            if (result.items.length <= 0) {
                this.EmployeeInformationsListDto.pswidn = this.RefEID
                this.EmployeeInformationsListDto.pswtax = idc
                this.EmployeeInformationsListDto.pswsoc = idc
                this.UpdatingS = 0
            } else {
                this.EmployeeInformationsListDto = result.items[0]
                this.EmployeeInformationsListDto.pswpvd = this.fnddmmyyyy(this.EmployeeInformationsListDto.pswpvd)
                this.EmployeeInformationsListDto.pswpvp = this.fnddmmyyyy(this.EmployeeInformationsListDto.pswpvp)
                this.EmployeeInformationsListDto.pswpve = this.fnddmmyyyy(this.EmployeeInformationsListDto.pswpve)
                this.EmployeeInformationsListDto.pswmtd = this.fnddmmyyyy(this.EmployeeInformationsListDto.pswmtd)

                this.UpdatingS = 1
            }
            this.getmorebank()
            this.getmoreincome()
            setTimeout(() => {
                $('.m_select2_1a').select2({
                    placeholder: "Please Select.."
                });
            }, 500);
            setTimeout(() => {
                $('.m_select2_1at').select2({
                    placeholder: "Please Select.."
                });
            }, 500);
        })
        console.log(agewrk)
        if (agewrk != '' && agewrk != null && agewrk != undefined) {
            $('#agewrk').val(agewrk.toString())
            var percal = parseInt(agewrk.years)
            var perwrk
            if (percal <= 5) {
                perwrk = '3%'
            } else {
                perwrk = '5%'
            }
            $('#agewrkcal').val(perwrk)
        } else {
            $('#agewrk').val('')
            $('#agewrkcal').val('')
        }
    }
    getmorebank() {
        this.MoreBankAccountsServiceProxy.getMoreBankAccountsByEmpId(this.RefEID).subscribe((result) => {
            this.MoreBankAccountsListDto = result.items
        })
    }
    getmoreincome() {
        this.MoreIncomesServiceProxy.getMoreIncomesByEmpId(this.RefEID).subscribe((result) => {
            this.MoreIncomesListDto = result.items
        })
    }
    select_item_togle() {
        // alert(888)
        $("#panel_togle").slideToggle("slow");
    }
    save(pswtax,pswsoc) {
        var pswpvd = $('#pswpvd').val()
        var pswpvp = $('#pswpvp').val()
        var pswpve = $('#pswpve').val()
        var dtebj = $('#dtebj').val()
        var pswmtd = $('#pswmtd').val()

console.log(pswtax);
console.log(pswsoc);

        if (pswpvd != '' && pswpvd != null && pswpvd != undefined) {
            this.EmployeeInformationsListDto.pswpvd = this.fnyyyymmdd(pswpvd)
        }
        if (pswpvp != '' && pswpvp != null && pswpvp != undefined) {
            this.EmployeeInformationsListDto.pswpvp = this.fnyyyymmdd(pswpvp)
        }
        if (pswpve != '' && pswpve != null && pswpve != undefined) {
            this.EmployeeInformationsListDto.pswpve = this.fnyyyymmdd(pswpve)
        }
        if (pswpve != '' && pswpve != null && pswpve != undefined) {

        }
        if (pswmtd != '' && pswmtd != null && pswmtd != undefined) {
            this.EmployeeInformationsListDto.pswmtd = this.fnyyyymmdd(pswmtd)
        }

        if (pswtax != null && pswtax != '' && pswtax != undefined) {
            this.EmployeeInformationsListDto.pswtax = pswtax.replace(/-/g, "")
        }

        if (pswsoc != null && pswsoc != '' && pswsoc != undefined) {
            this.EmployeeInformationsListDto.pswsoc = pswsoc.replace(/-/g, "")
        }

        var pswbnk = $('#pswbnk').val()
        var pswbbn = $('#pswbbn').val()
        if (pswbnk != '' && pswbnk != null && pswbnk != undefined) {
            this.EmployeeInformationsListDto.pswbnk = pswbnk.toString()
        }
        if (pswbbn != '' && pswbbn != null && pswbbn != undefined) {
            this.EmployeeInformationsListDto.pswbbn = pswbbn.toString()
        }

        var pswshf = $('#pswshf').val()
        var pswsfad = $('#pswsfad').val()
        var pswsfaa = $('#pswsfaa').val()
        if (pswshf != '' && pswshf != null && pswshf != undefined) {
            this.EmployeeInformationsListDto.pswshf = pswshf.toString()
        }
        if (pswsfad != '' && pswsfad != null && pswsfad != undefined) {
            this.EmployeeInformationsListDto.pswsfad = pswsfad.toString()
        }
        if (pswsfaa != '' && pswsfaa != null && pswsfaa != undefined) {
            this.EmployeeInformationsListDto.pswsfaa = pswsfaa.toString()
        }

        if (this.UpdatingS == 0) {
            this.EmployeeInformationsServiceProxy.createEmployeeInformations(this.EmployeeInformationsListDto).subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close()
            })
        } else if (this.UpdatingS == 1) {
            this.EmployeeInformationsServiceProxy.updateEmployeeInformations(this.EmployeeInformationsListDto).subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close()
            })
        }
        this.MoreBankAccountsServiceProxy.deleteMoreBankAccountsEmpId(this.RefEID).subscribe(() => {
            for (let index = 0; index < this.MoreBankAccountsListDto.length; index++) {
                setTimeout(() => {
                    this.MoreBankAccountsServiceProxy.createMoreBankAccounts(this.MoreBankAccountsListDto[index]).subscribe(() => {

                    })
                }, 180 * index);
            }
        })
        this.MoreIncomesServiceProxy.deleteMoreIncomesEmpId(this.RefEID).subscribe(() => {
            for (let index = 0; index < this.MoreIncomesListDto.length; index++) {
                setTimeout(() => {
                    this.MoreIncomesServiceProxy.createMoreIncomes(this.MoreIncomesListDto[index]).subscribe(() => {

                    })
                }, 180 * index);
            }
        })
    }

    activeBnk(data) {
        console.log(data)
        this.MoreBankAccountsListDto.forEach(element => {
            element.bnkacc == data.bnkacc ? data.bnkact == "A" ? element.bnkact = "I" : element.bnkact = "A" : element.bnkact = "I"
        });

    }
    CMoreBankAccounts: MoreBankAccountsListDto = new MoreBankAccountsListDto()
    addacc(bnkaccid, bnkcod, bnkbrn) {
        if (bnkaccid != '' && bnkaccid != null && bnkaccid != undefined && bnkcod != '' && bnkcod != null && bnkcod != undefined && bnkbrn != '' && bnkbrn != null && bnkbrn != undefined) {
            this.CMoreBankAccounts = new MoreBankAccountsListDto()
            this.CMoreBankAccounts.bnkemp = this.RefEID
            this.CMoreBankAccounts.bnkacc = bnkaccid
            this.CMoreBankAccounts.bnkbnk = bnkcod
            this.CMoreBankAccounts.bnkbrn = bnkbrn
            // if (this.MoreBankAccountsListDto.length <= 0) {
            //     this.CMoreBankAccounts.bnkact = 'A'
            // } else {
            //     this.CMoreBankAccounts.bnkact = 'I'
            // }
            this.MoreBankAccountsListDto.length <= 0 ? this.CMoreBankAccounts.bnkact = 'A' : this.CMoreBankAccounts.bnkact = 'I'
            this.MoreBankAccountsListDto.push(this.CMoreBankAccounts)
            $('#bnkaccid').val('')
        } else {
            alert('กรุณากรอกข้อมูลให้ครบถ้วน')
        }

    }
    activeINC(data, AI) {
        data.inests = AI
    }
    CMoreINCOME: MoreIncomesListDto = new MoreIncomesListDto()
    addinc(code, amt) {
        if (code != '' && code != null && code != undefined && amt != '' && amt != null && amt != undefined) {
            this.CMoreINCOME = new MoreIncomesListDto()
            this.CMoreINCOME.ineeid = this.RefEID
            this.CMoreINCOME.inecod = code
            this.CMoreINCOME.ineamt = this.fnreplace(amt)
            this.CMoreINCOME.inests = 'A'

            this.MoreIncomesListDto.push(this.CMoreINCOME)
            $('#cdcode').val('')
            $('#cdamt').val('0.00')
        } else {
            alert('กรุณากรอกข้อมูลให้ครบถ้วน')
        }
    }
    fnnum() {
        $('#cdamt').val(this.fnformat($('#cdamt').val()))
    }
    close(): void {

        $("#panel_togle").slideUp();
        $('#bnkaccid').val('')
        
        this.modal.hide();
        this.active = false;
    }

}

