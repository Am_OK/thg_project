import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, PunishmentInformationServiceProxy, PunishmentInformationListDto, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';



@Component({
    selector: 'MisconductRecord',
    templateUrl: './MisconductRecord.html',
})
export class MisconductRecordComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private PunishmentInformationServiceProxy: PunishmentInformationServiceProxy,
        private data_Empmas: data_Empmas,
        private KPShareServiceProxy: KPShareServiceProxy
    ) {
        super(injector);
    }
    nameMR
    REFID;
    PunishmentInformationListDto: PunishmentInformationListDto[] = []
    PunishmentInformationObject: PunishmentInformationListDto = new PunishmentInformationListDto()
    NOTICEREASON: DbmtabListDto[] = []
    WARNINGISSUE: DbmtabListDto[] = []
    NOTICERESULT: DbmtabListDto[] = []
    show(id, psnfnm, psnlnm): void {
        this.nameMR = psnfnm + ' ' + psnlnm
        this.active = true;
        setTimeout(() => {
            this.modal.show();
        }, 100);

        this.REFID = id
        this.NOTICEREASON = this.data_Empmas.NOTICEREASON
        this.WARNINGISSUE = this.data_Empmas.WARNINGISSUE
        this.NOTICERESULT = this.data_Empmas.NOTICERESULT
        this.getPunish()

    }
    getPunish() {
        this.PunishmentInformationServiceProxy.getPunishmentInformationByEmpId(this.REFID).subscribe((result) => {
            this.PunishmentInformationListDto = result.items
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    fnnum() {
        $('#notdsl').val(this.fnformat($('#notdsl').val()))
    }
    showedit(e) {
        this.Editing = 1
        this.PunishmentInformationObject = new PunishmentInformationListDto()
        this.PunishmentInformationObject.id = e.data.id
        this.PunishmentInformationObject.notdte = this.fnddmmyyyy(e.data.notdte)
        this.PunishmentInformationObject.notdoc = e.data.notdoc
        this.PunishmentInformationObject.notefd = this.fnddmmyyyy(e.data.notefd)
        this.PunishmentInformationObject.notlev = e.data.notlev
        this.PunishmentInformationObject.noteid = e.data.noteid
        this.PunishmentInformationObject.notaut = e.data.notaut
        this.PunishmentInformationObject.notrea = e.data.notrea
        this.PunishmentInformationObject.notiss = e.data.notiss
        this.PunishmentInformationObject.notres = e.data.notres
        this.PunishmentInformationObject.notsts = e.data.notsts
        this.PunishmentInformationObject.notstd = this.fnddmmyyyy(e.data.notstd)
        this.PunishmentInformationObject.notrM1 = e.data.notrM1
        this.PunishmentInformationObject.notrM2 = e.data.notrM2
        this.PunishmentInformationObject.notrM3 = e.data.notrM3
        this.PunishmentInformationObject.notdsl = e.data.notdsl
        this.PunishmentInformationObject.notfmd = this.fnddmmyyyy(e.data.notfmd)
        this.PunishmentInformationObject.nottod = this.fnddmmyyyy(e.data.nottod)
        setTimeout(() => {
            this.fnnum()
        }, 250);
        this.modal2.show();
    }

    show2(): void {
        this.modal2.show();
        this.PunishmentInformationObject = new PunishmentInformationListDto()
        this.PunishmentInformationObject.noteid = this.REFID
    }
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var notdte = $('#notdte').val() != null && $('#notdte').val() != undefined ? notdte = this.fnyyyymmdd($('#notdte').val()) : notdte = ''
                    var notefd = $('#notefd').val() != null && $('#notefd').val() != undefined ? notefd = this.fnyyyymmdd($('#notefd').val()) : notefd = ''
                    var notstd = $('#notstd').val() != null && $('#notstd').val() != undefined ? notstd = this.fnyyyymmdd($('#notstd').val()) : notstd = ''
                    var notfmd = $('#notfmd').val() != null && $('#notfmd').val() != undefined ? notfmd = this.fnyyyymmdd($('#notfmd').val()) : notfmd = ''
                    var nottod = $('#nottod').val() != null && $('#nottod').val() != undefined ? nottod = this.fnyyyymmdd($('#nottod').val()) : nottod = ''
                    var notdsl = $('#notdsl').val() != null && $('#notdsl').val() != undefined ? notdsl = this.fnreplace($('#notdsl').val()) : notdsl = 0
                    this.PunishmentInformationObject.notdte = notdte
                    this.PunishmentInformationObject.notefd = notefd
                    this.PunishmentInformationObject.notstd = notstd
                    this.PunishmentInformationObject.notfmd = notfmd
                    this.PunishmentInformationObject.nottod = nottod
                    this.PunishmentInformationObject.notdsl = notdsl
                    if (this.Editing == 1) {
                        this.PunishmentInformationServiceProxy.updatePunishmentInformation(this.PunishmentInformationObject).subscribe(() => {
                            this.close2()
                            this.getPunish()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.PunishmentInformationObject.notdoc != '' && this.PunishmentInformationObject.notdoc != null && this.PunishmentInformationObject.notdoc != undefined) {
                            this.PunishmentInformationServiceProxy.createPunishmentInformation(this.PunishmentInformationObject).subscribe(() => {
                                this.close2()
                                this.getPunish()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.KPShareServiceProxy.getDocumentNumber('MSN', this.PunishmentInformationObject.notdte, 'THI').subscribe((result) => {
                                this.PunishmentInformationObject.notdoc = result[0].documentNumber
                                this.PunishmentInformationServiceProxy.createPunishmentInformation(this.PunishmentInformationObject).subscribe(() => {
                                    this.close2()
                                    this.getPunish()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }
                    }

                }
            })
    }
    close2(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    removed(e) {
        this.PunishmentInformationServiceProxy.deletePunishmentInformation(e.data.id).subscribe(() => {
            this.getPunish()
        })
    }
    Editing = 0
}

