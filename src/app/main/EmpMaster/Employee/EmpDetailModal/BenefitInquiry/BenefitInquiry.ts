import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeBenefitsListDto, EmployeeBenefitsServiceProxy, GetRoleForEditOutput } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';


@Component({
    selector: 'BenefitInquiry',
    templateUrl: './BenefitInquiry.html',
})
export class BenefitInquiryComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeBenefitsServiceProxy: EmployeeBenefitsServiceProxy,
        private data_Empmas:data_Empmas
    ) {
        super(injector);
    }
    EmployeeBenefitsListDto: EmployeeBenefitsListDto[] = []
    EmployeeBenefitsObject: EmployeeBenefitsListDto = new EmployeeBenefitsListDto()
    REFEID;
    BENTYPE: DbmtabListDto[] = []
    BENCAUSE: DbmtabListDto[] = []
    nameBE
    show(id,psnfnm,psnlnm): void {
        this.active = true;
        this.nameBE = psnfnm+' '+psnlnm
        this.REFEID = id;
        this.BENTYPE = this.data_Empmas.BENTYPE
        this.BENCAUSE = this.data_Empmas.BENCAUSE
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getEmpBenefit()
    }
    getEmpBenefit() {
        this.EmployeeBenefitsServiceProxy.getEmployeeBenefitsByEmpId(this.REFEID).subscribe((result) => {
            this.EmployeeBenefitsListDto = result.items
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    Editing = 0;
    showedit(e) {
        this.Editing = 1
        this.EmployeeBenefitsObject = new EmployeeBenefitsListDto()
        this.EmployeeBenefitsObject.id = e.data.id
        this.EmployeeBenefitsObject.paydoc = e.data.paydoc
        this.EmployeeBenefitsObject.payeid = e.data.payeid
        this.EmployeeBenefitsObject.paydte = this.fnddmmyyyy(e.data.paydte)
        this.EmployeeBenefitsObject.paybty = e.data.paybty
        this.EmployeeBenefitsObject.paysts = e.data.paysts
        this.EmployeeBenefitsObject.paystd = this.fnddmmyyyy(e.data.paystd)
        this.EmployeeBenefitsObject.payrno = e.data.payrno
        this.EmployeeBenefitsObject.payrdt = this.fnddmmyyyy(e.data.payrdt)
        this.EmployeeBenefitsObject.paycau = e.data.paycau
        this.EmployeeBenefitsObject.payrmk = e.data.payrmk
        this.EmployeeBenefitsObject.payamt = e.data.payamt
        this.EmployeeBenefitsObject.payamr = e.data.payamr
        this.EmployeeBenefitsObject.payfnm = e.data.payfnm
        this.EmployeeBenefitsObject.payfsq = e.data.payfsq
        this.EmployeeBenefitsObject.paytyp = e.data.paytyp
        this.EmployeeBenefitsObject.paypdt = this.fnddmmyyyy(e.data.paypdt)
        this.EmployeeBenefitsObject.payrfn = e.data.payrfn
        this.EmployeeBenefitsObject.payaut = e.data.payaut
        this.EmployeeBenefitsObject.payaud = this.fnddmmyyyy(e.data.payaud)

        setTimeout(() => {
            $('#payamt').val(this.fnformat(this.EmployeeBenefitsObject.payamt))
            $('#payamr').val(this.fnformat(this.EmployeeBenefitsObject.payamr))
        }, 250);
        this.modal2.show();

    }
    fnnum() {
        $('#payamt').val(this.fnformat($('#payamt').val()))
        $('#payamr').val(this.fnformat($('#payamr').val()))
    }
    show2(): void {
        this.modal2.show();
        this.EmployeeBenefitsObject = new EmployeeBenefitsListDto()
        this.EmployeeBenefitsObject.payeid = this.REFEID
    }
    close2(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    if (this.EmployeeBenefitsObject.paydoc != '' && this.EmployeeBenefitsObject.paydoc != null && this.EmployeeBenefitsObject.paydoc != undefined) {
                        var paydte = $('#paydte').val() != null && $('#paydte').val() != undefined ? paydte = this.fnyyyymmdd($('#paydte').val()) : paydte = ''
                        var paystd = $('#paystd').val() != null && $('#paystd').val() != undefined ? paystd = this.fnyyyymmdd($('#paystd').val()) : paystd = ''
                        var payrdt = $('#payrdt').val() != null && $('#payrdt').val() != undefined ? payrdt = this.fnyyyymmdd($('#payrdt').val()) : payrdt = ''
                        var paypdt = $('#paypdt').val() != null && $('#paypdt').val() != undefined ? paypdt = this.fnyyyymmdd($('#paypdt').val()) : paypdt = ''
                        var payaud = $('#payaud').val() != null && $('#payaud').val() != undefined ? payaud = this.fnyyyymmdd($('#payaud').val()) : payaud = ''
                        var payamt = $('#payamt').val() != null && $('#payamt').val() != undefined ? payamt = this.fnreplace($('#payamt').val()) : payamt = 0
                        var payamr = $('#payamr').val() != null && $('#payamr').val() != undefined ? payamr = this.fnreplace($('#payamr').val()) : payamr = 0
                        this.EmployeeBenefitsObject.paydte = paydte
                        this.EmployeeBenefitsObject.paystd = paystd
                        this.EmployeeBenefitsObject.payrdt = payrdt
                        this.EmployeeBenefitsObject.paypdt = paypdt
                        this.EmployeeBenefitsObject.payaud = payaud
                        this.EmployeeBenefitsObject.payamt = payamt
                        this.EmployeeBenefitsObject.payamr = payamr
                        if (this.Editing == 1) {
                            this.EmployeeBenefitsServiceProxy.updateEmployeeBenefits(this.EmployeeBenefitsObject).subscribe(() => {
                                this.close2()
                                this.getEmpBenefit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.EmployeeBenefitsServiceProxy.createEmployeeBenefits(this.EmployeeBenefitsObject).subscribe(() => {
                                this.close2()
                                this.getEmpBenefit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        }
                    } else {
                        this.message.warn('กรุณาระบุ Document No.')
                    }
                }
            })
    }
    removed(e) {
        this.EmployeeBenefitsServiceProxy.deleteEmployeeBenefits(e.data.id).subscribe(() => {
            this.getEmpBenefit()
        })
    }
}

