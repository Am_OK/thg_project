import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, WorkAcrossPositionsServiceProxy, WorkAcrossPositionsListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '@app/main/EmpMaster/data_center';



@Component({
    selector: 'CodeListing',
    templateUrl: './CodeListing.html',
})
export class CodeListingComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    dbmtab: DbmtabListDto[] = [];
    
    active:boolean;
    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private _dbmtabService: DbmtabServiceProxy,
        private WorkAcrossPositionsServiceProxy:WorkAcrossPositionsServiceProxy
    ) {
        super(injector);
    }
    EmpID;
    nameEmpIDs;
    show(id,psnfnm,psnlnm): void {
        this.active = true;
        this.nameEmpIDs = psnfnm+' '+psnlnm
        this.EmpID = id
        this.dbmtab = this.data_Empmas.dbmtab
        this.get_Across()
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
    }
    WorkAcrossPositionsListDto:WorkAcrossPositionsListDto[]=[]
    WorkAcrossPositionsObject:WorkAcrossPositionsListDto = new WorkAcrossPositionsListDto()
    get_Across() {
        this.WorkAcrossPositionsServiceProxy.getWorkAcrossPositionsByEid(this.EmpID).subscribe((result) => {
            this.WorkAcrossPositionsListDto = result.items

        })
    }
    save() {
        if ($('#levels').val() != null && $('#levels').val() != '' && $('#levels').val() != undefined && $('#pos').val() != null && $('#pos').val() != '' && $('#pos').val() != undefined) {
            var pos = $('#pos').val().toString()
            var levels = $('#levels').val().toString()
            if (this.WorkAcrossPositionsListDto.filter(res => res.wkdpoS_POS == pos).length <= 0) {
                this.WorkAcrossPositionsObject = new WorkAcrossPositionsListDto()
                this.WorkAcrossPositionsObject.wkdpoS_ID = this.EmpID
                this.WorkAcrossPositionsObject.wkdpoS_POS = pos
                this.WorkAcrossPositionsObject.wkdpoS_SEQ = this.fnreplace(levels)
                this.WorkAcrossPositionsServiceProxy.createWorkAcrossPositions(this.WorkAcrossPositionsObject).subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.get_Across()
                    $('#levels').val(this.fnreplace(levels)+1*1)
                })
            }else{
                this.message.warn('พบรหัสตำแหน่งนี้ในระบบแล้ว', 'พบข้อผิดพลาด')
            }
        } else {
            this.message.warn('กรุณากรอกข้อมูลให้ครบถ้วน', 'พบข้อผิดพลาด')
        }
    }
    deleted(e) {
        this.WorkAcrossPositionsServiceProxy.deleteWorkAcrossPositions(e.data.id).subscribe(() => {
            this.notify.info(this.l('DeleteSuccessfully'));
            this.get_Across()
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    
}

