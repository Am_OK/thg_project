import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, MemberOfFamilysServiceProxy, MemberOfFamilyListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { now } from 'moment';
import { EIDRM } from 'constants';

@Component({
    selector: 'MemberOFFamilyModal',
    templateUrl: './MemberOFFamily.component.html',
})
export class MemberOFFamilyComponent extends AppComponentBase  {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private MemberOfFamilysServiceProxy: MemberOfFamilysServiceProxy
    ) {
        super(injector);
    }
    MemberOfFamilyListDto: MemberOfFamilyListDto[] = []
    EID;
    show(id): void {
        this.active = true;
        setTimeout(() => {
            this.modal.show();
        }, 100);


        this.EID = id
        this.editing = 0
        this.MemberOfFamilysServiceProxy.getMemberOfFamilyByEmpId(id).subscribe((result) => {
            this.MemberOfFamilyListDto = result.items

        })
        $(document).ready(function () {
            $('#dtebrd').change(function () {
                $('#dtewstar').click()
            });
            $('#dteexp').change(function () {
                $('#dtewstar').click()
            });
        });
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    save() {
        this.MemberOfFamilysServiceProxy.deleteMemberOfFamilyEmpId(this.EID).subscribe(() => {
            for (let i = 0; i < this.MemberOfFamilyListDto.length; i++) {
                setTimeout(() => {
                    this.MemberOfFamilysServiceProxy.createMemberOfFamily(this.MemberOfFamilyListDto[i]).subscribe(() => {
                        if (this.MemberOfFamilyListDto.length <= i + 1 * 1) {
                            this.modalSave.emit()
                        }
                    })
                }, 180 * i);
                if (this.MemberOfFamilyListDto.length <= i + 1 * 1) {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close()
                }
            }
        })
    }
    editing = 0
    editmem(e) {
        // console.log(e)
        this.CreateMemberOfFamilyListDto = e.data
        setTimeout(() => {
            this.CreateMemberOfFamilyListDto.chdbrd = this.fnddmmyyyy(this.CreateMemberOfFamilyListDto.chdbrd)
            this.CreateMemberOfFamilyListDto.chdstd = this.fnddmmyyyy(this.CreateMemberOfFamilyListDto.chdstd)
            this.editing = 1
        }, 100);

        this.show2S()
    }
    onContentReady(e) {
        e.component.columnOption("command:edit", {
            visibleIndex: -1, width: 45, fixed: true
        });
    }
    deletemem(e) {
        this.MemberOfFamilyListDto.splice(this.MemberOfFamilyListDto.findIndex(result => result.id == e.data.id), 1)
    }
    CreateMemberOfFamilyListDto: MemberOfFamilyListDto = new MemberOfFamilyListDto()
    show2(): void {
        this.CreateMemberOfFamilyListDto = new MemberOfFamilyListDto()
        this.CreateMemberOfFamilyListDto.chdidn = this.EID
        this.CreateMemberOfFamilyListDto.chdbrd = ''
        this.CreateMemberOfFamilyListDto.chdstd = ''
        this.show2S()
    }
    show2S() {
        this.modal.hide();
        this.modal2.show();
    }
    close2(): void {
        this.modal2.hide();
        this.modal.show();

        if (this.editing == 1) {

            var d1 = $('#dtebrd').val()
            var d2 = $('#dteexp').val()
            if (d1 != '' && d1 != null && d1 != undefined) {
                d1 = this.fnyyyymmdd(d1)
            } else {
                d1 = ''
            }
            if (d2 != '' && d2 != null && d2 != undefined) {
                d2 = this.fnyyyymmdd(d2)
            }
            else {
                d2 = ''
            }

            this.CreateMemberOfFamilyListDto.chdbrd = d1.toString()
            this.CreateMemberOfFamilyListDto.chdstd = d2.toString()
            if (this.CreateMemberOfFamilyListDto.chdlst != 'P') {
                this.CreateMemberOfFamilyListDto.chdstd = ""
            }
            this.notify.info(this.l('UpdatedSuccessfully'));
            setTimeout(() => {
                this.editing = 0
            }, 200);
        }


    }
    saveC() {
        var d1 = $('#dtebrd').val()
        var d2 = $('#dteexp').val()
        if (d1 != '' && d1 != null && d1 != undefined) {
            d1 = this.fnyyyymmdd(d1)
        } else {
            d1 = ''
        }
        if (d2 != '' && d2 != null && d2 != undefined) {
            d2 = this.fnyyyymmdd(d2)
        }
        else {
            d2 = ''
        }
        this.CreateMemberOfFamilyListDto.chdbrd = d1.toString()
        this.CreateMemberOfFamilyListDto.chdstd = d2.toString()
        if (this.CreateMemberOfFamilyListDto.chdlst != 'P') {
            this.CreateMemberOfFamilyListDto.chdstd = ""
        }
        this.MemberOfFamilyListDto.push(this.CreateMemberOfFamilyListDto)
        this.close2()

        // 

    }
    psnresign
    datedi(): void {
        if ($('#dtebrd').val() != null) {
        var d = $('#dtebrd').val().toString()
        console.log(d);
        var someDate2 = new Date(this.fninyyyymmdd(d));            
        }

        if (this.CreateMemberOfFamilyListDto.chdlst == 'A') {
            var datenew = new Date()
        }
        else {
            if ($('#dteexp').val() != null) {
                var v = $('#dteexp').val().toString()
            }
            var datenew = new Date(this.fninyyyymmdd(v))
        }

        
        
        var dtn2 = this.age(someDate2, datenew)
        var dt2s = dtn2.toString()
        this.emprefage = dtn2
        setTimeout(() => {
            $('#ddxx').val(dt2s);
        }, 500);
    }
    emprefage
    age(dob, today) {

        var today = today,
            result = {
                years: 0,
                months: 0,
                days: 0,
                toString: function () {
                    return (this.years ? this.years + ' ปี ' : '');
                }
            };
        result.months =
            ((today.getFullYear() * 12) + (today.getMonth() + 1))
            - ((dob.getFullYear() * 12) + (dob.getMonth() + 1));
        if (0 > (result.days = today.getDate() - dob.getDate())) {
            var y = today.getFullYear(), m = today.getMonth();
            m = (--m < 0) ? 11 : m;
            result.days +=
                [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m]
                + (((1 == m) && ((y % 4) == 0) && (((y % 100) > 0) || ((y % 400) == 0)))
                    ? 1 : 0);
            --result.months;
        }
        result.years = (result.months - (result.months % 12)) / 12;
        result.months = (result.months % 12);
        return result;
    }




}

