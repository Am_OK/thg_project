import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, WorkingExperiencesServiceProxy, WorkingExperienceListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'EmployeeExperience',
    templateUrl: './EmployeeExperience.html',
})
export class EmployeeExperienceComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private WorkingExperiencesServiceProxy: WorkingExperiencesServiceProxy
    ) {
        super(injector);
    }
    REFID;
    WorkingExperienceListDto: WorkingExperienceListDto[] = []
    WorkingExperienceObj: WorkingExperienceListDto = new WorkingExperienceListDto()
    show(id): void {
        this.active = true;
        this.REFID = id
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getExperience()
    }
    getExperience() {
        this.WorkingExperiencesServiceProxy.getWorkingExperience(this.REFID).subscribe((result) => {
            this.WorkingExperienceListDto = result.items
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    Editing = 0
    showedit(e) {
        this.Editing = 1
        this.WorkingExperienceObj = new WorkingExperienceListDto()
        this.WorkingExperienceObj.id = e.data.id
        this.WorkingExperienceObj.wrkseq = e.data.wrkseq
        this.WorkingExperienceObj.wrkdte = this.fnddmmyyyy(e.data.wrkdte)
        this.WorkingExperienceObj.wrkled = this.fnddmmyyyy(e.data.wrkled)
        this.WorkingExperienceObj.wrkcom = e.data.wrkcom
        this.WorkingExperienceObj.wrkpos = e.data.wrkpos
        this.WorkingExperienceObj.wrkloc = e.data.wrkloc
        this.WorkingExperienceObj.wrksal = e.data.wrksal
        this.WorkingExperienceObj.wrkrmk = e.data.wrkrmk
        setTimeout(() => {
            $('#wrksal').val(this.fnformat(this.WorkingExperienceObj.wrksal))
        }, 250);
        
        this.modal2.show();

    }

    fnnum(){
        $('#wrksal').val(this.fnformat($('#wrksal').val()))
    }
    show2(): void {
        this.WorkingExperienceObj = new WorkingExperienceListDto()
        this.modal2.show();
    }
    close2(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    saveData() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    if (this.WorkingExperienceObj.wrkseq != NaN && this.WorkingExperienceObj.wrkseq != null && this.WorkingExperienceObj.wrkseq != undefined) {
                        var wrkdte = $('#wrkdte').val() != null && $('#wrkdte').val() != undefined ? wrkdte = this.fnyyyymmdd($('#wrkdte').val()) : wrkdte = ''
                        var wrkled = $('#wrkled').val() != null && $('#wrkled').val() != undefined ? wrkled = this.fnyyyymmdd($('#wrkled').val()) : wrkled = ''
                        var wrksal = $('#wrksal').val() != null && $('#wrksal').val() != undefined ? wrksal = this.fnreplace($('#wrksal').val()) : wrksal = 0
                        this.WorkingExperienceObj.wrkidn = this.REFID
                        this.WorkingExperienceObj.wrkdte = wrkdte
                        this.WorkingExperienceObj.wrkled = wrkled
                        this.WorkingExperienceObj.wrksal = wrksal
                        if (this.Editing == 1) {
                            this.WorkingExperiencesServiceProxy.updateWorkingExperience(this.WorkingExperienceObj).subscribe(() => {
                                this.close2()
                                this.getExperience()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.WorkingExperiencesServiceProxy.createWorkingExperience(this.WorkingExperienceObj).subscribe(() => {
                                this.close2()
                                this.getExperience()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        }
                    } else {
                        this.message.warn('กรุณาระบุ Items ให้ถูกต้อง')
                    }
                }
            })
    }
    removed(e){
        this.WorkingExperiencesServiceProxy.deleteWorkingExperiences(e.data.id).subscribe(()=>{
            this.getExperience()
        })
    }
}

