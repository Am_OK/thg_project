import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmergencyContactServiceProxy, EmergencyContactListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'EmergencyContact',
    templateUrl: './EmergencyContact.html',
})
export class EmergencyContactComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private EmergencyContactServiceProxy: EmergencyContactServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    Editing = 0
    REFID
    EmergencyContactListDto: EmergencyContactListDto[] = []
    EmergencyContactObject: EmergencyContactListDto = new EmergencyContactListDto()
    getCommendation() {
        this.EmergencyContactServiceProxy.getEmergencyContactByEmpId(this.REFID).subscribe((result) => {
            this.EmergencyContactListDto = result.items
        })
    }

    show(id): void {
        this.active = true;
        this.REFID = id
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getCommendation()
    }
    // fnnum() {
    //     $('#astprc').val(this.fnformat($('#astprc').val()))
    // }

    showedit(e) {
        this.Editing = 1
        this.EmergencyContactObject = new EmergencyContactListDto()
        this.EmergencyContactObject.id = e.data.id
        this.EmergencyContactObject.eplidn= e.data.eplidn
        this.EmergencyContactObject.eplseq=e.data.eplseq
        this.EmergencyContactObject.eplnam=e.data.eplnam
        this.EmergencyContactObject.eplaD1=e.data.eplaD1
        this.EmergencyContactObject.eplaD2=e.data.eplaD2
        this.EmergencyContactObject.eplsta=e.data.eplsta
        this.EmergencyContactObject.eplcon=e.data.eplcon
        this.EmergencyContactObject.eplzip=e.data.eplzip
        this.EmergencyContactObject.epltel=e.data.epltel
        this.EmergencyContactObject.eplfax=e.data.eplfax
        this.EmergencyContactObject.eplmbl=e.data.eplmbl
        this.EmergencyContactObject.eplpag=e.data.eplpag
        this.EmergencyContactObject.epleml=e.data.epleml
        this.EmergencyContactObject.eplurl=e.data.eplurl
        this.modal2.show();
    }
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    if (this.Editing == 1) {
                        this.EmergencyContactServiceProxy.updateEmergencyContact(this.EmergencyContactObject).subscribe(() => {
                            this.close4()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        this.EmergencyContactServiceProxy.createEmergencyContact(this.EmergencyContactObject).subscribe(() => {
                            this.close4()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    }
                }
            })
    }
    show2(): void {
        this.modal2.show();
        this.EmergencyContactObject = new EmergencyContactListDto()
        this.EmergencyContactObject.eplidn = this.REFID
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close4(): void {
        this.modal2.hide();
        this.Editing = 0
    }
    removed(e) {
        this.EmergencyContactServiceProxy.deleteEmergencyContactByEid(e.data.id).subscribe(() => {
            this.getCommendation()
        })
    }
}

