import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeMovmentsServiceProxy, EmployeeMovmentsListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'EmployeeMovementRec',
    templateUrl: './EmployeeMovementRec.html',
})
export class EmployeeMovementRecComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active:boolean;
    constructor(
        injector: Injector,
        private EmployeeMovmentsServiceProxy: EmployeeMovmentsServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    REFID
    EmployeeMovmentsListDto: EmployeeMovmentsListDto[] = []
    EmployeeMovmentsObject: EmployeeMovmentsListDto = new EmployeeMovmentsListDto()
    getCommendation() {
        this.EmployeeMovmentsServiceProxy.getEmployeeMovmentsByEmpId(this.REFID).subscribe((result) => {
            this.EmployeeMovmentsListDto = result.items
        })
    }
    show(id): void {
        this.active = true;
        this.REFID = id
        setTimeout(() => {
            this.modal.show();
        }, 100);
        
        
        this.getCommendation()
    }
    showAdd(): void {
        this.modal.hide();
        this.modal2.show();
      }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    closeAdd(): void {
        this.modal2.hide();
        this.modal.show();
    }
}

