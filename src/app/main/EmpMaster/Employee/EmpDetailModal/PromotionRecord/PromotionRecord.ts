import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeePromotionsServiceProxy, EmployeePromotionsListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { empmoveComponent } from '@app/main/EmpMaster/Empmove/empmove.component';
import { data_Empmas } from '@app/main/EmpMaster/data_center';


@Component({
    selector: 'PromotionRecord',
    templateUrl: './PromotionRecord.html',
})
export class PromotionRecordComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private EmployeePromotionsServiceProxy: EmployeePromotionsServiceProxy,
        private data_Empmas: data_Empmas,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    POSITIONLEVEL: DbmtabListDto[];
    POSITION: DbmtabListDto[] = [];
    PROMOTIONREASON: DbmtabListDto[] = [];
    namePR
    show(id, pos, plv,psnfnm,psnlnm): void {
        this.active = true;
        this.namePR = psnfnm+' '+psnlnm
        this.POSITION = this.data_Empmas.dbmtab;
        this.POSITIONLEVEL = this.data_Empmas.POSITIONLEVEL;
        this.PROMOTIONREASON = this.data_Empmas.PROMOTIONREASON;
        this.REFID = id
        this.REFPOS = pos
        this.REFPLV = plv
        setTimeout(() => {
                    this.modal.show();
        }, 100);


        this.getCommendation()
    }
    Editing = 0
    REFID
    REFPOS
    REFPLV
    EmployeePromotionsListDto: EmployeePromotionsListDto[] = []
    EmployeePromotionsObject: EmployeePromotionsListDto = new EmployeePromotionsListDto()

    getCommendation() {
        this.EmployeePromotionsServiceProxy.getEmployeePromotionsByEmpId(this.REFID).subscribe((result) => {
            this.EmployeePromotionsListDto = result.items
        })
    }
    fnnum() {
        $('#prmcsa').val(this.fnformat($('#prmcsa').val()))
        $('#prmnsa').val(this.fnformat($('#prmnsa').val()))
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }

    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var prmdte = $('#prmdte').val() != null && $('#prmdte').val() != undefined ? prmdte = this.fnyyyymmdd($('#prmdte').val()) : prmdte = ''
                    var prmcsa = $('#prmcsa').val() != null && $('#prmcsa').val() != undefined ? prmcsa = this.fnreplace($('#prmcsa').val()) : prmcsa = 0
                    var prmnsa = $('#prmnsa').val() != null && $('#prmnsa').val() != undefined ? prmnsa = this.fnreplace($('#prmnsa').val()) : prmnsa = 0

                    this.EmployeePromotionsObject.prmdte = prmdte
                    this.EmployeePromotionsObject.prmcsa = prmcsa
                    this.EmployeePromotionsObject.prmnsa = prmnsa


                    if (this.Editing == 1) {
                        this.EmployeePromotionsServiceProxy.updateEmployeePromotions(this.EmployeePromotionsObject).subscribe(() => {
                            this.closeAddPro()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        this.EmployeePromotionsServiceProxy.createEmployeePromotions(this.EmployeePromotionsObject).subscribe(() => {
                            this.closeAddPro()
                            this.getCommendation()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    }
                }
            })
    }

    showedit(e) {
        this.Editing = 1
        this.EmployeePromotionsObject = new EmployeePromotionsListDto()
        this.EmployeePromotionsObject.id = e.data.id
        this.EmployeePromotionsObject.prmeid = e.data.prmeid
        this.EmployeePromotionsObject.prmdte = this.fnddmmyyyy(e.data.prmdte)
        this.EmployeePromotionsObject.prmrea = e.data.prmrea
        this.EmployeePromotionsObject.prmcps = e.data.prmcps
        this.EmployeePromotionsObject.prmclv = e.data.prmclv
        this.EmployeePromotionsObject.prmcsa = e.data.prmcsa
        this.EmployeePromotionsObject.prmnps = e.data.prmnps
        this.EmployeePromotionsObject.prmnlv = e.data.prmnlv
        this.EmployeePromotionsObject.prmnsa = e.data.prmnsa
        this.EmployeePromotionsObject.prmrM1 = e.data.prmrM1
        this.EmployeePromotionsObject.prmrM2 = e.data.prmrM2
        setTimeout(() => {
            $('#prmcsa').val(this.fnformat(this.EmployeePromotionsObject.prmcsa))
            $('#prmnsa').val(this.fnformat(this.EmployeePromotionsObject.prmnsa))
        }, 250);
        this.modal2.show();
    }


    AddPromotion(): void {
        // this.modal.hide();
        this.modal2.show();
        this.EmployeePromotionsObject = new EmployeePromotionsListDto()
        this.EmployeePromotionsObject.prmeid = this.REFID
        this.EmployeePromotionsObject.prmcps = this.REFPOS
        this.EmployeePromotionsObject.prmclv = this.REFPLV
    }
    closeAddPro() {
        this.modal2.hide()
        // this.modal.show()
        this.Editing = 0
    }
    removed(e) {
        this.EmployeePromotionsServiceProxy.deleteEmployeePromotionsByEid(e.data.id).subscribe(() => {
            this.getCommendation()
        })
    }
}

