import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeUpSaralyServiceProxy, EmployeeUpSaralyListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'PaymentInquiry',
    templateUrl: './PaymentInquiry.html',
})
export class PaymentInquiryComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active:boolean;
    constructor(
        injector: Injector,
        private EmployeeUpSaralyServiceProxy: EmployeeUpSaralyServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    REFID;
    show(id): void {
        this.active = true;        
        this.REFID = id
        setTimeout(() => {
        this.modal.show();            
        }, 100);

        this.getUpSaraly()
    }
    EmployeeUpSaralyListDto: EmployeeUpSaralyListDto[] = []
    getUpSaraly() {
        this.EmployeeUpSaralyServiceProxy.getEmployeeUpSaralyByEmpId(this.REFID).subscribe((result) => {
            this.EmployeeUpSaralyListDto = result.items
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}

