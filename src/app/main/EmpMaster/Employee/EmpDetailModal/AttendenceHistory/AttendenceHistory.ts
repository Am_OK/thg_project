import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'AttendenceHistory',
    templateUrl: './AttendenceHistory.html',
})
export class AttendenceHistoryComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active:boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    show(): void {
        this.modal.show();
        this.active = true;
    }
    
    close(): void {
        this.modal.hide();
        this.active = false;
    }

    show2(){
        this.modal2.show()
    }
    close2(){
        this.modal2.hide()
    }
}

