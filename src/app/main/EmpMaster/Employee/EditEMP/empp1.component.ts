import { Component, OnInit, Injector, ViewChild, EventEmitter, Output, ElementRef } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { DbmtabListDto, DbmtabServiceProxy, UpdateMSMPSN00Input, MSMPSN00ServiceProxy, JobCertificatesServiceProxy, JobCertificatesListDto, CreateJobCertificatesInput, EmployeePicturesServiceProxy, CreateEmployeePicturesInput, CreateEmployeeCarsInput, EmployeeCarsServiceProxy, EmployeeCarsListDto, EmployeePicturesListDto, MSMPSN00ListDto, EmployeeResignsServiceProxy, EmployeeResignsListDto, CreateEmployeeResignsInput, KPShareServiceProxy, GetLeave, MemberOfFamilysServiceProxy, MemberOfFamilyListDto, EnglishCertificatesServiceProxy, EnglishCertificatesListDto, CreateEnglishCertificates } from '@shared/service-proxies/service-proxies';
import { Router, ActivatedRoute } from '@angular/router';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { data_Empmas } from '../../data_center';
import { AppConsts } from '@shared/AppConsts';
import { DxDataGridComponent } from 'devextreme-angular';
import { DatePipe } from '@angular/common';

@Component({
    templateUrl: './empp1.component.html',
    styleUrls: ['./emp.component.less'],
    animations: [appModuleAnimation()]
})
// class UpdateEmpMaster{}
export class EmpP1ModalComponent extends AppComponentBase implements OnInit {

    // @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    // @ViewChild('modal') modal: ModalDirective;
    // @ViewChild('nameInput') nameInput: ElementRef;
    emp: UpdateMSMPSN00Input = new UpdateMSMPSN00Input;
    // emp = {}
    dbmtab: DbmtabListDto[] = [];
    dbmtabcontry: DbmtabListDto[] = [];
    PROVINCE: DbmtabListDto[] = [];
    dbmtabemti: DbmtabListDto[] = [];
    dbmtabenen: DbmtabListDto[] = [];
    dbmtabfna: DbmtabListDto[] = [];
    dbmtabsna: DbmtabListDto[] = [];
    dbmtabamo: DbmtabListDto[] = [];
    dbmtabpty: DbmtabListDto[] = [];
    dbmtabsmr: DbmtabListDto[] = [];
    dbmtabsgd: DbmtabListDto[] = [];
    dbmtabppt: DbmtabListDto[] = [];
    dbmtabsor: DbmtabListDto[] = [];
    HOSPITALFSW: DbmtabListDto[] = []
    CARTYP: DbmtabListDto[];
    CARBRD: DbmtabListDto[];
    CARMDL: DbmtabListDto[];
    CARCOL: DbmtabListDto[];
    INSURCOM: DbmtabListDto[];
    INSURTYPE: DbmtabListDto[];
    DEPARTMENT: DbmtabListDto[];
    POSITIONLEVEL: DbmtabListDto[];
    PROBATIONCODE: DbmtabListDto[] = []
    BLOODGROUP: DbmtabListDto[];
    CARTYPTXT = ""
    CARBRDTXT = ""
    CARMDLTXT = ""
    CARCOLTXT = ""
    CARIDTXT = ""
    active: boolean;
    saving: boolean;
    state_selected = ""
    state = ["ไทย"];
    nopic = AppConsts.remoteServiceBaseUrl + '/Pictures/noPhotoAvailable.jpg'
    Emps = { "data": [{ "tabtB2": "A", "tabdsc": "พนักงาน ", }, { "tabtB2": "O", "tabdsc": "ลาออก", }] };
    STATUS: DbmtabListDto[] = [];
    AccPayrollGroup: DbmtabListDto[];
    DIVISION: DbmtabListDto[];
    CERTIFICATETYP: DbmtabListDto[];
    LANGUAGETYP: DbmtabListDto[];
    constructor(
        injector: Injector,
        // private _empMasterService: EmpMasterServiceProxy,
        private router: ActivatedRoute,
        private _dbmtabService: DbmtabServiceProxy,
        private data_Empmas: data_Empmas,
        private router2: Router,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private JobCertificatesServiceProxy: JobCertificatesServiceProxy,
        private EmployeePicturesServiceProxy: EmployeePicturesServiceProxy,
        private EmployeeCarsServiceProxy: EmployeeCarsServiceProxy,
        private EmployeeResignsServiceProxy: EmployeeResignsServiceProxy,
        private KPShareServiceProxy: KPShareServiceProxy,
        private MemberOfFamilysServiceProxy: MemberOfFamilysServiceProxy,
        private EnglishCertificatesServiceProxy: EnglishCertificatesServiceProxy

    ) {
        super(injector);
        this.uploadUrl = AppConsts.remoteServiceBaseUrl + '/DemoUiComponents/UploadFilesToServer' + '?folderNameStr=PICEMP';
    }
    ngOnInit(): void {
        this.active = true
        this.DOC_REF = this.router.snapshot.params['doc'];
        if (this.data_Empmas.empmasterl != 1) {
            this.data_Empmas.Empmaster()
        }
        this.Probation = this.data_Empmas.Probation
        this.state_selected = this.state[0];
        this.dbmtabemti = this.data_Empmas.dbmtabemti
        this.dbmtabenen = this.data_Empmas.dbmtabenen
        this.dbmtab = this.data_Empmas.dbmtab
        this.dbmtabcontry = this.data_Empmas.dbmtabcontry
        this.PROVINCE = this.data_Empmas.PROVINCE
        this.dbmtabfna = this.data_Empmas.dbmtabfna
        this.dbmtabsna = this.data_Empmas.dbmtabsna
        this.dbmtabamo = this.data_Empmas.dbmtabamo
        this.dbmtabpty = this.data_Empmas.dbmtabpty
        this.dbmtabsmr = this.data_Empmas.dbmtabsmr
        this.dbmtabsgd = this.data_Empmas.dbmtabsgd
        this.dbmtabppt = this.data_Empmas.dbmtabppt
        this.dbmtabsor = this.data_Empmas.dbmtabsor
        this.STATUS = this.data_Empmas.STATUS
        this.HOSPITALFSW = this.data_Empmas.HOSPITALFSW
        this.CARTYP = this.data_Empmas.CARTYP
        this.CARBRD = this.data_Empmas.CARBRD
        this.CARMDL = this.data_Empmas.CARMDL
        this.CARCOL = this.data_Empmas.CARCOL
        this.INSURCOM = this.data_Empmas.INSURCOM
        this.INSURTYPE = this.data_Empmas.INSURTYPE
        this.DEPARTMENT = this.data_Empmas.DEPARTMENT
        this.POSITIONLEVEL = this.data_Empmas.POSITIONLEVEL
        this.PROBATIONCODE = this.data_Empmas.PROBATIONCODE
        this.BLOODGROUP = this.data_Empmas.BLOODGROUP
        this.AccPayrollGroup = this.data_Empmas.AccPayrollGroup
        this.DIVISION = this.data_Empmas.DIVISION
        this.CERTIFICATETYP = this.data_Empmas.CERTIFICATETYP
        this.LANGUAGETYP = this.data_Empmas.LANGUAGETYP
        this.newEmpID = ''
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.empM = result.items;
        });
        $(document).ready(function () {
            $('#dtewstart').change(function () {
                $('#dtewstartcl').click()
            });
            $('#dtewsts').change(function () {
                $('#dtewstartcl').click()
            });
            $('#psnbrd').change(function () {
                $('#psnbrdcl').click()
            });
            $("#psneml").inputmask({
                mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
                greedy: false,
                onBeforePaste: function (pastedValue, opts) {
                    pastedValue = pastedValue.toLowerCase();
                    return pastedValue.replace("mailto:", "");
                },
                definitions: {
                    '*': {
                        validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                        cardinality: 1,
                        casing: "lower"
                    }
                }
            });
            $("#psnpid").inputmask({
                "mask": "9-9999-99999-99-9",
                placeholder: "" // remove underscores from the input mask
            });
        });

        this.employee(this.DOC_REF)
    }
    DOC_REF
    Probation = []
    nmwk: boolean
    EnglishCertificatesListDto: EnglishCertificatesListDto[] = []

    fnchknowork() {

        setTimeout(() => {
            if (this.nmwk == true) {
                this.emp.psnmwk = 'NOWORK'
            } else {
                this.emp.psnmwk = ''
            }
        }, 50);
    }
    employee(doc) {

        this.MSMPSN00ServiceProxy.getMSMPSN00(doc).subscribe((result) => {
            this.emp = result.items[0]
            this.emp.psnbrd = this.fnddmmyyyy(this.emp.psnbrd)
            this.emp.psnidt = this.fnddmmyyyy(this.emp.psnidt)
            this.emp.psniep = this.fnddmmyyyy(this.emp.psniep)
            this.emp.psncerisdte = this.fnddmmyyyy(this.emp.psncerisdte)
            this.emp.psncerexdte = this.fnddmmyyyy(this.emp.psncerexdte)
            this.emp.psninw = this.fnddmmyyyy(this.emp.psninw)
            this.emp.psnetd = this.fnddmmyyyy(this.emp.psnetd)
            this.emp.psnstd = this.fnddmmyyyy(this.emp.psnstd)
            this.emp.psnfbr = this.fnddmmyyyy(this.emp.psnfbr)
            this.emp.psnmbr = this.fnddmmyyyy(this.emp.psnmbr)
            this.emp.psnmabr = this.fnddmmyyyy(this.emp.psnmabr)

            this.emp.psndrexr = this.fnddmmyyyy(this.emp.psndrexr)
            this.emp.psndrexR2 = this.fnddmmyyyy(this.emp.psndrexR2)
            this.emp.psnaaf = this.fnddmmyyyy(this.emp.psnaaf)
            this.emp.psnatt = this.fnddmmyyyy(this.emp.psnatt)
            this.emp.psnvdte = this.fnddmmyyyy(this.emp.psnvdte)
            this.emp.psnresign == 'Y' ? this.psnresign = true : this.psnresign = undefined
            setTimeout(() => {
                this.formatInt()
            }, 500);

            if (this.emp.psnmwk == 'NOWORK') {
                this.nmwk = true
            } else {
                this.nmwk = false
            }

            this._dbmtabService.getAddressPostCode('AMPHUR', '', this.emp.psnupc, '', '').subscribe((result) => {
                this.AMPHUR = result.items;
            });
            this._dbmtabService.getAddressPostCode('TUMBOL', '', '', this.emp.psndst, '').subscribe((result) => {
                this.TUMBOL = result.items;

            });

            this.JobCertificatesServiceProxy.getJobCertificatesByEmpId(doc).subscribe((result) => {
                this.JobCertificatesListDto = result.items
                if (this.JobCertificatesListDto.length <= 0) { this.Seq = 1 } else {


                    var value = this.JobCertificatesListDto[0].cerseq;
                    this.Seq = this.JobCertificatesListDto[0].cerseq;
                    for (let i = 0; i < this.JobCertificatesListDto.length; i++) {
                        if (this.JobCertificatesListDto[i].cerseq >= value) {
                            value = this.JobCertificatesListDto[i].cerseq;
                            this.Seq = (this.JobCertificatesListDto[i].cerseq + 1 * 1)
                        }
                        this.idc = this.idc + 1
                        this.certificate.push({
                            "id": this.idc,
                            "seq": this.JobCertificatesListDto[i].cerseq,
                            "cerno": this.JobCertificatesListDto[i].cerno,
                            "cerisdate": this.JobCertificatesListDto[i].ceriss,
                            "cerexdate": this.JobCertificatesListDto[i].cerexp
                        })


                    }
                }
            })

            this.EnglishCertificatesServiceProxy.getEnglishCertificatesByEmpId(doc).subscribe((result) => {
                this.EnglishCertificatesListDto = result.items
                if (this.EnglishCertificatesListDto.length <= 0) { this.SeqL = 1 } else {
                    var value = this.EnglishCertificatesListDto[0].renseq;
                    this.SeqL = this.EnglishCertificatesListDto[0].renseq;
                    for (let i = 0; i < this.EnglishCertificatesListDto.length; i++) {
                        if (this.EnglishCertificatesListDto[i].renseq >= value) {
                            value = this.EnglishCertificatesListDto[i].renseq;
                            this.SeqL = (this.EnglishCertificatesListDto[i].renseq + 1 * 1)
                        }
                        this.idl = this.idl + 1
                        this.language.push({
                            "id": this.idl,
                            "seq": this.EnglishCertificatesListDto[i].renseq,
                            "cerno": this.EnglishCertificatesListDto[i].rencod,
                            "certyp": this.EnglishCertificatesListDto[i].rentyp,
                            "cerscore": this.EnglishCertificatesListDto[i].renscore,
                            "cerisdate": this.EnglishCertificatesListDto[i].renfmd,
                            "cerexdate": this.EnglishCertificatesListDto[i].rentod
                        })

                    }
                }
            })


            this.EmployeeCarsServiceProxy.getEmployeeCarsByEmpId(doc).subscribe((result) => {
                this.EmployeeCarsListDto = result.items
                for (let i = 0; i < this.EmployeeCarsListDto.length; i++) {
                    this.idcar = (this.idcar + 1 * 1)
                    this.ListofCar.push({ 'id': this.idcar, 'CarID': this.EmployeeCarsListDto[i].carlic, 'CarTyp': this.EmployeeCarsListDto[i].cartyp, 'CarBrd': this.EmployeeCarsListDto[i].carbrd, 'CarMdl': this.EmployeeCarsListDto[i].carmdl, 'CarCol': this.EmployeeCarsListDto[i].carcol })

                }
            })
            this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(doc).subscribe((result) => {
                this.picture = result.items
            })

            this.datedi(this.emp.psninw, this.emp.psnpbc, this.emp.psnstd)
            this.LeavData = []
            this.KPShareServiceProxy.getLeave(doc).subscribe((result) => {
                this.GetLeave = result
                for (let index = 0; index < this.GetLeave.length; index++) {
                    
                    if (this.GetLeave[index].psncod == 'V' || this.GetLeave[index].psncod == 'PL' || this.GetLeave[index].psncod == 'TL' || this.GetLeave[index].psncod == 'OT' || this.GetLeave[index].psncod == 'ML' || this.GetLeave[index].psncod == 'SL') {
                        var dat = this.GetLeave[index].psnbal.split(',')
                        for (let p = 0; p < dat.length; p++) {
                            var idx = '#' + this.GetLeave[index].psncod + p
                            // console.log(idx)
                            // this.LeavData.push({'psncod':this.GetLeave[index].psncod,'psnbal':dat,'psnmax':this.GetLeave[index].psnmax})
                            // console.log(this.LeavData)



                            $(idx).val(dat[p])
                            if (dat.length <= (p + 1 * 1)) {
                                var idx2 = '#' + this.GetLeave[index].psncod + (p + 1 * 1)
                                $(idx2).val(this.GetLeave[index].psnmax)
                            }
                        }
                    }
                }
            })
            this.fnsumChild()
            setTimeout(() => {
                $('.m_select2_1a').select2({
                    placeholder: "Please Select.."
                });
            }, 250);
        })
    }
    LeavData = []
    JobCertificatesListDto: JobCertificatesListDto[] = []
    GetLeave: GetLeave[] = []
    back() {
        this.router2.navigate(['/app/main/EmployeeMaster']);
    }

    formatInt() {

        $('#psnnsl').val(this.fnformat(this.emp.psnnsl))
    }
    RESIGNREASON: DbmtabListDto[] = []
    save(psnbrd, psnidt, psniep, psncerisdte, psncerexdte, dat, psnetd, dtests, psnfbr, psnmbr, psnmabr, psndrexr, psndrexR2, psnaaf, psnatt, psnvdte, psnpid): void {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    this.psnresign == true ? this.emp.psnresign = 'Y' : this.emp.psnresign = 'N'
                    if (this.emp.psnresign == 'Y') {
                        this.EmployeeResignsServiceProxy.getEmployeeResignsByEmpId(this.emp.psnidn).subscribe((result) => {
                            if (result.items.length <= 0) {
                                this._dbmtabService.getDbmtabFromTABFG1('RESIGNREASON', '').subscribe((result) => {
                                    this.RESIGNREASON = result.items;
                                    this.modalREs.show()
                                    $('#resefd').val(dtests)
                                });
                            }
                            this.saving = true;
                            if ($('#titln').val() != null) {
                                this.emp.psntth = $('#titln').val().toString()
                            }
                            if ($('#psnteg').val() != null) {
                                this.emp.psnteg = $('#psnteg').val().toString()
                            }
                            if ($('#psnpos').val() != null) {
                                this.emp.psnpos = $('#psnpos').val().toString()
                            }
                            if ($('#psnpospf').val() != null) {
                                this.emp.psnpospf = $('#psnpospf').val().toString()
                            }
                            if ($('#psnpoS02').val() != null) {
                                this.emp.psnpoS02 = $('#psnpoS02').val().toString()
                            }
                            if ($('#psnpoS03').val() != null) {
                                this.emp.psnpoS03 = $('#psnpoS03').val().toString()
                            }
                            if ($('#psnplv').val() != null) {
                                this.emp.psnplv = $('#psnplv').val().toString()
                            }
                            if ($('#psneml').val() != null) {
                                this.emp.psneml = $('#psneml').val().toString()
                            }
                            if ($('#psncom').val() != null) {
                                this.emp.psncom = $('#psncom').val().toString()
                            }
                            if ($('#psnbrn').val() != null) {
                                this.emp.psnbrn = $('#psnbrn').val().toString()
                            }
                            if ($('#psndev').val() != null) {
                                this.emp.psndev = $('#psndev').val().toString()
                            }
                            if ($('#psndep').val() != null) {
                                this.emp.psndep = $('#psndep').val().toString()
                            }
                            if ($('#psnacc').val() != null) {
                                this.emp.psnacc = $('#psnacc').val().toString()
                            }
                            if ($('#psnins').val() != null) {
                                this.emp.psnins = parseFloat($('#psnins').val().toString())
                            }
                            if (psnpid != null && psnpid != '' && psnpid != undefined) {
                                this.emp.psnpid = psnpid.replace(/-/g, "")
                            }
                            this.emp.psnplV1 = this.emp.psnplv
                            this.emp.psnleV1 = this.emp.psnlev
                            this.emp.psnpoS1 = this.emp.psnpos
                            this.emp.psnbrd = this.fnyyyymmdd(psnbrd)
                            this.emp.psnidt = this.fnyyyymmdd(psnidt)
                            this.emp.psniep = this.fnyyyymmdd(psniep)
                            this.emp.psncerisdte = this.fnyyyymmdd(psncerisdte)
                            this.emp.psncerexdte = this.fnyyyymmdd(psncerexdte)
                            this.emp.psninw = this.fnyyyymmdd(dat)
                            this.emp.psnetd = this.fnyyyymmdd(psnetd)
                            this.emp.psnstd = this.fnyyyymmdd(dtests)
                            this.emp.psnfbr = this.fnyyyymmdd(psnfbr)
                            this.emp.psnmbr = this.fnyyyymmdd(psnmbr)
                            this.emp.psnmabr = this.fnyyyymmdd(psnmabr)

                            this.emp.psndrexr = this.fnyyyymmdd(psndrexr)
                            this.emp.psndrexR2 = this.fnyyyymmdd(psndrexR2)
                            this.emp.psnaaf = this.fnyyyymmdd(psnaaf)
                            this.emp.psnatt = this.fnyyyymmdd(psnatt)
                            this.emp.psnvdte = this.fnyyyymmdd(psnvdte)
                            this.emp.psnnsl = this.fnreplace(this.emp.psnnsl)

                            this.MSMPSN00ServiceProxy.updateMSMPSN00(this.emp).subscribe(() => {
                                this.saving = false;
                                this.emp.psnbrd = this.fnddmmyyyy(this.emp.psnbrd)
                                this.emp.psnidt = this.fnddmmyyyy(this.emp.psnidt)
                                this.emp.psniep = this.fnddmmyyyy(this.emp.psniep)
                                this.emp.psncerisdte = this.fnddmmyyyy(this.emp.psncerisdte)
                                this.emp.psncerexdte = this.fnddmmyyyy(this.emp.psncerexdte)
                                this.emp.psninw = this.fnddmmyyyy(this.emp.psninw)
                                this.emp.psnetd = this.fnddmmyyyy(this.emp.psnetd)
                                this.emp.psnstd = this.fnddmmyyyy(this.emp.psnstd)
                                this.emp.psnfbr = this.fnddmmyyyy(this.emp.psnfbr)
                                this.emp.psnmbr = this.fnddmmyyyy(this.emp.psnmbr)
                                this.emp.psnmabr = this.fnddmmyyyy(this.emp.psnmabr)

                                this.emp.psndrexr = this.fnddmmyyyy(this.emp.psndrexr)
                                this.emp.psndrexR2 = this.fnddmmyyyy(this.emp.psndrexR2)
                                this.emp.psnaaf = this.fnddmmyyyy(this.emp.psnaaf)
                                this.emp.psnatt = this.fnddmmyyyy(this.emp.psnatt)
                                this.emp.psnvdte = this.fnddmmyyyy(this.emp.psnvdte)

                            });
                            this.saveJob(this.emp.psnidn)
                            this.saveLng(this.emp.psnidn)
                            this.saveCars(this.emp.psnidn)

                        })
                    } else {
                        this.saving = true;
                        if ($('#titln').val() != null) {
                            this.emp.psntth = $('#titln').val().toString()
                        }
                        if ($('#psnteg').val() != null) {
                            this.emp.psnteg = $('#psnteg').val().toString()
                        }
                        if ($('#psnpos').val() != null) {
                            this.emp.psnpos = $('#psnpos').val().toString()
                        }
                        if ($('#psnpospf').val() != null) {
                            this.emp.psnpospf = $('#psnpospf').val().toString()
                        }
                        if ($('#psnpoS02').val() != null) {
                            this.emp.psnpoS02 = $('#psnpoS02').val().toString()
                        }
                        if ($('#psnpoS03').val() != null) {
                            this.emp.psnpoS03 = $('#psnpoS03').val().toString()
                        }
                        if ($('#psnplv').val() != null) {
                            this.emp.psnplv = $('#psnplv').val().toString()
                        }
                        if ($('#psneml').val() != null) {
                            this.emp.psneml = $('#psneml').val().toString()
                        }
                        if ($('#psncom').val() != null) {
                            this.emp.psncom = $('#psncom').val().toString()
                        }
                        if ($('#psnbrn').val() != null) {
                            this.emp.psnbrn = $('#psnbrn').val().toString()
                        }
                        if ($('#psndev').val() != null) {
                            this.emp.psndev = $('#psndev').val().toString()
                        }
                        if ($('#psndep').val() != null) {
                            this.emp.psndep = $('#psndep').val().toString()
                        }
                        if ($('#psnacc').val() != null) {
                            this.emp.psnacc = $('#psnacc').val().toString()
                        }
                        if ($('#psnins').val() != null) {
                            this.emp.psnins = parseFloat($('#psnins').val().toString())
                        }
                        if (psnpid != null && psnpid != '' && psnpid != undefined) {
                            this.emp.psnpid = psnpid.replace(/-/g, "")
                        }
                        this.emp.psnplV1 = this.emp.psnplv
                        this.emp.psnleV1 = this.emp.psnlev
                        this.emp.psnpoS1 = this.emp.psnpos
                        this.emp.psnbrd = this.fnyyyymmdd(psnbrd)
                        this.emp.psnidt = this.fnyyyymmdd(psnidt)
                        this.emp.psniep = this.fnyyyymmdd(psniep)
                        this.emp.psncerisdte = this.fnyyyymmdd(psncerisdte)
                        this.emp.psncerexdte = this.fnyyyymmdd(psncerexdte)
                        this.emp.psninw = this.fnyyyymmdd(dat)
                        this.emp.psnetd = this.fnyyyymmdd(psnetd)
                        this.emp.psnstd = this.fnyyyymmdd(dtests)
                        this.emp.psnfbr = this.fnyyyymmdd(psnfbr)
                        this.emp.psnmbr = this.fnyyyymmdd(psnmbr)
                        this.emp.psnmabr = this.fnyyyymmdd(psnmabr)

                        this.emp.psndrexr = this.fnyyyymmdd(psndrexr)
                        this.emp.psndrexR2 = this.fnyyyymmdd(psndrexR2)
                        this.emp.psnaaf = this.fnyyyymmdd(psnaaf)
                        this.emp.psnatt = this.fnyyyymmdd(psnatt)
                        this.emp.psnvdte = this.fnyyyymmdd(psnvdte)
                        this.emp.psnnsl = this.fnreplace(this.emp.psnnsl)

                        this.MSMPSN00ServiceProxy.updateMSMPSN00(this.emp).subscribe(() => {
                            this.saving = false;
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.close();
                        });
                        this.saveJob(this.emp.psnidn)
                        this.saveLng(this.emp.psnidn)
                        this.saveCars(this.emp.psnidn)

                    }
                }
            })
    }
    saveNewEmp() {
        var empsmaschk: MSMPSN00ListDto[] = []
        this.newEmpModal = false
        // console.log(this.newEmpID)
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    if (this.newEmpID.replace(/ /g, "").length > 0 && this.newEmpID != null && this.newEmpID != undefined) {
                        this.MSMPSN00ServiceProxy.getMSMPSN00(this.newEmpID).subscribe((result) => {
                            empsmaschk = result.items
                            if (empsmaschk.length <= 0) {
                                this.emp.psnidn = this.newEmpID
                                this.saving = true;
                                if ($('#titln').val() != null) {
                                    this.emp.psntth = $('#titln').val().toString()
                                }
                                if ($('#psnteg').val() != null) {
                                    this.emp.psnteg = $('#psnteg').val().toString()
                                }
                                if ($('#psnpos').val() != null) {
                                    this.emp.psnpos = $('#psnpos').val().toString()
                                }
                                if ($('#psnpospf').val() != null) {
                                    this.emp.psnpospf = $('#psnpospf').val().toString()
                                }
                                if ($('#psnpoS02').val() != null) {
                                    this.emp.psnpoS02 = $('#psnpoS02').val().toString()
                                }
                                if ($('#psnpoS03').val() != null) {
                                    this.emp.psnpoS03 = $('#psnpoS03').val().toString()
                                }
                                if ($('#psnplv').val() != null) {
                                    this.emp.psnplv = $('#psnplv').val().toString()
                                }
                                if ($('#psneml').val() != null) {
                                    this.emp.psneml = $('#psneml').val().toString()
                                }
                                if ($('#psncom').val() != null) {
                                    this.emp.psncom = $('#psncom').val().toString()
                                }
                                if ($('#psnbrn').val() != null) {
                                    this.emp.psnbrn = $('#psnbrn').val().toString()
                                }
                                if ($('#psndev').val() != null) {
                                    this.emp.psndev = $('#psndev').val().toString()
                                }
                                if ($('#psndep').val() != null) {
                                    this.emp.psndep = $('#psndep').val().toString()
                                }
                                if ($('#psnacc').val() != null) {
                                    this.emp.psnacc = $('#psnacc').val().toString()
                                }
                                if ($('#psnins').val() != null) {
                                    this.emp.psnins = parseFloat($('#psnins').val().toString())
                                }
                                this.emp.psnplV1 = this.emp.psnplv
                                this.emp.psnleV1 = this.emp.psnlev
                                this.emp.psnpoS1 = this.emp.psnpos


                                this.emp.psnbrd = this.fnyyyymmdd(this.emp.psnbrd)
                                this.emp.psnidt = this.fnyyyymmdd(this.emp.psnidt)
                                this.emp.psniep = this.fnyyyymmdd(this.emp.psniep)
                                this.emp.psncerisdte = this.fnyyyymmdd(this.emp.psncerisdte)
                                this.emp.psncerexdte = this.fnyyyymmdd(this.emp.psncerexdte)
                                this.emp.psninw = this.fnyyyymmdd(this.emp.psninw)
                                this.emp.psnetd = this.fnyyyymmdd(this.emp.psnetd)
                                this.emp.psnstd = this.fnyyyymmdd(this.emp.psnstd)
                                this.emp.psnfbr = this.fnyyyymmdd(this.emp.psnfbr)
                                this.emp.psnmbr = this.fnyyyymmdd(this.emp.psnmbr)
                                this.emp.psnmabr = this.fnyyyymmdd(this.emp.psnmabr)

                                this.emp.psndrexr = this.fnyyyymmdd(this.emp.psndrexr)
                                this.emp.psndrexR2 = this.fnyyyymmdd(this.emp.psndrexR2)
                                this.emp.psnaaf = this.fnyyyymmdd(this.emp.psnaaf)
                                this.emp.psnatt = this.fnyyyymmdd(this.emp.psnatt)
                                this.emp.psnvdte = this.fnyyyymmdd(this.emp.psnvdte)
                                this.emp.psnnsl = this.fnreplace(this.emp.psnnsl)
                                this.emp.psnresign = 'N'
                                this.MSMPSN00ServiceProxy.createMSMPSN00(this.emp).subscribe(() => {
                                    this.saving = false;
                                    this.notify.info(this.l('SavedSuccessfully'));
                                    this.close();
                                });
                                // this.saveJob(this.emp.psnidn)
                                // this.saveCars(this.emp.psnidn)
                            } else {
                                this.message.warn('พบรหัสพนักงานนี้อยู่ในระบบแล้ว', 'ข้อผิดพลาด')
                            }
                        })
                    } else {
                        // this.newEmpModal = true
                        this.message.warn('กรุณาระบุรหัสพนักงานใหม่ให้ถูกต้อง', 'ข้อผิดพลาด')
                    }
                }
            })
    }
    saveNewEmpMD(psnbrd, psnidt, psniep, psncerisdte, psncerexdte, dat, psnetd, dtests, psnfbr, psnmbr, psnmabr, psndrexr, psndrexR2, psnaaf, psnatt, psnvdte, psnpid) {
        this.newEmpModal = true
        if (psnpid != null && psnpid != '' && psnpid != undefined) {
            this.emp.psnpid = psnpid.replace(/-/g, "")
        }

        this.emp.psnbrd = psnbrd
        this.emp.psnidt = psnidt
        this.emp.psniep = psniep
        this.emp.psncerisdte = psncerisdte
        this.emp.psncerexdte = psncerexdte
        this.emp.psninw = dat
        this.emp.psnetd = psnetd
        this.emp.psnstd = dtests
        this.emp.psnfbr = psnfbr
        this.emp.psnmbr = psnmbr
        this.emp.psnmabr = psnmabr

        this.emp.psndrexr = psndrexr
        this.emp.psndrexR2 = psndrexR2
        this.emp.psnaaf = psnaaf
        this.emp.psnatt = psnatt
        this.emp.psnvdte = psnvdte
        this.emp.psnnsl = this.emp.psnnsl
    }
    newEmpID = ''
    newEmpModal: boolean
    CreateJobCertificatesInput: CreateJobCertificatesInput = new CreateJobCertificatesInput()
    saveJob(eid) {
        // this.JobCertificatesServiceProxy.deleteJobCertificates
        this.JobCertificatesServiceProxy.deleteJobCertificatesByCerEID(eid).subscribe(() => {
            for (let i = 0; i < this.certificate.length; i++) {
                setTimeout(() => {
                    this.CreateJobCertificatesInput.cereid = eid
                    this.CreateJobCertificatesInput.cerseq = this.certificate[i].seq
                    this.CreateJobCertificatesInput.cerno = this.certificate[i].cerno
                    this.CreateJobCertificatesInput.certyp = this.certificate[i].certyp
                    this.CreateJobCertificatesInput.ceriss = this.certificate[i].cerisdate
                    this.CreateJobCertificatesInput.cerexp = this.certificate[i].cerexdate
                    this.JobCertificatesServiceProxy.createJobCertificates(this.CreateJobCertificatesInput).subscribe(() => {

                    })
                }, 250 * i);

            }
        })

    }
    CreateEnglishCertificates: CreateEnglishCertificates = new CreateEnglishCertificates()
    saveLng(eid) {
        this.EnglishCertificatesServiceProxy.deleteEnglishCertificatesByEmpId(eid).subscribe(() => {
            for (let i = 0; i < this.language.length; i++) {
                setTimeout(() => {
                    this.CreateEnglishCertificates.reneid = eid
                    this.CreateEnglishCertificates.renseq = this.language[i].seq
                    this.CreateEnglishCertificates.rencod = this.language[i].cerno
                    this.CreateEnglishCertificates.rentyp = this.language[i].certyp
                    this.CreateEnglishCertificates.renscore = this.language[i].cerscore
                    this.CreateEnglishCertificates.renfmd = this.language[i].cerisdate
                    this.CreateEnglishCertificates.rentod = this.language[i].cerexdate
                    this.EnglishCertificatesServiceProxy.createEnglishCertificates(this.CreateEnglishCertificates).subscribe(() => {

                    })
                }, 250 * i);

            }
        })
    }
    EmployeeCarsListDto: EmployeeCarsListDto[] = []
    CreateEmployeeCarsInput: CreateEmployeeCarsInput = new CreateEmployeeCarsInput()

    saveCars(eid) {
        this.EmployeeCarsServiceProxy.deleteEmployeeCarsCAREID(eid).subscribe(() => {


            this.ListofCar.forEach(element => {
                this.CreateEmployeeCarsInput.careid = eid
                this.CreateEmployeeCarsInput.cartyp = element.CarTyp
                this.CreateEmployeeCarsInput.carbrd = element.CarBrd
                this.CreateEmployeeCarsInput.carmdl = element.CarMdl
                this.CreateEmployeeCarsInput.carcol = element.CarCol
                this.CreateEmployeeCarsInput.carlic = element.CarID
                this.EmployeeCarsServiceProxy.createEmployeeCars(this.CreateEmployeeCarsInput).subscribe(() => {

                })
            });
        })

    }
    Delete() {
        this.message.confirm(
            this.l('AreYouSureToDeleteTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    this.MSMPSN00ServiceProxy.deleteMSMPSN00(this.emp.id).subscribe(() => {
                        this.JobCertificatesServiceProxy.deleteJobCertificatesByCerEID(this.emp.psnidn).subscribe(() => {
                        })
                        this.EnglishCertificatesServiceProxy.deleteEnglishCertificatesByEmpId(this.emp.psnidn).subscribe(() => {
                        })
                        this.EmployeeCarsServiceProxy.deleteEmployeeCarsCAREID(this.emp.psnidn).subscribe(() => {
                        })
                        this.close()
                    })
                }
            })
    }
    close(): void {
        this.active = false
        this.back()
    }
    psnresign
    datedi(d, v, s): void {
        if ((d != '' && d != null && d != undefined) && (v != '' && v != null && v != undefined) && (s != '' && s != null && s != undefined)) {


            this.datedi2(d, v, s)
            if (this.emp.psnsts == 'A') {
                var date1 = new Date(Date.now());
                var date2 = new Date(this.fninyyyymmdd(d));

            }
            else {
                var date1 = new Date(this.fninyyyymmdd(s));
                var date2 = new Date(this.fninyyyymmdd(d));
                this.psnresign = false;
            }
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());

            var diffDays = this.fnformatInt(Math.ceil(timeDiff / (1000 * 3600 * 24))) + " วัน";
            $('#dd').val(diffDays);

        }
    }

    datedi2(d, v, s): void {

        // alert(d)
        var someDate = new Date(this.fninyyyymmdd(d));
        var someDate2 = new Date(this.fninyyyymmdd(d));
        var duration = parseInt(v); //In Days
        // console.log(v);
        // console.log(duration);


        someDate.setTime(someDate.getTime() + (duration * 24 * 60 * 60 * 1000));

        if (this.emp.psnsts == 'A') {
            var datenew = new Date()
            datenew.setDate(datenew.getDate() + 1)


        }
        else {
            var datenew = new Date(this.fninyyyymmdd(s))
        }

        //console.log(someDate.toISOString().substr(5, 2))
        //console.log(d.substr(3, 2))
        if (duration != 0) {
            if ((parseInt(someDate.toISOString().substr(8, 2)) <= 15) || (someDate.toISOString().substr(5, 2)) == d.substr(3, 2)) {
                //console.log(11)

                if ((parseInt(someDate.toISOString().substr(8, 2)) <= 15)) {
                    var datt = someDate.setMonth(someDate.getMonth())
                    var vrr = new Date(datt);
                }
                else if ((someDate.toISOString().substr(5, 2)) == d.substr(3, 2)) {
                    someDate.setDate(1)
                    var datt = someDate.setMonth(someDate.getMonth() + 1)
                    var vrr = new Date(datt);
                }


            } else {
                //console.log(22)
                var datt = someDate.setMonth(someDate.getMonth() + 1)
                var vrr = new Date(datt);
            }
        } else {
            if ((parseInt(someDate.toISOString().substr(8, 2)) != 1)) {
                someDate.setDate(1)
                var datt = someDate.setMonth(someDate.getMonth() + 1)
                var vrr = new Date(datt);
            } else {
                var datt = someDate.setMonth(someDate.getMonth())
                var vrr = new Date(datt);
            }
        }
        vrr.setDate(1)
        //console.log(vrr.toISOString())
        var dteps = this.fninISOddmmyyyy(vrr.toISOString())
        //console.log(this.fninISOddmmyyyy(vrr.toISOString()))
        var dtn2 = this.age(someDate2, datenew)
        var dt2s = dtn2.toString()
        this.emprefage = dtn2
        setTimeout(() => {
            $('#dd22').val(dt2s);
            $('#dtepass').val(dteps);

        }, 500);
    }
    emprefage
    age(dob, today) {
        var today = today,
            result = {
                years: 0,
                months: 0,
                days: 0,
                toString: function () {
                    return (this.years ? this.years + ' ปี ' : '')
                        + (this.months ? this.months + ' เดือน ' : '')
                        + (this.days ? this.days + ' วัน' : '');
                }
            };
        result.months =
            ((today.getFullYear() * 12) + (today.getMonth() + 1))
            - ((dob.getFullYear() * 12) + (dob.getMonth() + 1));
        if (0 > (result.days = today.getDate() - dob.getDate())) {
            var y = today.getFullYear(), m = today.getMonth();
            m = (--m < 0) ? 11 : m;
            result.days +=
                [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m]
                + (((1 == m) && ((y % 4) == 0) && (((y % 100) > 0) || ((y % 400) == 0)))
                    ? 1 : 0);
            --result.months;
        }
        result.years = (result.months - (result.months % 12)) / 12;
        result.months = (result.months % 12);
        return result;
    }

    fncalage(dte) {
        console.log(dte)
        var datenew = new Date()
        var olddate = new Date(this.fninyyyymmdd(dte))
        var agepr = this.ageperson(olddate, datenew)
        console.log(agepr)
        this.emp.psnage = agepr.years
    }
    ageperson(dob, today) {
        var today = today,
            result = {
                years: 0,
                months: 0,
                days: 0
            };
        result.months =
            ((today.getFullYear() * 12) + (today.getMonth() + 1))
            - ((dob.getFullYear() * 12) + (dob.getMonth() + 1));
        if (0 > (result.days = today.getDate() - dob.getDate())) {
            var y = today.getFullYear(), m = today.getMonth();
            m = (--m < 0) ? 11 : m;
            result.days +=
                [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m]
                + (((1 == m) && ((y % 4) == 0) && (((y % 100) > 0) || ((y % 400) == 0)))
                    ? 1 : 0);
            --result.months;
        }
        result.years = (result.months - (result.months % 12)) / 12;
        result.months = (result.months % 12);
        return result;
    }



    AMPHUR: DbmtabListDto[];
    TUMBOL: DbmtabListDto[];

    onValueChanged($event) {
        this.state_selected = $event.value;

    }
    change_province(province) {
        // this.CreateCustomerMaster.massta = province.substr(3, province.length);
        var province_sub = province.substr(0, 3);
        $('#amph,#tumb').val('0');
        $('#zipc').val('');
        this.emp.psnzip = ''
        this._dbmtabService.getAddressPostCode('AMPHUR', '', province_sub, '', '').subscribe((result) => {
            this.AMPHUR = result.items;
            //console.log(this.AMPHUR);
        });

    }
    change_amphur(amphur) {

        // this.CreateCustomerMaster.masamphur = amphur.substr(4, amphur.length);
        var amphur_sub = amphur.substr(0, 4);
        $('#tumb').val('0');
        $('#zipc').val('');
        this.emp.psnzip = ''
        this._dbmtabService.getAddressPostCode('TUMBOL', '', '', amphur_sub, '').subscribe((result) => {
            this.TUMBOL = result.items;

        });

    }
    tumb2;
    change_tumbol(tumbol) {

        // this.CreateCustomerMaster.mastumbol = tumbol.substr(4, tumbol.length);
        var tumbol_sub = tumbol.substr(0, 4);
        var tumb = this.TUMBOL.filter(res => res.tabtB2 == tumbol_sub);
        this.tumb2 = tumb[0].tabfG3
        // this.CreateCustomerMaster.maszip = tumb[0].tabfG3;
        $('#zipc').val(this.tumb2).end()
        this.emp.psnzip = this.tumb2

    }
    ListofCar = []
    idcar = 0
    saveCar() {
        this.idcar = (this.idcar + 1 * 1)
        this.ListofCar.push({ 'id': this.idcar, 'CarID': this.CARIDTXT, 'CarTyp': this.CARTYPTXT, 'CarBrd': this.CARBRDTXT, 'CarMdl': this.CARMDLTXT, 'CarCol': this.CARCOLTXT })
        this.CARTYPTXT = ""
        this.CARBRDTXT = ""
        this.CARMDLTXT = ""
        this.CARCOLTXT = ""
        this.CARIDTXT = ""
    }

    uploadUrl: string;
    uploadedFiles0: any[] = [];
    uploadedFiles: any[] = [];
    uploadedFiles2: any[] = [];
    uploadedFiles3: any[] = [];
    uploadedFiles4: any[] = [];
    uploadedFiles5: any[] = [];
    uploadedFiles6: any[] = [];
    uploadedFiles7: any[] = [];
    uploadedFiles8: any[] = [];
    downloadServerLink = AppConsts.remoteServiceBaseUrl + '/PICEMP/';
    src2;


    onBeforeSend(event): void {
        // console.log(event)
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
        //console.log(abp.auth.getToken());        
    }
    CreateEmployeePicturesInput: CreateEmployeePicturesInput = new CreateEmployeePicturesInput()
    pictureList = []
    onUpload1S(event) {
        this.uploadedFiles0 = []
        for (const file of event.files) {
            this.uploadedFiles0.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles0[0].name
            this.emp.psnemg = pathimg
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '001', 'picseq': null })
            this.savePIC(pathimg, '001', 0)
        }
    }
    upimg() {
        // this.emp.psnidn != '' && this.emp.psnidn != null ? $('#file-upload').click() : ''
    }
    onUpload1(event) {
        this.uploadedFiles = []
        for (const file of event.files) {
            this.uploadedFiles.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles[0].name
            this.emp.psnemg = pathimg
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '001', 'picseq': null })
            this.savePIC(pathimg, '001', 0)
        }
    }
    onUpload2(event) {
        this.uploadedFiles2 = []
        for (const file of event.files) {
            this.uploadedFiles2.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles2[0].name
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '002', 'picseq': null })
            this.savePIC(pathimg, '002', 0)
        }
    }
    onUpload3(event) {
        this.uploadedFiles3 = []
        for (const file of event.files) {
            this.uploadedFiles3.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles3[0].name
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '003', 'picseq': null })
            this.savePIC(pathimg, '003', 0)
        }
    }
    onUpload4(event) {
        this.uploadedFiles4 = []
        for (const file of event.files) {
            this.uploadedFiles4.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles4[0].name
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '004', 'picseq': null })
            this.savePIC(pathimg, '004', 0)
        }
    }
    onUpload5(event) {
        this.uploadedFiles5 = []
        for (const file of event.files) {
            this.uploadedFiles5.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles5[0].name
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '005', 'picseq': this.Seq })
            this.savePIC(pathimg, '005', this.Seq)
        }
    }
    onUpload6(event) {
        this.uploadedFiles6 = []
        for (const file of event.files) {
            this.uploadedFiles6.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles6[0].name
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '006', 'picseq': null })
            this.savePIC(pathimg, '006', 0)
        }
    }
    onUpload7(event) {
        this.uploadedFiles7 = []
        for (const file of event.files) {
            this.uploadedFiles7.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles7[0].name
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '007', 'picseq': null })
            this.savePIC(pathimg, '007', 0)
        }
    }
    onUpload8(event) {
        this.uploadedFiles8 = []
        for (const file of event.files) {
            this.uploadedFiles8.push(file);
            var pathimg = AppConsts.remoteServiceBaseUrl + '/PICEMP/' + this.uploadedFiles8[0].name
            this.pictureList.push({ 'picdir': pathimg, 'picdsc': '008', 'picseq': this.SeqL })
            this.savePIC(pathimg, '008', this.SeqL)
        }
    }
    savePIC(path, dsc, seq) {
        this.CreateEmployeePicturesInput = new CreateEmployeePicturesInput()
        this.CreateEmployeePicturesInput.picdoc = this.emp.psnidn
        this.CreateEmployeePicturesInput.picdir = path
        this.CreateEmployeePicturesInput.picdsc = dsc
        this.CreateEmployeePicturesInput.picseq = seq
        this.EmployeePicturesServiceProxy.createEmployeePictures(this.CreateEmployeePicturesInput).subscribe(() => {
            this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(this.emp.psnidn).subscribe((result) => {
                this.picture = result.items
            })
        })
    }
    Seq = 0

    // certificate_object = {
    //     "id": 0, "seq": 0, "cerno": "", "cerisdate": " ", "cerexdate": ""
    // };
    certificate = []
    idc = 0
    certyp = ""
    loopseq() {

        if (this.certificate.length <= 0) {
            this.Seq = 1
        } else {
            var value = this.certificate[0].seq;
            this.Seq = this.certificate[0].seq;
            for (let i = 0; i < this.certificate.length; i++) {
                if (this.certificate[i].seq >= value) {
                    value = this.certificate[i].seq;
                    this.Seq = (this.certificate[i].seq + 1 * 1)
                }
            }
        }
    }
    removcer(e) {

        for (let i = 0; i < this.certificate.length; i++) {
            var path = this.picture.filter(res => res.picdsc == '005' && res.picseq == this.certificate[i].seq)
            this.certificate[i].seq = (i + 1 * 1)
            if (path.length > 0) {
                for (let j = 0; j < path.length; j++) {
                    path[i].picseq = this.certificate[i].seq
                    this.EmployeePicturesServiceProxy.updateEmployeePictures(path[i]).subscribe(() => {

                    })

                }

            }


            if (this.certificate.length <= (i + 1 * 1)) {
                this.loopseq()
                this.saveJob(this.emp.psnidn)
                this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(this.emp.psnidn).subscribe((result) => {
                    this.picture = result.items
                })
            }
        }
        if (this.certificate.length <= 0) {
            this.Seq = 1
            this.saveJob(this.emp.psnidn)
            this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(this.emp.psnidn).subscribe((result) => {
                this.picture = result.items
            })
        }

    }
    addli3(psncerisdte, psncerexdte) {
        if (this.editcer_sts == 0) {
            this.idc = this.idc + 1
            this.certificate.push({
                "id": this.idc,
                "seq": this.Seq,
                "cerno": this.emp.psncertino,
                "certyp": this.certyp,
                "cerisdate": this.fnyyyymmdd(psncerisdte),
                "cerexdate": this.fnyyyymmdd(psncerexdte)
            })

            this.cer_clear()
            this.datagrid2.instance.clearSelection()
        } else {

            var editcer_data = this.certificate.findIndex(result => result.id == this.idselect)
            this.certificate[editcer_data].cerno = this.emp.psncertino
            this.certificate[editcer_data].seq = this.Seq
            this.certificate[editcer_data].certyp = this.certyp
            this.certificate[editcer_data].cerisdate = this.fnyyyymmdd(psncerisdte)
            this.certificate[editcer_data].cerexdate = this.fnyyyymmdd(psncerexdte)
            this.cer_clear()
            this.datagrid2.instance.clearSelection()
            this.editcer_sts = 0



        }
    }
    editcer_sts = 0;
    idselect = ""
    editcer(e) {

        console.log(e)

        if (e.selectedRowKeys.length != 0) {
            this.editcer_sts = 1
            var editcer_data = this.certificate.filter(result => result.id == e.selectedRowKeys[0].id)
            this.idselect = editcer_data[0].id
            this.emp.psncertino = editcer_data[0].cerno
            this.Seq = editcer_data[0].seq
            this.certyp = editcer_data[0].certyp
            $('#psncerisdte').val(this.fnddmmyyyy(editcer_data[0].cerisdate))
            $('#psncerexdte').val(this.fnddmmyyyy(editcer_data[0].cerexdate))
            // psncerexdte
            // this.emp.psncerisdte = this.fnddmmyyyy(editcer_data[0].cerisdate)
            // this.emp.psncerexdte = this.fnddmmyyyy(editcer_data[0].cerexdate)
        }

    }
    id_grid


    @ViewChild('id_grid') datagrid2: DxDataGridComponent
    cer_clear() {
        this.emp.psncertino = ""
        this.certyp = ""
        this.loopseq()
        $('#psncerisdte').val('')
        $('#psncerexdte').val('')

    }

    SeqL = 0
    language_object = {
        "id": 0, "seq": 0, "cerno": "", "cerisdate": " ", "cerexdate": ""
    };
    language = []
    idl = 0
    Lngno = ''
    Lngtyp = ''
    Lngscore = ''
    removLng(e) {

        for (let i = 0; i < this.language.length; i++) {
            var path = this.picture.filter(res => res.picdsc == '008' && res.picseq == this.language[i].seq)
            this.language[i].seq = (i + 1 * 1)
            if (path.length > 0) {
                for (let j = 0; j < path.length; j++) {
                    path[i].picseq = this.language[i].seq
                    this.EmployeePicturesServiceProxy.updateEmployeePictures(path[i]).subscribe(() => {

                    })

                }

            }


            if (this.language.length <= (i + 1 * 1)) {
                this.loopseqL()
                this.saveLng(this.emp.psnidn)
                this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(this.emp.psnidn).subscribe((result) => {
                    this.picture = result.items
                })
            }
        }
        if (this.language.length <= 0) {
            this.SeqL = 1
            this.saveLng(this.emp.psnidn)
            this.EmployeePicturesServiceProxy.getEmployeePicturesByEmpId(this.emp.psnidn).subscribe((result) => {
                this.picture = result.items
            })
        }

    }
    addLng(psncerisdte, psncerexdte) {
        if (this.editLng_sts == 0) {
            this.idl = this.idl + 1
            this.language.push({
                "id": this.idl,
                "seq": this.SeqL,
                "cerno": this.Lngno,
                "certyp": this.Lngtyp,
                "cerscore": this.Lngscore,
                "cerisdate": this.fnyyyymmdd(psncerisdte),
                "cerexdate": this.fnyyyymmdd(psncerexdte)
            })
            this.Lng_clear()
            this.id_gridL.instance.clearSelection()
        } else {

            var editcer_data = this.language.findIndex(result => result.id == this.idLngselect)
            this.language[editcer_data].cerno = this.Lngno
            this.language[editcer_data].seq = this.SeqL
            this.language[editcer_data].certyp = this.Lngtyp
            this.language[editcer_data].cerscore = this.Lngscore
            this.language[editcer_data].cerisdate = this.fnyyyymmdd(psncerisdte)
            this.language[editcer_data].cerexdate = this.fnyyyymmdd(psncerexdte)
            this.Lng_clear()
            this.id_gridL.instance.clearSelection()
            this.editLng_sts = 0


        }
    }
    editLng(e) {

        console.log(e)

        if (e.selectedRowKeys.length != 0) {
            this.editLng_sts = 1
            var editLng_data = this.language.filter(result => result.id == e.selectedRowKeys[0].id)
            this.idLngselect = editLng_data[0].id
            this.Lngno = editLng_data[0].cerno
            this.SeqL = editLng_data[0].seq
            this.Lngtyp = editLng_data[0].certyp
            this.Lngscore = editLng_data[0].cerscore
            $('#lngdte').val(this.fnddmmyyyy(editLng_data[0].cerisdate))
            $('#lngexdte').val(this.fnddmmyyyy(editLng_data[0].cerexdate))
            // psncerexdte
            // this.emp.psncerisdte = this.fnddmmyyyy(editcer_data[0].cerisdate)
            // this.emp.psncerexdte = this.fnddmmyyyy(editcer_data[0].cerexdate)
        }

    }
    editLng_sts = 0;
    idLngselect = ""
    @ViewChild('id_gridL') id_gridL: DxDataGridComponent
    loopseqL() {

        if (this.language.length <= 0) {
            this.SeqL = 1
        } else {
            var value = this.language[0].seq;
            this.SeqL = this.language[0].seq;
            for (let i = 0; i < this.language.length; i++) {
                if (this.language[i].seq >= value) {
                    value = this.language[i].seq;
                    this.SeqL = (this.language[i].seq + 1 * 1)
                }
            }
        }
    }
    Lng_clear() {
        this.Lngno = ""
        this.Lngscore = ""
        this.loopseqL()
        $('#lngdte').val('')
        $('#lngexdte').val('')

    }

    picture: EmployeePicturesListDto[] = []
    showpict(v) {
        var path = this.picture.filter(res => res.picdsc == v && res.picseq == this.Seq)
        if (path.length > 0) {
            var pt = path.length - 1

            window.open(path[pt].picdir, "_blank")
        } else {
            alert('ขออภัย, ไม่พบรูปภาพ')
        }
    }
    showpictDr(v) {
        var path = this.picture.filter(res => res.picdsc == v)
        if (path.length > 0) {
            var pt = path.length - 1

            window.open(path[pt].picdir, "_blank")
        } else {
            alert('ขออภัย, ไม่พบรูปภาพ')
        }
    }
    @ViewChild('modal5') modal5: ModalDirective;
    @ViewChild("gEmployeea") gEmployeea: DxDataGridComponent
    @ViewChild('modalREs') modalREs: ModalDirective;
    empM: MSMPSN00ListDto[] = []

    show5() {

        this.modal5.show();
    }
    closeemp() {
        this.gEmployeea.instance.clearSelection()
        this.modal5.hide();
    }
    get_datan(eid) {

        this.EmployeeResignsObject.resaut = eid
        var empname = this.empM.filter(res => res.psnidn == this.EmployeeResignsObject.resaut)
        setTimeout(() => {
            empname.length > 0 ? $('#autname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#autname').val('')
        }, 250);
        this.closeemp()

    }
    pipe = new DatePipe('en-US');
    EmployeeResignsObject: CreateEmployeeResignsInput = new CreateEmployeeResignsInput()
    PostResign(resefd) {

        this.KPShareServiceProxy.getDocumentNumber('RS', this.pipe.transform(Date(), 'yyyyMMdd'), 'THI').subscribe((res) => {
            if ($('#resres').val() != null) {
                this.EmployeeResignsObject.resres = $('#resres').val().toString()
            } else {
                this.EmployeeResignsObject.resres = null
            }
            this.EmployeeResignsObject.resdoc = res[0].documentNumber
            this.EmployeeResignsObject.restyp = 'A'
            this.EmployeeResignsObject.ressts = 'A'
            this.EmployeeResignsObject.residn = this.emp.psnidn
            this.EmployeeResignsObject.resefd = this.fnyyyymmdd(resefd)
            this.EmployeeResignsObject.resdte = this.pipe.transform(Date(), 'yyyyMMdd')
            this.EmployeeResignsObject.resstd = this.pipe.transform(Date(), 'yyyyMMdd')
            this.EmployeeResignsServiceProxy.createEmployeeResigns(this.EmployeeResignsObject).subscribe(() => {
                this.modalREs.hide()

                //this.saving = true;
                // if ($('#titln').val() != null) {
                //     this.emp.psntth = $('#titln').val().toString()
                // }
                // if ($('#psnteg').val() != null) {
                //     this.emp.psnteg = $('#psnteg').val().toString()
                // }
                // if ($('#psnpos').val() != null) {
                //     this.emp.psnpos = $('#psnpos').val().toString()
                // }
                // if ($('#psnpospf').val() != null) {
                //     this.emp.psnpospf = $('#psnpospf').val().toString()
                // }
                // if ($('#psnpoS02').val() != null) {
                //     this.emp.psnpoS02 = $('#psnpoS02').val().toString()
                // }
                // if ($('#psnpoS03').val() != null) {
                //     this.emp.psnpoS03 = $('#psnpoS03').val().toString()
                // }
                // if ($('#psnplv').val() != null) {
                //     this.emp.psnplv = $('#psnplv').val().toString()
                // }
                // if ($('#psneml').val() != null) {
                //     this.emp.psneml = $('#psneml').val().toString()
                // }
                // if ($('#psncom').val() != null) {
                //     this.emp.psncom = $('#psncom').val().toString()
                // }
                // if ($('#psnbrn').val() != null) {
                //     this.emp.psnbrn = $('#psnbrn').val().toString()
                // }
                // if ($('#psndev').val() != null) {
                //     this.emp.psndev = $('#psndev').val().toString()
                // }
                // if ($('#psndep').val() != null) {
                //     this.emp.psndep = $('#psndep').val().toString()
                // }
                // if ($('#psnacc').val() != null) {
                //     this.emp.psnacc = $('#psnacc').val().toString()
                // }
                // if ($('#psnins').val() != null) {
                //     this.emp.psnins = parseFloat($('#psnins').val().toString())
                // }

                // this.emp.psnbrd = this.fnyyyymmdd(psnbrd)
                // this.emp.psnidt = this.fnyyyymmdd(psnidt)
                // this.emp.psniep = this.fnyyyymmdd(psniep)
                // this.emp.psncerisdte = this.fnyyyymmdd(psncerisdte)
                // this.emp.psncerexdte = this.fnyyyymmdd(psncerexdte)
                // this.emp.psninw = this.fnyyyymmdd(dat)
                // this.emp.psnetd = this.fnyyyymmdd(psnetd)
                // this.emp.psnstd = this.fnyyyymmdd(dtests)
                // this.emp.psnfbr = this.fnyyyymmdd(psnfbr)
                // this.emp.psnmbr = this.fnyyyymmdd(psnmbr)
                // this.emp.psnmabr = this.fnyyyymmdd(psnmabr)

                // this.emp.psndrexr = this.fnyyyymmdd(psndrexr)
                // this.emp.psndrexR2 = this.fnyyyymmdd(psndrexR2)
                // this.emp.psnaaf = this.fnyyyymmdd(psnaaf)
                // this.emp.psnatt = this.fnyyyymmdd(psnatt)
                // this.emp.psnvdte = this.fnyyyymmdd(psnvdte)
                // this.emp.psnnsl = this.fnreplace(this.emp.psnnsl)

                // this.MSMPSN00ServiceProxy.updateMSMPSN00(this.emp).subscribe(() => {
                //     this.saving = false;
                //     this.notify.info(this.l('SavedAndPostResignSuccessfully'));
                //     this.close();
                // });
                // this.saveJob(this.emp.psnidn)
                // this.saveCars(this.emp.psnidn)
                this.notify.info(this.l('SavedAndPostResignSuccessfully'));
                this.close();
            })
        })

    }
    MemberOfFamilyListDto: MemberOfFamilyListDto[] = []
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    fnsumChild() {
        this.MemberOfFamilysServiceProxy.getMemberOfFamilyByEmpId(this.DOC_REF).subscribe((result) => {
            this.MemberOfFamilyListDto = result.items
            var psnmcd = 0
            var psnfcd = 0
            var psnscd = 0
            var psnycd = 0
            for (let index = 0; index < this.MemberOfFamilyListDto.length; index++) {
                this.MemberOfFamilyListDto[index].chdsex == 'M' ? psnmcd = (psnmcd + 1 * 1) : this.MemberOfFamilyListDto[index].chdsex == 'F' ? psnfcd = (psnfcd + 1 * 1) : ''
                this.MemberOfFamilyListDto[index].chdedu == 'Y' ? psnscd = (psnscd + 1 * 1) : this.MemberOfFamilyListDto[index].chdedu == 'N' ? psnycd = (psnycd + 1 * 1) : ''
                if (this.MemberOfFamilyListDto.length <= (index + 1 * 1)) {
                    this.emp.psnmcd = psnmcd
                    this.emp.psnfcd = psnfcd
                    this.emp.psnscd = psnscd
                    this.emp.psnycd = psnycd
                }
            }
            if (this.MemberOfFamilyListDto.length <= 0) {
                this.emp.psnmcd = 0
                this.emp.psnfcd = 0
                this.emp.psnscd = 0
                this.emp.psnycd = 0
            }
        })
    }
    chklink;
    fnchklink(v) {
        this.chklink = v
    }

    getleaveinformation(){
        this.KPShareServiceProxy.getLeave(this.emp.psnidn).subscribe((result) => {
            this.GetLeave = result
            for (let index = 0; index < this.GetLeave.length; index++) {
                
                if (this.GetLeave[index].psncod == 'V' || this.GetLeave[index].psncod == 'PL' || this.GetLeave[index].psncod == 'TL' || this.GetLeave[index].psncod == 'OT' || this.GetLeave[index].psncod == 'ML' || this.GetLeave[index].psncod == 'SL') {
                    var dat = this.GetLeave[index].psnbal.split(',')
                    for (let p = 0; p < dat.length; p++) {
                        var idx = '#' + this.GetLeave[index].psncod + p
                        // console.log(idx)
                        // this.LeavData.push({'psncod':this.GetLeave[index].psncod,'psnbal':dat,'psnmax':this.GetLeave[index].psnmax})
                        // console.log(this.LeavData)



                        $(idx).val(dat[p])
                        if (dat.length <= (p + 1 * 1)) {
                            var idx2 = '#' + this.GetLeave[index].psncod + (p + 1 * 1)
                            $(idx2).val(this.GetLeave[index].psnmax)
                        }
                    }
                }
            }
        })
    }

}