import { Component, Injectable, OnInit, Injector } from '@angular/core';
import { DbmtabServiceProxy, DbmtabListDto, MSMPSN00ListDto ,MSMPSN00ServiceProxy} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
@Injectable()
export class data_Empmas extends AppComponentBase implements OnInit {
  timeou = 0;
  constructor(
    injector: Injector,
    private _dbmtabService: DbmtabServiceProxy,
    private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,

    
  ) {
    super(injector);
  }
  ngOnInit(): void {

  }
  empmasterl = 0
  dbmtab: DbmtabListDto[] = [];
  dbmtabcontry: DbmtabListDto[] = [];
  dbmtabprovince: DbmtabListDto[] = [];
  dbmtabemti: DbmtabListDto[] = [];
  dbmtabenen: DbmtabListDto[] = [];
  dbmtabfna: DbmtabListDto[] = [];
  dbmtabsna: DbmtabListDto[] = [];
  dbmtabamo: DbmtabListDto[] = [];
  dbmtabpty: DbmtabListDto[] = [];
  dbmtabsmr: DbmtabListDto[] = [];
  dbmtabsgd: DbmtabListDto[] = [];
  dbmtabppt: DbmtabListDto[] = [];
  dbmtabsor: DbmtabListDto[] = [];
  PROVINCE: DbmtabListDto[];
  Probation = [{ 'val': 30 }, { 'val': 60 }, { 'val': 90 }]
  STATUS: DbmtabListDto[] = [];
  HOSPITALFSW: DbmtabListDto[] = []
  CARTYP: DbmtabListDto[];
  CARBRD: DbmtabListDto[];
  CARMDL: DbmtabListDto[];
  CARCOL: DbmtabListDto[];
  INSURCOM: DbmtabListDto[];
  INSURTYPE: DbmtabListDto[];
  DEPARTMENT: DbmtabListDto[];
  BNKBRN: DbmtabListDto[];
  BNKCOD: DbmtabListDto[];
  SHIFTCODE: DbmtabListDto[];
  PROVIDENTCOM: DbmtabListDto[];
  POVIDENTTYPE: DbmtabListDto[];
  EMPTYPE: DbmtabListDto[];
  EMPSUBTYPE: DbmtabListDto[];
  PAYSALARYTYPE: DbmtabListDto[];
  MAJOR: DbmtabListDto[];
  EDUCATION: DbmtabListDto[];
  CETIFICATED: DbmtabListDto[];
  REASON: DbmtabListDto[];
  EMPADDRTYPE:DbmtabListDto[]=[]
  BENTYPE:DbmtabListDto[]=[]
  BENCAUSE:DbmtabListDto[]=[]
  NOTICEREASON:DbmtabListDto[]=[]
  WARNINGISSUE:DbmtabListDto[]=[]
  NOTICERESULT:DbmtabListDto[]=[]
  PROBATIONCODE:DbmtabListDto[]=[]
  Empmaster() {
    this.empmasterl = 0

    this._dbmtabService.getAddressPostCode('PROVINCE', '', '', '', '').subscribe((result) => {
      this.PROVINCE = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
      this.dbmtabemti = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('ENGTITLE', '').subscribe((result) => {
      this.dbmtabenen = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
      this.dbmtab = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('COUNTRY', '').subscribe((result) => {
      this.dbmtabcontry = result.items;
    });

    this._dbmtabService.getDbmtabFromTABTB1('NATIONALITY', '').subscribe((result) => {
      this.dbmtabfna = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('CITIZENT', '').subscribe((result) => {
      this.dbmtabsna = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('REGION', '').subscribe((result) => {
      this.dbmtabamo = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('CARDTYPE', '').subscribe((result) => {
      this.dbmtabpty = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('MARRIEDSTATUS', '').subscribe((result) => {
      this.dbmtabsmr = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('SALARYGRADE', '').subscribe((result) => {
      this.dbmtabsgd = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('PAYROLLTYPE', '').subscribe((result) => {
      this.dbmtabppt = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('MILITARY', '').subscribe((result) => {
      this.dbmtabsor = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('STATUS', '').subscribe((result) => {
      this.STATUS = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('HOSPITALFSW', '').subscribe((result) => {
      this.HOSPITALFSW = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('CARTYP', '').subscribe((result) => {
      this.CARTYP = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('CARBRD', '').subscribe((result) => {
      this.CARBRD = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('CARMDL', '').subscribe((result) => {
      this.CARMDL = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('CARCOL', '').subscribe((result) => {
      this.CARCOL = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('INSURCOM', '').subscribe((result) => {
      this.INSURCOM = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('INSURTYPE', '').subscribe((result) => {
      this.INSURTYPE = result.items;
    });
    this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
      this.DEPARTMENT = result.items;
    });
    this._dbmtabService.getDbmtabFromTABFG1('BNKBRN', '').subscribe((result) => {
      this.BNKBRN = result.items;
    });

    this._dbmtabService.getDbmtabFromTABFG1('BNKCOD', '').subscribe((result) => {
      this.BNKCOD = result.items;
    });
    this._dbmtabService.getDbmtabFromTABFG1('SHIFTCODE', '').subscribe((result) => {
      this.SHIFTCODE = result.items

    })

    this._dbmtabService.getDbmtabFromTABFG1('PROVIDENTCOM', '').subscribe((result) => {
      this.PROVIDENTCOM = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('POVIDENTTYPE', '').subscribe((result) => {
      this.POVIDENTTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('EMPTYPE', '').subscribe((result) => {
      this.EMPTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('EMPSUBTYPE', '').subscribe((result) => {
      this.EMPSUBTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('PAYSALARYTYPE', '').subscribe((result) => {
      this.PAYSALARYTYPE = result.items

    })

    this._dbmtabService.getDbmtabFromTABFG1('MAJOR', '').subscribe((result) => {
      this.MAJOR = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('EDUCATION', '').subscribe((result) => {
      this.EDUCATION = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('CETIFICATED', '').subscribe((result) => {
      this.CETIFICATED = result.items

    })

    this._dbmtabService.getDbmtabFromTABFG1('GOODREASON', '').subscribe((result) => {
      this.REASON = result.items

    })

    this._dbmtabService.getDbmtabFromTABFG1('LOANREASON', '').subscribe((result) => {
      this.LOANREASON = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('LOANSUBREASON', '').subscribe((result) => {
      this.LOANSUBREASON = result.items

    })

    this._dbmtabService.getDbmtabFromTABFG1('PAYDAY', '').subscribe((result) => {
      this.PAYDAY = result.items

    })

    this._dbmtabService.getDbmtabFromTABFG1('PAYTYPE', '').subscribe((result) => {
      this.PAYTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('RECTYPE', '').subscribe((result) => {
      this.RECTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('INTEREST', '').subscribe((result) => {
      this.INTEREST = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('INTERESTTYPE', '').subscribe((result) => {
      this.INTERESTTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('EMPADDRTYPE', '').subscribe((result) => {
      this.EMPADDRTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('BENTYPE', '').subscribe((result) => {
      this.BENTYPE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('BENCAUSE', '').subscribe((result) => {
      this.BENCAUSE = result.items

    })

    this._dbmtabService.getDbmtabFromTABFG1('NOTICEREASON', '').subscribe((result) => {
      this.NOTICEREASON = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('WARNINGISSUE', '').subscribe((result) => {
      this.WARNINGISSUE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('NOTICERESULT', '').subscribe((result) => {
      this.NOTICERESULT = result.items

    })
     this._dbmtabService.getDbmtabFromTABFG1('POSITIONLEVEL', '').subscribe((result) => {
      this.POSITIONLEVEL = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('PROMOTIONREASON', '').subscribe((result) => {
      this.PROMOTIONREASON = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('PROBATIONCODE', '').subscribe((result) => {
      this.PROBATIONCODE = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('BLOODGROUP', '').subscribe((result) => {
      this.BLOODGROUP = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('AccPayrollGroup', '').subscribe((result) => {
      this.AccPayrollGroup = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('DIVISION', '').subscribe((result) => {
      this.DIVISION = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('CERTIFICATETYP', '').subscribe((result) => {
      this.CERTIFICATETYP = result.items

    })
    this._dbmtabService.getDbmtabFromTABFG1('LANGUAGETYP', '').subscribe((result) => {
      this.LANGUAGETYP = result.items

    })
    

    this.empmasterl = 1
  }
  CERTIFICATETYP:DbmtabListDto[];
  LANGUAGETYP:DbmtabListDto[];
  
  DIVISION:DbmtabListDto[];
  AccPayrollGroup:DbmtabListDto[];
  BLOODGROUP:DbmtabListDto[];
  PROMOTIONREASON:DbmtabListDto[];
  POSITIONLEVEL:DbmtabListDto[];
  LOANREASON: DbmtabListDto[];
  LOANSUBREASON: DbmtabListDto[];
  PAYDAY: DbmtabListDto[];
  RECTYPE:DbmtabListDto[];
  PAYTYPE:DbmtabListDto[];
  INTEREST:DbmtabListDto[];
  INTERESTTYPE:DbmtabListDto[];
}