import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TRBUD00ListDto, TRBUD00AppserviceServiceProxy, RecruitmentServiceProxy, RecruitmentListDto, DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'recruitment',
  templateUrl: './recruitment.component.html'
})
export class RecruitmentComponent extends AppComponentBase implements OnInit {

  constructor(
    private RecruitmentServiceProxy: RecruitmentServiceProxy,
    private _dbmtabService:DbmtabServiceProxy,
    injector: Injector,
  ) {
    super(injector);


  }

  ngOnInit() {
    $('#DIV2').val('ALL')
    $('#DEP2').val('ALL')
    this.getrecruit('', 'ALL', '', 'ALL')
    this.getmaster()
  }
  RecruitmentListDto: RecruitmentListDto[] = []
  POSITION:DbmtabListDto[]=[]
  DEPARTMENT:DbmtabListDto[]=[]
  DIVISION:DbmtabListDto[]=[]
  getrecruit(frm, to, en, toen) {
    this.RecruitmentServiceProxy.getRecruitmentFromTo(frm, to, en, toen).subscribe((result) => {
      this.RecruitmentListDto = result.items
    })
  }
  getmaster(){
    this._dbmtabService.getDbmtabFromTABFG1('POSITION', '').subscribe((result) => {
      this.POSITION = result.items

  })
  this._dbmtabService.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
      this.DEPARTMENT = result.items;
    });
    this._dbmtabService.getDbmtabFromTABFG1('DIVISION', '').subscribe((result) => {
      this.DIVISION = result.items;
      setTimeout(() => {
        $('.m_select2_1a').select2({
            placeholder: "Please Select.."
        });
    }, 250);
    });
  }
  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
