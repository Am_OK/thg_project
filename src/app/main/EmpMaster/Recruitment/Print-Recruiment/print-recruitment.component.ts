import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto, DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './print-recruitment.component.html',
    animations: [appModuleAnimation()]
})
export class PrintrecruitmentComponent extends AppComponentBase implements OnInit {
    emp: MSMPSN00ListDto[] = [];

    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private _dbmtabService: DbmtabServiceProxy

    ) {
        super(injector);

    }
    DEPARTMENT: DbmtabListDto[] = []
    DIVISION: DbmtabListDto[] = []
    ngOnInit(): void {
        // this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
        //     this.emp = result.items;
        // });
        this.getmaster()
        this.typ1s = false
    }
    getmaster() {

        this._dbmtabService.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('DIVISION', '').subscribe((result) => {
            this.DIVISION = result.items;
            setTimeout(() => {
                $('.m_select2_1a').select2({
                    placeholder: "Please Select.."
                });
            }, 250);
        });
    }
    typ1s: boolean
    // http://27.254.140.187:5433/WebForms/EmployeeMaster/WebMSF9310.aspx?strDIV=51&endDIV=&strDEP=511&endDEP=&sDate=20171201&Excel=False
    prints(DIV1, DIV2, DEP1, DEP2, dte, typ1) {
        // console.log(this.typ1s)
        var typ = ""
        if (DIV2 != "" && DEP2 != "" && dte != "") {
            if (DIV1 == "NO") { DIV1 = '' }
            // if (DIV2 == "NO") { DIV2 = '' }
            if (DEP1 == "NO") { DEP1 = '' }
            // if (DEP2 == "NO") { DEP2 = '' }
            if (typ1 == null || typ1 == undefined || typ1 == '') { typ1 = false }
            
            if (this.typ1s == true) { typ = "&Excel=True" } else { typ = "&Excel=False" }
            window.open("http://27.254.140.187:5433/WebForms/EmployeeMaster/WebMSF9310.aspx?strDIV=" + DIV1 + "&endDIV=" + DIV2 + "&strDEP=" + DEP1 + "&endDEP=" + DEP2 + "&sDate=" + this.fnyyyymmdd(dte) + typ)
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }

}
