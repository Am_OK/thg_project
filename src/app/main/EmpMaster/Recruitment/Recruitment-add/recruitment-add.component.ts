import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, RecruitmentServiceProxy, RecruitmentListDto, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { data_Empmas } from '../../data_center';
import { RecruitmentComponent } from '../recruitment.component';

@Component({
    selector: 'recruitmentadd',
    templateUrl: './recruitment-add.component.html',
})
export class recruitmentaddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private RecruitmentServiceProxy: RecruitmentServiceProxy,
        private RecruitmentComponent:RecruitmentComponent,
        private KPShareServiceProxy: KPShareServiceProxy,
    ) {
        super(injector);
    }
    pipe = new DatePipe('en-US');
    Editing = 0
    dbmtab: DbmtabListDto[] = [];
    
    dbmtabemti: DbmtabListDto[] = [];
    MOVERESON: DbmtabListDto[] = [];
    POSITION:DbmtabListDto[]=[]
    DEPARTMENT:DbmtabListDto[]=[]
    DIVISION:DbmtabListDto[]=[]
    RecruitmentObject: RecruitmentListDto = new RecruitmentListDto()
    show(): void {
        this.getItemMaster()
        this.modal.show();
        this.active = true;
        this.RecruitmentObject = new RecruitmentListDto()
        this.RecruitmentObject.uitsts = 'A'
    }

    getItemMaster() {
        this.POSITION = this.RecruitmentComponent.POSITION
        this.DEPARTMENT = this.RecruitmentComponent.DEPARTMENT
        this.DIVISION = this.RecruitmentComponent.DIVISION
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
    }

    showedit(e) {
        this.getItemMaster()
        this.Editing = 1
        this.RecruitmentObject = new RecruitmentListDto()
        this.RecruitmentObject.id = e.data.id
        this.RecruitmentObject.uitdoc = e.data.uitdoc
        this.RecruitmentObject.uitdiv =e.data.uitdiv
        this.RecruitmentObject.uitdep=e.data.uitdep
        this.RecruitmentObject.uitpos=e.data.uitpos
        this.RecruitmentObject.uitrmk=e.data.uitrmk
        this.RecruitmentObject.uitwanm=e.data.uitwanm
        this.RecruitmentObject.uitwand=e.data.uitwand
        this.RecruitmentObject.uitsts = e.data.uitsts
        this.RecruitmentObject.uitdte = e.data.uitdte
        this.modal.show();
    }

    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    var uitdte = $('#uitdte').val() != null && $('#uitdte').val() != undefined ? uitdte = this.fnyyyymmdd($('#uitdte').val()) : uitdte = ''


                    if ($('#uitdiv').val() != null) {
                        this.RecruitmentObject.uitdiv = $('#uitdiv').val().toString()
                    }
                    if ($('#uitdep').val() != null) {
                        this.RecruitmentObject.uitdep = $('#uitdep').val().toString()
                    }
                    if ($('#uitpos').val() != null) {
                        this.RecruitmentObject.uitpos = $('#uitpos').val().toString()
                    }
                    this.RecruitmentObject.uitdte = uitdte
                    // this.RecruitmentObject.mandte = mandte

                    if (this.Editing == 1) {
                        this.RecruitmentServiceProxy.updateRecruitment(this.RecruitmentObject).subscribe(() => {
                            this.close()
                            this.modalSave.emit()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.RecruitmentObject.uitdoc != '' && this.RecruitmentObject.uitdoc != null && this.RecruitmentObject.uitdoc != undefined) {
                            this.RecruitmentServiceProxy.createRecruitment(this.RecruitmentObject).subscribe(() => {
                                this.RecruitmentObject.uitdte = this.fnddmmyyyy($('#uitdte').val())
                                this.close()
                                this.modalSave.emit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.KPShareServiceProxy.getDocumentNumber('RCM', this.pipe.transform(Date(), 'yyyyMMdd'), 'THI').subscribe((result) => {
                                this.RecruitmentObject.uitdoc = result[0].documentNumber
                                this.RecruitmentServiceProxy.createRecruitment(this.RecruitmentObject).subscribe(() => {
                                    this.close()
                                    this.modalSave.emit()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }
                    }

                }
            })
    }

    close(): void {
        this.modal.hide();
        this.active = false;
        this.Editing = 0
    }
}

