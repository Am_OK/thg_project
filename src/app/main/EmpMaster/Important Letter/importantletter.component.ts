import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, ImportantLetterServiceProxy, ImportantLetterListDto, MSMPSN00ListDto, MSMPSN00ServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'importantletter',
    templateUrl: './importantletter.component.html',
})
export class importantletterComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private ImportantLetterServiceProxy: ImportantLetterServiceProxy,
        private MSMPSN00ServiceProxy:MSMPSN00ServiceProxy,
        private _dbmtabService:DbmtabServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        $('#to').val('ALL')
        this.getLetter('', 'ALL')
        this.getEmployees()
        this.getmaster()
    }
    ImportantLetterListDto: ImportantLetterListDto[] = []
    emp: MSMPSN00ListDto[] = []
    dbmtabemti: DbmtabListDto[] = []
    LETTERTYPE: DbmtabListDto[] = []
    LETTERREASON: DbmtabListDto[] = []
    getLetter(frm, to) {
        this.ImportantLetterServiceProxy.getImportantLetterFromToEid(frm, to).subscribe((result) => {
            this.ImportantLetterListDto = result.items
        })
    }
    getEmployees(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
          this.emp = result.items;
        });
      }
      getmaster() {
    
        this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
          this.dbmtabemti = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('LETTERTYPE', '').subscribe((result) => {
            this.LETTERTYPE = result.items;
          });
          this._dbmtabService.getDbmtabFromTABTB1('LETTERREASON', '').subscribe((result) => {
            this.LETTERREASON = result.items;
          });
        
        
      }
      getDisplayExpr(item) {
        // console.log(item)
        if (!item) {
          return "";
        }
        // console.log(item.psnfnm + " " + item.psnlnm)
        return item.psnfnm + " " + item.psnlnm
      }
}

