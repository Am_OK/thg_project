import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, MSMPSN00ListDto, ImportantLetterListDto, ImportantLetterServiceProxy, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { importantletterComponent } from '../importantletter.component';
import { DatePipe } from '@angular/common';
import { DxDataGridComponent } from 'devextreme-angular';


@Component({
    selector: 'importantletteradd',
    templateUrl: './importantletter-add.component.html',
})
export class importantletteraddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private importantletterComponent: importantletterComponent,
        private ImportantLetterServiceProxy:ImportantLetterServiceProxy,
        private KPShareServiceProxy:KPShareServiceProxy
    ) {
        super(injector);
    }
    ImportantLetterObject: ImportantLetterListDto = new ImportantLetterListDto()
    REASON: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    dbmtabemti: DbmtabListDto[] = []
    LETTERTYPE: DbmtabListDto[] = []
    LETTERREASON: DbmtabListDto[] = []
    getItemMaster() {

        this.emp = this.importantletterComponent.emp
        this.dbmtabemti = this.importantletterComponent.dbmtabemti
        this.LETTERTYPE = this.importantletterComponent.LETTERTYPE
        this.LETTERREASON = this.importantletterComponent.LETTERREASON
    }
    showedit(e) {
        this.getItemMaster()
        this.Editing = 1
        this.ImportantLetterObject = new ImportantLetterListDto()
        this.ImportantLetterObject.id = e.data.id
        this.ImportantLetterObject.letdoc = e.data.letdoc
        this.ImportantLetterObject.letdte = this.fnddmmyyyy(e.data.letdte)
        this.ImportantLetterObject.letefd = this.fnddmmyyyy(e.data.letefd)
        this.ImportantLetterObject.leteid = e.data.leteid
        this.ImportantLetterObject.letaut = e.data.letaut
        this.ImportantLetterObject.lettyp = e.data.lettyp
        this.ImportantLetterObject.letrea = e.data.letrea
        this.ImportantLetterObject.letsts = e.data.letsts
        this.ImportantLetterObject.letstd = this.fnddmmyyyy(e.data.letstd)
        this.ImportantLetterObject.letrM1 = e.data.letrM1
        this.ImportantLetterObject.letrM2 = e.data.letrM2
        this.ImportantLetterObject.letrM3 = e.data.letrM3

        var empname = this.emp.filter(res => res.psnidn == this.ImportantLetterObject.leteid)
        var autname = this.emp.filter(res => res.psnidn == this.ImportantLetterObject.letaut)
        setTimeout(() => {
            empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            autname.length > 0 ? $('#autname').val(autname[0].psnfnm + ' ' + autname[0].psnlnm) : $('#autname').val('')
        }, 250);
        this.modal.show();

    }
    show(): void {
        this.getItemMaster()
        this.modal.show();
        this.ImportantLetterObject = new ImportantLetterListDto()
    }
    close(): void {
        this.modal.hide();
        this.Editing = 0
        $('#empname').val('')
        $('#autname').val('')
    }
    Editing = 0
    pipe = new DatePipe('en-US');
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var letdte = $('#letdte').val() != null && $('#letdte').val() != undefined ? letdte = this.fnyyyymmdd($('#letdte').val()) : letdte = ''
                    var letefd = $('#letefd').val() != null && $('#letefd').val() != undefined ? letefd = this.fnyyyymmdd($('#letefd').val()) : letefd = ''
                    var letstd = $('#letstd').val() != null && $('#letstd').val() != undefined ? letstd = this.fnyyyymmdd($('#letstd').val()) : letstd = ''

                    this.ImportantLetterObject.letdte = letdte
                    this.ImportantLetterObject.letefd = letefd
                    this.ImportantLetterObject.letstd = letstd

                    if (this.Editing == 1) {
                        this.ImportantLetterServiceProxy.updateImportantLetter(this.ImportantLetterObject).subscribe(() => {
                            this.close()
                            this.modalSave.emit()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.ImportantLetterObject.letdoc != '' && this.ImportantLetterObject.letdoc != null && this.ImportantLetterObject.letdoc != undefined) {
                            this.ImportantLetterServiceProxy.createImportantLetter(this.ImportantLetterObject).subscribe(() => {
                                this.close()
                                this.modalSave.emit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.KPShareServiceProxy.getDocumentNumber('CR', this.ImportantLetterObject.letdte, 'THI').subscribe((result) => {
                                this.ImportantLetterObject.letdoc = result[0].documentNumber
                                this.ImportantLetterServiceProxy.createImportantLetter(this.ImportantLetterObject).subscribe(() => {
                                    this.close()
                                    this.modalSave.emit()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }

                    }

                }
            })
    }
    removed() {
        this.message.confirm(
            this.l('AreYouSureToDeleteTheData'),
            isConfirmed => {
                if (isConfirmed) {
        this.ImportantLetterServiceProxy.deleteImportantLetter(this.ImportantLetterObject.id).subscribe(() => {
            this.modalSave.emit()
            this.close()
        })
    }})
    }
    @ViewChild('modal5') modal5: ModalDirective;
    @ViewChild("gEmployeea") gEmployeea: DxDataGridComponent
    show5(v) {
        this.OpMdl = v
        this.modal5.show();
    }
    closeemp() {
        this.gEmployeea.instance.clearSelection()
        this.modal5.hide();
    }
    OpMdl
    get_datan(eid) {
        if (this.OpMdl == 'Emp') {
            this.ImportantLetterObject.leteid = eid
            var empname = this.emp.filter(res => res.psnidn == this.ImportantLetterObject.leteid)
            setTimeout(() => {
                empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            }, 250);
            this.closeemp()
        }
        if (this.OpMdl == 'Aut') {
            this.ImportantLetterObject.letaut = eid
            var empname = this.emp.filter(res => res.psnidn == this.ImportantLetterObject.letaut)
            setTimeout(() => {
                empname.length > 0 ? $('#autname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#autname').val('')
            }, 250);
            this.closeemp()
        }
    }

}

