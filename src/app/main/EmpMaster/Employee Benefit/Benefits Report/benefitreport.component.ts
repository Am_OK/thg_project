import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './benefitreport.component.html',
    animations: [appModuleAnimation()]
})
export class BenefitsReportComponent extends AppComponentBase implements OnInit {
    emp: MSMPSN00ListDto[] = [];

    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });

    }
    kuacc(dt) {
        if (dt.indexOf("+") >= 0) {
            var accode = dt.replace("+", "")
            $('#hsreids1').val(accode).toString()
        }
    }
    chgindex(event) {
        var id = "#" + event
        var hsreid1 = $('#hsreids1').val().toString()
        setTimeout(() => {
            if (hsreid1 != null && hsreid1 != undefined) {
                var empc = this.emp.filter(res => res.psnidn == hsreid1)
                if (empc != null && empc != undefined) {
                    $(id).focus()
                    // 
                } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
            } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
        }, 500);


    }
    prints(year, dtefm1, dteto, hsreid1, hsreid2) {
        if (dtefm1 != "" && dteto != "" && hsreid1 != "") {
            if (hsreid1 == "" || hsreid1 == null || hsreid1 == undefined) { hsreid1 = '' }
            window.open("http://27.254.140.187:5433/Training/RegistrationByID?YEAR=" + (year - 543 * 1) + "&FmDate=" + this.fnyyyymmdd(dtefm1) + "&ToDate=" + this.fnyyyymmdd(dteto) + "&FROMID=" + hsreid1 + "&TOID=" + hsreid2 + "&ReportType=PDF")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
    download(year, dtefm1, dteto, hsreid1, hsreid2) {
        if (dtefm1 != "" && dteto != "" && hsreid1 != "") {
            if (hsreid1 == "" || hsreid1 == null || hsreid1 == undefined) { hsreid1 = '' }
            window.open("http://27.254.140.187:5433/Training/RegistrationByID?YEAR=" + (year - 543 * 1) + "&FmDate=" + this.fnyyyymmdd(dtefm1) + "&ToDate=" + this.fnyyyymmdd(dteto) + "&FROMID=" + hsreid1 + "&TOID=" + hsreid2 + "&ReportType=EXCEL")
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
}
