import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TRBUD00ListDto, TRBUD00AppserviceServiceProxy, KPShareServiceProxy, EmployeeResignsServiceProxy, EmployeeResignsListDto, MSMPSN00ServiceProxy, DbmtabServiceProxy, MSMPSN00ListDto, DbmtabListDto } from '@shared/service-proxies/service-proxies';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'employeeresign',
  templateUrl: './employeeresign.component.html'
})
export class EmployeeresignComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,
    private EmployeeResignsServiceProxy: EmployeeResignsServiceProxy,
    private KPShareServiceProxy: KPShareServiceProxy,
    private MSMPSN00ServiceProxy:MSMPSN00ServiceProxy,
    private _dbmtabService:DbmtabServiceProxy
  ) {
    super(injector);


  }

  ngOnInit() {
    $('#to').val('ALL')
    this.getResign('', 'ALL')
    this.getEmployees()
    this.getmaster()
  }
  EmployeeResignsListDto: EmployeeResignsListDto[] = []
  RESIGNREASON: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    dbmtabemti: DbmtabListDto[] = []
  getResign(frm, to) {
    this.EmployeeResignsServiceProxy.getEmployeeResignsFromToEid(frm, to).subscribe((result) => {
      this.EmployeeResignsListDto = result.items
    })
  }
  getEmployees(): void {
    this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
      this.emp = result.items;
    });
  }
  getmaster() {

    this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
      this.dbmtabemti = result.items;
    });
    this._dbmtabService.getDbmtabFromTABFG1('RESIGNREASON', '').subscribe((result) => {
      this.RESIGNREASON = result.items;

    })
    
  }
  getDisplayExpr(item) {
    // console.log(item)
    if (!item) {
      return "";
    }
    // console.log(item.psnfnm + " " + item.psnlnm)
    return item.psnfnm + " " + item.psnlnm
  }

  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
