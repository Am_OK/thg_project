import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeResignsListDto, MSMPSN00ListDto, KPShareServiceProxy, EmployeeResignsServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { EmployeeresignComponent } from '../employeeresign.component';
import { DatePipe } from '@angular/common';
import { DxDataGridComponent } from 'devextreme-angular';


@Component({
    selector: 'employeeresignadd',
    templateUrl: './employeeresign-add.component.html',
})
export class EmployeeresignaddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeresignComponent: EmployeeresignComponent,
        private KPShareServiceProxy: KPShareServiceProxy,
        private EmployeeResignsServiceProxy: EmployeeResignsServiceProxy
    ) {
        super(injector);
    }
    EmployeeResignsObject: EmployeeResignsListDto = new EmployeeResignsListDto()
    RESIGNREASON: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    dbmtabemti: DbmtabListDto[] = []
    getItemMaster() {
        this.RESIGNREASON = this.EmployeeresignComponent.RESIGNREASON
        this.emp = this.EmployeeresignComponent.emp
        this.dbmtabemti = this.EmployeeresignComponent.dbmtabemti
    }
    showedit(e) {
        this.getItemMaster()
        this.Editing = 1
        this.EmployeeResignsObject = new EmployeeResignsListDto()
        this.EmployeeResignsObject.id = e.data.id
        this.EmployeeResignsObject.resaut = e.data.resaut
        this.EmployeeResignsObject.resdte = this.fnddmmyyyy(e.data.resdte)
        this.EmployeeResignsObject.resefd = this.fnddmmyyyy(e.data.resefd)
        this.EmployeeResignsObject.resdoc = e.data.resdoc
        this.EmployeeResignsObject.residn = e.data.residn
        this.EmployeeResignsObject.resres = e.data.resres
        this.EmployeeResignsObject.resrmk = e.data.resrmk
        this.EmployeeResignsObject.ressts = e.data.ressts
        this.EmployeeResignsObject.restyp = e.data.restyp

        var empname = this.emp.filter(res => res.psnidn == this.EmployeeResignsObject.residn)
        var autname = this.emp.filter(res => res.psnidn == this.EmployeeResignsObject.resaut)
        setTimeout(() => {
            empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            autname.length > 0 ? $('#autname').val(autname[0].psnfnm + ' ' + autname[0].psnlnm) : $('#autname').val('')
        }, 250);
        this.modal.show();

    }
    show(): void {
        this.getItemMaster()
        this.modal.show();
        this.EmployeeResignsObject = new EmployeeResignsListDto()
    }
    close(): void {
        this.modal.hide();
        this.Editing = 0
        $('#empname').val('')
        $('#autname').val('')
    }
    Editing = 0
    pipe = new DatePipe('en-US');
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var resdte = $('#resdte').val() != null && $('#resdte').val() != undefined ? resdte = this.fnyyyymmdd($('#resdte').val()) : resdte = ''
                    var resefd = $('#resefd').val() != null && $('#resefd').val() != undefined ? resefd = this.fnyyyymmdd($('#resefd').val()) : resefd = ''

                    this.EmployeeResignsObject.resdte = resdte
                    this.EmployeeResignsObject.resefd = resefd

                    if (this.Editing == 1) {
                        this.EmployeeResignsServiceProxy.updateEmployeeResigns(this.EmployeeResignsObject).subscribe(() => {
                            this.close()
                            this.modalSave.emit()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.EmployeeResignsObject.resdoc != '' && this.EmployeeResignsObject.resdoc != null && this.EmployeeResignsObject.resdoc != undefined) {
                            this.EmployeeResignsServiceProxy.createEmployeeResigns(this.EmployeeResignsObject).subscribe(() => {
                                this.close()
                                this.modalSave.emit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.KPShareServiceProxy.getDocumentNumber('RS', this.EmployeeResignsObject.resdte, 'THI').subscribe((result) => {
                                this.EmployeeResignsObject.resdoc = result[0].documentNumber
                                this.EmployeeResignsServiceProxy.createEmployeeResigns(this.EmployeeResignsObject).subscribe(() => {
                                    this.close()
                                    this.modalSave.emit()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }

                    }

                }
            })
    }
    removed() {
        this.EmployeeResignsServiceProxy.deleteEmployeeResigns(this.EmployeeResignsObject.id).subscribe(() => {
            this.modalSave.emit()
        })
    }
    cancelResign() {
        this.message.confirm(
            this.l('AreYouSureToCancelResigns'),
            isConfirmed => {
                if (isConfirmed) {
                    this.KPShareServiceProxy.cancleResignDocument(this.EmployeeResignsObject.resdoc, this.EmployeeResignsObject.residn).subscribe(() => {
                        this.modalSave.emit()
                        this.notify.info(this.l('CancelSuccessfully'));
                        this.close()
                    })
                }
            })
    }
    @ViewChild('modal5') modal5: ModalDirective;
    @ViewChild("gEmployeea") gEmployeea: DxDataGridComponent
    show5(v) {
        this.OpMdl = v
        this.modal5.show();
    }
    closeemp() {
        this.gEmployeea.instance.clearSelection()
        this.modal5.hide();
    }
    OpMdl
    get_datan(eid) {
        if (this.OpMdl == 'Emp') {
            this.EmployeeResignsObject.residn = eid
            var empname = this.emp.filter(res => res.psnidn == this.EmployeeResignsObject.residn)
            setTimeout(() => {
                empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            }, 250);
            this.closeemp()
        }
        if (this.OpMdl == 'Aut') {
            this.EmployeeResignsObject.resaut = eid
            var empname = this.emp.filter(res => res.psnidn == this.EmployeeResignsObject.resaut)
            setTimeout(() => {
                empname.length > 0 ? $('#autname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#autname').val('')
            }, 250);
            this.closeemp()
        }
    }


}

