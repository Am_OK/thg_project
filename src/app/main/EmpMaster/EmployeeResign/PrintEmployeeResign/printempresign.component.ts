import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto, DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './printempresign.component.html',
    animations: [appModuleAnimation()]
})
export class PrintEmpResignComponent extends AppComponentBase implements OnInit {
    emp: MSMPSN00ListDto[] = [];
    DIVISION:DbmtabListDto[]=[]
    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private _dbmtabService:DbmtabServiceProxy

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this._dbmtabService.getDbmtabFromTABFG1('DIVISION', '').subscribe((result) => {
            this.DIVISION = result.items;
            setTimeout(() => {
              $('.m_select2_1a').select2({
                  placeholder: "Please Select.."
              });
          }, 250);
          });

    }
    typ1s: boolean
    // http://27.254.140.187:5433/WebForms/EmployeeMaster/WebResignationrate.aspx?YEA=2019&STRMON=01&ENDMON=12&STRDEV=21&ENDDEV=71&ReportType=False
    prints(year,mnt1,mnt2,DIV1,DIV2) {
        var typ = ""
        if (year != "" && mnt1 != "" && mnt2 != "") {
            if (DIV1 == "ZZZZ") { DIV1 = '' }
            if (DIV2 == "ZZZZ") { DIV1 = '' }
            if (this.typ1s == true) { typ = "&ReportType=True" } else { typ = "&ReportType=False" }
            window.open("http://27.254.140.187:5433/WebForms/EmployeeMaster/WebResignationrate.aspx?YEA=" + (year - 543 * 1) + "&STRMON=" + mnt1 + "&ENDMON=" + mnt2 + "&STRDEV=" + DIV1 + "&ENDDEV=" + DIV2 + typ)
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
    
}
