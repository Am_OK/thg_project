import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ListDto, MSMPSN00ServiceProxy, DbmtabListDto, DbmtabServiceProxy, EmployeeMovmentsListDto, EmployeeMovmentsServiceProxy } from '@shared/service-proxies/service-proxies';
import { data_Empmas } from '../../data_center';




@Component({
    selector: 'empmovementlist',
    templateUrl: './empmovement-list.component.html',
})
export class EmployeemovementlistComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    emp: MSMPSN00ListDto[] = [];
    dbmtabemti: DbmtabListDto[] = [];
    empmove: EmployeeMovmentsListDto[] = [];

    active: boolean;
    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private EmployeeMovments: EmployeeMovmentsServiceProxy,
        private _dbmtabService: DbmtabServiceProxy


    ) {
        super(injector);
    }
    doc;
    ngOnInit(): void {

        this.getMovement('','ALL')

    }
       
    chksho2;    
    show(id): void {
        this.modal.show();
        this.active = true;
        this.chksho2 = id
    }
    getMovement(frm,to){
        this.EmployeeMovments.getEmployeeMovmentsFromToEid(frm,to).subscribe((result) => {
            this.empmove = result.items;
        });
    }
   
    get_datan2(e) {
        console.log(e);
        var empmove = this.empmove.filter(res => res.movdoc == e)
        if (this.chksho2 == '1') {$('#docfm').val(empmove[0].movdoc),this.close()}
        if (this.chksho2 == '2') {$('#docto').val(empmove[0].movdoc),this.close()}

    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

}

