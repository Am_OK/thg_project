import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ListDto, MSMPSN00ServiceProxy, DbmtabListDto, DbmtabServiceProxy, EmployeeMovmentsListDto, EmployeeMovmentsServiceProxy, CreateDbmtabInput } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';




@Component({
    selector: 'setpositionbycer',
    templateUrl: './setpositionbycer.component.html',
})
export class SetpositionbycerComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;

    dbmtabposcer: DbmtabListDto[] = [];
    createpositoncer: CreateDbmtabInput = new CreateDbmtabInput();

    active: boolean;
    saving: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy


    ) {
        super(injector);
    }
    doc;
    ngOnInit(): void {
        this._dbmtabService.getDbmtabFromTABTB1('CertificatePosition','').subscribe((result) => {
            this.dbmtabposcer = result.items;
        });
    }
       
 
    show(): void {
        this.modal.show();
        this.active = true;
      
    }
    Add():void{
        this.modal2.show();
        this.active = true;
    }
    save() {
        this.message.confirm(
          this.l('AreYouSureToSaveTheData'),
          isConfirmed => {
            if (isConfirmed) {
              this.saving = true;
              this.createpositoncer.tabtB1 = 'CertificatePosition'
              this._dbmtabService.createDbmtab(this.createpositoncer)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                  this.notify.info(this.l('SavedSuccessfully'));
                  this.modalSave.emit();
                  this._dbmtabService.getDbmtabFromTABTB1('CertificatePosition', '').subscribe((result) => {
                    this.dbmtabposcer = result.items;
    
                  });
                  this.close2();
    
                });
            }
    
          })
      }
    get_datan2(e) {
        console.log(e);

        var poscer = this.dbmtabposcer.filter(res => res.id == e)
        $('#empposf').val(poscer[0].tabtB2)
        this.close()
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close2(): void {
        this.modal2.hide();
        this.active = false;
    }

}

