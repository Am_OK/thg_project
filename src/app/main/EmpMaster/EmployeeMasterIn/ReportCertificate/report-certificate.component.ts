import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';
import { data_Empmas } from '../../data_center';



@Component({
    templateUrl: './report-certificate.component.html',
    animations: [appModuleAnimation()]
})
export class ReportCertificateComponent extends AppComponentBase implements OnInit {
    dbmtab: DbmtabListDto[] = [];
    DEPARTMENT: DbmtabListDto[] = [];
    DIVISION: DbmtabListDto[] = [];
    STATUS: DbmtabListDto[] = [];
    emp:MSMPSN00ListDto[] = [];

    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private _dbmtabService: DbmtabServiceProxy,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;
          });
          this._dbmtabService.getDbmtabFromTABFG1('DIVISION', '').subscribe((result) => {
            this.DIVISION = result.items
      
          })
          this._dbmtabService.getDbmtabFromTABTB1('STATUS', '').subscribe((result) => {
            this.STATUS = result.items;
          });
          this._dbmtabService.getDbmtabFromTABTB1('CertificatePosition', '').subscribe((result) => {
            this.dbmtab = result.items;
          });
        //   this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
        //     this.emp = result.items;
        //   });
        this.dbmtab = this.data_Empmas.dbmtab
        this.DEPARTMENT = this.data_Empmas.DEPARTMENT
        this.DIVISION = this.data_Empmas.DIVISION
        this.STATUS  = this.data_Empmas.STATUS
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);

    }
    // chex(){
    //     $('#chkex').val('True')
    // }
    typ1:boolean
    prints(eidfm,eidto,empposf,emppost,depf,dept,divsfm,divsto,datf,datt,typrep,chkex) {
        var typ1s = 'PDF'
        this.typ1 == true ? typ1s = 'Excel' : typ1s = 'PDF'
        if (eidto != "" && emppost != "" && dept != "") {
            if (eidto == "" || eidto == null || eidto == undefined || emppost == "" || emppost == null || emppost == undefined || dept == "" || dept == null || dept == undefined || divsto == "" || divsto == null || divsto == undefined ) { eidto = '',emppost = '',dept='',divsto='' }
            http://27.254.140.187:5433/EmployeeMaster/ListEmplicenseexpired?strDEV=&endDEV=ZZZZ&strDEP=&endDEP=ZZZZ&strPOS=&endPOS=ZZZZ&strID=&endID=ZZZZ&strDTE=20180601&endDTE=20190618&Type=1&ReportType=         
            eidto.toUpperCase() == 'ALL' ? eidto = 'ZZZZ' : ''
            window.open("http://27.254.140.187:5433/EmployeeMaster/ListEmplicenseexpired?strDEV=" + divsfm.replace(/ /g, "") + "&endDEV=" + divsto.replace(/ /g, "") + "&strDEP="+ depf.replace(/ /g, "") + "&endDEP=" + dept.replace(/ /g, "") 
            + "&strPOS="+ empposf.replace(/ /g, "") + "&endPOS="+ emppost.replace(/ /g, "") + "&strID=" + eidfm.replace(/ /g, "") +"&endID=" + eidto.replace(/ /g, "") + "&strDTE=" +this.fnyyyymmdd(datf) + "&endDTE=" + this.fnyyyymmdd(datt) + "&Type=" + typrep + "&ReportType=" + typ1s )
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
}
