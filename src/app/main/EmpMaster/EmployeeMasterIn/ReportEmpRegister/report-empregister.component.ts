import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto, DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './report-empregister.component.html',
    animations: [appModuleAnimation()]
})
export class ReportEmpRegisterComponent extends AppComponentBase implements OnInit {
    emptype: DbmtabListDto[] = [];
    section: DbmtabListDto[] = [];
    status: DbmtabListDto[] = [];
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this._dbmtabService.getDbmtabFromTABFG1('EMPLOYEETYPE', '').subscribe((result) => {
            this.emptype = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
            this.section = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('STATUS', '').subscribe((result) => {
            this.status = result.items;
        });
        $('#emprridt').val('ALL')
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
    }
    typ1:boolean;
    fnexcel(){
        // $('#typemp2').val('True')  
    }
    prints(emptype1, secfm, secto, empsts, empidf,empidt,wdtefm,wdteto,typ1) {
        var typ1s = 'False'
        this.typ1 == true ? typ1s = 'True' : typ1s = 'False'
        console.log(this.typ1)
        if (emptype1 != "" && empsts != "" && empidt != "") {
            if (empidt == "" || empidt == null || empidt == undefined || wdtefm == "" || wdtefm == null || wdtefm == undefined || wdteto == "" || wdteto == null || wdteto == undefined) { empidt = '',wdteto='' }
            // http://27.254.140.187:5433/WebForms/WebEmployeeResign.aspx?LNKEMT=B&LNKTYP=A&LNKFMD=20170601&LNKTOD=20190618&LNKFME=&LNKTOE=zz&LNKFMS=2101&LNKTOS=2101&Excel=False
            empidt.toUpperCase() == 'ALL' ? empidt = 'ZZZZ' : ''
            
            window.open("http://27.254.140.187:5433/WebForms/WebEmployeeResign.aspx?LNKEMT=B" + "&LNKTYP=" + emptype1  + "&LNKFMD=" + this.fnyyyymmdd(wdtefm) + "&LNKTOD=" + this.fnyyyymmdd(wdteto) + "&LNKFME=" + secfm.replace(/ /g, "") +  "&LNKTOE=" + secto.replace(/ /g, "") + "&LNKFMS=" + empidf.replace(/ /g, "") + "&LNKTOS=" + empidt.replace(/ /g, "") + "&Excel="+ typ1s)
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
}
