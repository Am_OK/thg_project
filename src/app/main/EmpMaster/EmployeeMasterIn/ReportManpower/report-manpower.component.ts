import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto, DbmtabListDto, DbmcomServiceProxy, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './report-manpower.component.html',
    animations: [appModuleAnimation()]
})
export class ReportManpowerComponent extends AppComponentBase implements OnInit {
    @ViewChild('modal2') modal2: ModalDirective;
    emp: MSMPSN00ListDto[] = [];


    DEPARTMENT: DbmtabListDto[] = [];
    POSITION: DbmtabListDto[] = [];
    DIVISION: DbmtabListDto[] = [];
    MANPOWERTYP: DbmtabListDto[] = [];

    active: boolean;
    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private _dbmtabService: DbmtabServiceProxy

    ) {
        super(injector);

    }
    ngOnInit(): void {
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;
        });

        this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
            this.POSITION = result.items;
        });

        this._dbmtabService.getDbmtabFromTABTB1('DIVISION', '').subscribe((result) => {
            this.DIVISION = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('MANPOWERTYP', '').subscribe((result) => {
            this.MANPOWERTYP = result.items;
        });

    }
    kuacc(dt) {
        if (dt.indexOf("+") >= 0) {
            var accode = dt.replace("+", "")
            $('#hsreids1').val(accode).toString()
        }
    }
    // chgindex(event) {
    //     var id = "#" + event
    //     var hsreid1 = $('#hsreids1').val().toString()
    //     setTimeout(() => {
    //         if (hsreid1 != null && hsreid1 != undefined) {
    //             var empc = this.emp.filter(res => res.psnidn == hsreid1)
    //             if (empc != null && empc != undefined) {
    //                 $(id).focus()
    //                 // 
    //             } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
    //         } else { this.message.info('รหัสไม่ถูกต้อง "ไม่พบชื่อที่ตองการเลือก"') }
    //     }, 500);


    // }
    fordep = ' '
    todep = 'ZZZZ'
    forsec = ' '
    tosec = 'ZZZZ'
    reA1 = 'ALL'
    typ1:boolean
    ditell(){
        $('#typm1').val('True')
    }
    prints(dayf, dayto,typ1) {
        var typ1s = 'PDF'
        this.typ1 == true ? typ1s = 'EXCEL' : typ1s = 'PDF'
        var fordep = $('#fordep').val().toString()
        var todep = $('#todep').val().toString()
        var forsec = $('#forsec').val().toString()
        var tosec = $('#tosec').val().toString()
        var dayf = this.fnyyyymmdd(dayf)
        var dayto = this.fnyyyymmdd(dayto)
        
        window.open("http://27.254.140.187:5433/EmployeeMaster/PowerIndicator?MANDEPFM=" + fordep.replace(/ /g, "") + "&MANDEPTO=" + todep.replace(/ /g, "") + "&MANPOSFM=" + forsec.replace(/ /g, "") + "&MANPOSTO=" + tosec.replace(/ /g, "") + "&MANDTEFM=" + dayf + "&MANDTETO=" + dayto + "&MANTYPE=" + this.reA1 + "&ReportType=" + typ1s)
        // http://27.254.140.187:5433/EmployeeMaster/PowerIndicator?MANDEPFM=&MANDEPTO=ZZZZ&MANPOSFM=&MANPOSTO=ZZZZ&MANDTEFM=20190618&MANDTETO=20190618&MANTYPE=All&ReportType=
    }
    // download(year, dtefm1, dteto, hsreid1, hsreid2) {
    //     if (dtefm1 != "" && dteto != "" && hsreid1 != "") {
    //         if (hsreid1 == "" || hsreid1 == null || hsreid1 == undefined) { hsreid1 = '' }
    //         window.open("http://27.254.140.187:5433/Training/RegistrationByID?YEAR=" + (year - 543 * 1) + "&FmDate=" + this.fnyyyymmdd(dtefm1) + "&ToDate=" + this.fnyyyymmdd(dteto) + "&FROMID=" + hsreid1 + "&TOID=" + hsreid2 + "&ReportType=EXCEL")
    //     } else {
    //         this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
    //     }
    // }
    chksho2
    show2(id): void {
        this.chksho2 = id
        this.modal2.show();
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
    }
    close2(): void {
        this.modal2.hide();
        this.active = false;
    }

    // get_datan2(e) {
    //     console.log(e);
    //     if (this.chksho2 == '1') { this.fordep = e[0].psnidn, this.close2() }
    //     if (this.chksho2 == '2') { this.todep = e[0].psnidn,  this.close2() }

    // }
}
