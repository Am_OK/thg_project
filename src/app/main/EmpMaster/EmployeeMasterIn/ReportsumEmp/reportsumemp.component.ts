import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './reportsumemp.component.html',
    animations: [appModuleAnimation()]
})
export class ReportsumEmpComponent extends AppComponentBase implements OnInit {
    emp: MSMPSN00ListDto[] = [];

    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy

    ) {
        super(injector);

    }
    empType
    stsType
    ngOnInit(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
        $('#agF').val(0)
        $('#agT').val(99)
        $('#waF').val(0)
        $('#waT').val(99)
        $('#sf').val(0)
        $('#st').val(9999999999)
        this.stsType = 'No'
        this.empType = 'M'

        this.fnnum()
    }
    kuacc(dt) {
        if (dt.indexOf("+") >= 0) {
            var accode = dt.replace("+", "")
            $('#hsreids1').val(accode).toString()
        }
    }

    fnnum() {
        $('#sf').val(this.fnformat($('#sf').val()))
        $('#st').val(this.fnformat($('#st').val()))
    }
    prints(Excel,agF,agT,waF,waT) {

     var sf = $('#sf').val() != null && $('#sf').val() != undefined ? sf = this.fnreplace($('#sf').val()) : sf = 0
     var st = $('#st').val() != null && $('#st').val() != undefined ? st = this.fnreplace($('#st').val()) : st = 0

        window.open("http://27.254.140.187:5433/WebForms/EmployeeMaster/WEbMSF9101.aspx?PSNSTS=" +  this.stsType + "&EMPTYPE=" + this.empType + "&STRPSNNSL=" + sf + "&ENDPSNNSL=" + st + "&STRAGE=" + agF+ "&ENDAGE=" + agT + "&STRPSNINW=" + waF + "&ENDPSNINW=" + waT + "&Excel=" + Excel)
    }

}
