import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ListDto, MSMPSN00ServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';
import { data_Empmas } from '../../data_center';




@Component({
    selector: 'empmasterlist',
    templateUrl: './empmasterlist.component.html',
})
export class EmployeemasterlistComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    emp: MSMPSN00ListDto[] = [];
    dbmtabemti: DbmtabListDto[] = [];

    active: boolean;
    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private _dbmtabService: DbmtabServiceProxy


    ) {
        super(injector);
    }
    ngOnInit(): void {
        if (this.data_Empmas.empmasterl != 1) {
            this.data_Empmas.Empmaster()
        }
        // this.emp = this.data_Empmas.emp
        this.getemp()
    }
    getemp(){
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
            this.dbmtabemti = result.items;
        });
    }
    chksho2;    
    show(id): void {
        this.modal.show();
        this.active = true;
        this.chksho2 = id
        if(this.emp.length <= 0){
            this.getemp()
        }
    }
    get_datan2(e) {
        console.log(e);
        var emp = this.emp.filter(res => res.psnidn == e)
        if (this.chksho2 == '1') {$('#empidfm').val(emp[0].psnidn),this.close(),$('#eidfm').val(emp[0].psnidn),this.close(),$('#empreidf').val(emp[0].psnidn),this.close(),$('#emprridf').val(emp[0].psnidn),this.close()}
        if (this.chksho2 == '2') {$('#empidto').val(emp[0].psnidn),this.close(),$('#eidto').val(emp[0].psnidn),this.close(),$('#empreidt').val(emp[0].psnidn),this.close(),$('#emprridt').val(emp[0].psnidn),this.close()}

    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

}

