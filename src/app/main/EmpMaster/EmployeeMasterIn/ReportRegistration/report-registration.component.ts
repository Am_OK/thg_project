import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';



@Component({
    templateUrl: './report-registration.component.html',
    animations: [appModuleAnimation()]
})
export class ReportRegistrationComponent extends AppComponentBase implements OnInit {
    emptype: DbmtabListDto[] = [];
    section: DbmtabListDto[] = [];
    status: DbmtabListDto[] = [];
    shownLoginid13;

    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,

    ) {
        super(injector);
        this.shownLoginid13 = this.appSession.user.userName;

    }
    ngOnInit(): void {
        this._dbmtabService.getDbmtabFromTABFG1('EMPLOYEETYPE', '').subscribe((result) => {
            this.emptype = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
            this.section = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('STATUS', '').subscribe((result) => {
            this.status = result.items;
        });
        $('#empreidt').val('ALL')
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
    }
    fnexcel(){
        $('#typre1').val('True')
        
    }
    prints(emptype1, secfm, secto, empsts,empidf ,empidt,wdtefm,wdteto,typ1) {
        if (emptype1 != "" && empsts != "" && empidt != "") {
            if (empidt == "" || empidt == null || empidt == undefined) { empidt = '' }
            empidt.toUpperCase() == 'ALL' ? empidt = 'ZZZZ' : ''
            http://27.254.140.187:5433/WebForms/EmployeeMaster/WebMSR0733.aspx?EMPTYPE=B&STRSEC=2101&ENDSEC=2101&EMPSTATUS=A&STREMPID=&ENDEMPID=zz&STRWDTE=20160101&ENDWDTE=20190618&User=HR@ADMIN&Excel=False
            window.open("http://27.254.140.187:5433/WebForms/EmployeeMaster/WebMSR0733.aspx?EMPTYPE=" + emptype1  + "&STRSEC=" + secfm.replace(/ /g, "") + "&ENDSEC=" + secto.replace(/ /g, "") + "&EMPSTATUS=" + empsts + "&STREMPID=" + empidf.replace(/ /g, "") + "&ENDEMPID=" + empidt.replace(/ /g, "") + "&STRWDTE=" + this.fnyyyymmdd(wdtefm) + "&ENDWDTE=" + this.fnyyyymmdd(wdteto) + "&User=" + this.shownLoginid13 + "&Excel="+ typ1)
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
}
