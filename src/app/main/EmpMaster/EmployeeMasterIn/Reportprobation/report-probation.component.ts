import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';



@Component({
    templateUrl: './report-probation.component.html',
    animations: [appModuleAnimation()]
})
export class ReportProbationComponent extends AppComponentBase implements OnInit {
    emp: MSMPSN00ListDto[] = [];

    constructor(
        injector: Injector,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });

    }
    excel(){
        $('#typpro1').val('True')
    }
    chkstatus(){
        $('#status').val('A')

    }
    periodshow
    yearshow
    dateshow
    onchange(period, year) {
        if (year >= 2662 || year <= 2462) {
            this.dateshow = ""
        }
        if (period >= 1 && period <= 12) {
            if (period.length == 1) {
                period = '0' + period
            }
        } else {
            this.dateshow = ""
            this.periodshow = ""
        }
        if (period >= 1 && period <= 12 && year >= 2462 && year <= 2662) {
            var date = new Date(year - 543 + '-' + period + '-' + '01')
            var datestart = year - 543 + period + '01'
            var dateend = date.getFullYear() + '' + ("0" + (date.getMonth() + 1)).slice(-2) + new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate()
            this.dateshow = this.fnddmmyyyy(datestart) + " - " + this.fnddmmyyyy(dateend)
        }
    }

    // fnmonth(m)
    // {
    //     $('#date').val(moment(m).add(543, 'years').format('DD/MM/YYYY'));
    // }
    ditell(){
        
    }
    prints(psnstst,dtefm1,type) {
            window.open("http://27.254.140.187:5433/WebForms/EmployeeMaster/WebMSF0711.aspx?PSNSTS="+ psnstst + "&STRDTE=" + this.fnyyyymmdd(dtefm1) + "&ENDDTE=" + this.fnyyyymmdd(dtefm1) + "&Excel=" + type )
       
    }
   
}
