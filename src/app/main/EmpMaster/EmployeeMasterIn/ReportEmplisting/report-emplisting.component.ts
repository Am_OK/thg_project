import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { MSMPSN00ServiceProxy, MSMPSN00ListDto, DbmtabServiceProxy, DbmtabListDto } from '@shared/service-proxies/service-proxies';
import { data_Empmas } from '../../data_center';



@Component({
    templateUrl: './report-emplisting.component.html',
    animations: [appModuleAnimation()]
})
export class ReportEmpListingComponent extends AppComponentBase implements OnInit {
    
    dbmtab: DbmtabListDto[] = [];
    DEPARTMENT: DbmtabListDto[] = [];
    DIVISION: DbmtabListDto[] = [];
    STATUS: DbmtabListDto[] = [];
    emp:MSMPSN00ListDto[] = [];
    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private _dbmtabService: DbmtabServiceProxy,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,



    ) {
        super(injector);

    }
    ngOnInit(): void {
        $('#empidto').val('ALL')
          this._dbmtabService.getDbmtabFromTABTB1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;
          });
          this._dbmtabService.getDbmtabFromTABFG1('DIVISION', '').subscribe((result) => {
            this.DIVISION = result.items
      
          })
          this._dbmtabService.getDbmtabFromTABTB1('STATUS', '').subscribe((result) => {
            this.STATUS = result.items;
          });
          this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
            this.dbmtab = result.items;
          });
        //   this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
        //     this.emp = result.items;
        //   });
        this.dbmtab = this.data_Empmas.dbmtab
        this.DEPARTMENT = this.data_Empmas.DEPARTMENT
        this.DIVISION = this.data_Empmas.DIVISION
        this.STATUS  = this.data_Empmas.STATUS
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
      
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
    }
    show():void{

    }
   ditell(){
    $('#typex1').val('True')
   }
    prints(empidfm,empidto,emppositionf,emppositiont,emppositionf1,emppositiont1,depfm,depto,divfm,divto,stsfmd,ststo,typex1) {
        if (empidto != "") {
            if (empidto == "" || empidto == null || empidto == undefined) { empidto = '' }
            empidto.toUpperCase() == 'ALL' ? empidto = 'ZZZZ' : ''
            http://27.254.140.187:5433/EmployeeMaster/EmployeeList?PSNIDNFM=272005%20&PSNIDNTO=362024%20&PSNSTSFM=A&PSNSTSTO=A&PSNPOSFM=013&PSNPOSTO=013&PSNPOSPFFM=008&PSNPOSPFTO=008&PSNDEPFM=421&PSNDEPTO=421&PSNDIVFM=46&PSNDIVTO=46&Excel=            
            window.open(" http://27.254.140.187:5433/EmployeeMaster/EmployeeList?PSNIDNFM=" + empidfm.replace(/ /g, "") + "&PSNIDNTO=" + empidto.replace(/ /g, "") + "&PSNSTSFM=" + stsfmd.replace(/ /g, "") + "&PSNSTSTO="+ ststo.replace(/ /g, "") +"&PSNPOSFM="+ emppositionf.replace(/ /g, "") + "&PSNPOSTO=" + emppositiont.replace(/ /g, "") +"&PSNPOSPFFM="+ emppositionf1.replace(/ /g, "") + "&PSNPOSPFTO=" + emppositiont1.replace(/ /g, "") +"&PSNDEPFM=" + depfm.replace(/ /g, "") + "&PSNDEPTO=" + depto.replace(/ /g, "") + "&PSNDIVFM=" + divfm.replace(/ /g, "") + "&PSNDIVTO=" + divto.replace(/ /g, "") + "&Excel=" + typex1)
        } else {
            this.message.warn('กรุณาป้อนข้อมูลให้ครบถ้วน')
        }
    }
}
