import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, INCPOS00ListDto, INCPOS00ServiceProxy, PositionCertificatesServiceProxy, PositionCertificatesListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { SetcodepositionComponent } from '../setcode-position.component';


@Component({
    selector: 'PositionCF',
    templateUrl: './PositionCF.html',
})
export class PositionCFComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    @ViewChild('modal3') modal3: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private DbmtabServiceProxy: DbmtabServiceProxy,
        private INCPOS00ServiceProxy: INCPOS00ServiceProxy,
        private SetcodepositionComponent: SetcodepositionComponent,
        private PositionCertificatesServiceProxy: PositionCertificatesServiceProxy
    ) {
        super(injector);
    }
    POSITIONEMP: DbmtabListDto = new DbmtabListDto()
    INCOMEPOSITION: DbmtabListDto[] = []
    INCPOS00ListDto: INCPOS00ListDto[] = []
    INCPOS00OBJ: INCPOS00ListDto = new INCPOS00ListDto()
    POSITION: DbmtabListDto[] = []
    CERTIFICATETYP: DbmtabListDto[] = []

    // PositionCertificates: PositionCertificatesListDto = new PositionCertificatesListDto()
    // PositionCertificatesListDto: PositionCertificatesListDto[]= []

    PositionCertificates: PositionCertificatesListDto[] = []
    PositionCertificatesObject: PositionCertificatesListDto = new PositionCertificatesListDto()
    EDITS = 0
    show(): void {
        this.modal.show();
        this.active = true;

        this.getpositionCF()
        this.getPOSITION()
        this.getCERTIFICATETYP()
    }

    show2(): void {
        this.clr()
        this.getPositionCertifi = []
        this.modal2.show();
        this.active = true;
        this.getCERTIFICATETYP()
    }
    show3(): void {
        this.modal3.show();
        this.active = true;
        this.getPOSITION()
    }
    ECertificates
    namepos
    CERTIFICATE = [({ 'cetfID': '', 'ECertificates': '' })];
    // CERTIFICATEc : PositionCertificatesListDto[] = []
    Add() {
        var idss = this.getPositionCertifi.filter(res => res.certyp == this.ECertificates)
        if (idss.length <= 0) {

            this.PositionCertificatesObject.cerpos = this.idpos
            this.PositionCertificatesObject.certyp = this.ECertificates

            this.PositionCertificatesServiceProxy.createPositionCertificates(this.PositionCertificatesObject).subscribe(() => {

                this.PositionCertificatesServiceProxy.getPositionCertificatesByPos(this.idpos).subscribe((result) => {
                    this.getPositionCertifi = result.items

                })

            })



        } else { this.message.warn("ข้อมูลมีในระบบแล้ว กรุณาตรวจสอบข้อมูล") }
    }
    clr() {
        this.namepos = ''
        this.idpos = ''
        this.ECertificates = ''
    }
    showedit(e) {
        var idss = this.POSITION.filter(res => res.tabtB2 == e.data.cerpos)
        this.idpos = e.data.cerpos,
            this.namepos = idss[0].tabdsc,
            this.PositionCertificatesServiceProxy.getPositionCertificatesByPos(e.data.cerpos).subscribe((result) => {
                this.getPositionCertifi = result.items
            })
        this.modal2.show();
    }

    // save() {
    //     for (let i = 0; i < this.CERTIFICATE.length; i++) {
    //         this.PositionCertificatesObject.cerpos = this.CERTIFICATE[i].cetfID
    //         this.PositionCertificatesObject.certyp = this.CERTIFICATE[i].ECertificates
    //         console.log(this.PositionCertificatesObject)
    //         this.PositionCertificatesServiceProxy.createPositionCertificates(this.PositionCertificatesObject).subscribe(() => {
    //             if ((this.CERTIFICATE.length <= (i + 1 * 1))) {
    //                 this.close4()
    //                 this.getpositionCF()
    //             }
    //         })
    //     }
    // }
    PositionCertificatesc: PositionCertificatesListDto[] = []
    PositionCer=[]
    getpositionCF() {
        this.PositionCertificatesServiceProxy.getPositionCertificatesAll('', 'ALL').subscribe((result) => {
            this.PositionCertificatesc = result.items
            for (let i = 0; i < this.PositionCertificatesc.length; i++) {
                var Positionc = this.PositionCer.filter(res => res.cerpos == this.PositionCertificatesc[i].cerpos)
                if (Positionc.length<=0) { 
                    this.PositionCer.push({'cerpos': this.PositionCertificatesc[i].cerpos})
                }

            }

        })
    }
    removed(e) {
        this.PositionCertificatesServiceProxy.deletePositionCertificates(e.data.id).subscribe(() => {
            this.PositionCertificatesServiceProxy.getPositionCertificatesByPos(e.data.cerpos).subscribe((result) => {
                this.getPositionCertifi = result.items
            })
        })
    }
    delete(e) {
        this.PositionCertificatesServiceProxy.deletePositionCertificatesByPos(e.data.cerpos).subscribe(() => {
            this.getpositionCF()
        })
    }

    idpos
    getPOSITION() {
        this.DbmtabServiceProxy.getDbmtabFromTABFG1('POSITION', '').subscribe((result) => {
            this.POSITION = result.items
        })
    }
    getCERTIFICATETYP() {
        this.DbmtabServiceProxy.getDbmtabFromTABFG1('CERTIFICATETYP', '').subscribe((result) => {
            this.CERTIFICATETYP = result.items
        })
    }

    getPositionCertifi: PositionCertificatesListDto[] = []
    get_datan2(e) {
        this.idpos = e[0].tabtB2,
            this.namepos = e[0].tabdsc,
            this.PositionCertificatesServiceProxy.getPositionCertificatesByPos(e[0].tabtB2).subscribe((result) => {
                this.getPositionCertifi = result.items
                for (let i = 0; i < this.getPositionCertifi.length; i++) {
                    this.CERTIFICATE.push({ 'cetfID': this.getPositionCertifi[i].cerpos, 'ECertificates': this.getPositionCertifi[i].certyp })
                }
            })
        this.close3()
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    close4(): void {
        this.modal2.hide();
        this.getpositionCF()
    }
    close3(): void {
        this.modal3.hide();
    }
}

