import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TRBUD00ListDto, TRBUD00AppserviceServiceProxy, DbmtabServiceProxy, DbmtabListDto, INCPOS00ServiceProxy } from '@shared/service-proxies/service-proxies';
// import { BankAccountListDto, BankAccountServiceProxy, DbmtabListDto, DbmtabServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'setcodeposition',
  templateUrl: './setcode-position.component.html'
})
export class SetcodepositionComponent extends AppComponentBase implements OnInit {

  constructor(
    injector: Injector,
    private DbmtabServiceProxy : DbmtabServiceProxy
  ) {
    super(injector);


  }
  POSITION:DbmtabListDto[]=[]
  INCOMEPOSITION:DbmtabListDto[]=[]
  ngOnInit() {
   this.getPOSITION()
   this.getINCPOS()
  }

  getPOSITION(){
    this.DbmtabServiceProxy.getDbmtabFromTABFG1('POSITION','').subscribe((result)=>{
      this.POSITION = result.items
    })
  }

  getINCPOS(){
    this.DbmtabServiceProxy.getDbmtabFromTABFG1('INCOMEPOSITION','').subscribe((result)=>{
      this.INCOMEPOSITION = result.items
    })
  }

  onContentReady(e) {
    e.component.columnOption("command:edit", {
      visibleIndex: -1,
      width: 70
    });
  }
}
