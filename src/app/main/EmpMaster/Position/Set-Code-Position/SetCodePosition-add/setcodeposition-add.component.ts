import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, INCPOS00ListDto, INCPOS00ServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { SetcodepositionComponent } from '../setcode-position.component';


@Component({
    selector: 'setcodepositionadd',
    templateUrl: './setcodeposition-add.component.html',
})
export class SetcodepositionaddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private DbmtabServiceProxy: DbmtabServiceProxy,
        private INCPOS00ServiceProxy: INCPOS00ServiceProxy,
        private SetcodepositionComponent: SetcodepositionComponent
    ) {
        super(injector);
    }
    POSITIONEMP: DbmtabListDto = new DbmtabListDto()
    INCOMEPOSITION: DbmtabListDto[] = []
    INCPOS00ListDto: INCPOS00ListDto[] = []
    INCPOS00OBJ: INCPOS00ListDto = new INCPOS00ListDto()
    EDITS = 0
    show(id): void {
        if (id != 'A') {
            this.POSITIONEMP = this.SetcodepositionComponent.POSITION.filter(res => res.tabtB2 == id)[0]
            this.INCPOS00ServiceProxy.getINCPOS00(id).subscribe((result) => {
                this.INCPOS00ListDto = result.items
            })
            this.EDITS = 1
        } else {
            this.POSITIONEMP = new DbmtabListDto()
            this.INCPOS00ListDto = []
            this.POSITIONEMP.tabtB2 = ''
            this.POSITIONEMP.tabdsc = ''
            this.EDITS = 0
        }
        this.INCOMEPOSITION = this.SetcodepositionComponent.INCOMEPOSITION
        this.modal.show();
        this.active = true;
    }

    Add() {
        if (this.POSITIONEMP.tabtB2 != '' && this.POSITIONEMP.tabtB2 != null && this.POSITIONEMP.tabtB2 != undefined) {
            var inctyp = ''
            var incamt = 0
            if ($('#inctyp').val() != '' && $('#inctyp').val() != null && $('#inctyp').val() != undefined) {
                inctyp = $('#inctyp').val().toString()
                if ($('#incamt').val() != '' && $('#incamt').val() != null && $('#incamt').val() != undefined) {
                    incamt = this.fnreplace($('#incamt').val())
                }
                if (this.INCPOS00ListDto.filter(res => res.incins == inctyp).length <= 0) {
                    this.INCPOS00OBJ = new INCPOS00ListDto()
                    this.INCPOS00OBJ.incins = inctyp
                    this.INCPOS00OBJ.incamt = incamt
                    this.INCPOS00OBJ.incpos = this.POSITIONEMP.tabtB2
                    this.INCPOS00ListDto.push(this.INCPOS00OBJ)

                    $('#incamt').val('')
                } else {
                    this.message.warn('ไม่สามารถระบุรหัสรายได้ซ้ำได้', 'พบข้อผิดพลาด')
                }


            }
        } else {
            this.message.warn('กรุณาระบุรหัสตำแหน่ง', 'พบข้อผิดพลาด')
        }
    }

    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    if (this.EDITS == 1) {
                        this.DbmtabServiceProxy.updateDbmtab(this.POSITIONEMP).subscribe(() => {
                            this.close()
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.modalSave.emit()
                            this.INCPOS00ServiceProxy.deleteByINCPOS(this.POSITIONEMP.tabtB2).subscribe(() => {
                                this.INCPOS00ServiceProxy.createINCPOS00(this.INCPOS00ListDto).subscribe(() => {

                                })
                            })
                        })

                    } else if (this.EDITS == 0) {
                        this.POSITIONEMP.tabaM1 = 0
                        this.POSITIONEMP.tabaM2 = 0
                        this.POSITIONEMP.tabrT1 = 0
                        this.POSITIONEMP.tabrT2 = 0
                        this.POSITIONEMP.tabtB1 = 'POSITION'
                        this.POSITIONEMP.tabdse = ''
                        this.DbmtabServiceProxy.createDbmtab(this.POSITIONEMP).subscribe(() => {
                            this.close()
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.modalSave.emit()
                            this.INCPOS00ServiceProxy.deleteByINCPOS(this.POSITIONEMP.tabtB2).subscribe(() => {
                                this.INCPOS00ServiceProxy.createINCPOS00(this.INCPOS00ListDto).subscribe(() => {

                                })
                            })
                        })

                    }

                }
            })
    }
    delete() {
        this.message.confirm(
            this.l('AreYouSureToDeleteTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    this.DbmtabServiceProxy.deleteDbmtab(this.POSITIONEMP.id).subscribe(() => {
                        this.INCPOS00ServiceProxy.deleteByINCPOS(this.POSITIONEMP.tabtB2).subscribe(() => {
                            this.close()
                            this.notify.info(this.l('DeletedSuccessfully'));
                            this.modalSave.emit()
                        })
                    })
                }
            })
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}

