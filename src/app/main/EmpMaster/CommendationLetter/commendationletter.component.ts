import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeCommendationServiceProxy, EmployeeCommendationListDto, MSMPSN00ServiceProxy, MSMPSN00ListDto, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'commendationletter',
    templateUrl: './commendationletter.component.html',
})
export class CommendationletterComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeCommendationServiceProxy: EmployeeCommendationServiceProxy,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private KPShareServiceProxy:KPShareServiceProxy

    ) {
        super(injector);
    }

    EmployeeCommendationListDto: EmployeeCommendationListDto[] = []
    dbmtabemti: DbmtabListDto[] = []
    REASON: DbmtabListDto[] = []
    
    emp: MSMPSN00ListDto[] = []
    ngOnInit(): void {
        $('#to').val('ALL')
        this.getCommendation('','ALL')
        this.getmaster()
        this.getEmployees();
    }
    getDisplayExpr(item) {
        // console.log(item)
        if (!item) {
          return "";
        }
        // console.log(item.psnfnm + " " + item.psnlnm)
        return item.psnfnm + " " + item.psnlnm
      }
    getEmployees(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
    }
    getmaster() {
        this._dbmtabService.getDbmtabFromTABFG1('GOODREASON', '').subscribe((result) => {
            this.REASON = result.items

        })
        this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
            this.dbmtabemti = result.items;
          });
         

    }
    getCommendation(frm,to) {
        this.EmployeeCommendationServiceProxy.getEmployeeCommendationFromTo(frm,to).subscribe((result) => {
            this.EmployeeCommendationListDto = result.items
        })
    }
    

    
}

