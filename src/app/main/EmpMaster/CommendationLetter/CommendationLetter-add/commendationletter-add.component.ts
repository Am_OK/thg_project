import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, EmployeeCommendationListDto, EmployeeCommendationServiceProxy, MSMPSN00ListDto, KPShareServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { CommendationletterComponent } from '../commendationletter.component';
import { DxDataGridComponent } from 'devextreme-angular';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'commendationletteradd',
    templateUrl: './commendationletter-add.component.html',
})
export class commendationletteraddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal2') modal2: ModalDirective;
    @ViewChild('modal5') modal5: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private _dbmtabService: DbmtabServiceProxy,
        private EmployeeCommendationServiceProxy: EmployeeCommendationServiceProxy,
        private CommendationletterComponent: CommendationletterComponent,
        private KPShareServiceProxy: KPShareServiceProxy
    ) {
        super(injector);
    }
    dbmtabemti: DbmtabListDto[] = []
    REASON: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    
    fnnum() {
        $('#godprc').val(this.fnformat($('#godprc').val()))
    }
    EmployeeCommendationObject: EmployeeCommendationListDto = new EmployeeCommendationListDto()
    getItemMaster() {
        this.REASON = this.CommendationletterComponent.REASON
        this.emp = this.CommendationletterComponent.emp
        this.dbmtabemti = this.CommendationletterComponent.dbmtabemti
        
    }
    showedit(e) {
        this.getItemMaster()
        this.Editing = 1
        this.EmployeeCommendationObject = new EmployeeCommendationListDto()
        this.EmployeeCommendationObject.id = e.data.id
        this.EmployeeCommendationObject.goddoc = e.data.goddoc
        this.EmployeeCommendationObject.goddte = this.fnddmmyyyy(e.data.goddte)
        this.EmployeeCommendationObject.godefd = this.fnddmmyyyy(e.data.godefd)
        this.EmployeeCommendationObject.godeid = e.data.godeid
        this.EmployeeCommendationObject.godaut = e.data.godaut
        this.EmployeeCommendationObject.godrea = e.data.godrea
        this.EmployeeCommendationObject.godsts = e.data.godsts
        this.EmployeeCommendationObject.godstd = this.fnddmmyyyy(e.data.godstd)
        this.EmployeeCommendationObject.godrM1 = e.data.godrM1
        this.EmployeeCommendationObject.godrM2 = e.data.godrM2
        this.EmployeeCommendationObject.godrM3 = e.data.godrM3
        this.EmployeeCommendationObject.godprc = e.data.godprc
        var empname = this.emp.filter(res => res.psnidn == this.EmployeeCommendationObject.godeid)
        var autname = this.emp.filter(res => res.psnidn == this.EmployeeCommendationObject.godaut)
        setTimeout(() => {
            $('#godprc').val(this.fnformat(this.EmployeeCommendationObject.godprc))
            empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            autname.length > 0 ? $('#autname').val(autname[0].psnfnm + ' ' + autname[0].psnlnm) : $('#autname').val('')
        }, 250);
        this.modal2.show();

    }
    show2(): void {
        this.getItemMaster()
        this.modal2.show();
        this.EmployeeCommendationObject = new EmployeeCommendationListDto()
        this.EmployeeCommendationObject.godprc = 0
        setTimeout(() => {
            this.fnnum()
        }, 200);
    }
    close2(): void {
        this.modal2.hide();
        this.Editing = 0
        $('#empname').val('')
        $('#autname').val('')
    }
    Editing = 0
    pipe = new DatePipe('en-US');
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var goddte = $('#goddte').val() != null && $('#goddte').val() != undefined ? goddte = this.fnyyyymmdd($('#goddte').val()) : goddte = ''
                    var godefd = $('#godefd').val() != null && $('#godefd').val() != undefined ? godefd = this.fnyyyymmdd($('#godefd').val()) : godefd = ''
                    var godstd = $('#godstd').val() != null && $('#godstd').val() != undefined ? godstd = this.fnyyyymmdd($('#godstd').val()) : godstd = ''
                    var godprc = $('#godprc').val() != null && $('#godprc').val() != undefined ? godprc = this.fnreplace($('#godprc').val()) : godprc = 0
                    this.EmployeeCommendationObject.goddte = goddte
                    this.EmployeeCommendationObject.godefd = godefd
                    this.EmployeeCommendationObject.godstd = godstd
                    this.EmployeeCommendationObject.godprc = godprc
                    if (this.Editing == 1) {
                        this.EmployeeCommendationServiceProxy.updateEmployeeCommendation(this.EmployeeCommendationObject).subscribe(() => {
                            this.close2()
                            this.modalSave.emit()
                            this.notify.info(this.l('SavedSuccessfully'));
                        })
                    } else {
                        if (this.EmployeeCommendationObject.goddoc != '' && this.EmployeeCommendationObject.goddoc != null && this.EmployeeCommendationObject.goddoc != undefined) {
                            this.EmployeeCommendationServiceProxy.createEmployeeCommendation(this.EmployeeCommendationObject).subscribe(() => {
                                this.close2()
                                this.modalSave.emit()
                                this.notify.info(this.l('SavedSuccessfully'));
                            })
                        } else {
                            this.KPShareServiceProxy.getDocumentNumber('CM', this.EmployeeCommendationObject.goddte, 'THI').subscribe((result) => {
                                this.EmployeeCommendationObject.goddoc = result[0].documentNumber
                                this.EmployeeCommendationServiceProxy.createEmployeeCommendation(this.EmployeeCommendationObject).subscribe(() => {
                                    this.close2()
                                    this.modalSave.emit()
                                    this.notify.info(this.l('SavedSuccessfully'));
                                })
                            })
                        }

                    }

                }
            })
    }
    removed() {
        this.message.confirm(
            this.l('AreYouSureToDeleteTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    this.EmployeeCommendationServiceProxy.deleteEmployeeCommendation(this.EmployeeCommendationObject.id).subscribe(() => {
                        this.modalSave.emit()
                        this.close2()
                    })
                }
            })
    }
    @ViewChild("gEmployeea") gEmployeea: DxDataGridComponent
    show5(v) {
        this.OpMdl = v
        this.modal5.show();
    }
    closeemp() {
        this.gEmployeea.instance.clearSelection()
        this.modal5.hide();
    }
    OpMdl
    get_datan(eid) {
        // this.EmployeeCommendationObject.godeid = eid
        // var empname = this.emp.filter(res => res.psnidn == this.EmployeeCommendationObject.godeid)
        // setTimeout(() => {
        //     empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
        // }, 250);
        // this.closeemp()

        if (this.OpMdl == 'Emp') {
            this.EmployeeCommendationObject.godeid = eid
            var empname = this.emp.filter(res => res.psnidn == this.EmployeeCommendationObject.godeid)
            setTimeout(() => {
                empname.length > 0 ? $('#empname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#empname').val('')
            }, 250);
            this.closeemp()
        }
        if (this.OpMdl == 'Aut') {
            this.EmployeeCommendationObject.godaut = eid
            var empname = this.emp.filter(res => res.psnidn == this.EmployeeCommendationObject.godaut)
            setTimeout(() => {
                empname.length > 0 ? $('#autname').val(empname[0].psnfnm + ' ' + empname[0].psnlnm) : $('#autname').val('')
            }, 250);
            this.closeemp()
        }
    }

}

