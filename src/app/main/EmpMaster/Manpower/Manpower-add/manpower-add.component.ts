import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, ManPowerServiceProxy, ManPowerListDto, MSMPSN00ListDto, KPShareServiceProxy, MSMPSN00ServiceProxy, ManPowerListServiceProxy, ManPowerListListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '../../data_center';
import { ManpowerComponent } from '../manpower.component';
import { puts } from 'util';
@Component({
    selector: 'manpoweradd',
    templateUrl: './manpower-add.component.html',
})
export class ManpoweraddComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('modal2') modal2: ModalDirective;
    @ViewChild('modal3') modal3: ModalDirective;


    active: boolean;
    constructor(
        injector: Injector,

        private ManPowerServiceProxy: ManPowerServiceProxy,
        private ManpowerComponent: ManpowerComponent,
        private ManPowerListServiceProxy: ManPowerListServiceProxy,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private KPShareServiceProxy: KPShareServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    dbmtab: DbmtabListDto[] = [];
    POSITION: DbmtabListDto[] = [];
    DEPARTMENT: DbmtabListDto[] = [];
    dbmtabemti: DbmtabListDto[] = [];
    MOVERESON: DbmtabListDto[] = [];
    emp: MSMPSN00ListDto[] = [];
    MARRIEDSTATUS: DbmtabListDto[] = [];
    EDUCATION: DbmtabListDto[] = [];
    MANPOWERTYP: DbmtabListDto[] = [];
    DIVISION: DbmtabListDto[] = [];
    Editing = 0;
    ManPowerObject: ManPowerListDto = new ManPowerListDto()

    ManPowerListListDto: ManPowerListListDto[] = [];
    ManPowerListObject: ManPowerListListDto = new ManPowerListListDto()

    getItemMaster(doc) {
        this.POSITION = this.ManpowerComponent.POSITION
        this.DEPARTMENT = this.ManpowerComponent.DEPARTMENT
        this.dbmtabemti = this.ManpowerComponent.dbmtabemti
        this.MOVERESON = this.ManpowerComponent.MOVERESON
        this.dbmtab = this.ManpowerComponent.dbmtab
        this.MARRIEDSTATUS = this.ManpowerComponent.MARRIEDSTATUS
        this.EDUCATION = this.ManpowerComponent.EDUCATION
        this.MANPOWERTYP = this.ManpowerComponent.MANPOWERTYP
        this.DIVISION = this.ManpowerComponent.DIVISION
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
        this.ManPowerListServiceProxy.getManManPowerListByDoc(doc).subscribe((result) => {
            this.ManPowerListListDto = result.items;
        });
    }

    showedit(e) {
        this.getItemMaster(e.data.mandoc)
        this.Editing = 1
        this.ManPowerObject = new ManPowerListDto()
        this.ManPowerObject.id = e.data.id
        this.ManPowerObject.mandoc = e.data.mandoc
        this.ManPowerObject.manstR1 = e.data.manstR1
        this.ManPowerObject.mandte = this.fnddmmyyyy(e.data.mandte)
        this.ManPowerObject.mandiv = e.data.mandiv
        this.ManPowerObject.mandep = e.data.mandep
        this.ManPowerObject.manpos = e.data.manpos
        this.ManPowerObject.manqty = e.data.manqty
        this.ManPowerObject.mansex = e.data.mansex
        this.ManPowerObject.manstR2 = e.data.manstR2
        this.ManPowerObject.manweg = e.data.manweg
        this.ManPowerObject.manhig = e.data.manhig
        this.ManPowerObject.manedu = e.data.manedu
        this.ManPowerObject.manepr = e.data.manepr
        this.ManPowerObject.manexpert = e.data.manexpert
        this.ManPowerObject.manprp = e.data.manprp
        this.ManPowerObject.manreA1 = e.data.manreA1
        this.ManPowerObject.mannam = e.data.mannam
        this.ManPowerObject.manreA2 = e.data.manreA2
        this.ManPowerObject.manbeg = e.data.manbeg
        this.ManPowerObject.mannoit = e.data.mannoit
        this.ManPowerObject.mandst = this.fnddmmyyyy(e.data.mandst)
        this.ManPowerObject.manprovedte = this.fnddmmyyyy(e.data.manprovedte)
        this.ManPowerObject.manprovehr = e.data.manprovehr
        this.ManPowerObject.manreA3 = e.data.manreA3
        this.ManPowerObject.manprov = e.data.manprov
        this.ManPowerObject.manreA4 = e.data.manreA4

        this.ManPowerObject.manprV3 = e.data.manprV3
        this.ManPowerObject.manhr = e.data.manhr
        this.ManPowerObject.manprV1 = e.data.manprV1
        this.ManPowerObject.manprV2 = e.data.manprV2



        if (this.emp.length > 0) {
            var manprV3name = this.emp.filter(res => res.psnidn == this.ManPowerObject.manprV3)
            var manhrname = this.emp.filter(res => res.psnidn == this.ManPowerObject.manhr)
            var manprV1name = this.emp.filter(res => res.psnidn == this.ManPowerObject.manprV1)
            var manprV2name = this.emp.filter(res => res.psnidn == this.ManPowerObject.manprV2)
            manprV3name.length > 0 ? this.manprV3name = manprV3name[0].psnfnm + ' ' + manprV3name[0].psnlnm : this.manprV3name = ''
            manhrname.length > 0 ? this.manhrname = manhrname[0].psnfnm + ' ' + manhrname[0].psnlnm : this.manhrname = ''
            manprV2name.length > 0 ? this.manprV2name = manprV2name[0].psnfnm + ' ' + manprV2name[0].psnlnm : this.manprV2name = ''
            manprV1name.length > 0 ? this.manprV1name = manprV1name[0].psnfnm + ' ' + manprV1name[0].psnlnm : this.manprV1name = ''
        } else {
            this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
                this.emp = result.items;
                var manprV3name = this.emp.filter(res => res.psnidn == this.ManPowerObject.manprV3)
                var manhrname = this.emp.filter(res => res.psnidn == this.ManPowerObject.manhr)
                var manprV1name = this.emp.filter(res => res.psnidn == this.ManPowerObject.manprV1)
                var manprV2name = this.emp.filter(res => res.psnidn == this.ManPowerObject.manprV2)
                manprV3name.length > 0 ? this.manprV3name = manprV3name[0].psnfnm + ' ' + manprV3name[0].psnlnm : this.manprV3name = ''
                manhrname.length > 0 ? this.manhrname = manhrname[0].psnfnm + ' ' + manhrname[0].psnlnm : this.manhrname = ''
                manprV2name.length > 0 ? this.manprV2name = manprV2name[0].psnfnm + ' ' + manprV2name[0].psnlnm : this.manprV2name = ''
                manprV1name.length > 0 ? this.manprV1name = manprV1name[0].psnfnm + ' ' + manprV1name[0].psnlnm : this.manprV1name = ''
            });
        }
        this.modal.show();
    }
    num2 = 0
    save2(num){
        this.num2 = 0
        this.num2 = num
        this.save()
    }
    save() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {

                    var mandte = $('#mandte').val() != null && $('#mandte').val() != undefined ? mandte = this.fnyyyymmdd($('#mandte').val()) : mandte = ''
                    var mandst = $('#mandst').val() != null && $('#mandst').val() != undefined ? mandst = this.fnyyyymmdd($('#mandst').val()) : mandst = ''
                    var manprovedte = $('#manprovedte').val() != null && $('#manprovedte').val() != undefined ? manprovedte = this.fnyyyymmdd($('#manprovedte').val()) : manprovedte = ''
                    if ($('#mandiv').val() != null) {
                        this.ManPowerObject.mandiv = $('#mandiv').val().toString()
                    }
                    if ($('#mandep').val() != null) {
                        this.ManPowerObject.mandep = $('#mandep').val().toString()
                    }
                    if ($('#manpos').val() != null) {
                        this.ManPowerObject.manpos = $('#manpos').val().toString()
                    }
                    if ($('#manedu').val() != null) {
                        this.ManPowerObject.manedu = $('#manedu').val().toString()
                    }

                    this.ManPowerObject.mandte = mandte
                    this.ManPowerObject.mandst = mandst
                    this.ManPowerObject.manprovedte = manprovedte
                    if (this.Editing == 1) {
                        this.ManPowerServiceProxy.updateManPower(this.ManPowerObject).subscribe(() => {
                            this.ManPowerListServiceProxy.deleteManPowerListByDoc(this.ManPowerObject.mandoc).subscribe(() => {
                                setTimeout(() => {
                                    for (let i = 0; i < this.ManPowerListListDto.length; i++) {
                                        this.ManPowerListListDto[i].mandoc = this.ManPowerObject.mandoc

                                        if (this.ManPowerListListDto.length <= (i + 1 * 1)) {
                                            this.ManPowerListServiceProxy.createManPowerList(this.ManPowerListListDto).subscribe(() => {
                                                if(this.num2 == 1){
                                                    this.close3()
                                                    this.ManPowerObject.mandte = this.fnddmmyyyy($('#mandte').val())
                                                    this.ManPowerObject.mandst = this.fnddmmyyyy($('#mandst').val())
                                                    this.ManPowerObject.manprovedte =this.fnddmmyyyy($('#manprovedte').val()) 
                                                    // this.close()
                                                    // this.modalSave.emit()
                                                    // this.notify.info(this.l('SavedSuccessfully'));
                                                }else{
                                                this.close()
                                                this.modalSave.emit()
                                                this.notify.info(this.l('SavedSuccessfully'));
                                            }
                                            })
                                        }
                                    }
                                }, 50);
                            })
                        })
                    } else {
                        // this.ManPowerServiceProxy.createManPower(this.ManPowerObject).subscribe(() => {
                        //     this.close()
                        //     this.modalSave.emit()
                        //     this.notify.info(this.l('SavedSuccessfully'));
                        // })
                        this.KPShareServiceProxy.getDocumentNumber('MP', this.ManPowerObject.mandte, 'THI').subscribe((result) => {
                            this.ManPowerObject.mandoc = result[0].documentNumber
                            var dogs = result[0].documentNumber
                            this.ManPowerServiceProxy.createManPower(this.ManPowerObject).subscribe(() => {
                                setTimeout(() => {
                                    for (let i = 0; i < this.ManPowerListListDto.length; i++) {
                                        this.ManPowerListListDto[i].mandoc = dogs
                                        if (this.ManPowerListListDto.length <= (i + 1 * 1)) {
                                            this.ManPowerListServiceProxy.createManPowerList(this.ManPowerListListDto).subscribe(() => {
                                                this.close()
                                                this.modalSave.emit()
                                                this.notify.info(this.l('SavedSuccessfully'));
                                            })
                                        }
                                    }
                                }, 50);

                            })
                        })

                    }

                }
            })
    }
    number = 0
    manreA1doc
    manreA1no
    manreA1name
    manreA1sername
    manreA1day
    addlist() {

        var manreA1day = $('#manreA1day').val() != null && $('#manreA1day').val() != undefined ? manreA1day = this.fnyyyymmdd($('#manreA1day').val()) : manreA1day = ''
        // this.ManPowerListObject.mandoc 
        if ((this.ManPowerListListDto.length + 1) * 1 <= this.ManPowerObject.manqty) {
            this.number = (this.ManPowerListListDto.length + 1) * 1
            this.ManPowerListObject = new ManPowerListListDto()
            this.ManPowerListObject.mandte = manreA1day
            this.ManPowerListObject.manseq = this.number
            this.ManPowerListObject.manfnm = this.manreA1name
            this.ManPowerListObject.manlnm = this.manreA1sername
            this.ManPowerListObject.manRefId = 0
            this.ManPowerListListDto.push(this.ManPowerListObject)
            this.cler()
        } else (this.message.warn('เพิ่มจำนวนเกินอัตราที่กำหนดแล้วนะจ๊ะ'))
    }
    cler() {
        this.manreA1name = ''
        this.manreA1sername = ''
    }

    removed(e) {


        this.calseq()

    }
    calseq() {
        console.log(1111);
        this.number = 0
        setTimeout(() => {
            console.log(this.ManPowerListListDto.length)
            for (let i = 0; i < this.ManPowerListListDto.length; i++) {

                this.ManPowerListListDto[i].manseq = (i + 1 * 1)
                console.log(this.number);
            }
        }, 50);
    }
    removed2(){
        this.ManPowerServiceProxy.deleteManPowerByDoc(this.ManPowerObject.mandoc).subscribe(() => {
            this.ManPowerListServiceProxy.deleteManPowerListByDoc(this.ManPowerObject.mandoc).subscribe(() => {
                this.close()
                this.modalSave.emit()
                this.notify.info(this.l('DeleteSuccessfully'));
            })
        })
    }

    show(): void {
        this.Editing = 0
        var doc
        this.getItemMaster(doc)
        this.modal.show();
        this.active = true;
        this.ManPowerObject = new ManPowerListDto()
        setTimeout(() => {
            $('.m_select2_1a').select2({
                placeholder: "Please Select.."
            });
        }, 250);
    }

    close(): void {
        this.modal.hide();
        this.active = false;
        this.Editing = 0
        this.manprV3name = ''
        this.manhrname = ''
        this.manprV1name = ''
        this.manprV2name = ''
        this.manreA1name = ''
        this.manreA1sername =''
        this.manreA1day =''
    }
    /////////////////////////////////////////modal2
    chksho2;
    show2(id): void {
        this.chksho2 = id
        this.modal2.show();
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
    }
    manhrname
    manprV1name
    manprV3name
    manprV2name
    get_datan2(e) {
        console.log(e);

        if (this.chksho2 == '1') { this.ManPowerObject.manhr = e[0].psnidn, this.manhrname = e[0].psnfnm + ' ' + e[0].psnlnm, this.close2() }
        if (this.chksho2 == '2') { this.ManPowerObject.manprV1 = e[0].psnidn, this.manprV1name = e[0].psnfnm + ' ' + e[0].psnlnm, this.close2() }
        if (this.chksho2 == '3') { this.ManPowerObject.manprV3 = e[0].psnidn, this.manprV3name = e[0].psnfnm + ' ' + e[0].psnlnm, this.close2() }
        if (this.chksho2 == '4') { this.ManPowerObject.manprV2 = e[0].psnidn, this.manprV2name = e[0].psnfnm + ' ' + e[0].psnlnm, this.close2() }
    }
    close2(): void {
        this.modal2.hide();
        this.active = false;
    }
    show3(): void {
        this.modal3.show();
    }
    close3(): void {
        this.modal3.hide();
        this.active = false;
        this.manreA1name = ''
        this.manreA1sername =''
        this.manreA1day =''
    }
}

