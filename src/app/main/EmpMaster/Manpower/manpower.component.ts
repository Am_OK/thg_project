import { Component, OnInit, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DbmtabServiceProxy, DbmtabListDto, ManPowerServiceProxy, ManPowerListDto, MSMPSN00ServiceProxy, MSMPSN00ListDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { data_Empmas } from '../data_center';

@Component({
    selector: 'manpower',
    templateUrl: './manpower.component.html',
})
export class ManpowerComponent extends AppComponentBase implements OnInit {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    constructor(
        injector: Injector,
        private data_Empmas: data_Empmas,
        private ManPowerServiceProxy: ManPowerServiceProxy,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private _dbmtabService: DbmtabServiceProxy
    ) {
        super(injector);
    }
    atvc = { "data": [{ "tabtB2": "C", "tabdsc": "CANCEL", }, { "tabtB2": "A", "tabdsc": "ACTIVE", }] };
    Sex = { "data": [{ "tabtB2": "M", "tabdsc": "ชาย", }, { "tabtB2": "F", "tabdsc": "หญิง", }] };
    POSITION: DbmtabListDto[] = []
    DEPARTMENT: DbmtabListDto[] = []
    dbmtabemti: DbmtabListDto[] = []
    emp: MSMPSN00ListDto[] = []
    MOVERESON: DbmtabListDto[] = []
    dbmtab: DbmtabListDto[] = [];
    MARRIEDSTATUS: DbmtabListDto[] = [];
    EDUCATION: DbmtabListDto[] = [];
    MANPOWERTYP: DbmtabListDto[] = [];
    DIVISION: DbmtabListDto[] = [];
    show(): void {
        this.modal.show();
        this.active = true;


    }
    getmaster() {
        this._dbmtabService.getDbmtabFromTABTB1('EMPTITLE', '').subscribe((result) => {
            this.dbmtabemti = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('POSITION', '').subscribe((result) => {
            this.POSITION = result.items

        })
        this._dbmtabService.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
            this.DEPARTMENT = result.items;
        });
        this._dbmtabService.getDbmtabFromTABFG1('MOVERESON', '').subscribe((result) => {
            this.MOVERESON = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('POSITION', '').subscribe((result) => {
            this.dbmtab = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('MARRIEDSTATUS', '').subscribe((result) => {
            this.MARRIEDSTATUS = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('EDUCATION', '').subscribe((result) => {
            this.EDUCATION = result.items;
        });

        this._dbmtabService.getDbmtabFromTABTB1('MANPOWERTYP', '').subscribe((result) => {
            this.MANPOWERTYP = result.items;
        });
        this._dbmtabService.getDbmtabFromTABTB1('DIVISION', '').subscribe((result) => {
            this.DIVISION = result.items;
        });
    }
    ManPowerListDto: ManPowerListDto[] = []
    getManPower(frm, to, en, toen) {
        this.ManPowerServiceProxy.getManPowerFromTo(frm, to, en, toen).subscribe((result) => {
            this.ManPowerListDto = result.items
        })
    }
    // emp: MSMPSN00ListDto[] = []
    getEmployees(): void {
        this.MSMPSN00ServiceProxy.getMSMPSN00('').subscribe((result) => {
            this.emp = result.items;
        });
    }
    ngOnInit(): void {
        this.getmaster()
        this.getManPower('', 'ALL', '', 'ALL')
        this.getEmployees()
        $('#DocumentFromTo').val('ALL') 
         $('#DepartmentTo').val('ALL') 
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}

