import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-payroll-work-sheet-report',
  templateUrl: './print-payroll-work-sheet-report.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintPayrollWorkSheetReportComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
