import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'TransferPayrollDataToBank',
  templateUrl: './TransferPayrollDataToBank.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class TransferPayrollDataToBankComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.m_select2_1a').select2({
      placeholder: "Please Select.."
  });
  }

}