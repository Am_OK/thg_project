import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-monthly-report-for-social-department',
  templateUrl: './print-monthly-report-for-social-department.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintMonthlyReportForSocialDepartmentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
