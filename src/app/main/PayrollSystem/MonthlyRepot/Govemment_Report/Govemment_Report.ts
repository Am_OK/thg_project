import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';


@Component({
  selector: 'Govemment_Report',
  templateUrl: './Govemment_Report.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class GovemmentReportComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
