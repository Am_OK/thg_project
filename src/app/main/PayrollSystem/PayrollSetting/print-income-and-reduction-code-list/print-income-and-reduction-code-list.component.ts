import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-income-and-reduction-code-list',
  templateUrl: './print-income-and-reduction-code-list.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintIncomeAndReductionCodeListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
