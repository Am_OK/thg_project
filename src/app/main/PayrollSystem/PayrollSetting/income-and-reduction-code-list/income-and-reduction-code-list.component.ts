import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-income-and-reduction-code-list',
  templateUrl: './income-and-reduction-code-list.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class IncomeAndReductionCodeListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
