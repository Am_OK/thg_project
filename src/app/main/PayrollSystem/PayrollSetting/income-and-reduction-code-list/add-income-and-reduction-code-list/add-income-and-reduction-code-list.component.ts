import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-add-income-and-reduction-code-list',
  templateUrl: './add-income-and-reduction-code-list.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class AddIncomeAndReductionCodeListComponent extends AppComponentBase implements OnInit {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal_rate') modal_rate: ModalDirective;
  @ViewChild('modal_employee') modal_employee: ModalDirective;
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }
  ngOnInit() {

  }
  show(): void {
    this.modal_rate.show()
  }

  show_modal_employee() {
    this.modal_rate.hide()
    this.modal_employee.show()
  }

  save() {

  }

  close(data) {
    if (data == "rate") {
      this.modal_rate.hide()
    } else if(data == "employee"){
      this.modal_employee.hide()
      this.modal_rate.show()
    }
  }
}