import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-reduction-rate',
  templateUrl: './print-reduction-rate.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintReductionRateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
