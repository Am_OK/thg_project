import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-social-rate',
  templateUrl: './print-social-rate.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintSocialRateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
