import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-tax-rate',
  templateUrl: './print-tax-rate.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintTaxRateComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
