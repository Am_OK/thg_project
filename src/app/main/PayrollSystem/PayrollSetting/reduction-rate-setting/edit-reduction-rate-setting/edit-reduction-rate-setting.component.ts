import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-edit-reduction-rate-setting',
  templateUrl: './edit-reduction-rate-setting.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class EditReductionRateSettingComponent extends AppComponentBase implements OnInit {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal2') modal2: ModalDirective;
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit() {
  }
  show(): void {
    this.modal2.show()
  }

  save() {

  }

  close(data) {
    this.modal2.hide()
  }
  
}
