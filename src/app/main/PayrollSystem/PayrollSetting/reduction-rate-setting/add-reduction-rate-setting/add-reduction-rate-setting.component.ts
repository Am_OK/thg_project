import { Component, OnInit, Output, EventEmitter, ViewChild, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'app-add-reduction-rate-setting',
  templateUrl: './add-reduction-rate-setting.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class AddReductionRateSettingComponent extends AppComponentBase implements OnInit {

  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('modal') modal: ModalDirective;
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit() {
  }
  show(): void {
    this.modal.show()
  }

  save() {

  }

  close(data) {
    this.modal.hide()
  }
  
}
