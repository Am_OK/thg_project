import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-reduction-rate-setting',
  templateUrl: './reduction-rate-setting.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class ReductionRateSettingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
