import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-social-rate-setting',
  templateUrl: './social-rate-setting.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class SocialRateSettingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
