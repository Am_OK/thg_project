import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-tax-rate-setting',
  templateUrl: './tax-rate-setting.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class TaxRateSettingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
