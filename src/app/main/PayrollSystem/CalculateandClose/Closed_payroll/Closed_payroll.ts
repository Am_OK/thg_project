import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'Closed_payroll',
  templateUrl: './Closed_payroll.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class ClosedpayrollComponent implements OnInit {

  constructor() { }

  progressbarGen
  ngOnInit() {
    this.progressbarGen = "0%"
    document.getElementById("pbb").style.width = this.progressbarGen
  }
  submit() {
    this.progressbarGen = "100%"
    document.getElementById("pbb").style.width = this.progressbarGen
    setTimeout(() => {
      this.progressbarGen = "0%"
      document.getElementById("pbb").style.width = this.progressbarGen
    }, 5000);
  }

}
