import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-cancel-calculated-payroll-of-period',
  templateUrl: './cancel-calculated-payroll-of-period.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class CancelCalculatedPayrollOfPeriodComponent implements OnInit {

  constructor() { }

  progressbarGen
  ngOnInit() {
    this.progressbarGen = "0%"
    document.getElementById("pbb").style.width = this.progressbarGen
  }
  submit() {
    this.progressbarGen = "100%"
    document.getElementById("pbb").style.width = this.progressbarGen
    setTimeout(() => {
      this.progressbarGen = "0%"
      document.getElementById("pbb").style.width = this.progressbarGen
    }, 5000);
  }

}
