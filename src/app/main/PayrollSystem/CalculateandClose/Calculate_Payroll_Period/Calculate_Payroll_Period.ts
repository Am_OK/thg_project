import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';

@Component({
  selector: 'Calculate_Payroll_Period',
  templateUrl: './Calculate_Payroll_Period.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class CalculatePayrollPeriodComponent implements OnInit {

  constructor() { }

  progressbarGen
  ngOnInit() {
    this.progressbarGen = "0%"
    document.getElementById("pbb").style.width = this.progressbarGen
  }
  submit() {
    this.progressbarGen = "100%"
    document.getElementById("pbb").style.width = this.progressbarGen
    setTimeout(() => {
      this.progressbarGen = "0%"
      document.getElementById("pbb").style.width = this.progressbarGen
    }, 5000);
  }

}
