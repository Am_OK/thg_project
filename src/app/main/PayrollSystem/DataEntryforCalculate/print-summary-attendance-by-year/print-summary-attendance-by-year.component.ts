import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-summary-attendance-by-year',
  templateUrl: './print-summary-attendance-by-year.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintSummaryAttendanceByYearComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
