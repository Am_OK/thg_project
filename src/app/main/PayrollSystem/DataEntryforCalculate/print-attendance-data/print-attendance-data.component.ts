import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-print-attendance-data',
  templateUrl: './print-attendance-data.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class PrintAttendanceDataComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
