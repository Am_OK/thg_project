import { Component, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-summary-income-and-reduce-by-year',
  templateUrl: './summary-income-and-reduce-by-year.component.html',
  styles: [],
  animations: [appModuleAnimation()]
})
export class SummaryIncomeAndReduceByYearComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
