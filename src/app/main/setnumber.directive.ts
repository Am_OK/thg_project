import { Directive, Input, Output, EventEmitter } from '@angular/core';
import { type } from 'os';

@Directive({
  selector: '[setnumber]',
  host: {
    '[value]': 'setnumber',
    '(input)': 'format($event.target.value)'
  }
})
export class SetnumberDirective {
  @Input() setnumber: string;
  @Output() setnumberChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit() {
    this.format(this.setnumber);
  }

  format(value) {
    // console.log("-->" + value);

    // undefined

    if (value == undefined || value == null) {

      if(typeof value == 'number'){
        setTimeout(() => {
          var a = parseFloat((value.toString()).replace(/,/g, ""))
            // .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          if (a == 'NaN') {
            a = '0.00';
          }
          this.setnumberChange.next(a);
        }, 1000);
      }else if(typeof value == 'string'){
        setTimeout(() => {
          var a = parseFloat(value.replace(/,/g, ""))
            // .toFixed(2)
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          if (a == 'NaN') {
            a = '0.00';
          }
          this.setnumberChange.next(a);
        }, 1000);
      }
     
    }

    if(typeof value == 'number'){
      setTimeout(() => {
        var a = parseFloat((value.toString()).replace(/,/g, ""))
          // .toFixed(2)
          .toString()
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (a == 'NaN') {
          a = '0.00';
        }
        this.setnumberChange.next(a);
      }, 1000);
    }else if(typeof value == 'string'){
      setTimeout(() => {
        var a = parseFloat(value.replace(/,/g, ""))
          // .toFixed(2)
          .toString()
          .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (a == 'NaN') {
          a = '0.00';
        }
        this.setnumberChange.next(a);
      }, 1000);
    }
   

  }
}
