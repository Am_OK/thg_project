import { Component, Injector, OnInit, ViewEncapsulation, NgZone, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { NotificationServiceProxy, UserNotification, MSMPSN00ServiceProxy, MSMPSN00ListDto, JobCertificatesServiceProxy, JobCertificatesListDto, KPShareServiceProxy, GetExprieDocumentList, DbmtabListDto, DbmtabServiceProxy, EnglishCertificatesServiceProxy, EnglishCertificatesListDto } from '@shared/service-proxies/service-proxies';
import { IFormattedUserNotification, UserNotificationHelper } from './UserNotificationHelper';
import * as _ from 'lodash';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap';
import { DxDataGridComponent } from 'devextreme-angular';


@Component({
    templateUrl: './header-notifications.component.html',
    selector: '[headerNotifications]',
    encapsulation: ViewEncapsulation.None
})
export class HeaderNotificationsComponent extends AppComponentBase implements OnInit {

    notifications: IFormattedUserNotification[] = [];
    unreadNotificationCount = 0;

    constructor(
        injector: Injector,
        private _notificationService: NotificationServiceProxy,
        private _userNotificationHelper: UserNotificationHelper,
        private MSMPSN00ServiceProxy: MSMPSN00ServiceProxy,
        private JobCertificatesServiceProxy: JobCertificatesServiceProxy,
        private KPShareServiceProxy: KPShareServiceProxy,
        private DbmtabServiceProxy: DbmtabServiceProxy,
        private EnglishCertificatesServiceProxy: EnglishCertificatesServiceProxy,
        public _zone: NgZone
    ) {
        super(injector);
    }

    ngOnInit(): void {
        // this.loadNotifications();
        // this.registerToEvents();
        this.getEmployee()

    }
    emp: MSMPSN00ListDto = new MSMPSN00ListDto;
    noti = []
    DEPARTMENT: DbmtabListDto[] = []
    EnglishCertificatesListDto: EnglishCertificatesListDto[] = []
    ExprieDocumentList: GetExprieDocumentList[] = []
    NOTIF: DbmtabListDto[] = []
    NOTIFHR: DbmtabListDto[] = []
    em = new DbmtabListDto()
    ce = new DbmtabListDto()
    emHR = new DbmtabListDto()
    ceHR = new DbmtabListDto()
    getEmployee() {

        var dtetoday = new Date()
        this.MSMPSN00ServiceProxy.getMSMPSN00(this.appSession.user.userName).subscribe((result) => {
            this.emp = result.items[0]
            if (this.emp.psndep == '10HR') {
                this.DbmtabServiceProxy.getDbmtabFromTABFG1('DEPARTMENT', '').subscribe((result) => {
                    this.DEPARTMENT = result.items
                    this.KPShareServiceProxy.getExprieDocumentList().subscribe((result2) => {
                        this.ExprieDocumentList = result2
                        this.modalnoti(2)
                        this.DbmtabServiceProxy.getDbmtabFromTABFG1('NOTIFICATIONHR', '').subscribe((resultnotiHR) => {
                            this.NOTIFHR = resultnotiHR.items
                            this.emHR = this.NOTIFHR.filter(res => res.tabtB2 == 'EMP')[0]
                            this.ceHR = this.NOTIFHR.filter(res => res.tabtB2 == 'CER')[0]
                        })
                    })
                })
            }
            this.DbmtabServiceProxy.getDbmtabFromTABFG1('NOTIFICATION', '').subscribe((resultnoti) => {
                this.NOTIF = resultnoti.items
                this.em = this.NOTIF.filter(res => res.tabtB2 == 'EMP')[0]
                this.ce = this.NOTIF.filter(res => res.tabtB2 == 'CER')[0]
                this.JobCertificatesServiceProxy.getJobCertificatesByEmpId(this.appSession.user.userName).subscribe((result2) => {
                    this.JobCertificatesListDto = result2.items
                    this.EnglishCertificatesServiceProxy.getEnglishCertificatesByEmpId(this.appSession.user.userName).subscribe((result3) => {
                        this.EnglishCertificatesListDto = result3.items
                        if (result.items.length > 0) {
                            if (this.emp.psniep != null && this.emp.psniep != '') {

                                var dte1 = new Date(this.emp.psniep.substr(0, 4) + '-' + this.emp.psniep.substr(4, 2) + '-' + this.emp.psniep.substr(6, 2))
                                var dtex1 = Math.abs(dte1.getTime() - dtetoday.getTime());
                                var diffDays1 = Math.ceil(dtex1 / (1000 * 60 * 60 * 24));
                                if (diffDays1 <= this.em.tabaM1) {
                                    var dte = this.fnddmmyyyy(this.emp.psniep)
                                    this.noti.push({ 'text': 'บัตรประชาชนจะหมดอายุในวันที่ ' + dte })
                                }
                            }

                            if (this.emp.psndrexr != null && this.emp.psndrexr != '') {

                                var dte2 = new Date(this.emp.psndrexr.substr(0, 4) + '-' + this.emp.psndrexr.substr(4, 2) + '-' + this.emp.psndrexr.substr(6, 2))
                                var dtex2 = Math.abs(dte2.getTime() - dtetoday.getTime());
                                var diffDays2 = Math.ceil(dtex2 / (1000 * 60 * 60 * 24));
                                if (diffDays2 <= this.em.tabaM2) {
                                    var dte = this.fnddmmyyyy(this.emp.psndrexr)
                                    this.noti.push({ 'text': 'ใบขับขี่รถยนต์จะหมดอายุในวันที่ ' + dte })
                                }
                            }

                            if (this.emp.psndrexR2 != null && this.emp.psndrexR2 != '') {

                                var dte3 = new Date(this.emp.psndrexR2.substr(0, 4) + '-' + this.emp.psndrexR2.substr(4, 2) + '-' + this.emp.psndrexR2.substr(6, 2))
                                var dtex3 = Math.abs(dte3.getTime() - dtetoday.getTime());
                                var diffDays3 = Math.ceil(dtex3 / (1000 * 60 * 60 * 24));
                                if (diffDays3 <= this.em.tabrT1) {
                                    var dte = this.fnddmmyyyy(this.emp.psndrexR2)
                                    this.noti.push({ 'text': 'ใบขับขี่รถจักรยานยนต์จะหมดอายุในวันที่ ' + dte })
                                }
                            }
                            for (let i = 0; i < this.JobCertificatesListDto.length; i++) {
                                if (this.JobCertificatesListDto[i].cerexp != null && this.JobCertificatesListDto[i].cerexp != '') {

                                    var dtes = new Date(this.JobCertificatesListDto[i].cerexp.substr(0, 4) + '-' + this.JobCertificatesListDto[i].cerexp.substr(4, 2) + '-' + this.JobCertificatesListDto[i].cerexp.substr(6, 2))
                                    var dtexs = Math.abs(dtes.getTime() - dtetoday.getTime());
                                    var diffDayss = Math.ceil(dtexs / (1000 * 60 * 60 * 24));
                                    if (diffDayss <= this.ce.tabaM1) {
                                        var dte = this.fnddmmyyyy(this.JobCertificatesListDto[i].cerexp)
                                        this.noti.push({ 'text': 'JobCertificate ' + this.JobCertificatesListDto[i].cerno + ' จะหมดอายุในวันที่ ' + dte })
                                    }
                                }

                            }

                            for (let i = 0; i < this.EnglishCertificatesListDto.length; i++) {
                                if (this.EnglishCertificatesListDto[i].rentod != null && this.EnglishCertificatesListDto[i].rentod != '') {

                                    var dtes2 = new Date(this.EnglishCertificatesListDto[i].rentod.substr(0, 4) + '-' + this.EnglishCertificatesListDto[i].rentod.substr(4, 2) + '-' + this.EnglishCertificatesListDto[i].rentod.substr(6, 2))
                                    var dtexs2 = Math.abs(dtes2.getTime() - dtetoday.getTime());
                                    var diffDayss2 = Math.ceil(dtexs2 / (1000 * 60 * 60 * 24));
                                    if (diffDayss2 <= this.ce.tabaM2) {
                                        var dte = this.fnddmmyyyy(this.EnglishCertificatesListDto[i].rentod)
                                        this.noti.push({ 'text': 'LanguageCertificate ' + this.EnglishCertificatesListDto[i].rencod + ' จะหมดอายุในวันที่ ' + dte })
                                    }
                                }

                            }

                            this.unreadNotificationCount = this.noti.length
                            if (this.unreadNotificationCount > 0) {
                                this.fnnoti(1)
                                if (this.emp.psndep != '10HR') {
                                    this.modalnoti(1)
                                }
                            }
                        }
                    })
                })
            })

        })

    }
    JobCertificatesListDto: JobCertificatesListDto[] = []
    fnnoti(v) {
        var tm = v
        if (v == 1) {
            setTimeout(() => {
                $('#shake').addClass('m-animate-shake');
            }, 1500);
            setTimeout(() => {
                $('#shake').removeClass('m-animate-shake');
                this.fnnoti(tm)
            }, 3000);
        } else {
            $('#shake').removeClass('m-animate-shake');
        }
    }

    loadNotifications(): void {
        this._notificationService.getUserNotifications(undefined, 3, 0).subscribe(result => {
            this.unreadNotificationCount = result.unreadCount;
            this.notifications = [];
            _.forEach(result.items, (item: UserNotification) => {
                this.notifications.push(this._userNotificationHelper.format(<any>item));
            });
        });
    }

    registerToEvents() {
        let self = this;

        function onNotificationReceived(userNotification) {
            self._userNotificationHelper.show(userNotification);
            self.loadNotifications();
        }

        abp.event.on('abp.notifications.received', userNotification => {
            self._zone.run(() => {
                onNotificationReceived(userNotification);
            });
        });

        function onNotificationsRefresh() {
            self.loadNotifications();
        }

        abp.event.on('app.notifications.refresh', () => {
            self._zone.run(() => {
                onNotificationsRefresh();
            });
        });

        function onNotificationsRead(userNotificationId) {
            for (let i = 0; i < self.notifications.length; i++) {
                if (self.notifications[i].userNotificationId === userNotificationId) {
                    self.notifications[i].state = 'READ';
                }
            }

            self.unreadNotificationCount -= 1;
        }

        abp.event.on('app.notifications.read', userNotificationId => {
            self._zone.run(() => {
                onNotificationsRead(userNotificationId);
            });
        });
    }

    setAllNotificationsAsRead(): void {
        this._userNotificationHelper.setAllAsRead();
    }

    openNotificationSettingsModal(): void {
        this._userNotificationHelper.openSettingsModal();
    }

    setNotificationAsRead(userNotification: IFormattedUserNotification): void {
        this._userNotificationHelper.setAsRead(userNotification.userNotificationId);
    }

    gotoUrl(url): void {
        if (url) {
            location.href = url;
        }
    }
    VRMODAL;
    @ViewChild('modal') modal: ModalDirective;
    active: boolean;
    modalnoti(v) {
        this.VRMODAL = v
        this.active = true
        setTimeout(() => {
            this.modal.show()
        }, 100);

    }
    close() {
        this.modal.hide()
        this.active = false
    }
    shownoti() {
        this.VRMODAL = 2
        this.active = true
        // setTimeout(() => {
        this.modal.show()
        // }, 100);


    }

    @ViewChild('modal4') modal4: ModalDirective;
    NOTIFICATIONEMP: DbmtabListDto = new DbmtabListDto()
    NOTIFICATIONCER: DbmtabListDto = new DbmtabListDto()
    showsetting() {
        this.modal4.show()
    }
    close4() {
        this.modal4.hide()
    }
    savesetting() {
        this.message.confirm(
            this.l('AreYouSureToSaveTheData'),
            isConfirmed => {
                if (isConfirmed) {
                    this.DbmtabServiceProxy.updateDbmtab(this.emHR).subscribe(() => {
                        this.DbmtabServiceProxy.updateDbmtab(this.ceHR).subscribe(() => {
                           
                        })
                    })
                    this.DbmtabServiceProxy.updateDbmtab(this.em).subscribe(() => {
                        this.DbmtabServiceProxy.updateDbmtab(this.ce).subscribe(() => {
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.close4()
                        })
                    })
                }
            })


    }
}
