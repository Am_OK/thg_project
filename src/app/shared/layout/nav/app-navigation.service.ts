import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import { AppSessionService } from '@shared/common/session/app-session.service';

import { Injectable } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';

@Injectable()
export class AppNavigationService {

    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService
    ) {

    }

    getMenu(): AppMenu {

        return new AppMenu('MainMenu', 'MainMenu', [
            new AppMenuItem('Dashboard', 'Pages.Administration.Host.Dashboard', 'flaticon-line-graph', '/app/admin/hostDashboard'),
            new AppMenuItem('Dashboard', '', 'flaticon-line-graph', '/app/main/dashboard'),

            new AppMenuItem("Dashboard SUTH", "", "flaticon-line-graph", "", [
                new AppMenuItem("ผู้รับบริการ", "", "flaticon-line-graph", "", [
                    new AppMenuItem("IPD (ผู้ป่วยใน)", "Pages.DashBoardModule.IPDDashBoard", "flaticon-line-graph", "/app/main/dashboardclientipd"),
                    new AppMenuItem("OPD (ผู้ป่วยนอก)", "Pages.DashBoardModule.OPDDashBoard", "flaticon-line-graph", "/app/main/dashboardclientopd")
                ]),
                new AppMenuItem("งบดุล", "Pages.DashBoardModule.ACCDashBoard", "flaticon-line-graph", "/app/main/dashboardbala")
            ]),

            new AppMenuItem('Tenants', 'Pages.Tenants', 'flaticon-list-3', '/app/admin/tenants'),
            new AppMenuItem('Editions', 'Pages.Editions', 'flaticon-app', '/app/admin/editions'),
            new AppMenuItem("TestRunProgram", '', "flaticon-book", "/app/main/phonebook"),
            // new AppMenuItem("BudgetControlSetting", "", "flaticon-cogwheel", "", [
            //          new AppMenuItem("ChartOfAccount", "Pages.BudgetControlSetting.ChartOfAccount", "flaticon-edit", "/app/main/gl"),
            //          new AppMenuItem("BudgetName", "Pages.BudgetControlSetting.BudgetName", "flaticon-edit", "/app/main/BudgetSetting"),
            //          new AppMenuItem("BudgetPeriod", "Pages.BudgetControlSetting.BudgetPeriod", "flaticon-edit", "/app/main/BudgetPeriod"),
            //          new AppMenuItem("DepartmentMaster", "Pages.BudgetControlSetting.DepartmentMaster", "flaticon-edit", "/app/main/DepartmentMaster"),  
            //          new AppMenuItem("AddEstRequestBudget", "Pages.BudgetControlSetting.AddEstRequestBudget", "flaticon-edit", "/app/main/AddEstRequestBudget")
            // ]),

            new AppMenuItem("VersionCheck", '', "flaticon-multimedia-1", "/app/main/VersionCheck"),
            // new AppMenuItem("GeneralLedger", "", "flaticon-folder-1", "", [            
            //     new AppMenuItem("GeneralJournalEntry", "Pages.GeneralLedger.GeneralJournalEntry", "flaticon-list", "/app/main/AddRequestBudget"),                
            // ]),
            // new AppMenuItem("TransactionReceivingSystem", "", "flaticon-piggy-bank", "", [            
            //     new AppMenuItem("CustomerMaster", "Pages.TransactionReceivingSystem.CustomerMaster", "flaticon-users", "/app/main/AddRequestBudget"),
            //     new AppMenuItem("FinanceReceive", "Pages.TransactionReceivingSystem.FinanceReceive", "flaticon-piggy-bank", "/app/main/AddRequestBudget")
            // ]),
            // new AppMenuItem("PaymentandPettyCash", "", "flaticon-edit", "", [            
            //     new AppMenuItem("VendorMaster", "Pages.PaymentandPettyCash.VendorMaster", "flaticon-users", "/app/main/VendorMaster"),
            //     new AppMenuItem("FinancePayment", "Pages.PaymentandPettyCash.FinancePayment", "flaticon-edit", "/app/main/FinancePayment")
            // ]),
            new AppMenuItem("AccountingModule", "", "flaticon-graph", "", [
                new AppMenuItem("InterfaceData", "Pages.AccountingModule.InterfaceData", "flaticon-multimedia-1", "/app/main/AccInterfaceData"),
                new AppMenuItem("TransactionReceivingSystem", "Pages.AccountingModule.TransactionReceiving", "flaticon-piggy-bank", "/app/main/TransactionReceiving"),
                new AppMenuItem("CustomerMasterInformation", "Pages.AccountingModule.CustomerMaster", "flaticon-users", "/app/main/CustomerMaster"),
                new AppMenuItem("VendorSupplierInformation", "Pages.AccountingModule.VendorSupplier", "flaticon-users", "/app/main/VendorMasterWeb"),
                new AppMenuItem("TaxCenter", "Pages.AccountingModule.TaxCenter", "flaticon-list-1", "/app/main/TaxCenter"),
                new AppMenuItem("AssetsManagement", "Pages.AccountingModule.Assets", "flaticon-rocket", "/app/main/AssetManagement"),
                new AppMenuItem("PaymentandPettyCash", "Pages.AccountingModule.PaymentandPetty", "flaticon-edit", "/app/main/PaymentandPTT"),
                new AppMenuItem("InventoryandWarehouse", "Pages.AccountingModule.Inventory", "flaticon-truck", "/app/main/InventoryandWarehouse"),
                new AppMenuItem("BankandChequeCenter", "Pages.AccountingModule.BankandCheque", "flaticon-coins", "/app/main/ChqueAndBank"),
                new AppMenuItem("GeneralLedger", "Pages.AccountingModule.GeneralLedger", "flaticon-analytics", "/app/main/GL"),
                new AppMenuItem("GlobalandApplicationSetting", "Pages.AccountingModule.GlobalSetting", "flaticon-settings-1", "/app/main/GlobalSetting"),
            ]),
            new AppMenuItem("HRModule", "", "flaticon-users", "", [
                new AppMenuItem("EmpMaster", "", "flaticon-presentation-1", "", [
                    new AppMenuItem("Position & JobDescription", "Pages.HRModule.EmployeeMaster", "flaticon-customer", "/app/main/Setcodeposition"),
                    new AppMenuItem("Employee Master", "Pages.HRModule.EmployeeMaster", "flaticon-users", "/app/main/EmployeeMaster"),
                    new AppMenuItem("Employee Master Information", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "",[
                        new AppMenuItem("รายงานสรุปยอดพนักงาน", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportsumEmp"),
                        new AppMenuItem("รายงานสรุปยอดพนักงานตามตำแหน่ง", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportsumEmpposition"),
                        new AppMenuItem("รายงานพนักงานที่จะถึง Probation", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportProbation"),
                        new AppMenuItem("รายงานทะเบียนลูกจ้าง", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportRegistration"),
                        new AppMenuItem("รายงานเข้าออกประจำเดือน", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportEmpRegister"),
                        new AppMenuItem("รายงานรายชื่อพนักงาน", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportEmpListing"),
                        new AppMenuItem("รายชื่อพนักงานที่ใบประกอบวิชาชีพใกล้หมดอายุ", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportCertificate"),
                        new AppMenuItem("รายงานตัวชี้วัดอัตรากำลัง", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/ReportManpower"),

                    ]),
                    new AppMenuItem("Commendation Letter", "Pages.HRModule.EmployeeMaster", "flaticon-trophy", "/app/main/Commendationletter"),
                    new AppMenuItem("Employee Resign", "Pages.HRModule.EmployeeMaster", "flaticon-logout", "",[
                        new AppMenuItem("บันทึกข้อมูลพนักงานที่ลาออก", "Pages.HRModule.EmployeeMaster", "flaticon-logout", "/app/main/Employeeresign"),
                        new AppMenuItem("รายงานอัตราพนักงานลาออกประจำปี", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/PrintEmpResign"),

                    ]),
                    new AppMenuItem("Employee Movement", "Pages.HRModule.EmployeeMaster", "flaticon-network", "",[
                        new AppMenuItem("บันทึกพนักงานที่ย้ายหน่วยงาน", "Pages.HRModule.EmployeeMaster", "flaticon-network", "/app/main/empmove"),
                        new AppMenuItem("รายงานการย้ายหน่วยงานประจำวัน", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/PrintEmpMove"),

                    ]),
                    new AppMenuItem("Employee Notice", "Pages.HRModule.EmployeeMaster", "flaticon-exclamation", "/app/main/Employeenotice"),
                    new AppMenuItem("Important Letter", "Pages.HRModule.EmployeeMaster", "flaticon-email", "/app/main/importantletter"),
                    new AppMenuItem("Manpower", "Pages.HRModule.EmployeeMaster", "flaticon-user-add", "/app/main/Manpower"),
                    new AppMenuItem("Recruitment", "Pages.HRModule.EmployeeMaster", "flaticon-profile", "",[
                        new AppMenuItem("Add Recruitment", "Pages.HRModule.EmployeeMaster", "flaticon-profile-1", "/app/main/Recruitment"),
                        new AppMenuItem("Print Recruitment", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/Printrecruitment"),

                    ]),
                    new AppMenuItem("Employee Benefits", "Pages.HRModule.EmployeeMaster", "flaticon-user-add", "",[
                        new AppMenuItem("Benefits Report", "Pages.HRModule.EmployeeMaster", "flaticon-notes", "/app/main/BenefitsReport"),
                    ]),

                    
                ]),
                new AppMenuItem("EmployeeWorkPlan", "Pages.HRModule.EmployeeWorkPlan", "flaticon-calendar-3", "/app/main/Workplan"),
                new AppMenuItem("TimeandAttendanceHR", "Pages.HRModule.TimeandAttendanceHR", "flaticon-clock-1", "/app/main/TimeandAttendanceHR"),
                new AppMenuItem("TimeandAttendance", "Pages.HRModule.TimeandAttendance", "flaticon-clock-1", "/app/main/TimeandAttendance"),
                new AppMenuItem("PayrollSystem", "Pages.HRModule.PayrollSystem", "flaticon-coins", "/app/main/Payroll"),
                new AppMenuItem("TrainingandDevelopment", "Pages.HRModule.TrainingandDevelopment", "flaticon-computer", "/app/main/TrainingandDevelopment"),
                new AppMenuItem("PerformanceAppraisalSystem", "Pages.HRModule.PerformanceAppraisalSystem", "flaticon-list-1", "/app/main/PerformanceAppraisal"),
                new AppMenuItem("AppraisalSystem", "Pages.HRModule.AppraisalSystem", "flaticon-piggy-bank", "/app/main/AppraisalSystem")
            ]),
            new AppMenuItem("DFModule", "", "flaticon-profile-1", "", [
                new AppMenuItem("InterfaceDoctorData", "Pages.DFModule.InterfaceDoctorData", "flaticon-multimedia-1", "/app/main/InterfaceDF"),
                new AppMenuItem("DoctorMasterSystem", "Pages.DFModule.DoctorMasterSystem", "flaticon-profile-1", "/app/main/DoctorMaster"),
                new AppMenuItem("DoctorFeeSystem", "Pages.DFModule.DoctorFeeSystem", "flaticon-interface", "/app/main/DoctorFee"),
                new AppMenuItem("DoctorPaySystem", "Pages.DFModule.DoctorPaySystem", "flaticon-coins", "/app/main/DoctorPay")
            ]),
            new AppMenuItem('Manual', "", 'flaticon-book', "/app/main/UserManual"),
            // new AppMenuItem("BudgetControlSetting", "", "icon-wrench", "", [
            //     new AppMenuItem("ChartOfAccount", null, "icon-settings", "/app/main/gl"),
            //     new AppMenuItem("BudgetName", null, "icon-settings", "/app/main/BudgetSetting"),
            //     new AppMenuItem("BudgetPeriod", null, "icon-settings", "/app/main/BudgetPeriod"),
            //     new AppMenuItem("DepartmentMaster", null, "icon-notebook", "/app/main/DepartmentMaster"),  
            //     new AppMenuItem("AddEstRequestBudget", null, "icon-note", "/app/main/AddEstRequestBudget"),
            //     new AppMenuItem("VendorMaster", null, "icon-notebook", "/app/main/VendorMaster")                       
            // ]),
            new AppMenuItem("Training System", "", "flaticon-presentation", "", [
                new AppMenuItem('Setup Training', "Pages.HRModule.TrainingSystem", 'flaticon-cogwheel', '', [
                    new AppMenuItem("Set Up Budget", "Pages.HRModule.TrainingSystem", "flaticon-settings-1", "/app/main/Setupbudget"),
                    new AppMenuItem("SetUp Trainingprogram", "Pages.HRModule.TrainingSystem", "flaticon-settings-1", "/app/main/Setuptraining"),

                ]),
                new AppMenuItem('Training Information', "", 'flaticon-presentation-1', '', [
                    new AppMenuItem("Add TrainingMaster", "Pages.HRModule.TrainingInformation", "flaticon-add", "/app/main/Traininginformation"),
                    new AppMenuItem("Training Search", "Pages.HRModule.TrainingInformation", "flaticon-search", "/app/main/Trainingsearch"),
                    new AppMenuItem("สมัครเข้าอบรม", "Pages.HRModule.TrainingInformation", "flaticon-user-add", "/app/main/Trainingregister"),

                ]),
                new AppMenuItem('Summary Report', "", 'flaticon-clipboard', '', [                 
                    new AppMenuItem("ใบลงทะเบียนผู้รับฝึกอบรม", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/Reportregister"),
                    new AppMenuItem("ประวัติการฝึกอบรมพนักงาน", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/ReportEmpHistory"),
                    new AppMenuItem("แผนการฝึกอบรมในงานตามแผนก", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/ReportPlantraining"),
                    new AppMenuItem("พิมพ์ใบบันทึกประวัติฝึกอบรมในงาน(OJT)", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/ReportHistoryOJT"),
                    new AppMenuItem("รายงานฝึกอบรมพนักงาน", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/ReportTrainingemp"),
                    // new AppMenuItem("รายงานจำนวนผู้ฝึกอบรมที่มียอดรวมจำนวน 12 ชั่วโมงขึ้นไป", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/ReportnumEmptrain"),
                    // new AppMenuItem("รายงานสรุปค่าใช้จ่ายฝึกอบรม", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/Reportsumpaytrain"),
                    // new AppMenuItem("รายงานจำนวนชั่วโมงฝึกอบรมประจำปี", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/ReportHrstrainyear"),
                    // new AppMenuItem("รายงานค่าใช้จ่ายฝึกอบรมต่างๆ", "Pages.Administration", "flaticon-notes", "/app/main/Reporttrainingpay"),
                    new AppMenuItem("รายงานหลักสูตรอบรมประจำเดือน", "Pages.HRModule.TrainingReport", "flaticon-notes", "/app/main/ReportCoursemonth"),
                    // new AppMenuItem("ใบเบิกเงินสำรองจ่าย", "Pages.Administration", "flaticon-notes", "/app/main/ReportTakereserve" ),
                    // new AppMenuItem("รายงานฝึกอบรมรายเดือน", "Pages.Administration", "flaticon-notes", "/app/main/ReportTrainingmonth" ),
                    // new AppMenuItem("รายละเอียดค่าใช้จ่ายประจำเดือน", "Pages.Administration", "flaticon-notes", "/app/main/ReportDetailpaymonth"),

                ]),
            ]),
            new AppMenuItem('Administration', '', 'flaticon-interface-8', '', [
                new AppMenuItem('OrganizationUnits', 'Pages.Administration.OrganizationUnits', 'flaticon-map', '/app/admin/organization-units'),
                new AppMenuItem('Roles', 'Pages.Administration.Roles', 'flaticon-suitcase', '/app/admin/roles'),
                new AppMenuItem('Users', 'Pages.Administration.Users', 'flaticon-users', '/app/admin/users'),
                new AppMenuItem('Languages', 'Pages.Administration.Languages', 'flaticon-tabs', '/app/admin/languages'),
                new AppMenuItem('AuditLogs', 'Pages.Administration.AuditLogs', 'flaticon-folder-1', '/app/admin/auditLogs'),
                new AppMenuItem('Maintenance', 'Pages.Administration.Host.Maintenance', 'flaticon-lock', '/app/admin/maintenance'),
                // new AppMenuItem('Subscription', 'Pages.Administration.Tenant.SubscriptionManagement', 'flaticon-refresh', '/app/admin/subscription-management'),
                new AppMenuItem('VisualSettings', 'Pages.Administration.UiCustomization', 'flaticon-medical', '/app/admin/ui-customization'),
                new AppMenuItem('Settings', 'Pages.Administration.Host.Settings', 'flaticon-settings', '/app/admin/hostSettings'),
                new AppMenuItem('Settings', 'Pages.Administration.Tenant.Settings', 'flaticon-settings', '/app/admin/tenantSettings')
            ]),
            // new AppMenuItem('DemoUiComponents', 'Pages.DemoUiComponents', 'flaticon-shapes', '/app/admin/demo-ui-components')
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {

        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (subMenuItem.permissionName && this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
                return true;
            } else if (subMenuItem.items && subMenuItem.items.length) {
                return this.checkChildMenuItemPermission(subMenuItem);
            }
        }

        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' && this._appSessionService.tenant && !this._appSessionService.tenant.edition) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
            hideMenuItem = true;
        }

        if (this._appSessionService.tenant || !abp.multiTenancy.ignoreFeatureCheckForHostUsers) {
            if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
                hideMenuItem = true;
            }
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }
}
