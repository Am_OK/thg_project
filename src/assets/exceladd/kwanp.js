var data_com;
function kwanp(_url) {
    var oReq = new XMLHttpRequest();
    oReq.open("GET", _url.toString(), true);
    oReq.responseType = "arraybuffer";

    oReq.onload = function (e) {
        
        var arraybuffer = oReq.response;

        /* convert data to binary string */
        var data = new Uint8Array(arraybuffer);
        var arr = new Array();
        for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
        var bstr = arr.join("");

        /* Call XLSX */
        var workbook = XLSX.read(bstr, {
            type: "binary"
        });

        /* DO SOMETHING WITH workbook HERE */
        var first_sheet_name = workbook.SheetNames[0];
        var worksheet = workbook.Sheets[first_sheet_name];

        data_com = XLSX.utils.sheet_to_json(worksheet, {
            raw: true
        });
    };
    oReq.send();
   return data_com;
}
